package com.oec.wis.Interfaces;

/**
 * Created by asareri12 on 28/02/17.
 */

public interface MenuClickListener {
    public void onMenuItemClick(int position);
}
