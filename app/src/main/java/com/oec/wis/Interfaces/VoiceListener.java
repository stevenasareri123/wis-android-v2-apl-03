package com.oec.wis.Interfaces;

import android.media.MediaPlayer;

/**
 * Created by asareri12 on 08/03/17.
 */

public interface VoiceListener {

    public void onPlayButtonClicked(String duration);

    public void onStreamingPlayed(MediaPlayer mp);
    public void onTouchTracking();
    public void onstopTracking();
    public void onStartPostDelayed();

}
