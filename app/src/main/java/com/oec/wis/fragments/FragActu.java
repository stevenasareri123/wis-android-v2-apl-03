package com.oec.wis.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.Image;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.Display;
import android.view.Gravity;
import android.view.InflateException;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.easyandroidanimations.library.BounceAnimation;
import com.easyandroidanimations.library.SlideInAnimation;
import com.github.siyamed.shapeimageview.CircularImageView;
import com.kbeanie.imagechooser.api.ChooserType;
import com.kbeanie.imagechooser.api.ChosenImage;
import com.kbeanie.imagechooser.api.ChosenImages;
import com.kbeanie.imagechooser.api.ImageChooserListener;
import com.kbeanie.imagechooser.api.ImageChooserManager;
import com.kbeanie.imagechooser.exceptions.ChooserException;
import com.oec.wis.ApplicationController;
import com.oec.wis.Dashboard;
import com.oec.wis.GPSTracker;
import com.oec.wis.Manifest;
import com.oec.wis.R;
import com.oec.wis.adapters.AdaptActu;
import com.oec.wis.adapters.CustomPhotoAdapter;
import com.oec.wis.adapters.CutomVideoAdapter;
import com.oec.wis.adapters.GalleryListener;
import com.oec.wis.adapters.GroupListAdapter;
import com.oec.wis.adapters.ImagePickerListener;
import com.oec.wis.adapters.PhotoAdapter;
import com.oec.wis.adapters.Popupadapter;
import com.oec.wis.adapters.UserLocationListener;
import com.oec.wis.adapters.VideoAdapter;
import com.oec.wis.dialogs.ActuDetails;
import com.oec.wis.dialogs.Chat;
import com.oec.wis.dialogs.Phone;
import com.oec.wis.dialogs.PubView;
import com.oec.wis.entities.Popupelements;
import com.oec.wis.entities.UserNotification;
import com.oec.wis.entities.WISActuality;
import com.oec.wis.entities.WISAds;
import com.oec.wis.entities.WISCmt;
import com.oec.wis.entities.WISPhoto;
import com.oec.wis.entities.WISUser;
import com.oec.wis.entities.WISVideo;
import com.oec.wis.tools.ItemClickSupport;
import com.oec.wis.tools.TextWatcherAdapter;
import com.oec.wis.tools.Tools;
import com.oec.wis.tools.UserLocation;
import com.oec.wis.wisdirect.WisDirect;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnBackPressListener;
import com.orhanobut.dialogplus.OnItemClickListener;
import com.readystatesoftware.viewbadger.BadgeView;
import com.squareup.picasso.Picasso;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.SystemUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Pattern;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

public class FragActu extends Fragment implements SwipeRefreshLayout.OnRefreshListener, ImagePickerListener, GalleryListener, EasyPermissions.PermissionCallbacks,ImageChooserListener,OnBackPressListener{
    int status_activity = 0;
    View view;
    List<WISActuality> actuList;
    RecyclerView lvActu;
    LinearLayoutManager actuLM;
    RecyclerView.Adapter adaptActu;
    int cPage = 0;
    Boolean loadingMore = true;
    Boolean stopLoadingData = false;
    boolean isTimerRunning = false;
    int cPub = 0;
    List<WISAds> banAds;
    TextView tvBanTitle, tvBanDesc, userName, live_status;
    ImageView ivBanPhoto;
    public Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            loadPubBanner(cPub);
        }
    };
    int previousTotal = 0;
    int visibleThreshold = 10;
    ImageView write_status, share_loc, share_status, profile_image, camera_btn, close_btn, post_image, chat_circle, edit_activity_image, chat_phoneIv;
    UserNotification user_notify;
    TextView chat_circle_text, notify_circle_text, group_chat_circle_text, live_circle_text, music_circle_text, demand_circle_text, weather_txt, headerTitleTV;
    Popupadapter simpleAdapter, live_status_adapter;
    ArrayList<Popupelements> popupList, popupListMedia;
    LocationManager locationManager;
    GPSTracker tracker;
    BadgeView badge;
    Button post_status, media_button;
    String imgPath = "";
    EditText message;
    BroadcastReceiver mBroadcastReceiver;
    TextView notificationCount, editTV, wispopTV;
    public static List<WISPhoto> photos;
    EditText etFilter;
    public static List<WISVideo> video;
    CircularImageView userNotificationView;


    private SwipeRefreshLayout swipeRefreshLayout;
    private PopupWindow popWindow, editpopWindow;
    private final int SELECT_PHOTO = 1;

    private final int REQUEST_CAMERA = 2;
    String selectedObjectId = "", selectedObjectType = "";
    View editinflatedView;


    SharedPreferences settings;
    public static final String PREFS_NAME = "CustomPermission";
    boolean dialogShown;

    Button floatButton;

    String addressDetails;
    Typeface font;
    DialogPlus dialog;

    private static final int RC_CAMERA_PERM = 123;
    private static final int RC_LOCATION_CONTACTS_PERM = 124;

    Uri fileUri = null;

    ImageChooserManager imageChooserManager;






    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            Tools.setLocale(getActivity(), Tools.getData(getActivity(), "lang_pr"));
            view = inflater.inflate(R.layout.frag_actu, container, false);

            checkPermissions();

            initControls();
            setListener();
            loadActu();
            loadBannerPub();
            setupProfileInfos();


            getActivity().getApplicationContext().registerReceiver(mBroadcastReceiver, new IntentFilter("start.fragment.action"));


            mBroadcastReceiver = new BroadcastReceiver() {

                @Override
                public void onReceive(Context context, Intent intent) {

                    Log.i("broad cast", "broad cast");
                    //This piece of code will be executed when you click on your item
                    // Call your fragment...
                }
            };


        } catch (InflateException e) {
        }
        return view;
    }


    @AfterPermissionGranted(RC_LOCATION_CONTACTS_PERM)
    public void checkPermissions() {
        String[] perms = {android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.READ_CONTACTS, android.Manifest.permission.CAMERA, android.Manifest.permission.RECORD_AUDIO, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_NETWORK_STATE, android.Manifest.permission.INTERNET};
        if (EasyPermissions.hasPermissions(getActivity(), perms)) {
            // Have permissions, do the thing!
//            Toast.makeText(getActivity(), "TODO: Location and Contacts things", Toast.LENGTH_LONG).show();
        } else {
            // Ask for both permissions
            EasyPermissions.requestPermissions(this, getString(R.string.rationale_ask_again),
                    RC_LOCATION_CONTACTS_PERM, perms);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // EasyPermissions handles the request result.
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }


//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//        if (requestCode == AppSettingsDialog.DEFAULT_SETTINGS_REQ_CODE) {
//            // Do something after user returned from app settings screen, like showing a Toast.
//            Toast.makeText(getActivity(),"Do something after user", Toast.LENGTH_LONG)
//                    .show();
//        }
//    }


    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {

        Toast.makeText(getActivity(), "onPermissionsGranted", Toast.LENGTH_LONG)
                .show();
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {


        // (Optional) Check whether the user denied any permissions and checked "NEVER ASK AGAIN."
        // This will display a dialog directing them to enable the permission in app settings.
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
//            new AppSettingsDialog.Builder(((Activity)context).build().show();
            if (isAdded()) {
                new AppSettingsDialog.Builder(getActivity(), getString(R.string.permissions_denied_title)).setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        getActivity().finish();
                    }
                });
//            new AppSettingsDialog.Builder(getActivity(), getString(R.string.permissions_denied_title));
            }
        }
    }


    public void refeshDataSource() {
        initControls();
        setListener();
        loadActu();
        loadBannerPub();
        setupProfileInfos();
    }

    public void setupProfileInfos() {

        userName.setText(Tools.getData(getActivity(), "name"));

        downloadProfileImage();
        downloadProfileImageTwo();


    }

    public void setUpBadgeView() {


//        notificationCount.setText(String.valueOf(user_notify.getBadge_count()));


    }

    public void setUpCircleBadgeView(String count, ImageView image) {
//        View target = findViewById(R.id.target_view);

        BadgeView badge = new BadgeView(getActivity(), image);
        badge.setText(String.valueOf(count));
        badge.show();


    }

    private void initControls() {

        try {
            lvActu = (RecyclerView) view.findViewById(R.id.actuRecyList);
            lvActu.setHasFixedSize(true);
            actuLM = new LinearLayoutManager(getActivity());
            actuLM.setOrientation(LinearLayoutManager.VERTICAL);
            lvActu.setLayoutManager(actuLM);

            tvBanTitle = (TextView) view.findViewById(R.id.tvPubTitle);
            tvBanDesc = (TextView) view.findViewById(R.id.tvPubDesc);
            ivBanPhoto = (ImageView) view.findViewById(R.id.ivPubPhoto);
            userNotificationView = (CircularImageView) view.findViewById(R.id.userNotificationView);
            write_status = (ImageView) view.findViewById(R.id.write);
            share_loc = (ImageView) view.findViewById(R.id.location);
            share_status = (ImageView) view.findViewById(R.id.share_status);
            profile_image = (ImageView) view.findViewById(R.id.profileImage);
            userName = (TextView) view.findViewById(R.id.userName);
            live_status = (TextView) view.findViewById(R.id.live_status);
            chat_circle = (ImageView) view.findViewById(R.id.chat_circle);
            etFilter = (EditText) view.findViewById(R.id.etFilter);
            notificationCount = (TextView) view.findViewById(R.id.target_view);
//        chat_phoneIv=(ImageView)view.findViewById(R.id.chat_phoneIv);


            chat_circle_text = (TextView) view.findViewById(R.id.chat_circle_text);
            notify_circle_text = (TextView) view.findViewById(R.id.notify_circle_text);
            group_chat_circle_text = (TextView) view.findViewById(R.id.group_chat_circle_text);
            live_circle_text = (TextView) view.findViewById(R.id.live_circle_text);
            music_circle_text = (TextView) view.findViewById(R.id.music_circle_text);
            demand_circle_text = (TextView) view.findViewById(R.id.demand_circle_text);
            weather_txt = (TextView) view.findViewById(R.id.weather_txt);
            headerTitleTV = (TextView) view.findViewById(R.id.headerTitleTV);

            actuList = new ArrayList<>();


            adaptActu = new AdaptActu(actuList, this);

            lvActu.setAdapter(adaptActu);
            setUPFilterConfig((AdaptActu) adaptActu);

            settings = getActivity().getSharedPreferences(PREFS_NAME, 0);
            dialogShown = settings.getBoolean("weatherdialogShown", false);


            swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
            swipeRefreshLayout.setOnRefreshListener(this);
            swipeRefreshLayout.post(new Runnable() {
                                        @Override
                                        public void run() {
//                                            ======
                                            swipeRefreshLayout.setRefreshing(true);

//                                        fetchDatas();

                                        }
                                    }
            );


            setupPopUPView();


            locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

            badge = new BadgeView(getActivity(), userNotificationView);


            chat_circle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new UserNotification().setChatSelected(false);
                    FragChat fragment = new FragChat();
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.frame_container, fragment).commit();
                }
            });

        } catch (NullPointerException ex) {
            System.out.println("Null pointer Exception" + ex.getMessage());
        }

//        floatButton = (Button)view.findViewById(R.id.floatButton);
//
//        floatButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//               lvActu.scrollToPosition(actuList.size()-1);
//            }
//        });


        try {

            Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Viner Hand ITC.ttf");
            live_status.setTypeface(typeface);
            font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Harmattan-R.ttf");
            etFilter.setTypeface(font);
            tvBanTitle.setTypeface(font);
            tvBanDesc.setTypeface(font);
            userName.setTypeface(font);
            chat_circle_text.setTypeface(font);
            notify_circle_text.setTypeface(font);
            group_chat_circle_text.setTypeface(font);
            live_circle_text.setTypeface(font);
            music_circle_text.setTypeface(font);
            demand_circle_text.setTypeface(font);
            weather_txt.setTypeface(font);
            headerTitleTV.setTypeface(font);
        } catch (NullPointerException exce) {
            System.out.println(" Null exception" + exce.getMessage());
        }

//        livestatus.setFontFeatureSettings(ge);


//        chat_phoneIv.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent it=new Intent(getActivity(), Phone.class);
//                startActivity(it);
//            }
//        });


    }

    public void setupPopUPView() {

        popupList = new ArrayList<>();
        popupList.add(new Popupelements(R.drawable.share_profile, getString(R.string.actuality)));
        popupList.add(new Popupelements(R.drawable.write_status, getString(R.string.post_status)));
        popupList.add(new Popupelements(R.drawable.copy_chat, getString(R.string.chat)));
        popupList.add(new Popupelements(R.drawable.copy_link, getString(R.string.copy_link)));


        simpleAdapter = new Popupadapter(getActivity(), false, popupList);

        ArrayList popupLisTwo = new ArrayList<>();

        popupLisTwo.add(new Popupelements(R.drawable.share_profile, getString(R.string.write_live)));
        popupLisTwo.add(new Popupelements(R.drawable.camera, getString(R.string.photo_video)));
        popupLisTwo.add(new Popupelements(R.drawable.location, getString(R.string.location)));


        live_status_adapter = new Popupadapter(getActivity(), false, popupLisTwo);


    }

    private void setListener() {

//        view.findViewById(R.id.toggle).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                ((DrawerLayout) getActivity().findViewById(R.id.drawer_layout)).openDrawer(GravityCompat.START);
//            }
//        });


        CircularImageView wis_direct = (CircularImageView) view.findViewById(R.id.live_circle);
//
        wis_direct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent direct = new Intent(getActivity(), WisDirect.class);
                direct.putExtra("is_publisher", "YES");
                startActivity(direct);
            }
        });

//        ItemClickSupport.addTo(lvActu).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
//            @Override
//            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
//                ImageView share_image = (ImageView) recyclerView.findViewById(R.id.ivShare);
//
//                Log.i("share status", String.valueOf(share_image));
//
//
////                AdaptActu.SELECTED = actuList.get(position);
////                startActivity(new Intent(getActivity(), ActuDetails.class));
//            }
//        });
//
        lvActu.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                try {

                    int visibleItemCount = lvActu.getChildCount();
                    int totalItemCount = actuLM.getItemCount();
                    int firstVisibleItem = actuLM.findFirstVisibleItemPosition();

                    if (loadingMore) {
                        if (totalItemCount > previousTotal) {
                            loadingMore = false;
                            previousTotal = totalItemCount;
                        }
                    }
                    if (!loadingMore && (totalItemCount - visibleItemCount)
                            <= (firstVisibleItem + visibleThreshold)) {
                        if (!loadingMore) {
                            if (stopLoadingData == false) {
                                loadingMore = true;
                                cPage++;
                                loadActu();
                            }
                        }
                    }

                } catch (NullPointerException exception) {
                    System.out.println("Null pointerException" + exception.getMessage());
                }


            }
        });


        tvBanTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), PubView.class);
                i.putExtra("idpub", banAds.get(cPub).getId());
                startActivity(i);
            }
        });
        tvBanDesc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), PubView.class);
                i.putExtra("idpub", banAds.get(cPub).getId());
                startActivity(i);
            }
        });
        ivBanPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), PubView.class);
                i.putExtra("idpub", banAds.get(cPub).getId());
                startActivity(i);
            }
        });

        share_loc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareLocation();

            }
        });

        share_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogPlus dialog = DialogPlus.newDialog(getActivity())
                        .setAdapter(simpleAdapter)
                        .setOnItemClickListener(new OnItemClickListener() {
                            @Override
                            public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
//
//                                if (position == KeyEvent.KEYCODE_BACK) {
//                                    getActivity().finish();
//                                    dialog.dismiss();
//                                }

                                if (position == 0) {
                                    dialog.dismiss();
                                    showWriteStatusPopup(view);

                                } else if (position == 1) {
                                    dialog.dismiss();
                                    showWriteStatusPopup(view);

                                } else if (position == 2) {
                                    dialog.dismiss();
                                    Log.i("WIS chat", "chat click");
                                    user_notify = new UserNotification();
                                    user_notify.setChatSelected(Boolean.TRUE);
                                    user_notify.setChatWithCustomtextSelected(Boolean.TRUE);
                                    showWriteStatusPopup(view);

                                } else if (position == 3) {
                                    dialog.dismiss();
                                    showWriteStatusPopup(view);

                                }

                            }
                        })
                        .setExpanded(false)
                        // This will enable the expand feature, (similar to android L share dialog)
                        .create();
                dialog.show();
            }
        });
        live_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DialogPlus dialog = DialogPlus.newDialog(getActivity())
                        .setAdapter(live_status_adapter)
                        .setOnItemClickListener(new OnItemClickListener() {
                            @Override
                            public void onItemClick(DialogPlus dialog, Object item, View view, int position) {

                                if (position == 0) {
                                    Log.i("pos", String.valueOf(position));
                                    dialog.dismiss();
                                    showWriteStatusPopup(view);
                                } else if (position == 1) {
                                    Log.i("pos", String.valueOf(position));
                                    dialog.dismiss();
                                    showWriteStatusPopup(view);

                                } else if (position == 2) {
                                    Log.i("pos", String.valueOf(position));
                                    dialog.dismiss();
                                    shareLocation();
                                }
                            }
                        })
                        .setExpanded(false)  // This will enable the expand feature, (similar to android L share dialog)
                        .create();
                dialog.show();

            }
        });

        profile_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragProfile fragment = new FragProfile();
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame_container, fragment).commit();
            }
        });

        write_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showWriteStatusPopup(view);
            }
        });


        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                view.setAlpha(1);
                return false;
            }
        });


////        redirect To weather view

        view.findViewById(R.id.weather).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (dialogShown) {
                    FragWeather fragment = new FragWeather();
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.frame_container, fragment).commit();
                } else {
                    showWeatherAlert();
                }


            }
        });


    }

    private void showWeatherAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(false).setView(R.layout.custom_permission_alert);


        final AlertDialog alert = builder.create();
        alert.show();
        TextView content = (TextView) alert.findViewById(R.id.alertMessage);
        content.setText(R.string.weather_permission_msg);
        alert.findViewById(R.id.cancel_alert).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.dismiss();
            }
        });
        alert.findViewById(R.id.accept_alert).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences.Editor editor = settings.edit();
                editor.putBoolean("weatherdialogShown", true);
                editor.commit();
                alert.dismiss();

//                rediret to fragment
                Fragment fragment = new FragWeather();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.frame_container, fragment).commit();
                ((Dashboard) getActivity()).mDrawerLayout.closeDrawer(GravityCompat.START);


            }
        });
    }

    public void showMediaoption() {

        final CharSequence[] items = {"Caméra", "Choisir Parmi la galerie", "Annuler"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Choisissez Photo De");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(
                    DialogInterface dialog, int item) {
                if (items[item].equals("Caméra")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, REQUEST_CAMERA);

//                    takeImageFromCamera();
//

                } else if (items[item].equals("Choisir Parmi la galerie")) {

//                    Intent intent = new Intent();
//                    intent.setType("image/*");
//                    intent.setAction(Intent.ACTION_GET_CONTENT);//
//                    startActivityForResult(Intent.createChooser(intent, "Select File"),SELECT_PHOTO);






                    Intent i = new Intent(Intent.ACTION_PICK,
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(i, 1);
                } else if (items[item].equals("Annuler")) {
                    dialog.dismiss();
                }
            }

        });
        builder.show();
    }

    private void choseImageFromLibrary() {

        Log.e("Image custom","Library");
        imageChooserManager = new ImageChooserManager(this, ChooserType.REQUEST_PICK_PICTURE);
        imageChooserManager.setImageChooserListener(FragActu.this);
        try {
            imageChooserManager.choose();
        } catch (ChooserException e) {
            e.printStackTrace();
        }

    }


    private void takeImageFromCamera() {

        Log.e("Image custom","Library");
        imageChooserManager = new ImageChooserManager(this, ChooserType.REQUEST_CAPTURE_PICTURE);
        imageChooserManager.setImageChooserListener(FragActu.this);
        try {
            imageChooserManager.choose();
        } catch (ChooserException e) {
            e.printStackTrace();
        }

    }

    public void shareLocation() {

        swipeRefreshLayout.setRefreshing(true);
        tracker = new GPSTracker(getActivity());

        if (tracker.canGetLocation()) {
//                    view.findViewById(R.id.loadingMap).setVisibility(View.VISIBLE);

//            Log.i("user location", getCityName(tracker.getLatitude(), tracker.getLongitude()));
//            Log.i("user lat", String.valueOf(tracker.getLatitude()));
//            Log.i("user lon", String.valueOf(tracker.getLongitude()));

            String coordinates = String.valueOf(tracker.getLatitude()).concat(",").concat(String.valueOf(tracker.getLongitude()));
            String name = getCityName(tracker.getLatitude(), tracker.getLongitude());

        } else {
            Log.i("not", "");
            tracker.showSettingsAlert();
        }
//
    }

    public void showImageLibrary() {

        Log.i("showImageLibrary", "showImageLibrary");
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, SELECT_PHOTO);

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.i("showImageLibrary", "showImageLibrary");

//        if (resultCode == Activity.RESULT_OK &&
//                (requestCode == ChooserType.REQUEST_PICK_PICTURE||
//                        requestCode == ChooserType.REQUEST_CAPTURE_PICTURE)) {
//            imageChooserManager.submit(requestCode, data);
//
//        }




        if (requestCode == AppSettingsDialog.DEFAULT_SETTINGS_REQ_CODE) {
            // Do something after user returned from app settings screen, like showing a Toast.
            Toast.makeText(getActivity(), "Do something after user", Toast.LENGTH_LONG)
                    .show();
        } else if (requestCode == 1 && resultCode == Activity.RESULT_OK) {

            {


                Log.i("showImageLibrary", "showImageLibrary");

                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);

                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String imgDecodableString = cursor.getString(columnIndex);
                cursor.close();

                post_image.setImageURI(selectedImage);
//                post_image.setImageBitmap(BitmapFactory
//                        .decodeFile(imgDecodableString));


                imgPath = imgDecodableString;


//                Uri selectedMedia = null;
//                if (data != null) {
//                    selectedMedia = data.getData();
//                    fileUri = null;
//                } else {
//                    selectedMedia = fileUri;
//                }
//
//                Log.e("Seleted medias", String.valueOf(selectedMedia));
//                if (selectedMedia != null) {
//                    if (selectedMedia.toString().contains("images") || selectedMedia.toString().contains("Pictures")) {
//                        InputStream imageStream = null;
//                        try {
//                            imageStream = getActivity().getContentResolver().openInputStream(selectedMedia);
//                        } catch (FileNotFoundException e) {
//                            e.printStackTrace();
//                        }
//
//                        Bitmap sImage = BitmapFactory.decodeStream(imageStream);
//                        post_image.setImageBitmap(sImage);
//
//                        if (selectedMedia.toString().contains("file:///")) {
//                            imgPath = selectedMedia.toString();
//                        } else {
//                            final String[] proj = {MediaStore.Images.Media.DATA};
//                            final Cursor cursor = getActivity().getContentResolver().query(selectedMedia, proj, null, null,
//                                    null);
//                            final int column_index = cursor
//                                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
//                            cursor.moveToLast();
//                            imgPath = cursor.getString(column_index);
//                        }
//
//                    }
//                }else{
//
//                    Toast.makeText(getActivity(),"Unable to get your media files",Toast.LENGTH_SHORT).show();
//                }
            }


//
//            Uri uri = data.getData();
//
//            try {
//
//                Uri targetUri = data.getData();
//
//                Log.e("Target uri", String.valueOf(targetUri));
//                Bitmap bitmap;
//                try {
//                    bitmap = BitmapFactory.decodeStream(getActivity().getContentResolver().openInputStream(targetUri));
//                    post_image.setImageBitmap(bitmap);
//                } catch (FileNotFoundException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                }


//
//                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
//
////                new code
//
//                Uri URI = data.getData();
//                String[] FILE = { MediaStore.Images.Media.DATA };
//
//
//                Cursor cursor = getActivity().getContentResolver().query(URI,
//                        FILE, null, null, null);
//
//                cursor.moveToFirst();
//
//                int columnIndex = cursor.getColumnIndex(FILE[0]);
//                String ImageDecode = cursor.getString(columnIndex);
//                cursor.close();
//
//                post_image.setImageBitmap(BitmapFactory
//                        .decodeFile(ImageDecode));

//            }
//        catch (Exception e){
//            Toast.makeText(getActivity(), "Please try again", Toast.LENGTH_LONG)
//                    .show();
//        }


//-------
//                 Log.i("TAG", String.valueOf(bitmap));
//
//
//
//                final String[] proj = {MediaStore.Images.Media.DATA};
//                final Cursor cursor_t = getActivity().managedQuery(uri, proj, null, null,
//                        null);
//                final int column_index = cursor_t
//                        .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
//                cursor_t.moveToLast();
//                imgPath = cursor_t.getString(column_index);
//
////                Log.i("image path%@",imgPath);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
        } else if (requestCode == 1) {

            Toast.makeText(getActivity(), "Please try again,This media not supported", Toast.LENGTH_LONG)
                    .show();
        } else if (requestCode == 2 && resultCode == Activity.RESULT_OK) {


            Log.i("image from camera", "camera");

            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
            File destination = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
            FileOutputStream fo;
            try {
                destination.createNewFile();
                fo = new FileOutputStream(destination);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            post_image.setImageBitmap(thumbnail);
            System.out.println("Welcome" + destination);

            //new DoUpload().execute();
            imgPath = String.valueOf(destination);

        }
    }

    static final Pattern CODE_PATTERN = Pattern.compile("([0-9]{0,4})|([0-9]{4}-)+|([0-9]{4}-[0-9]{0,4})+");

    public void showWriteStatusPopup(final View v) {
        // StartAction


        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // inflate the custom popup layout
        final View inflatedView = layoutInflater.inflate(R.layout.write_status_popup, null, false);


        // get device size
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        final Point size = new Point();
        display.getSize(size);
//        mDeviceHeight = size.y;


        // set height depends on the device size
        popWindow = new PopupWindow(inflatedView, size.x - 35, size.y - 300, true);

        popWindow.setTouchable(true);


        popWindow.setOutsideTouchable(false);

        popWindow.setAnimationStyle(R.style.animationName);

//        new SlideInAnimation(popWindow.getContentView()).setDirection(com.easyandroidanimations.library.Animation.DIRECTION_UP)
//                .animate();
//        this.view.setAlpha(0.4f);


//        getActivity().getV


//        final View views = this.view;

        // show the popup at bottom of the screen and set some margin at bottom ie,


        view.setAlpha(0.5f);


        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {

                popWindow.showAtLocation(v, Gravity.CENTER, 0, 0);
            }
        });


//

//
//        new AlertDialog.Builder(getActivity())
//                .setView(R.layout.write_status_popup)
//                .show();

        camera_btn = (ImageView) inflatedView.findViewById(R.id.open_gallery);
        close_btn = (ImageView) inflatedView.findViewById(R.id.close_btn);
        post_status = (Button) inflatedView.findViewById(R.id.post_status);
        post_image = (ImageView) inflatedView.findViewById(R.id.post_image);
        message = (EditText) inflatedView.findViewById(R.id.message);
        wispopTV = (TextView) inflatedView.findViewById(R.id.wispopTV);


        font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Harmattan-Regular.ttf");
        message.setTypeface(font);
        message.setTextColor(getResources().getColor(R.color.light_black));
        post_status.setTypeface(font);
        wispopTV.setTypeface(font);


//        clear previous data


        imgPath = "";
        message.getText().clear();


        close_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popWindow.dismiss();
                view.setAlpha(1);
            }
        });


        camera_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                showImageLibrary();

                showMediaoption();
            }
        });

        post_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (message.getText().toString() != "") {
                    popWindow.dismiss();

                    if (user_notify.getChatWithCustomtextSelected() == Boolean.TRUE) {

                        if (!imgPath.equals("")) {

                            user_notify.setCustomImagePath(imgPath);

                        }

                        user_notify.setCustomText(message.getText().toString());

                        FragChat fragment = new FragChat();
                        FragmentManager fragmentManager = getActivity().getFragmentManager();
                        fragmentManager.beginTransaction()
                                .replace(R.id.frame_container, fragment).commit();

                    } else if (Patterns.WEB_URL.matcher(message.getText().toString()).matches()) {
                        Log.i("is valid url", message.getText().toString());
                        sendUrlPosttoServer(message.getText().toString());
                    } else {

                        Log.i("message 1", message.getText().toString());

                        if (!imgPath.equals("")) {
                            Log.i("message 2", message.getText().toString());
                            new DoUpload().execute();
                        } else {
                            Log.i("message 1", message.getText().toString());
                            doUpdate(null);
                        }
                    }


                }


            }
        });


    }

    public void sendMessgeToChat() {
        Log.i("send", "send message to chat");
    }


    public String getCityName(final double latitude, final double longitude) {
        String knownName = "";
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(getActivity(), Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1);
            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            knownName = addresses.get(0).getAddressLine(1);

            Log.i("address", String.valueOf(addresses));
            Log.i("address", city);


        } catch (IOException e) {
            e.printStackTrace();

//            knownName = Tools.getCurrentLocationViaJSON(latitude,longitude);


        }


        if (knownName != null || !knownName.isEmpty()) {

            new UserLocation(getActivity(), new UserLocationListener() {
                @Override
                public void updateUserLocationInfo(String formatted_address) {


                    addressDetails = formatted_address;

                    Log.e("return response", formatted_address);

                    sendCurrentLocationToSerer(String.valueOf(latitude).concat(",").concat(String.valueOf(longitude)), addressDetails);
                }
            }).execute(String.valueOf(latitude), String.valueOf(longitude));

        }


        return knownName;
    }

    public void sendCurrentLocationToSerer(String cooridinates, String name) {


        JSONObject jsonBody = null;

        try {
//            jsonBody = new JSONObject("{\"user_id\":\"" + Tools.getData(getActivity(), "idprofile") + "\",coordinate\":\"" +cooridinates+ "\",location_name\":\"" + name+ "\"}");
            jsonBody = new JSONObject();
            jsonBody.put("user_id", Tools.getData(getActivity(), "idprofile"));
            jsonBody.put("coordinates", cooridinates);
            jsonBody.put("location_name", name);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("jsone body%@", jsonBody.toString());
        JsonObjectRequest reqActu = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.share_location), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
//                        view.findViewById(R.id.loadingMap).setVisibility(View.INVISIBLE);
                        Log.i("map response", response.toString());

//                        UPDATE BADGE
//                        user_notify.setBadge_count(user_notify.getBadge_count()+1);
//                        setUpBadgeView();

                        Log.i("old coiunt", String.valueOf(user_notify.getBadge_count()));
                        Log.i("new  coiunt", String.valueOf(user_notify.getBadge_count() + 1));
//
                        cPage = 0;
                        loadActu();
                        adaptActu.notifyDataSetChanged();
                        tracker.stopUsingGPS();


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                tracker.stopUsingGPS();
//                view.findViewById(R.id.loadingMap).setVisibility(View.INVISIBLE);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getActivity(), "token"));
                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
                return headers;
            }
        };


        ApplicationController.getInstance().addToRequestQueue(reqActu);


    }



    private void loadActu() {
        try{
            if (cPage == 0) {
                Log.i("on resume from loadACtu","from resume called");
                swipeRefreshLayout.setRefreshing(false);
                actuList.clear();

                adaptActu.notifyDataSetChanged();
                view.findViewById(R.id.loading).setVisibility(View.VISIBLE);
            } else {
                try{
                    view.findViewById(R.id.lMore).setVisibility(View.VISIBLE);

                }catch (NullPointerException exce){
                    System.out.println("Exce" +exce.getMessage());
                }
            }
        }catch (NullPointerException exception){
            System.out.println("Null pointer Exception" +exception);
        }



        JSONObject jsonBody = null;

        try {
            jsonBody = new JSONObject("{\"id_profil\":\"" + Tools.getData(getActivity(), "idprofile") + "\",\"num_page\":\"" + cPage + "\"}");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.i("json body", jsonBody.toString());
        Log.i("json url", getString(R.string.server_url) + getString(R.string.actu_meth));
        JsonObjectRequest reqActu = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.actu_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.i("response", String.valueOf(response));
                            if (response.getString("result").equals("true")) {

                                if (response.getInt("nbr_total_act") > 0) {
                                    JSONArray data = response.getJSONArray("data");
                                    JSONObject bade_obj = response.getJSONObject("badge");




                                    Log.i("badg data", response.toString());

                                    Log.i("user data", data.toString());

//                                    UPDATE BADGE COUNT

                                    user_notify = new UserNotification();

                                    int badge_count = 0,chat_count=0;
                                    badge_count = bade_obj.getInt("total_badge_count") + response.getInt("nbr_total_act");
                                    chat_count =  bade_obj.getInt("chat");

                                    user_notify.setBadge_count(badge_count);

                                    setUpBadgeView();
                                    if(chat_count>0) {
                                        setUpCircleBadgeView(String.valueOf(chat_count), chat_circle);
                                    }

                                    int nPage = (response.getInt("nbr_total_act") / 10) + 1;
                                    stopLoadingData = nPage <= cPage;






                                    for (int i = 0; i < data.length(); i++) {
                                        try {
                                            JSONObject obj = data.getJSONObject(i);

                                            String photo = "";
                                            try {
                                                photo = obj.getString("path_photo");
                                            } catch (Exception e) {
                                            }

                                            String video = "";
                                            try {
                                                video = obj.getString("path_video");
                                            } catch (Exception e) {
                                            }

                                            int likeCount = obj.getInt("nbr_jaime");
                                            int dislikeCount = obj.getInt("dis_like");

                                            List<WISUser> likedUser = new ArrayList<>();
                                            List<WISUser> disLikeUser = new ArrayList<>();
                                            if(dislikeCount>0){
                                                JSONArray disliked_user = obj.getJSONArray("disliked_user");

                                                for(int d=0;d<disliked_user.length();d++){

                                                    JSONObject lUser = disliked_user.getJSONObject(d);
                                                    disLikeUser.add(new WISUser(lUser.getInt("id_profil"),lUser.getString("fullname"),lUser.getString("photo")));

                                                }
                                            }


                                            if(likeCount>0){
                                                JSONArray liked_user = obj.getJSONArray("liked_user");

                                                for(int l=0;l<liked_user.length();l++){

                                                    JSONObject lUser = liked_user.getJSONObject(l);
                                                    likedUser.add(new WISUser(lUser.getInt("id_profil"),lUser.getString("fullname"),lUser.getString("photo")));

                                                }
                                            }

//                                            get viewed user


                                            int numberOfViews =  obj.getInt("nbr_vue");

                                            List<WISUser>viewedUser = new ArrayList<>();

                                            if(numberOfViews>0) {

                                                JSONArray viewedUsersArray = obj.getJSONArray("viewedUser");
                                                for (int l = 0; l < viewedUsersArray.length(); l++) {

                                                    JSONObject oUser = viewedUsersArray.getJSONObject(l);
                                                    viewedUser.add(new WISUser(oUser.getInt("idprofile"), oUser.getString("fullname"), oUser.getString("photo")));


                                                }
                                            }


                                            int nCmt = obj.getInt("nbr_comment");

//                                            get all commented users

                                            List<WISUser>comentsUserList = new ArrayList<>();
                                            if (nCmt > 0) {

                                                JSONArray commentedUsers = obj.getJSONArray("all_comment");
                                                for (int k = 0; k < commentedUsers.length(); k++) {


                                                    JSONObject cmntsJson = commentedUsers.getJSONObject(k);
                                                    JSONObject oUser = cmntsJson.getJSONObject("created_by");

                                                    Log.i("comments user", String.valueOf(oUser));

                                                    comentsUserList.add(new WISUser(oUser.getInt("idprofile"), oUser.getString("name"), oUser.getString("photo")));
                                                }
                                            }



                                            List<WISCmt> cmts = new ArrayList<>();
                                            if (nCmt > 0) {
                                                JSONArray aCmts = obj.getJSONArray("comment");
                                                for (int j = 0; j < aCmts.length(); j++) {
                                                    try {
                                                        JSONObject ocmt = aCmts.getJSONObject(j);
                                                        JSONObject oUser = ocmt.getJSONObject("created_by");
                                                        cmts.add(new WISCmt(ocmt.getInt("id_comment"), new WISUser(oUser.getInt("idprofile"), oUser.getString("name"), oUser.getString("photo")), ocmt.getString("text"), ocmt.getString("last_edit_at"),"false"));
                                                    } catch (Exception e2) {
                                                        String x = e2.getMessage();
                                                    }
                                                }
                                            }
                                            String text = "";
                                            try {
                                                text = obj.getString("text");
                                            } catch (Exception e3) {
                                            }

                                            String cmtActu = "";
                                            try {
                                                cmtActu = obj.getString("commentair_act");
                                            } catch (Exception e3) {
                                            }

                                            String thumb = "";
                                            try {
                                                thumb = obj.getString("photo_video");
                                            } catch (Exception e3) {
                                            }




                                            Log.i("--liked user ----", String.valueOf(likedUser));
                                            Log.i("--disliked user ----", String.valueOf(disLikeUser));

                                            Log.i("--commets user ----", String.valueOf(comentsUserList));

                                            Log.i("--viewed user ----", String.valueOf(viewedUser));

                                            actuList.add(new WISActuality(obj.getInt("is_hide"),obj.getInt("id_act"), text, "", photo, video, thumb, obj.getInt("nbr_jaime"), obj.getInt("nbr_vue"), obj.getBoolean("like_current_profil"), obj.getString("type_act"), cmts, cmtActu, obj.getString("created_at"), new WISUser(obj.getJSONObject("created_by").getInt("idprofile"), obj.getJSONObject("created_by").getString("name"), obj.getJSONObject("created_by").getString("photo")), obj.getInt("dis_like"), obj.getBoolean("dislike_current_profil"), obj.getInt("share_count"), nCmt,likedUser,disLikeUser,comentsUserList,viewedUser));
                                        } catch (Exception e) {

                                        }
                                    }

                                    adaptActu.notifyDataSetChanged();
                                    view.findViewById(R.id.loading).setVisibility(View.INVISIBLE);



                                } else {
                                    stopLoadingData = true;
                                    Toast.makeText(getActivity(), getResources().getString(R.string.err_no_new_actu), Toast.LENGTH_SHORT).show();
                                }
                            }
                            if (cPage == 0) {
                                view.findViewById(R.id.loading).setVisibility(View.GONE);
                            } else {
                                view.findViewById(R.id.lMore).setVisibility(View.GONE);
                            }
                            loadingMore = false;
                        } catch (JSONException e) {
                            if (cPage == 0) {
                                view.findViewById(R.id.loading).setVisibility(View.GONE);
                            } else {
                                view.findViewById(R.id.lMore).setVisibility(View.GONE);
                            }
                            //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                            loadingMore = false;
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (cPage == 0) {
                    view.findViewById(R.id.loading).setVisibility(View.GONE);
                    stopLoadingData = true;
                } else {
                    cPage--;
                    stopLoadingData = false;
                    view.findViewById(R.id.lMore).setVisibility(View.GONE);
                }
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                loadingMore = false;
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getActivity(), "token"));
                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
                return headers;
            }
        };


        ApplicationController.getInstance().addToRequestQueue(reqActu);
    }

    private void loadBannerPub() {
        JSONObject jsonBody = null;
        try {
            jsonBody = new JSONObject("{\"id_profil\":\"" + Tools.getData(getActivity(), "idprofile") + "\"}");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        banAds = new ArrayList<>();
        JsonObjectRequest reqBan = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.banner_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {
                                try {
                                    JSONArray data = response.getJSONArray("data");

                                    for (int i = 0; i < data.length(); i++) {
                                        JSONObject obj = data.getJSONObject(i);
                                        String photo = "";
                                        try {
                                            photo = obj.getString("photo");
                                        } catch (Exception e) {
                                        }
                                        String video_thumb = "";
                                        try {
                                            video_thumb = obj.getString("photo_video");
                                        } catch (Exception e) {
                                        }
                                        banAds.add(new WISAds(obj.getInt("id"), obj.getString("title"), obj.getString("description"), 0, 0, "", "", "", "", photo, "", false, false, "", obj.getString("type_obj"), video_thumb));
                                    }

                                    if (banAds.size() > 0) {
                                        showBanContent(0);
                                        view.findViewById(R.id.llPubBanner).setVisibility(View.VISIBLE);
                                        if (banAds.size() > 1)
                                            startBannerShow();
                                    } else {
                                        view.findViewById(R.id.llPubBanner).setVisibility(View.GONE);
                                    }

                                } catch (Exception e) {
                                }
                            }
                        } catch (JSONException e) {
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String x = "";
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getActivity(), "token"));
                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));

                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqBan);
    }

    @Override
    public void onResume() {
        super.onResume();

        Log.i("on resume","resume called");


//        cPage = 0;
//        loadActu();


//       f customReloadData();



        adaptActu.notifyDataSetChanged();
//

    }

    private void startBannerShow() {
        isTimerRunning = true;
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                if (cPub < (banAds.size() - 1))
                    cPub++;
                else
                    cPub = 0;
                mHandler.obtainMessage(cPub).sendToTarget();
            }
        }, 0, 30000);
    }

    private void loadPubBanner(final int i) {
        if (getActivity() != null) {
            Animation a = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_out);
            a.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    showBanContent(i);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            view.findViewById(R.id.llPubBanner).startAnimation(a);
        }
    }

    private void showBanContent(int i) {
        tvBanTitle.setText(banAds.get(i).getTitle());
        tvBanDesc.setText(banAds.get(i).getDesc());
        if (!banAds.get(i).getTypeObj().equals("video")) {
            ApplicationController.getInstance().getImageLoader().get(getString(R.string.server_url3) + banAds.get(i).getPhoto(), new ImageLoader.ImageListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ivBanPhoto.setImageResource(R.drawable.empty);
                }

                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                    if (response.getBitmap() != null) {
                        ivBanPhoto.setImageBitmap(response.getBitmap());
                    } else {
                        ivBanPhoto.setImageResource(R.drawable.empty);
                    }
                }
            });
        } else {
            ApplicationController.getInstance().getImageLoader().get(getString(R.string.server_url4) + banAds.get(i).getThumb(), new ImageLoader.ImageListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ivBanPhoto.setImageResource(R.drawable.empty);
                }

                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                    if (response.getBitmap() != null) {
                        ivBanPhoto.setImageBitmap(response.getBitmap());
                    } else {
                        ivBanPhoto.setImageResource(R.drawable.empty);
                    }
                }
            });
        }
    }


    public void downloadProfileImage() {

        Picasso.with(getActivity())
                .load(getActivity().getString(R.string.server_url2) + Tools.getData(getActivity(), "photo"))
                .placeholder(R.drawable.empty)
                .error(R.drawable.empty)
                .into(userNotificationView);


    }

    public void downloadProfileImageTwo() {

        Picasso.with(getActivity())
                .load(getActivity().getString(R.string.server_url2) + Tools.getData(getActivity(), "photo"))
                .placeholder(R.drawable.empty)
                .error(R.drawable.empty)
                .into(profile_image);


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ApplicationController.getInstance().cancelPendingRequests(ApplicationController.TAG);
    }

    @Override
    public void onRefresh() {
        Log.i("referesh trigger", "Refresh Data");
        cPage = 0;
        loadActu();

    }

    public void customReloadData() {
        cPage = 0;
        loadActu();

    }

    @Override
    public void showImageGallery() {

        Log.i("referesh trigger", "Refresh Data");

        showImageLibrary();

    }

    @Override
    public void showCustomPopup(final int index,final int act_id, final String desc) {

//        showWriteStatusPopup(this.view);
        dialog = DialogPlus.newDialog(getActivity())
                .setAdapter(simpleAdapter)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View view, int position) {

                        if(position == 0){
                            dialog.dismiss();
                            sendShare(act_id,desc);

                        }
                        if(position==1){
                            dialog.dismiss();
                            showWriteStatusPopup(view);


                        }

                        if(position == 2){

                            user_notify = new UserNotification();
                            user_notify.setChatSelected(Boolean.TRUE);
                            user_notify.setAct_id(String.valueOf(act_id));
                            Log.i("notoficatuoin", String.valueOf(user_notify.getChatSelected()));


                            FragChat fragment = new FragChat();
                            FragmentManager fragmentManager =getActivity().getFragmentManager();
                            fragmentManager.beginTransaction()
                                    .replace(R.id.frame_container, fragment).commit();

                            dialog.dismiss();

                        }
                        if(position == 3){
                            dialog.dismiss();
                            showWriteStatusPopup(view);


                        }

                    }

                })
                .setExpanded(true).setCancelable(Boolean.TRUE)  // This will enable the expand feature, (similar to android L share dialog)
                .create();
        dialog.show();



    }

    @Override
    public void showViewMoreOptionView(final int act_position, int act_id) {

        String currentUser = Tools.getData(getActivity(),"idprofile");

        String activity_user = String.valueOf(actuList.get(act_position).getCreator().getId());

        int is_hide = actuList.get(act_position).isHide();


        if(currentUser.equals(activity_user)){

            popupList = new ArrayList<>();
            if(is_hide ==1){
                popupList.add(new Popupelements(R.drawable.hide,"UnHide"));
                status_activity = 0;

            }else{
                popupList.add(new Popupelements(R.drawable.hide, getString(R.string.Hide)));
                status_activity = 1;
            }

            popupList.add(new Popupelements(R.drawable.write_status, getString(R.string.Edit)));
            popupList.add(new Popupelements(R.drawable.save, getString(R.string.Save)));

        }else{
            popupList = new ArrayList<>();

            popupList.add(new Popupelements(R.drawable.hide, getString(R.string.Hide)));
            popupList.add(new Popupelements(R.drawable.save, getString(R.string.Save)));

        }

        Popupadapter  simpleAdapter = new Popupadapter(getActivity(), false, popupList);


        DialogPlus dialog = DialogPlus.newDialog(getActivity())
                .setAdapter(simpleAdapter)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View v, int position) {

                        if(popupList.size()==3){

                            if(position ==0){
                                dialog.dismiss();
                                view.findViewById(R.id.loading).setVisibility(View.VISIBLE);
                                hidePost(act_position,status_activity);
                            }
                            if(position == 1){

                                dialog.dismiss();
                                view.findViewById(R.id.loading).setVisibility(View.VISIBLE);
                                editPost(view,act_position);
                            }

                            if(position ==2){
                                dialog.dismiss();
                                view.findViewById(R.id.loading).setVisibility(View.VISIBLE);
                                savePostToWis(act_position);
                            }


                        }
                        else if(popupList.size()==2){
                            dialog.dismiss();
                            view.findViewById(R.id.loading).setVisibility(View.VISIBLE);

                            if(position == 0){
                                hidePost(act_position,1);
                            }
                            else if(position == 1){
                                savePostToWis(act_position);
                            }



                        }



                    }
                })
                .setExpanded(false)  // This will enable the expand feature, (similar to android L share dialog)
                .create();
        dialog.show();



    }

    public void hidePost(final int position,final int status){

        String id_act = String.valueOf(actuList.get(position).getId());


        JSONObject jsonBody = new JSONObject();

        try {
            jsonBody.put("user_id", Tools.getData(getActivity(), "idprofile"));
            jsonBody.put("act_id", String.valueOf(id_act));
            jsonBody.put("is_hide",status);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqSave = new JsonObjectRequest(getActivity().getString(R.string.server_url) + getActivity().getString(R.string.hide_post), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {

                                view.findViewById(R.id.loading).setVisibility(View.INVISIBLE);

                                cPage = 0;
                                loadActu();
                                if(status==0){
                                    Toast.makeText(getActivity(),getString(R.string.unhide_post_msg), Toast.LENGTH_SHORT).show();
                                }else{
                                    Toast.makeText(getActivity(),getString(R.string.hide_post_msg), Toast.LENGTH_SHORT).show();
                                }

//

                            }

                        } catch (JSONException e) {
                            //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //if (context != null)
                //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
                headers.put("token", Tools.getData(getActivity(), "token"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqSave);



    }

    public void  savePostToWis(final int position){

        String id_act = String.valueOf(actuList.get(position).getId());


        JSONObject jsonBody = new JSONObject();

        try {
            jsonBody.put("user_id", Tools.getData(getActivity(), "idprofile"));
            jsonBody.put("act_id", String.valueOf(id_act));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqSave = new JsonObjectRequest(getActivity().getString(R.string.server_url) + getActivity().getString(R.string.save_post), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {
                                view.findViewById(R.id.loading).setVisibility(View.INVISIBLE);

                                Toast.makeText(getActivity(),getString(R.string.save_post_msg), Toast.LENGTH_SHORT).show();
//

                            }

                        } catch (JSONException e) {
                            //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //if (context != null)
                //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
                headers.put("token", Tools.getData(getActivity(), "token"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqSave);

    }

    public void  editPost(final View v,final int position){

        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // inflate the custom popup layout
        editinflatedView = layoutInflater.inflate(R.layout.edit_activity, null, false);


        // get device size
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        final Point size = new Point();
        display.getSize(size);
//        mDeviceHeight = size.y;


        // set height depends on the device size
        editpopWindow = new PopupWindow(editinflatedView, size.x - 35, size.y - 300, true);

        editpopWindow.setTouchable(true);

        // make it focusable to show the keyboard to enter in `EditText`
//        popWindow.setFocusable(true);
        // make it outside touchable to dismiss the popup window
//        popWindow.setBackgroundDrawable(new BitmapDrawable());
        editpopWindow.setOutsideTouchable(false);

        editpopWindow.setAnimationStyle(R.style.animationName);

        this.view.setAlpha(0.4f);

        final View views = this.view;

        // show the popup at bottom of the screen and set some margin at bottom ie,


        media_button = (Button) editinflatedView.findViewById(R.id.open_gallery);
        close_btn = (ImageView) editinflatedView.findViewById(R.id.close_btn);
        post_status = (Button) editinflatedView.findViewById(R.id.post_status);
        edit_activity_image = (ImageView) editinflatedView.findViewById(R.id.post_image);
        message = (EditText) editinflatedView.findViewById(R.id.edit_act_message);
        editTV=(TextView)editinflatedView.findViewById(R.id.editTV);
        try{
            font=Typeface.createFromAsset(getActivity().getAssets(),"fonts/Harmattan-R.ttf");
            post_status.setTypeface(font);
            message.setTypeface(font);
            post_status.setTypeface(font);
            editTV.setTypeface(font);

        }catch (NullPointerException ex){
            System.out.println("exception===>" +ex.getMessage());
        }




        message.setText(actuList.get(position).getCmtActu());

        if(actuList.get(position).getpType().equals("partage_photo")){


            updateImage(position,edit_activity_image,actuList.get(position).getImg());

        }else if (actuList.get(position).getpType().equals("partage_video")){
            updateImage(position,edit_activity_image,actuList.get(position).getThumb());
        }

        close_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editpopWindow.dismiss();
                views.setAlpha(1);
            }
        });


        media_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseMedia(v,position);
            }
        });

        post_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                updateActivity(position,message.getText().toString());

            }
        });


        editpopWindow.showAtLocation(v, Gravity.CENTER, 0, 100);




    }

    public void updateImage(final int position,ImageView image,String path){
        Picasso.with(getActivity())
                .load(getString(R.string.server_url3) + path)
                .placeholder(R.drawable.empty)
                .error(R.drawable.empty)
                .into(image);
    }

    public void chooseMedia(final View view,final int pos){
        final CharSequence[] items = { getString(R.string.photo), getString(R.string.video),
                getString(R.string.cancel) };

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Choisissez Photo De");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(
                    DialogInterface dialog, int item) {
                if(items[item].equals(items[0])){
                    dialog.dismiss();

                    openWisMedia(view,pos,"photo");
                }
                else if(items[item].equals(items[1])){
                    dialog.dismiss();
                    openWisMedia(view,pos,"video");
                }

                else if(items[item].equals(items[2])){
                    dialog.dismiss();
                }
            }

        });
        builder.show();



    }

    public void openWisMedia(final View v,final int position,String type){

        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // inflate the custom popup layout
        final View inflatedView = layoutInflater.inflate(R.layout.media_gallery, null, false);


        // get device size
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        final Point size = new Point();
        display.getSize(size);
//        mDeviceHeight = size.y;


        // set height depends on the device size
        popWindow = new PopupWindow(inflatedView, size.x - 35, size.y - 300, true);

        popWindow.setTouchable(true);

        // make it focusable to show the keyboard to enter in `EditText`
//        popWindow.setFocusable(true);
        // make it outside touchable to dismiss the popup window
//        popWindow.setBackgroundDrawable(new BitmapDrawable());
        popWindow.setOutsideTouchable(false);

        popWindow.setAnimationStyle(R.anim.bounce);

        this.view.setAlpha(0.4f);

        final View views = this.view;


        close_btn = (ImageView) inflatedView.findViewById(R.id.close_btn);
        post_status = (Button) inflatedView.findViewById(R.id.post_status);


        TextView header = (TextView)inflatedView.findViewById(R.id.headerTitleForWindow);




        close_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popWindow.dismiss();
            }
        });

        // show the popup at bottom of the screen and set some margin at bottom ie,


        final GridView gvMedia = (GridView) inflatedView.findViewById(R.id.media_gallry);

        if(type.equals("photo")){
            header.setText(getString(R.string.photo));
            loadPhotos(gvMedia);
        }else{
            header.setText(getString(R.string.video));
            loadVideos(gvMedia);
        }


        popWindow.showAtLocation(v, Gravity.CENTER, 0, 100);

    }

    private void loadVideos(final GridView gridVideo) {
        video = new ArrayList<>();

        view.findViewById(R.id.loading).setVisibility(View.VISIBLE);

        JSONObject jsonBody = null;

        try {
            jsonBody = new JSONObject("{\"id_profil\":\"" + Tools.getData(getActivity(), "idprofile") + "\"}");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqVideos = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.getvideos_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {
                                JSONArray data = response.getJSONArray("data");
                                for (int i = 0; i < data.length(); i++) {
                                    try {
                                        JSONObject obj = data.getJSONObject(i);
                                        video.add(new WISVideo(obj.getInt("id"), "", getString(R.string.server_url3) + obj.getString("url_video"), obj.getString("created_at"), obj.getString("image_video")));
                                    } catch (Exception e) {
                                    }
                                }
                                if (video.size() == 0)
                                    Toast.makeText(getActivity(), getString(R.string.no_video_element), Toast.LENGTH_SHORT).show();
                                gridVideo.setAdapter(new CutomVideoAdapter(video,FragActu.this));

                            }
                            view.findViewById(R.id.loading).setVisibility(View.GONE);

                        } catch (JSONException e) {
                            view.findViewById(R.id.loading).setVisibility(View.GONE);
                            //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                view.findViewById(R.id.loading).setVisibility(View.GONE);
                //if (getActivity() != null)
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getActivity(), "token"));
                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqVideos);
    }



    private void loadPhotos(final GridView gridPhoto) {
        photos = new ArrayList<>();

        view.findViewById(R.id.loading).setVisibility(View.VISIBLE);

        JSONObject jsonBody = null;

        try {
            jsonBody = new JSONObject("{\"id_profil\":\"" + Tools.getData(getActivity(), "idprofile") + "\"}");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqPhotos = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.galeriephoto_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("Message").contains("any image")) {
                                Toast.makeText(getActivity(), getString(R.string.no_photo_element), Toast.LENGTH_SHORT).show();
                            } else {
                                if (response.getString("result").equals("true")) {

                                    JSONArray data = response.getJSONArray("data");
                                    for (int i = 0; i < data.length(); i++) {
                                        try {
                                            JSONObject obj = data.getJSONObject(i);
                                            photos.add(new WISPhoto(obj.getInt("idphoto"), "", obj.getString("pathoriginal"), obj.getString("created_at")));
                                        } catch (Exception e) {
                                        }
                                    }
//                                    gridPhoto.setAdapter(new PhotoAdapter((FragmentActivity) getActivity(), photos));
                                    gridPhoto.setAdapter(new CustomPhotoAdapter(photos,FragActu.this));
                                }
                            }
                            view.findViewById(R.id.loading).setVisibility(View.GONE);

                        } catch (JSONException e) {

                            view.findViewById(R.id.loading).setVisibility(View.GONE);
                            //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                view.findViewById(R.id.loading).setVisibility(View.GONE);
                //if (getActivity() != null)
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getActivity(), "token"));
                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqPhotos);
    }


    public void updateActivity(final int act_pos,String editedMessage){

        String object_id="",object_type="";
        String id_act = String.valueOf(actuList.get(act_pos).getId());


        if(!selectedObjectId.equals("")){

            object_id = selectedObjectId;
            object_type = selectedObjectType;

        }else{
            object_type="exists";
            object_id = "exists";

        }


        JSONObject jsonBody = new JSONObject();

        try {
            jsonBody.put("user_id", Tools.getData(getActivity(), "idprofile"));
            jsonBody.put("act_id", String.valueOf(id_act));
            jsonBody.put("object_model",object_type);
            jsonBody.put("object_id",object_id);
            jsonBody.put("commentaire",editedMessage);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqSave = new JsonObjectRequest(getActivity().getString(R.string.server_url) + getActivity().getString(R.string.updateActivity), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {

                                view.findViewById(R.id.loading).setVisibility(View.INVISIBLE);
                                editpopWindow.dismiss();

                                view.setAlpha(1);

                                Toast.makeText(getActivity(),getString(R.string.updated_msg), Toast.LENGTH_SHORT).show();
                                cPage = 0;
//                                loadingMore = Boolean.FALSE;
                                loadActu();

//

                            }

                        } catch (JSONException e) {
                            //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //if (context != null)
                //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
                headers.put("token", Tools.getData(getActivity(), "token"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqSave);

    }


    @Override
    public void showCustomPopup() {

    }

    private void sendShare(int idAct, String desc) {

        JSONObject jsonBody = null;

        try {
            jsonBody = new JSONObject("{\"id_profil\":\"" + Tools.getData(getActivity(), "idprofile") + "\",\"id_act\":\"" + idAct + "\",\"commentaire\":\"" + desc + "\"}");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.i("response parms",jsonBody.toString());
        JsonObjectRequest reqActu = new JsonObjectRequest(getActivity().getString(R.string.server_url) + getActivity().getString(R.string.cloneActuality), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i("response",response.toString());
                        try {
                            if (response.getString("result").equals("true")) {
                                Toast.makeText(getActivity(), getActivity().getString(R.string.msg_share_success), Toast.LENGTH_SHORT).show();

                                cPage = 0;
                                customReloadData();




                            }

                        } catch (JSONException e) {

                            //Toast.makeText(SharePost.this, SharePost.this.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                //Toast.makeText(SharePost.this, SharePost.this.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getActivity(), "token"));
                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
                return headers;
            }
        };
        ApplicationController.getInstance().addToRequestQueue(reqActu);
    }

    @Override
    public void pickedItems(String objectId, Bitmap image,String type,String name) {

        popWindow.dismiss();

        selectedObjectId = objectId;
        selectedObjectType = type;
        edit_activity_image=(ImageView)editinflatedView.findViewById(R.id.post_image);

        Log.i("photo id", String.valueOf(image));
        edit_activity_image.setImageBitmap(image);
        if(type.equals("partage_photo")){

            edit_activity_image.setImageBitmap(image);
        }






    }

    @Override
    public void onImageChosen(final ChosenImage chosenImage) {


        Log.e("choosen image", String.valueOf(chosenImage));
        Log.e("choosen image path", String.valueOf(chosenImage.getFilePathOriginal()));



        if(post_image!=null){


            final Uri uri = Uri.fromFile(new File(chosenImage.getFileThumbnail()));

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    post_image.setImageURI(uri);
                    imgPath = chosenImage.getFilePathOriginal();
                }
            });




        }


    }

    @Override
    public void onError(final String s) {

        Log.e("onError image", s);

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {

                Toast.makeText(getActivity(),s, Toast.LENGTH_SHORT).show();
            }
        });



    }

    @Override
    public void onImagesChosen(ChosenImages chosenImages) {

    }

    @Override
    public void onBackPressed(DialogPlus dialogPlus) {

        dialogPlus.dismiss();
    }


    private class DoUpload extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            view.findViewById(R.id.loading).setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (!result.equals("")) {
                try {
                    JSONObject img = new JSONObject(result);
                    if (img.getBoolean("result"))
                        Log.i("image data set",img.getString("data"));
                    doUpdate(img.getString("data"));
                    //else
                    //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    view.findViewById(R.id.loading).setVisibility(View.GONE);
                    Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                }

            } else {
                view.findViewById(R.id.loading).setVisibility(View.GONE);
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected String doInBackground(String... urls) {
            return Tools.doFileUpload(getActivity().getString(R.string.server_url) + getActivity().getString(R.string.upload_profile_meth), imgPath, Tools.getData(getActivity(), "token"));
        }
    }

    public void sendUrlPosttoServer(String url_txt){
        JSONObject jsonBody = null;

        try {

            String json = "{\"user_id\": \"" + Tools.getData(getActivity(), "idprofile") + "\",\"url\": \"" + url_txt + "\"}";
            jsonBody = new JSONObject(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqAddPub = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.url_post), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {
                                Log.i("respones write status",response.toString());
                                view.setAlpha(1);
                                customReloadData();
                            }
                        } catch (Exception e) {

                        }
                        view.findViewById(R.id.loading).setVisibility(View.GONE);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                view.findViewById(R.id.loading).setVisibility(View.GONE);
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getActivity(), "token"));
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("lang", "en_US");
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqAddPub);
    }


    private void doUpdate(final String photo) {


        if (isAdded()) {

            JSONObject jsonBody = null;

            try {

                String json = "{\"user_id\": \"" + Tools.getData(getActivity(), "idprofile") + "\",\"name_img\": \"" + photo + "\",\"message\": \"" + message.getText().toString() + "\"}";
                jsonBody = new JSONObject(json);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.e("Doupdate parsm", String.valueOf(jsonBody));

            JsonObjectRequest reqAddPub = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.write_status), jsonBody,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                if (response.getBoolean("result")) {
                                    Log.i("respones write status", response.toString());
                                    message.setText("");

                                    view.setAlpha(1);
                                    customReloadData();
                                }
                            } catch (Exception e) {

                            }
                            view.findViewById(R.id.loading).setVisibility(View.GONE);

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    view.findViewById(R.id.loading).setVisibility(View.GONE);
                    //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("token", Tools.getData(getActivity(), "token"));
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    headers.put("lang", "en_US");
                    return headers;
                }
            };

            ApplicationController.getInstance().addToRequestQueue(reqAddPub);
        }

    }


    // hide keyboard
    @Override
    public void onActivityCreated (Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        final InputMethodManager manager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        manager.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    private void setUPFilterConfig(final AdaptActu adapter) {
        etFilter.addTextChangedListener(new TextWatcherAdapter() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                super.beforeTextChanged(s, start, count, after);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                super.onTextChanged(s, start, before, count);


                try {
                    adapter.getFilter().filter(s.toString());

                } catch (NullPointerException ex) {
                    System.out.println("Exception" + ex.getMessage());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    super.afterTextChanged(s);

                } catch (NullPointerException ex) {
                    System.out.println("Exception" + ex.getMessage());
                }

            }
        });
    }



//    private Boolean exit = false;
//    @Override
//    public void onBackPressed() {
//        if (exit) {
//            getActivity().finish(); // finish activity
//        } else {
//            Toast.makeText(this, "Press Back again to Exit.",
//                    Toast.LENGTH_SHORT).show();
//            exit = true;
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    exit = false;
//                }
//            }, 3 * 1000);
//
//        }
//
//    }




}
