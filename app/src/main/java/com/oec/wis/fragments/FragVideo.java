package com.oec.wis.fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.net.Uri;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.text.format.DateFormat;
import android.text.format.Formatter;
import android.util.Log;
import android.view.Gravity;
import android.view.InflateException;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.oec.wis.ApplicationController;
import com.oec.wis.R;
import com.oec.wis.adapters.Popupadapter;
import com.oec.wis.adapters.VideoAdapter;
import com.oec.wis.entities.Popupelements;
import com.oec.wis.entities.WISVideo;
import com.oec.wis.tools.Tools;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnItemClickListener;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import tcking.github.com.giraffeplayer.GiraffePlayer;
import tcking.github.com.giraffeplayer.GiraffePlayerActivity;
import tv.danmaku.ijk.media.player.IMediaPlayer;


public class FragVideo extends Fragment {
    View view;
    List<WISVideo> video;
    GridView gridVideo;
    GiraffePlayer player;
    DialogPlus dialog;
    static final int REQUEST_VIDEO_CAPTURE = 1;
    ImageView playIV;
    TextView headerTitleTV;
    private AudioManager audioManager = null;
    SeekBar volumeSeekbar;
    Button btn_play;
    EditText et_url;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            Tools.setLocale(getActivity(), Tools.getData(getActivity(), "lang_pr"));
            view = inflater.inflate(R.layout.frag_video, container, false);
            getActivity().setVolumeControlStream(AudioManager.STREAM_MUSIC);

            initControls();
            setListener();
            loadVideos();
//            callMethodGiraffe();





        } catch (InflateException e) {
        }
        return view;
    }

//    private void callMethodGiraffe() {
//
//        try {
//            player = new GiraffePlayer(getActivity());
//            player.onComplete(new Runnable() {
//                @Override
//                public void run() {
//                    //callback when video is finish
//                    Toast.makeText(getActivity(), "video play completed", Toast.LENGTH_SHORT).show();
//                }
//            }).onInfo(new GiraffePlayer.OnInfoListener() {
//                @Override
//                public void onInfo(int what, int extra) {
//                    switch (what) {
//                        case IMediaPlayer.MEDIA_INFO_BUFFERING_START:
//                            //do something when buffering start
//                            break;
//                        case IMediaPlayer.MEDIA_INFO_BUFFERING_END:
//                            //do something when buffering end
//                            break;
//                        case IMediaPlayer.MEDIA_INFO_NETWORK_BANDWIDTH:
//                            //download speed
//                            ((TextView) view.findViewById(R.id.tv_speed)).setText(Formatter.formatFileSize(getActivity(), extra) + "/s");
//                            break;
//                        case IMediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START:
//                            //do something when video rendering
//                            view.findViewById(R.id.tv_speed).setVisibility(View.GONE);
//                            break;
//                    }
//                }
//            }).onError(new GiraffePlayer.OnErrorListener() {
//                @Override
//                public void onError(int what, int extra) {
//                    Toast.makeText(getActivity(), "video play error", Toast.LENGTH_SHORT).show();
//                }
//            });
//
//
//        } catch (NullPointerException ex) {
//
//            System.out.println("Null Exception===>" + ex.getMessage());
//        }
//    }





    private void setListener() {
        view.findViewById(R.id.toggle).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((DrawerLayout) getActivity().findViewById(R.id.drawer_layout)).openDrawer(GravityCompat.START);
            }
        });


        gridVideo.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                Uri uri = Uri.parse(video.get(position).getPath());
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                intent.setDataAndType(uri, "video/mp4");
                startActivity(intent);
//                Toast.makeText(getActivity(),"Video is playing",Toast.LENGTH_SHORT).show();

//
//
//                try {
//                    ImageView volumeIV = (ImageView) view.findViewById(R.id.app_video_volume_icon);
////                    audioManager = (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);
////                    volumeSeekbar.setMax(audioManager
////                            .getStreamMaxVolume(AudioManager.STREAM_MUSIC));
////                    volumeSeekbar.setProgress(audioManager
////                            .getStreamVolume(AudioManager.STREAM_MUSIC));
//
//
//                    volumeIV.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            Toast.makeText(getActivity(), "Clickable", Toast.LENGTH_SHORT).show();
//
//
//                        }
//                    });

//
//                    volumeSeekbar = (SeekBar) view.findViewById(R.id.app_volume_seekbar_IV);
//                    volumeSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
//                        @Override
//                        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//                            Toast.makeText(getActivity(), "Seeekbar1", Toast.LENGTH_SHORT).show();
//
//
//                        }
//
//                        @Override
//                        public void onStartTrackingTouch(SeekBar seekBar) {
//                            Toast.makeText(getActivity(), "Seeekbar2", Toast.LENGTH_SHORT).show();
//
//                        }
//
//                        @Override
//                        public void onStopTrackingTouch(SeekBar seekBar) {
//                            Toast.makeText(getActivity(), "Seeekbar333", Toast.LENGTH_SHORT).show();
//                            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
//                                    5, 0);
//
//                        }
//                    });

//                } catch (NullPointerException ex) {
//                    System.out.println("Null pointer Exception" + ex.getMessage());
//                }

            }
        });
        view.findViewById(R.id.bAdd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showMediaoption();
//                Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
//                startActivityForResult(Intent.createChooser(i, getString(R.string.pick_video)), 1);
            }
        });
    }


//    Video


    public void showMediaoption(){



        final ArrayList popupList = new ArrayList();

        popupList.add(new Popupelements(R.drawable.wis_photo_picker,"Pick Videos"));
        popupList.add(new Popupelements(R.drawable.camera,"Take Video"));

        Popupadapter simpleAdapter = new Popupadapter(getActivity(), false, popupList);


        dialog = DialogPlus.newDialog(getActivity())
                .setAdapter(simpleAdapter)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View v, int position) {

                        if(position ==0){

                            Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(Intent.createChooser(i, getString(R.string.pick_video)), 1);

                        }

                        else if(position == 1){

                            Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                            if (takeVideoIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                                startActivityForResult(takeVideoIntent, REQUEST_VIDEO_CAPTURE);
                            }

                        }

                    }
                })
                .setExpanded(false)  // This will enable the expand feature, (similar to android L share dialog)
                .create();
        dialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        dialog.dismiss();

        switch (requestCode) {
            case 1:
                if (resultCode == getActivity().RESULT_OK) {
                    Uri selectedVideo = imageReturnedIntent.getData();


                    final String[] proj = {MediaStore.Images.Media.DATA};
                    final Cursor cursor = getActivity().managedQuery(selectedVideo, proj, null, null,
                            null);
                    final int column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
                    cursor.moveToLast();
                    String videoPath = cursor.getString(column_index);
                    Log.i("video path",videoPath);



                    new DoUpload().execute(videoPath);
                }
                break;
            case 2:
                if (requestCode == REQUEST_VIDEO_CAPTURE && resultCode == getActivity().RESULT_OK) {


                    Uri videoUri = imageReturnedIntent.getData();

                    final String[] proj = {MediaStore.Images.Media.DATA};
                    final Cursor cursor = getActivity().managedQuery(videoUri, proj, null, null,
                            null);
                    final int column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
                    cursor.moveToLast();
                    String videoPath = cursor.getString(column_index);

                    Log.i("video path", String.valueOf(videoPath));
                    new DoUpload().execute(getPath(videoUri));

                }
                break;
            default:
                break;
        }
    }


        private String getPath(Uri uri) {
            String[] projection = { MediaStore.Video.Media.DATA, MediaStore.Video.Media.SIZE, MediaStore.Video.Media.DURATION};
            Cursor cursor = getActivity().managedQuery(uri, projection, null, null, null);
            cursor.moveToFirst();
            String filePath = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA));
            int fileSize = cursor.getInt(cursor.getColumnIndexOrThrow(MediaStore.Video.Media.SIZE));
            long duration = TimeUnit.MILLISECONDS.toSeconds(cursor.getInt(cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DURATION)));


            //some extra potentially useful data to help with filtering if necessary
            System.out.println("size: " + fileSize);
            System.out.println("path: " + filePath);
            System.out.println("duration: " + duration);

            return filePath;
        }



    private void initControls() {
        gridVideo = (GridView) view.findViewById(R.id.gridPhoto);
        headerTitleTV=(TextView)view.findViewById(R.id.headerTitleTV);


//        try{
//            playIV=(ImageView)view.findViewById(R.id.playIV);
//            playIV.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Toast.makeText(getActivity(),"clickable",Toast.LENGTH_SHORT).show();
//                }
//            });
//        }catch (NullPointerException ex){
//            Log.d("Exception",ex.getMessage());
//        }
        Typeface font= Typeface.createFromAsset(getActivity().getAssets(),"fonts/Harmattan-R.ttf");
        headerTitleTV.setTypeface(font);

    }


    private void loadVideos() {
        video = new ArrayList<>();

        view.findViewById(R.id.loading).setVisibility(View.VISIBLE);

        JSONObject jsonBody = null;

        try {
            jsonBody = new JSONObject("{\"id_profil\":\"" + Tools.getData(getActivity(), "idprofile") + "\"}");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqVideos = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.getvideos_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {
                                JSONArray data = response.getJSONArray("data");
                                for (int i = 0; i < data.length(); i++) {
                                    try {
                                        JSONObject obj = data.getJSONObject(i);
                                        video.add(new WISVideo(obj.getInt("id"), "", getString(R.string.server_url3) + obj.getString("url_video"), obj.getString("created_at"), obj.getString("image_video")));
                                    } catch (Exception e) {
                                    }
                                }
                                if (video.size() == 0)
                                    Toast.makeText(getActivity(), getString(R.string.no_video_element), Toast.LENGTH_SHORT).show();
                                gridVideo.setAdapter(new VideoAdapter(getActivity(), video));
                            }
                            view.findViewById(R.id.loading).setVisibility(View.GONE);

                        } catch (JSONException e) {
                            view.findViewById(R.id.loading).setVisibility(View.GONE);
                            //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                view.findViewById(R.id.loading).setVisibility(View.GONE);
                //if (getActivity() != null)
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getActivity(), "token"));
                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqVideos);
    }

    private void doAddVideo(final String video, final String thumb) {
        JSONObject jsonBody = null;

        try {
            String json = "{\"id_profil\": \"" + Tools.getData(getActivity(), "idprofile") + "\",\"video\": \"" + video + "\"}";
            jsonBody = new JSONObject(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqAddPub = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.add_video_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            Log.i("RE::", String.valueOf(response));
                            String x = "";
                            if (response.getBoolean("result")) {
                                DateFormat df = new android.text.format.DateFormat();
                                String dt = DateFormat.format("yyyy-MM-dd hh:mm:ss", new java.util.Date()).toString();
                                FragVideo.this.video.add(0, new WISVideo(response.getInt("data"), "", getString(R.string.server_url3) + video, dt, thumb));
                                if (isAdded())
                                gridVideo.setAdapter(new VideoAdapter(getActivity(), FragVideo.this.video));
                            }
                        } catch (Exception e) {

                        }
                        view.findViewById(R.id.loading).setVisibility(View.GONE);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                view.findViewById(R.id.loading).setVisibility(View.GONE);
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getActivity(), "token"));
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("lang", "en_US");
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqAddPub);
    }


    @Override
    public void onDestroyView(){
        super.onDestroyView();

        getActivity().finish();
//        if(player != null && player.onBackPressed()){
//            player.onDestroy();
//        }
        System.out.println("onDestroy View"  + "Called");
    }

    @Override
    public void onPause() {
        super.onPause();
//        if (player != null) {
//            player.onPause();
//        }
    }

    @Override
    public void onResume() {
        super.onResume();
//        if (player != null) {
//            player.onResume();
//        }


        if(getView() == null){
            return;
        }

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
                    // handle back button's click listener
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ApplicationController.getInstance().cancelPendingRequests(ApplicationController.TAG);
        System.out.println("OnDestroy"   +"called");
//        if (player != null) {
//            player.onDestroy();
//        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
//        if (player != null) {
//            player.onConfigurationChanged(newConfig);
//        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FragmentManager fm = getFragmentManager();
        fm.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                if(getFragmentManager().getBackStackEntryCount() == 0){
                    getActivity().finish();

                    System.out.println("onCreate"   +"called");
                }
            }
        });
    }

//    @Override
//    public void onBackPressed() {
//        if (player != null && player.onBackPressed()) {
//            return;
//        }
//        super.onBackPressed();
//    }
//    @Override
//    public void onBackPressed() {
//
//        int count = getFragmentManager().getBackStackEntryCount();
//
//        if (count == 0) {
//            super.onBackPressed();
//            //additional code
//        } else {
//            getFragmentManager().popBackStack();
//        }
//
//    }

//
//    @Override
//    public void onBackPressed() {
//
//        int count = getFragmentManager().getBackStackEntryCount();
//
//        if (count == 0) {
//            super.onBackPressed();
//            //additional code
//        } else {
//            getFragmentManager().popBackStack();
//        }
//
//    }

    protected static String uploadVideo(String videoPath, String url, String token) throws ParseException, IOException {

        HttpClient http = AndroidHttpClient.newInstance("WIS");
        HttpPost method = new HttpPost(url);
        String BOUNDARY = "--wisboundry--";
        method.setHeader("Content-Type", "multipart/form-data; boundary=" + BOUNDARY);
        method.addHeader("token", token);


        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        builder.setBoundary(BOUNDARY);
        builder.setCharset(Charset.defaultCharset());
        FileBody fileBody = new FileBody(new File(videoPath));
        builder.addPart("file", fileBody);



//        builder.addTextBody("photo_count", String.valueOf(fPath.size()));
        HttpEntity entity = builder.build();

        method.setHeader("Accept", "application/json");
        method.setHeader("Content-Type", "multipart/form-data; boundary=" + BOUNDARY);
        method.setEntity(entity);



        HttpResponse response = null;
        try {
            response = http.execute(method);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String res = "";
        HttpEntity resEntity = response.getEntity();
        try {
            res = EntityUtils.toString(resEntity);


            Log.i("muliple response", String.valueOf(res));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return res;
    } // end of upl
    private class DoUpload extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            view.findViewById(R.id.loading).setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            dialog.dismiss();
            if (!result.equals("")) {
                try {
                    JSONObject img = new JSONObject(result);
                    Log.i("response",result);
                    if (img.getBoolean("result")) {
                        if (isAdded())
                            doAddVideo(img.getJSONObject("data").getString("video"), img.getJSONObject("data").getString("thumbnail"));
                    }
                    //else
                    //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                }

            } else {
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected String doInBackground(String... urls) {
            try {

                Log.i("uploaded url",urls[0]);

                return uploadVideo(urls[0],getActivity().getString(R.string.server_url) + getActivity().getString(R.string.upload_video_meth),Tools.getData(getActivity(), "token"));

//                return Tools.doFileUpload(getActivity().getString(R.string.server_url) + getActivity().getString(R.string.upload_video_meth), urls[0], Tools.getData(getActivity(), "token"));
            } catch (Exception e) {
                return "";
            }
        }
    }


}
