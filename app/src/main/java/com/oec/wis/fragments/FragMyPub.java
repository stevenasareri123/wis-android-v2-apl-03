package com.oec.wis.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.oec.wis.ApplicationController;
import com.oec.wis.Dashboard;
import com.oec.wis.R;
import com.oec.wis.adapters.MyAdsAdapter;
import com.oec.wis.dialogs.NewAds;
import com.oec.wis.dialogs.PubDetails;
import com.oec.wis.entities.WISAds;
import com.oec.wis.tools.Tools;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FragMyPub extends Fragment {
    View view;
    ListView lvMyPub;
    List<WISAds> pubList;
    MyAdsAdapter pubAdapter;
    TextView headerTitleTV;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            Tools.setLocale(getActivity(), Tools.getData(getActivity(), "lang_pr"));
            view = inflater.inflate(R.layout.frag_my_ads, container, false);
            loadControls();
            setListener();
            pubList = new ArrayList<>();
            pubAdapter = new MyAdsAdapter(getActivity(), pubList);
            lvMyPub.setAdapter(pubAdapter);
        } catch (InflateException e) {

        }
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        loadMyAds();
    }

    private void loadControls() {
        lvMyPub = (ListView) view.findViewById(R.id.lvMyAds);
        headerTitleTV=(TextView)view.findViewById(R.id.headerTitleTV);

        Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Harmattan-Regular.ttf");
        headerTitleTV.setTypeface(font);
    }

    private void loadMyAds() {

        view.findViewById(R.id.loading).setVisibility(View.VISIBLE);

        JSONObject jsonBody = null;

        try {
            jsonBody = new JSONObject("{\"id_profil\":\"" + Tools.getData(getActivity(), "idprofile") + "\"}");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqActu = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.pub_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("result").equals("true")) {
                                JSONArray data = response.getJSONArray("data");
                                Log.d("response", String.valueOf(response));



                                Log.d("json array on pub", String.valueOf(data));

                                TextView date=(TextView)getActivity().findViewById(R.id.tvStatus) ;

                                pubList.clear();
                                for (int i = 0; i < data.length(); i++) {
                                    try {
                                        JSONObject obj = data.getJSONObject(i);
                                        String photo = "";
                                        try {
                                            photo = obj.getString("photo");
                                        } catch (Exception e) {
                                        }
                                        String video_thumb = "";
                                        try {
                                            video_thumb = obj.getString("photo_video");
                                        } catch (Exception e) {
                                        }

                                        pubList.add(new WISAds(obj.getInt("id_pub"), obj.getInt("id_act"), obj.getString("titre"), obj.getInt("etat"), obj.getInt("duree"), obj.getString("created_at"), obj.getString("date_lancement"), photo, obj.getString("type_obj"), video_thumb));
                                        pubAdapter.notifyDataSetChanged();
                                        lvMyPub.invalidateViews();

                                    } catch (Exception e) {

                                    }
                                }
                            }
                            view.findViewById(R.id.loading).setVisibility(View.GONE);

                        } catch (JSONException e) {
                            view.findViewById(R.id.loading).setVisibility(View.GONE);
                            //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                view.findViewById(R.id.loading).setVisibility(View.GONE);
                //if (getActivity() != null)
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getActivity(), "token"));
                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqActu);
    }

    private void setListener() {
        view.findViewById(R.id.toggle).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((Dashboard) getActivity()).mDrawerLayout.openDrawer(GravityCompat.START);
            }
        });
        view.findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), NewAds.class));
            }
        });
        lvMyPub.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (pubList.size() > 0) {
//                    Toast.makeText(getActivity(),"Clickable",Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(getActivity(), PubDetails.class);
                    i.putExtra("id", pubList.get(position).getId());
                    i.putExtra("idact", pubList.get(position).getIdAct());
                    System.out.println("id pub"   +pubList.get(position).getId()  +"idact" +pubList.get(position).getIdAct());
                    startActivity(i);
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ApplicationController.getInstance().cancelPendingRequests(ApplicationController.TAG);
    }

}
