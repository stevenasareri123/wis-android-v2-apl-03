package com.oec.wis.fragments;

import android.app.Fragment;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;

import com.oec.wis.ApplicationController;
import com.oec.wis.R;
import com.oec.wis.tools.Tools;

public class FragConditions extends Fragment {
    View view;
    WebView wv;
    TextView headerTitleTV;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            Tools.setLocale(getActivity(), Tools.getData(getActivity(), "lang_pr"));
            view = inflater.inflate(R.layout.frag_conditions, container, false);
            loadControls();
            loadCondition();
            setListener();
        } catch (InflateException e) {
        }
        return view;
    }

    private void setListener() {
        view.findViewById(R.id.toggle).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((DrawerLayout) getActivity().findViewById(R.id.drawer_layout)).openDrawer(GravityCompat.START);
            }
        });
    }

    private void loadControls() {
        wv = (WebView) view.findViewById(R.id.webView);
        headerTitleTV=(TextView)view.findViewById(R.id.headerTitleTV);
        Typeface font=Typeface.createFromAsset(getActivity().getAssets(),"fonts/Harmattan-R.ttf");
        headerTitleTV.setTypeface(font);
    }

    private void loadCondition() {
        String pref = Tools.getData(getActivity(), "lang_pr");
        if (Tools.getData(getActivity(), "lang_pr").equals("fr"))
            wv.loadUrl("file:///android_asset/cgu_fr.html");
        else if (Tools.getData(getActivity(), "lang_pr").equals("en"))
            wv.loadUrl("file:///android_asset/cgu_en.html");
        else if (Tools.getData(getActivity(), "lang_pr").equals("es"))
            wv.loadUrl("file:///android_asset/cgu_es.html");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ApplicationController.getInstance().cancelPendingRequests(ApplicationController.TAG);
    }
}
