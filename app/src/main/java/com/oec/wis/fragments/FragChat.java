package com.oec.wis.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.text.Editable;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.oec.wis.ApplicationController;
import com.oec.wis.R;
import com.oec.wis.adapters.DiscuAdapter;
import com.oec.wis.adapters.FriendsAdapter;
import com.oec.wis.adapters.GroupListAdapter;
import com.oec.wis.chatApi.ChatApi;
import com.oec.wis.dialogs.Chat;
import com.oec.wis.entities.UserNotification;
import com.oec.wis.entities.WISChat;
import com.oec.wis.entities.WISUser;
import com.oec.wis.groupcall.GroupCallMainActivity;
import com.oec.wis.tools.TextWatcherAdapter;
import com.oec.wis.tools.Tools;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

public class FragChat extends Fragment {
    View view;
    List<WISChat> chatList,groupList;
    ListView lvChat, lvFriend;
    List<WISUser> userList;

    DiscuAdapter adapter;
    EditText etFilter;
    TextView headerTitle;
    GroupListAdapter groupListAdapter;

    UserNotification userNotification;


    String currentChannel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            Tools.setLocale(getActivity(), Tools.getData(getActivity(), "lang_pr"));
            view = inflater.inflate(R.layout.frag_discussion, container, false);


            Log.i("notoficatuoin", String.valueOf(userNotification.getChatSelected()));
            headerTitle = (TextView)view.findViewById(R.id.headerTitleTV);

            Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Harmattan-R.ttf");
            headerTitle.setTypeface(font);


            if(userNotification.getChatSelected() == Boolean.TRUE){
                headerTitle.setText(R.string.chat_selected);
            }
            else{
                headerTitle.setText(R.string.chat);
            }



            initControls();
            loadData();
            setListener();


            if(UserNotification.getDISCUSSIONSELECTED()==Boolean.TRUE){
                changeChatView();
            }

        } catch (InflateException e) {
        }
        return view;
    }

    private void initControls() {
        lvChat = (ListView) view.findViewById(R.id.chatList);
        lvFriend = (ListView) view.findViewById(R.id.friendList);
        etFilter = (EditText) view.findViewById(R.id.etFilter);

        try {
            Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Harmattan-R.ttf");
            etFilter.setTypeface(font);
        }catch(NullPointerException ex){
            System.out.println("Null pointer excep" +ex.getMessage());
        }
        view.findViewById(R.id.bDiscu).setBackgroundColor(getResources().getColor(R.color.blue_color));
        view.findViewById(R.id.bFriend).setBackgroundColor(getResources().getColor(R.color.light_blue_color));



    }



    private void setListener() {

        view.findViewById(R.id.toggle).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((DrawerLayout) getActivity().findViewById(R.id.drawer_layout)).openDrawer(GravityCompat.START);
            }
        });
        lvChat.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                String receiverId = "";
                String chatType = "Amis";


                if(userNotification.getChatSelected() == Boolean.FALSE){
                    Intent i = new Intent(getActivity(), Chat.class);
                    i.putExtra("id", chatList.get(position).getUser().getId());
                    i.putExtra("is_group",Boolean.FALSE);
                    i.putExtra("currentChannel",chatList.get(position).getCurrentchannel());
                    getActivity().startActivity(i);
                }
                else if(userNotification.getChatSelected() == Boolean.TRUE ){
                    int object_id = userNotification.getObject();
                    String type = userNotification.getType();
                    String audio_path = userNotification.getAudio();

                    receiverId = chatList.get(position).getAmis_id();
                    chatType = "Amis";

                    if(userNotification.getShareVideo()){

                        sendMsg("",receiverId, Integer.parseInt(userNotification.getVideoId()),chatType,position,"direct_video",userNotification.getPublisherid());

                    }else{

                        sendMsg(audio_path,receiverId,object_id,chatType,position);

                    }


                }
                else {
//                    String receiverId = "";
//                    String chatType = "Amis";
                    if(UserNotification.getDISCUSSIONSELECTED()==Boolean.TRUE){
                        receiverId = String.valueOf(groupList.get(position).getGroupId());
                        chatType = "Groupe";
                    }
                    else{
                        receiverId = chatList.get(position).getAmis_id();
                        chatType = "Amis";
                    }
                    sendMsgToServer(userNotification.getAct_id(),receiverId,chatType,position);
                }



//                if(userNotification.getChatSelected() == Boolean.FALSE){
////
//
//
//                    Intent i = new Intent(getActivity(), Chat.class);
//                    i.putExtra("id", chatList.get(position).getUser().getId());
//                    i.putExtra("name",chatList.get(position).getUser().getFirstName());
//                    i.putExtra("image",chatList.get(position).getUser().getPic());
//                    i.putExtra("is_group",Boolean.FALSE);
//                    i.putExtra("currentChannel",chatList.get(position).getCurrentchannel());
//
//                    getActivity().startActivity(i);
//                }else {
//                    String receiverId = "";
//                    String chatType = "Amis";
//                    if(UserNotification.getDISCUSSIONSELECTED()==Boolean.TRUE){
//                        receiverId = String.valueOf(groupList.get(position).getGroupId());
//                        chatType = "Groupe";
//                    }
//                    else{
//                        receiverId = chatList.get(position).getAmis_id();
//                        chatType = "Amis";
//                    }
//                    sendMsgToServer(userNotification.getAct_id(),receiverId,chatType,position);
//                }

            }
        });
        view.findViewById(R.id.bDiscu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                resetIndic();
                resetLayout();
                resetButtonBg();
//                view.findViewById(R.id.chatIndic).setVisibility(View.VISIBLE);
                view.findViewById(R.id.chatList).setVisibility(View.VISIBLE);
                view.findViewById(R.id.bDiscu).setBackgroundColor(getResources().getColor(R.color.blue_color));



            }
        });
        view.findViewById(R.id.bFriend).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                resetIndic();
                resetLayout();
                resetButtonBg();
//                view.findViewById(R.id.friendIndic).setVisibility(View.VISIBLE);
                view.findViewById(R.id.llFriend).setVisibility(View.VISIBLE);
                view.findViewById(R.id.bFriend).setBackgroundColor(getResources().getColor(R.color.blue_color));


            }
        });

        lvFriend.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                ImageView openChat = (ImageView)view.findViewById(R.id.bChat);

                ImageView phoneChat = (ImageView)view.findViewById(R.id.bCall);

                phoneChat.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        redirectVideoScreen(groupList.get(position));
                    }
                });

                openChat.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if(userNotification.getChatSelected() == Boolean.FALSE){
                            Intent i = new Intent(getActivity(), Chat.class);
                            i.putExtra("id", groupList.get(position).getGroupId());
                            i.putExtra("is_group",Boolean.TRUE);
                            i.putExtra("groupName",groupList.get(position).getGroupName());

                            i.putStringArrayListExtra("group_mebers",ChatApi.getInstance().getGroupMembersDict().get(groupList.get(position).getCurrentchannel()));

                            i.putExtra("currentChannel",groupList.get(position).getCurrentchannel());

//                            set channels here


                            getActivity().startActivity(i);
                        }else {
                            String receiverId = "";

                            String chatType = "Groupe";
                            receiverId = String.valueOf(groupList.get(position).getGroupId());
                            sendMsgToServer(userNotification.getAct_id(),receiverId,chatType,position);
                        }


                    }
                });




                String receiverId = "";
                String chatType = "Amis";

                if(userNotification.getDISCUSSIONSELECTED() == Boolean.TRUE && userNotification.getChatSelected() == Boolean.FALSE ){
                    int object_id = userNotification.getObject();
                    String type = userNotification.getType();
                    String audio_path = userNotification.getAudio();

                    receiverId = String.valueOf(groupList.get(position).getGroupId());
                    chatType = "Groupe";


                    if(userNotification.getShareVideo()) {

                        sendMsg("", receiverId, Integer.parseInt(userNotification.getVideoId()), chatType, position, "direct_video", userNotification.getPublisherid());

                    }else{

                        sendMsg(audio_path,receiverId,object_id,chatType,position);
                    }



                    Log.i("calling group1","group");
                }
                else if(userNotification.getChatSelected() == Boolean.FALSE ){
                    Intent i = new Intent(getActivity(), Chat.class);
                    i.putExtra("id", groupList.get(position).getGroupId());
                    i.putExtra("is_group",Boolean.TRUE);
                    i.putExtra("currentChannel",groupList.get(position).getCurrentchannel());
                    getActivity().startActivity(i);
                    Log.i("calling group","group");
                }
                else {
                    if(UserNotification.getDISCUSSIONSELECTED()==Boolean.TRUE){
                        receiverId = String.valueOf(groupList.get(position).getGroupId());
                        chatType = "Groupe";
                    }
                    else{
                        receiverId = chatList.get(position).getAmis_id();
                        chatType = "Amis";
                    }
//                            String receiverId = "";
//
//                            String chatType = "Groupe";
                    //receiverId = String.valueOf(groupList.get(position).getGroupId());
                    sendMsgToServer(userNotification.getAct_id(),receiverId,chatType,position);
                }

            }
        });

//        etFilter.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                //final int DRAWABLE_LEFT = 0;
//                //final int DRAWABLE_TOP = 1;
//                final int DRAWABLE_RIGHT = 2;
//                //final int DRAWABLE_BOTTOM = 3;
//
//                if (event.getAction() == MotionEvent.ACTION_UP) {
//                    if (event.getRawX() >= (etFilter.getRight() - etFilter.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
//                        loadFFriends();
//                        return true;
//                    }
//                }
//                return false;
//            }
//        });
//
//        etFilter.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            @Override
//            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
//                if (i == EditorInfo.IME_ACTION_SEARCH) {
//                    loadFFriends();
//                }
//                return true;
//            }
//        });
    }
    public void changeChatView(){


//        resetIndic();
        resetLayout();
        resetButtonBg();
//        view.findViewById(R.id.friendIndic).setVisibility(View.VISIBLE);
        view.findViewById(R.id.llFriend).setVisibility(View.VISIBLE);
        view.findViewById(R.id.bFriend).setBackgroundColor(getResources().getColor(R.color.light_blue_color));




//
    }


    public void redirectVideoScreen(WISChat data){

        Tools.updateUserNotification(getActivity(),"Groupe_video", String.valueOf(data.getGroupId()));

        Intent groupVideoCall = new Intent(getActivity(), GroupCallMainActivity.class);

        groupVideoCall.putExtra("id", data.getGroupId());
        groupVideoCall.putExtra("receiver_name",data.getGroupName());
        groupVideoCall.putExtra("image", data.getGroupIcon());
        groupVideoCall.putExtra("actual_channel", data.getCurrentchannel());

        ApplicationController.getInstance().setUserCurrentChannel(data.getCurrentchannel());

        groupVideoCall.putExtra("is_group", Boolean.TRUE);
        groupVideoCall.putExtra("group_mebers",ChatApi.getInstance().getGroupMembersDict().get(data.getCurrentchannel()));
        groupVideoCall.putExtra("senderName",data.getGroupName());

        String mememberArray = ChatApi.getInstance().getGroupMembersBychannel(data.getCurrentchannel()).get(0);


        Log.d("Current group channel",data.getCurrentchannel());

        Log.d("Current group dict", String.valueOf(ChatApi.getInstance().getGroupMembersDetails()));

        Log.d("Current members_details",ChatApi.getInstance().getGroupMembersBychannel(data.getCurrentchannel()).get(0));


        String[] stringArray = new String[] {mememberArray};

        groupVideoCall.putExtra("members_details",stringArray);

        groupVideoCall.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        startActivity(groupVideoCall);
    }

    public void sendMsg(final String path, String id, final int songId,final String chatType,final int position,String type,String directID) {
        JSONObject jsonBody = new JSONObject();
        getActivity().findViewById(R.id.loading).setVisibility(View.VISIBLE);
        try {
            jsonBody.put("id_profil", Tools.getData(getActivity(), "idprofile"));
            jsonBody.put("id_profil_dest", String.valueOf(id));
            jsonBody.put("message", songId);
            jsonBody.put("type_message",type);
            jsonBody.put("chat_type", chatType);
            jsonBody.put("direct_publisher_id",directID);
            Log.i("chatype",String.valueOf(chatType));

            Log.i("jsonbody",String.valueOf(jsonBody));


        } catch (JSONException e) {
            e.printStackTrace();
        }


        Log.e("json parms", String.valueOf(jsonBody));

        JsonObjectRequest reqSend = new JsonObjectRequest(getActivity().getString(R.string.server_url) + getActivity().getString(R.string.sendmsg_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("result").equals("true")) {

                                Log.i("FragChat",String.valueOf(response));

                                UserNotification.setVideoId("");
                                UserNotification.setShareVideo(false);
                                UserNotification.setChatSelected(false);


                                if(chatType.equals("Amis"))
                                {

                                    Intent i = new Intent(getActivity(), Chat.class);
                                    i.putExtra("id", chatList.get(position).getUser().getId());
                                    i.putExtra("is_group", Boolean.FALSE);
                                    i.putExtra("currentChannel",chatList.get(position).getCurrentchannel());
                                    getActivity().startActivity(i);
                                    lvChat.invalidateViews();
                                    lvChat.refreshDrawableState();

                                    Log.i("status","Going to Chat");
                                }
                                else {
                                    Intent i = new Intent(getActivity(), Chat.class);
                                    i.putExtra("id", groupList.get(position).getGroupId());
                                    i.putExtra("is_group", Boolean.TRUE);
                                    i.putExtra("currentChannel",groupList.get(position).getCurrentchannel());
                                    getActivity().startActivity(i);
                                    lvFriend.invalidateViews();
                                    lvFriend.refreshDrawableState();
                                    Log.i("status","Going to group");
                                    //Toast.makeText(getActivity(),"Going to group", Toast.LENGTH_SHORT).show();
                                }



                            } else {
                                //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                            }
                            getActivity().findViewById(R.id.loading).setVisibility(View.GONE);
                        } catch (JSONException e) {
                            getActivity().findViewById(R.id.loading).setVisibility(View.GONE);
                            //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                getActivity().findViewById(R.id.loading).setVisibility(View.GONE);
                //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getActivity(), "token"));
                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqSend);
    }


    public void sendMsgToServer(String id_act, String amis_id, String chat_type, final int position){



        view.findViewById(R.id.loading).setVisibility(View.VISIBLE);

        JSONObject jsonBody = null;

        try {
//            jsonBody = new JSONObject("{\"user_id\":\"" + Tools.getData(getActivity(), "idprofile") + "\",\"amis_id\":\"" + amis_id + "\",\"id_act\":\"" + id_act + "\",\"chat_type\":\"" + id_act + "\"}");
            jsonBody = new JSONObject();
            jsonBody.put("user_id",Tools.getData(getActivity(), "idprofile"));
            jsonBody.put("amis_id",amis_id);
            jsonBody.put("chat_type",chat_type);
            jsonBody.put("group_id",amis_id);


            if(UserNotification.getChatWithCustomtextSelected()==Boolean.TRUE){

//                @{@"activity":@"YES",@"id_act":notifyData.activityId,@"user_id":user.idprofile,@"amis_id":receiver,@"chat_type":chatType,@"group_id":receiver};
//                @{@"activity":@"NO",@"id_act":@"",@"user_id":user.idprofile,@"amis_id":receiver,@"chat_type":chatType,@"group_id":receiver,@"photo":@"NO",@"msg":self.sharedMessage};
                jsonBody.put("activity","NO");
                jsonBody.put("id_act","NO");

                if(UserNotification.getCustomImagePath()!=""){
                    jsonBody.put("photo","YES");
                }
                else{
                    jsonBody.put("photo","NO");

                }
                jsonBody.put("msg",UserNotification.getCustomText());

            }else{

                jsonBody.put("activity","YES");
                jsonBody.put("id_act",id_act);
                jsonBody.put("msg","empty");
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.i("Share To ChatApi",jsonBody.toString());

        new DoUpload().execute(jsonBody.toString());

//        String ApiUrl = getString(R.string.server_url) + getString(R.string.share_discussion);
//
//        try {
//            String response = Tools.shareTextToChat(ApiUrl,UserNotification.getCustomImagePath(),Tools.getData(getActivity(), "token"),Tools.getData(getActivity(), "idprofile"),amis_id,UserNotification.getCustomText(),
//                    jsonBody.getString("chat_type"),jsonBody.getString("photo"),jsonBody.getString("activity"),jsonBody.getString("id_act"));
//
//            Log.i("chat share response",response.toString());
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }


//        JsonObjectRequest req = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.share_discussion), jsonBody,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        view.findViewById(R.id.loading).setVisibility(View.GONE);
//
//                        Log.i("Response",response.toString());
//                        userNotification.setChatSelected(Boolean.FALSE);
//                       Intent i = new Intent(getActivity(), ChatApi.class);
//                        i.putExtra("id", chatList.get(position).getUser().getId());
//                        getActivity().startActivity(i);
//
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                view.findViewById(R.id.loading).setVisibility(View.GONE);
//                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<>();
//                headers.put("token", Tools.getData(getActivity(), "token"));
//                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
//                return headers;
//            }
//        };
//
//        ApplicationController.getInstance().addToRequestQueue(req);
    }





//
//    private void resetIndic() {
//        view.findViewById(R.id.chatIndic).setVisibility(View.INVISIBLE);
//        view.findViewById(R.id.friendIndic).setVisibility(View.INVISIBLE);
//    }

    private void resetLayout() {
        view.findViewById(R.id.chatList).setVisibility(View.GONE);
        view.findViewById(R.id.llFriend).setVisibility(View.GONE);
    }

    private void resetButtonBg() {
        view.findViewById(R.id.bDiscu).setBackgroundColor(getResources().getColor(R.color.light_blue_color));
        view.findViewById(R.id.bFriend).setBackgroundColor(getResources().getColor(R.color.light_blue_color));
    }

    private void loadData() {
        userList = new ArrayList<>();



        view.findViewById(R.id.loading).setVisibility(View.VISIBLE);

        JSONObject jsonBody = null;

        try {
            jsonBody = new JSONObject("{\"id_profil\":\"" + Tools.getData(getActivity(), "idprofile") + "\"}");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqDisc = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.alldiscution_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        view.findViewById(R.id.loading).setVisibility(View.GONE);
                        try {
                            if (response.getString("Message").contains("any historique")) {
                                Toast.makeText(getActivity(), getString(R.string.no_chat_historique), Toast.LENGTH_SHORT).show();
                            } else if (response.getString("result").equals("true")) {
                                chatList = new ArrayList<>();
                                JSONArray data = response.getJSONArray("data");

                                System.out.println("channel response" +response);

                                Log.i("resp", String.valueOf(response));

                                JSONArray privateJsonArray = new JSONArray();

                                for (int i = 0; i < data.length(); i++) {
                                    try {
                                        JSONObject obj = data.getJSONObject(i);
                                        JSONObject oDisc = obj.getJSONObject("discution");

                                        //  get channel

                                        String privatechannel = "Private_".concat(obj.getString("channels"));

                                        privateJsonArray.put(i,privatechannel);


                                        JSONObject oFriend = obj.getJSONObject("amis");

                                        String firstname=oFriend.getString("firstname");
                                        System.out.println("firstname****" +firstname);
//                                        String Firstname =oFriend.getString("firstname");
//                                        String anis_id=oFriend.getString("lastname");
//                                        String photo=oFriend.getString("photo");
//                                        Date d=new Date();
//                                        Date now = new Date();

//                                        override date and time
                                        Date date=new Date();
                                        SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                        sourceFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                                        sourceFormat.format(date);
                                        Log.i("smiley", String.valueOf(sourceFormat.format(date)));

                                        TimeZone tz = TimeZone.getTimeZone(Tools.getData(getActivity(),"timezone"));
                                        SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                        destFormat.setTimeZone(tz);

                                        String userTimeZoneFormateddate = destFormat.format(date);
                                        Log.i("resilt timezone ",userTimeZoneFormateddate);


                                        String created_at= String.valueOf(DateUtils.getRelativeTimeSpanString(getDateInMillis(oDisc.getString("created_at")),getDateInMillis(userTimeZoneFormateddate), 0));


                                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                        Date d1 = null;
                                        try {
                                            d1 = format.parse(oDisc.getString("created_at"));
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                        Log.i("chate data",created_at);

                                        chatList.add(new WISChat(oDisc.getInt("id"), new WISUser(oFriend.getInt("id_ami"), oFriend.getString("firstname"), oFriend.getString("lastname"), oFriend.getString("photo"), 0), oDisc.getString("message"),created_at, true, oDisc.getString("type_message"),oDisc.getString("created_at") ,oFriend.getString("id_ami"),privatechannel));
                                    } catch (Exception e) {
                                        String x = "";
                                    }
                                }
                                lvChat.setAdapter(new DiscuAdapter(getActivity(), chatList));

                                Log.i("private", String.valueOf(privateJsonArray));

                                ChatApi.getInstance().removePrivatechannels();

                                Tools.getPrivateChannel(privateJsonArray);

                            }

                        } catch (JSONException e) {
                            view.findViewById(R.id.loading).setVisibility(View.GONE);
                            //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                view.findViewById(R.id.loading).setVisibility(View.GONE);
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getActivity(), "token"));
                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
                return headers;
            }
        };

        getGroupList();


//
//        try {
//            jsonBody = new JSONObject("{\"id_profil\":\"" + Tools.getData(getActivity(), "idprofile") + "\"}");
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        JsonObjectRequest reqFriend = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.friends_meth), jsonBody,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        try {
//                            Log.i("discussion list", String.valueOf(response));
//                            if (response.getString("data").contains("Acun amis"))
//                                Toast.makeText(getActivity(), getString(R.string.no_friend), Toast.LENGTH_SHORT).show();
//                            else {
//                                if (response.getString("result").equals("true")) {
//                                    JSONArray data = response.getJSONArray("data");
//                                    for (int i = 0; i < data.length(); i++) {
//                                        try {
//                                            JSONObject obj = data.getJSONObject(i);
//                                            userList.add(new WISUser(obj.getInt("idprofile"), obj.getString("firstname_prt"), obj.getString("lastname_prt"), obj.getString("photo"), obj.getInt("nbr_amis")));
//                                        } catch (Exception e) {
//
//                                        }
//                                    }
//                                    //is exist
//                                    lvFriend.setAdapter(new FriendsAdapter(getActivity(), userList));
//                                }
//                            }
//                            view.findViewById(R.id.loading).setVisibility(View.GONE);
//
//                        } catch (JSONException e) {
//
//                            view.findViewById(R.id.loading).setVisibility(View.GONE);
//                            //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
//                        }
//
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                view.findViewById(R.id.loading).setVisibility(View.GONE);
//                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<>();
//                headers.put("token", Tools.getData(getActivity(), "token"));
//                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
//                return headers;
//            }
//        };


        ApplicationController.getInstance().addToRequestQueue(reqDisc);
    }

    public void getGroupList(){

        JSONObject jsonBody = null;
        try {
            jsonBody = new JSONObject("{\"user_id\":\"" + Tools.getData(getActivity(), "idprofile") + "\"}");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest getGroups = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.get_groupList), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            if (response.getString("result").equals("true")) {

                                Log.i("group chat", String.valueOf(response));


                                JSONArray data = response.getJSONArray("data");

                                ArrayList<List> groupchannelMember = new ArrayList<>();

                                ArrayList <String> publicid = new ArrayList();

                                groupList = new ArrayList<>();

                                for (int i = 0; i < data.length(); i++) {
                                    try {
                                        JSONObject obj = data.getJSONObject(i);

                                        String publicId =  "Public_".concat(obj.getString("group_id"));

                                        publicid.add(publicId);

                                        JSONArray groupMember = obj.getJSONArray("channel_group_members");

                                        JSONArray groupMemberDetails = obj.getJSONArray("membersDetails");




                                        System.out.println("group meber");

                                        Log.i("group count", String.valueOf(groupMember.length()));

                                        Log.i("group members", String.valueOf(groupMember));

                                        Log.i("groupMemberDetails", String.valueOf(groupMemberDetails));



                                        ChatApi.getInstance().setGroupMembersDetails(publicId,groupMemberDetails);

                                        Log.i("parsinng pMemberDetails", String.valueOf(ChatApi.getInstance().getGroupMembersBychannel(publicId)));


                                        List<String> memberlist = new ArrayList<String>();
                                        for (int m=0; m<groupMember.length(); m++) {
                                            memberlist.add(groupMember.getString(m) );
                                        }

                                        groupchannelMember.add(memberlist);

                                        String lastMessage = "",created_at = "";
                                        JSONObject message = null;
                                        if(obj.optJSONObject("message")!=null){
                                            message =obj.getJSONObject("message");
                                            if(message!=null){
                                                lastMessage = message.getString("message");
                                                created_at = message.getString("created_at");
                                            }
                                        }

                                        Log.i("group chat date",created_at);
//                                            chatList.add(new WISChat(oDisc.getInt("id"), new WISUser(oFriend.getInt("id_ami"), oFriend.getString("firstname"), oFriend.getString("lastname"), oFriend.getString("photo"), 0), oDisc.getString("message"),created_at, true, oDisc.getString("type_message"),oDisc.getString("created_at") ,oFriend.getString("id_ami")));

                                        String currentchannel = "Public_".concat(String.valueOf(obj.getInt("group_id")));
                                        groupList.add(new WISChat(true,obj.getInt("group_id"),obj.getString("group_name"),obj.getString("photo"),obj.getInt("number_of_members"),lastMessage,created_at,currentchannel));

//
                                    } catch (Exception e) {

                                    }
                                }

                                Log.i("group chat size", String.valueOf(groupList.size()));
//                                    //is exist
                                groupListAdapter =new GroupListAdapter(getActivity(),groupList);
                                lvFriend.setAdapter(groupListAdapter);
                                setUPFilterConfig(groupListAdapter);


                                Log.i("gorup", String.valueOf(groupchannelMember));

                                Log.i("group", String.valueOf(publicid));




                                ChatApi.getInstance().removePublicchannels();

                                ChatApi.getInstance().addPublicChannels(groupchannelMember,publicid);
                            }



//                            view.findViewById(R.id.loading).setVisibility(View.GONE);

                        } catch (JSONException e) {

                            view.findViewById(R.id.loading).setVisibility(View.GONE);
                            //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                view.findViewById(R.id.loading).setVisibility(View.GONE);
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getActivity(), "token"));
                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(getGroups);

    }

    private void setUPFilterConfig(final GroupListAdapter adapter){
        etFilter.addTextChangedListener(new TextWatcherAdapter(){
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                super.beforeTextChanged(s, start, count, after);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                super.onTextChanged(s, start, before, count);
                adapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
                super.afterTextChanged(s);

            }
        });
    }

    public static long getDateInMillis(String srcDate) {

        Log.i("src date",srcDate);
        SimpleDateFormat desiredFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss");

        long dateInMillis = 0;
        try {
            Date date = desiredFormat.parse(srcDate);
            dateInMillis = date.getTime();
            return dateInMillis;
        } catch (ParseException e) {

            e.printStackTrace();
        }

        return 0;
    }


    private void loadFFriends() {
        Tools.hideKeyboard(getActivity());

        userList = new ArrayList<>();


        view.findViewById(R.id.loading).setVisibility(View.VISIBLE);

        JSONObject jsonBody = null;

        try {
            jsonBody = new JSONObject("{\"id_profil\":\"" + Tools.getData(getActivity(), "idprofile") + "\",\"filtre\":\"" + etFilter.getText().toString() + "\"}");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest req = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.ffriends_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("result").equals("true")) {
                                JSONArray data = response.getJSONArray("data");
                                for (int i = 0; i < data.length(); i++) {
                                    try {
                                        JSONObject obj = data.getJSONObject(i);
                                        userList.add(new WISUser(obj.getInt("idprofile"), obj.getString("firstname_prt"), obj.getString("lastname_prt"), obj.getString("photo"), obj.getInt("nbr_amis")));
                                    } catch (Exception e) {

                                    }
                                }
                                lvFriend.setAdapter(new FriendsAdapter(getActivity(), userList));
                            }
                            view.findViewById(R.id.loading).setVisibility(View.GONE);

                        } catch (JSONException e) {

                            view.findViewById(R.id.loading).setVisibility(View.GONE);
                            //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                view.findViewById(R.id.loading).setVisibility(View.GONE);
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getActivity(), "token"));
                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(req);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ApplicationController.getInstance().cancelPendingRequests(ApplicationController.TAG);
    }
    @Override

    public void onResume(){

        super.onResume();

        UserNotification notification = new UserNotification();

        if(notification.getChatSelected() == Boolean.FALSE){

            headerTitle.setText(R.string.chat);

        }


    }

    @Override
    public void onPause() {
        super.onPause();

        UserNotification.setChatSelected(Boolean.FALSE);
        UserNotification.setDISCUSSIONSELECTED(Boolean.FALSE);
        UserNotification.setChatWithCustomtextSelected(Boolean.FALSE);
    }


    private class DoUpload extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            view.findViewById(R.id.loading).setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            view.findViewById(R.id.loading).setVisibility(View.GONE);
            if (!result.equals("")) {
                try {
                    JSONObject img = new JSONObject(result);
                    if (img.getBoolean("result"))
                        Log.i("image data set",img.getString("data"));

                    //else
                    //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    view.findViewById(R.id.loading).setVisibility(View.GONE);
                    Toast.makeText(getActivity(), getActivity().getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                }

            } else {
                view.findViewById(R.id.loading).setVisibility(View.GONE);
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected String doInBackground(String... urls) {


            JSONObject jsonBody = null;
            try {
                jsonBody = new JSONObject(urls[0]);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            String ApiUrl = getString(R.string.server_url) + getString(R.string.share_discussion);

            try {

                return Tools.shareTextToChat(ApiUrl,UserNotification.getCustomImagePath(),Tools.getData(getActivity(), "token"),Tools.getData(getActivity(), "idprofile"),jsonBody.getString("amis_id"),UserNotification.getCustomText(),
                        jsonBody.getString("chat_type"),jsonBody.getString("photo"),jsonBody.getString("activity"),jsonBody.getString("id_act"));


            } catch (JSONException e) {
                e.printStackTrace();
            }

            return  null;

        }
    }

    public void sendMsg(final String path, String id, final int songId,final String chatType,final int position) {
        JSONObject jsonBody = new JSONObject();
        getActivity().findViewById(R.id.loading).setVisibility(View.VISIBLE);
        try {
            jsonBody.put("id_profil", Tools.getData(getActivity(), "idprofile"));
            jsonBody.put("id_profil_dest", String.valueOf(id));
            jsonBody.put("message", songId);
            jsonBody.put("type_message","audio");
            jsonBody.put("chat_type", chatType);
            Log.i("chatype",String.valueOf(chatType));

            Log.i("jsonbody",String.valueOf(jsonBody));


        } catch (JSONException e) {
            e.printStackTrace();
        }


        Log.e("json parms", String.valueOf(jsonBody));

        JsonObjectRequest reqSend = new JsonObjectRequest(getActivity().getString(R.string.server_url) + getActivity().getString(R.string.sendmsg_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("result").equals("true")) {

                                Log.i("FragChat",String.valueOf(response));


                                if(chatType.equals("Amis"))
                                {

                                    Intent i = new Intent(getActivity(), Chat.class);
                                    i.putExtra("id", chatList.get(position).getUser().getId());
                                    i.putExtra("is_group", Boolean.FALSE);
                                    i.putExtra("currentChannel",chatList.get(position).getCurrentchannel());
                                    getActivity().startActivity(i);
                                    lvChat.invalidateViews();
                                    lvChat.refreshDrawableState();

                                    Log.i("status","Going to Chat");
                                }
                                else {
                                    Intent i = new Intent(getActivity(), Chat.class);
                                    i.putExtra("id", groupList.get(position).getGroupId());
                                    i.putExtra("is_group", Boolean.TRUE);
                                    i.putExtra("currentChannel",groupList.get(position).getCurrentchannel());
                                    getActivity().startActivity(i);
                                    lvFriend.invalidateViews();
                                    lvFriend.refreshDrawableState();
                                    Log.i("status","Going to group");
                                    //Toast.makeText(getActivity(),"Going to group", Toast.LENGTH_SHORT).show();
                                }



                            } else {
                                //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                            }
                            getActivity().findViewById(R.id.loading).setVisibility(View.GONE);
                        } catch (JSONException e) {
                            getActivity().findViewById(R.id.loading).setVisibility(View.GONE);
                            //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                getActivity().findViewById(R.id.loading).setVisibility(View.GONE);
                //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getActivity(), "token"));
                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqSend);
    }
}
