package com.oec.wis.tools;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationManager;
import android.media.MediaMetadataRetriever;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.util.TimeUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.oec.wis.ApplicationController;
import com.oec.wis.Dashboard;
import com.oec.wis.R;
import com.oec.wis.adapters.FriendsAdapter;
import com.oec.wis.adapters.ShowDialogAdapter;
import com.oec.wis.adapters.WisAmisAdapter;
import com.oec.wis.chatApi.ChatApi;
import com.oec.wis.dialogs.Chat;
import com.oec.wis.dialogs.PubDetails;
import com.oec.wis.dialogs.PubView;
import com.oec.wis.dialogs.StreamAudio;
import com.oec.wis.entities.EntryItem;
import com.oec.wis.entities.SectionItem;
import com.oec.wis.entities.UserNotification;
import com.oec.wis.entities.WISMenu;
import com.oec.wis.entities.WISUser;
import com.oec.wis.wisdirect.WisDirect;

import org.apache.commons.lang3.ClassPathUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

import javax.microedition.khronos.opengles.GL10;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

public class Tools {

    GridView navGrid;
    ProgressDialog pd;
    public static void saveData(Context _context, String _name, String _value) {
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(_context).edit();
        editor.putString(_name, _value);
        editor.commit();
    }

    public static String getData(Context _context, String _name) {
        try {
            return PreferenceManager.getDefaultSharedPreferences(_context).getString(_name, "");
        } catch (Exception e) {
            return "";
        }
    }

    public static String doFileUpload(String sUrl, String fPath, String token) {
        HttpClient http = AndroidHttpClient.newInstance("WIS");
        HttpPost method = new HttpPost(sUrl);
        String BOUNDARY = "--wisboundry--";
        method.setHeader("Content-Type", "multipart/form-data; boundary=" + BOUNDARY);
        method.addHeader("token", token);

        MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE, BOUNDARY, Charset.defaultCharset());

        Log.i("server file", String.valueOf(new File(fPath)));
        entity.addPart("file", new FileBody(new File(fPath)));

        method.setHeader("Accept", "application/json");
        method.setHeader("Content-Type", "multipart/form-data; boundary=" + BOUNDARY);
        method.setEntity(entity);
        HttpResponse response = null;
        try {
            response = http.execute(method);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String res = "";
        HttpEntity resEntity = response.getEntity();
        try {
            res = EntityUtils.toString(resEntity);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return res;
    }
    public static String doFileUploadForChat(String sUrl, String fPath, String token,String userId,String message,String type) {
        HttpClient http = AndroidHttpClient.newInstance("WIS");
        HttpPost method = new HttpPost(sUrl);
        String BOUNDARY = "--wisboundry--";
        method.setHeader("Content-Type", "multipart/form-data; boundary=" + BOUNDARY);
        method.addHeader("token", token);


        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        builder.setBoundary(BOUNDARY);
        builder.setCharset(Charset.defaultCharset());
        FileBody fileBody = new FileBody(new File(fPath));
        builder.addPart("file", fileBody);



        builder.addTextBody("user_id", userId);
        builder.addTextBody("message", message);
//        builder.addTextBody("photo_count", String.valueOf(fPath.size()));
        HttpEntity entity = builder.build();

        method.setHeader("Accept", "application/json");
        method.setHeader("Content-Type", "multipart/form-data; boundary=" + BOUNDARY);
        method.setEntity(entity);

        Log.i("parms0", String.valueOf(token));
        Log.i("parms1", String.valueOf(userId));
        Log.i("fpath", String.valueOf(fPath));

        HttpResponse response = null;
        try {
            response = http.execute(method);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String res = "";
        HttpEntity resEntity = response.getEntity();
        try {
            res = EntityUtils.toString(resEntity);


            Log.i("muliple response", String.valueOf(res));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return res;
    }

    public static String doVoiceUploadForChat(String sUrl, String fPath, String token,String userId,String message,String type,String receiver) {

        Log.i("parms0", String.valueOf(token));
        Log.i("parms1", String.valueOf(userId));
        Log.i("fpath", String.valueOf(fPath));
        Log.i("server url", sUrl);



        HttpClient http = AndroidHttpClient.newInstance("WIS");
        HttpPost method = new HttpPost(sUrl);
        String BOUNDARY = "--wisboundry--";
        method.setHeader("Content-Type", "multipart/form-data; boundary=" + BOUNDARY);
        method.addHeader("token", token);


        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        builder.setBoundary(BOUNDARY);
        builder.setCharset(Charset.defaultCharset());
        FileBody fileBody = new FileBody(new File(fPath));
        builder.addPart("file", fileBody);
        builder.addTextBody("user_id", userId);
        builder.addTextBody("chat_type", "Amis");
        builder.addTextBody("message_type","voice_recording");
        builder.addTextBody("receiver",receiver);
        HttpEntity entity = builder.build();
        method.setHeader("Accept", "application/json");
        method.setHeader("Content-Type", "multipart/form-data; boundary=" + BOUNDARY);
        method.setEntity(entity);



        HttpResponse response = null;
        try {
            response = http.execute(method);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String res = "";
        HttpEntity resEntity = response.getEntity();
        try {
            Log.i("RESPOSNE HERE","API RESPONSE");

            res = EntityUtils.toString(resEntity);
            Log.i("muliple response", String.valueOf(res));
            return res;


        } catch (IOException e) {
            e.printStackTrace();
        }

        return res;
    }

    public static String doMultipleFileUpload(String sUrl, ArrayList<String> fPath, String token,String userId,String message,String type) {
        HttpClient http = AndroidHttpClient.newInstance("WIS");
        HttpPost method = new HttpPost(sUrl);
        String BOUNDARY = "--wisboundry--";
        method.setHeader("Content-Type", "multipart/form-data; boundary=" + BOUNDARY);
        method.addHeader("token", token);


        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        builder.setBoundary(BOUNDARY);
        builder.setCharset(Charset.defaultCharset());

        for(int i=0;i<fPath.size();i++) {
            FileBody fileBody = new FileBody(new File(fPath.get(i)));
            builder.addPart("file".concat(String.valueOf(i)), fileBody);
            Log.i("file name","file".concat(String.valueOf(i)));

        }


        builder.addTextBody("user_id", userId);
        builder.addTextBody("message", message);
        builder.addTextBody("photo_count", String.valueOf(fPath.size()));
        HttpEntity entity = builder.build();

        method.setHeader("Accept", "application/json");
        method.setHeader("Content-Type", "multipart/form-data; boundary=" + BOUNDARY);
        method.setEntity(entity);

        Log.i("parms0", String.valueOf(token));
        Log.i("parms1", String.valueOf(userId));
        Log.i("fpath", String.valueOf(fPath));


        HttpResponse response = null;
        try {
            response = http.execute(method);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String res = "";
        HttpEntity resEntity = response.getEntity();
        try {
            res = EntityUtils.toString(resEntity);


            Log.i("muliple response", String.valueOf(res));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return res;
    }


    public static String uploadMusic(List<String> files,List<String> songIcons,String albumName,String albumArtist,String albumDuration,String type,String songNames,String songArtists,String durations, String sUrl,String userId,String token) {
        HttpClient http = AndroidHttpClient.newInstance("WIS");
        HttpPost method = new HttpPost(sUrl);
        String BOUNDARY = "--wisboundry--";
        method.setHeader("Content-Type", "multipart/form-data; boundary=" + BOUNDARY);
        method.addHeader("token", token);


        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        builder.setBoundary(BOUNDARY);
        builder.setCharset(Charset.defaultCharset());

        for(int i=0;i<files.size();i++) {
            FileBody fileBody = new FileBody(new File(files.get(i)));
            builder.addPart("file".concat(String.valueOf(i)), fileBody);
            Log.i("file name","file".concat(String.valueOf(i)));

        }
        for(int i=0;i<songIcons.size();i++) {
            FileBody fileBody = new FileBody(new File(songIcons.get(i)));
            builder.addPart("audio_icons".concat(String.valueOf(i)), fileBody);
            Log.i("audio_icons","audio_icons".concat(String.valueOf(i)));
        }

        builder.addTextBody("user_id", userId);
        builder.addTextBody("album_name", albumName);
        builder.addTextBody("album_artists", albumArtist);
        builder.addTextBody("type", type);
        builder.addTextBody("album_duration", albumDuration);
        builder.addTextBody("name",songNames);
        builder.addTextBody("artists",songArtists);
        builder.addTextBody("duration",durations);
        HttpEntity entity = builder.build();

        method.setHeader("Accept", "application/json");
        method.setHeader("Content-Type", "multipart/form-data; boundary=" + BOUNDARY);
        method.setEntity(entity);


        HttpResponse response = null;
        try {
            response = http.execute(method);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String res = "";
        HttpEntity resEntity = response.getEntity();
        try {
            res = EntityUtils.toString(resEntity);


            Log.i("muliple response", String.valueOf(res));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return res;
    }



    public static String loadingDirectData(String archiveId,String directId,String sUrl,String token,String userId)
    {
        HttpClient http = AndroidHttpClient.newInstance("WIS");
        HttpPost method = new HttpPost(sUrl);
        String BOUNDARY = "--wisboundry--";
        method.setHeader("Content-Type", "multipart/form-data; boundary=" + BOUNDARY);
        method.addHeader("token", token);


        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        builder.setBoundary(BOUNDARY);
        builder.setCharset(Charset.defaultCharset());
        builder.addTextBody("user_id", userId);
        builder.addTextBody("archiveId", archiveId);
        builder.addTextBody("direct_id",directId);
        HttpEntity entity = builder.build();
        method.setHeader("Accept", "application/json");
        method.setHeader("Content-Type", "multipart/form-data; boundary=" + BOUNDARY);
        method.setEntity(entity);

        HttpResponse response = null;
        try {
            response = http.execute(method);
        } catch (IOException e) {
            e.printStackTrace();
        }


        String res = "";
        HttpEntity resEntity=null;
        try {

            if(response.getEntity()!=null){

                resEntity = response.getEntity();
            }
        }
        catch (NullPointerException e)
        {

        }


        try {
            res = EntityUtils.toString(resEntity);


            Log.i("muliple response", String.valueOf(res));

            //http.getConnectionManager().shutdown();

            if( response.getEntity() != null ) {
                response.getEntity().consumeContent();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }




        return res;

    }

    public static String shareTextToChat(String sUrl, String fPath, String token,String userId,String amisId,String msg,String chatType,String photo,String activity,String actId) {
        HttpClient http = AndroidHttpClient.newInstance("WIS");
        HttpPost method = new HttpPost(sUrl);
        String BOUNDARY = "--wisboundry--";
        method.setHeader("Content-Type", "multipart/form-data; boundary=" + BOUNDARY);
        method.addHeader("token", token);

        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        builder.setBoundary(BOUNDARY);
        builder.setCharset(Charset.defaultCharset());

        if(photo.equals("YES")){


            FileBody fileBody = new FileBody(new File(fPath));
            builder.addPart("file", fileBody);
        }





        builder.addTextBody("user_id", userId);
        builder.addTextBody("amis_id",amisId);
        builder.addTextBody("chat_type",chatType);
        builder.addTextBody("group_id", amisId);
        builder.addTextBody("photo", photo);
        builder.addTextBody("activity",activity);
        builder.addTextBody("id_act",actId);
        builder.addTextBody("msg",msg);
        HttpEntity entity = builder.build();

        method.setHeader("Accept", "application/json");
        method.setHeader("Content-Type", "multipart/form-data; boundary=" + BOUNDARY);
        method.setEntity(entity);

        Log.i("parms0", String.valueOf(token));
        Log.i("parms1", String.valueOf(userId));
        Log.i("fpath", String.valueOf(fPath));
        Log.i("chat_type", String.valueOf(chatType));
        Log.i("photo", String.valueOf(photo));
        Log.i("activity", String.valueOf(activity));


        HttpResponse response = null;
        try {
            response = http.execute(method);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String res = "";
        HttpEntity resEntity = response.getEntity();
        try {
            res = EntityUtils.toString(resEntity);


            Log.i("chat response", String.valueOf(res));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return res;
    }

    public static String createGroup(String sUrl, String fPath, String token,String userId,String name,String members) {
        HttpClient http = AndroidHttpClient.newInstance("WIS");
        HttpPost method = new HttpPost(sUrl);
        String BOUNDARY = "--wisboundry--";
        method.setHeader("Content-Type", "multipart/form-data; boundary=" + BOUNDARY);
        method.addHeader("token", token);


        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        builder.setBoundary(BOUNDARY);
        builder.setCharset(Charset.defaultCharset());
        if(fPath!=null)
            builder.addPart("file",  new FileBody(new File(fPath)));
        builder.addTextBody("user_id", userId);
        builder.addTextBody("friends_list", members);
        builder.addTextBody("name", name);
        HttpEntity entity = builder.build();

        method.setHeader("Accept", "application/json");
        method.setHeader("Content-Type", "multipart/form-data; boundary=" + BOUNDARY);
        method.setEntity(entity);

        Log.i("parms0", String.valueOf(token));
        Log.i("parms1", String.valueOf(userId));
        Log.i("fpath", String.valueOf(fPath));
        Log.i("members", String.valueOf(members));
        Log.i("urls", String.valueOf(sUrl));
        Log.i("name", String.valueOf(name));


        HttpResponse response = null;
        try {
            response = http.execute(method);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String res = "";
        HttpEntity resEntity = response.getEntity();
        try {
            res = EntityUtils.toString(resEntity);


            Log.i("muliple response", String.valueOf(res));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return res;
    }
    public static String doWriteStatus(String sUrl, String fPath, String token,String message,String user_id) {
        HttpClient http = AndroidHttpClient.newInstance("WIS");
        HttpPost method = new HttpPost(sUrl);

        method.addHeader("token", token);
        method.setHeader("Accept", "application/json");
        String BOUNDARY = "--wisboundry--";
        MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE, BOUNDARY, Charset.defaultCharset());
        entity.addPart("file", new FileBody(new File(fPath)));
        method.setHeader("Content-Type", "multipart/form-data; boundary=" + BOUNDARY);
        method.setEntity(entity);
        HttpResponse response = null;
        try {
            response = http.execute(method);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String res = "";
        HttpEntity resEntity = response.getEntity();
        try {
            res = EntityUtils.toString(resEntity);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return res;
    }
    public static String doWriteStatusText(String sUrl, String fPath, String token,String message) {
        HttpClient http = AndroidHttpClient.newInstance("WIS");
        HttpPost method = new HttpPost(sUrl);
        String BOUNDARY = "--wisboundry--";
        method.setHeader("Content-Type", "multipart/form-data; boundary=" + BOUNDARY);
        method.addHeader("token", token);

        MultipartEntity entity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE, BOUNDARY, Charset.defaultCharset());
        entity.addPart("file", new FileBody(new File(fPath)));

        method.setHeader("Accept", "application/json");
        method.setHeader("Content-Type", "multipart/form-data; boundary=" + BOUNDARY);
        method.setEntity(entity);
        HttpResponse response = null;
        try {
            response = http.execute(method);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String res = "";
        HttpEntity resEntity = response.getEntity();
        try {
            res = EntityUtils.toString(resEntity);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return res;
    }

    static public String generateString(InputStream stream) {
        InputStreamReader reader = new InputStreamReader(stream);
        BufferedReader buffer = new BufferedReader(reader);
        StringBuilder sb = new StringBuilder();
        try {
            String cur;
            while ((cur = buffer.readLine()) != null) {
                sb.append(cur).append("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            stream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    public static void hideKeyboard(Activity context) {
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = null;
        if (view == null) {
            view = new View(context);
        }
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void showGPSDisabledAlert(final Context context) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setMessage(context.getString(com.oec.wis.R.string.msg_err_gps))
                .setCancelable(false)
                .setPositiveButton(context.getString(com.oec.wis.R.string.msg_err_gps_activate),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent callGPSSettingIntent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                context.startActivity(callGPSSettingIntent);
                            }
                        });
        alertDialogBuilder.setNegativeButton(context.getString(com.oec.wis.R.string.msg_err_gps_cancel),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();

    }

    public static void showNotif(Context _context, String id, String title, String desc) {
        NotificationManager notificationManager;
        notificationManager = (NotificationManager) _context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification;
        Intent i = new Intent(_context, PubDetails.class);
        i.putExtra("id", Integer.parseInt(id));
        Log.i("pubIdsss",String.valueOf(Integer.parseInt(id)));
        new UserNotification().setAddress(new UserNotification().getAddress());
        PendingIntent pendingIntent = PendingIntent.getActivity(_context, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(
                _context);
        notification = builder.setContentIntent(pendingIntent)
                .setSmallIcon(R.drawable.logo_wis2).setTicker(title)
                .setAutoCancel(true).setContentTitle(title).setWhen(System.currentTimeMillis())
                .setContentText(desc).build();
        notification.defaults |= Notification.DEFAULT_SOUND;
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notificationManager.notify((int) System.currentTimeMillis(), notification);
    }



    public static void showNotif2(Context _context, int id, String title, String desc) {
        System.out.println("-----------------------------------------------");
        System.out.println(id);
        System.out.println(title);
        System.out.println(desc);
        NotificationManager notificationManager;
        notificationManager = (NotificationManager) _context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification;
        Intent i = new Intent(_context, Chat.class);
        i.putExtra("id", id);
        i.putExtra("is_group", Boolean.FALSE);


        PendingIntent pendingIntent = PendingIntent.getActivity(_context, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(
                _context);
        notification = builder.setContentIntent(pendingIntent)
                .setSmallIcon(R.drawable.logo_wis2).setTicker(title)
                .setAutoCancel(true).setContentTitle(title).setWhen(System.currentTimeMillis())
                .setContentText(desc).build();
        notification.defaults |= Notification.DEFAULT_SOUND;
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notificationManager.notify((int) System.currentTimeMillis(), notification);


//        PendingIntent pendingIntent = PendingIntent.getActivity(_context, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);
//
//        NotificationCompat.Builder builder = new NotificationCompat.Builder(
//                _context);
//        notification = builder
//                .setPriority(Notification.PRIORITY_HIGH)
//                .setSmallIcon(R.drawable.logo_wis2).setTicker(title)
//                .setAutoCancel(true).setContentTitle(title).setWhen(System.currentTimeMillis())
//                .setContentText(desc).build();
//        notification.defaults |= Notification.DEFAULT_SOUND;
//        notification.flags |= Notification.FLAG_AUTO_CANCEL;
//        notificationManager.notify(541269880, notification);

    }

    public static void showNotif3(Context _context, String title, String desc) {
        NotificationManager notificationManager;
        notificationManager = (NotificationManager) _context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification;
        Intent i = new Intent(_context, Dashboard.class);
        i.putExtra("invit", "invit");
        PendingIntent pendingIntent = PendingIntent.getActivity(_context, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(
                _context);
        notification = builder.setContentIntent(pendingIntent)
                .setSmallIcon(R.drawable.logo_wis2).setTicker(title)
                .setAutoCancel(true).setContentTitle(title).setWhen(System.currentTimeMillis())
                .setContentText(desc).build();
        notification.defaults |= Notification.DEFAULT_SOUND;
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notificationManager.notify((int) System.currentTimeMillis(), notification);
    }
    public static void wisNotify(Context ctx,String session,String token,String idAct,String apikey,String title,String sender,String publisherId,String direct)
    {
        Log.e("CallingNote","wisNotify");


        String curentId = Tools.getData(ctx, "idprofile");
        if(curentId.equals(publisherId))
        {
            System.out.print("YOU ARE PUBLISHER NOW ***");
        }
        else {
            if (!Tools.getData(ctx, "idprofile").equals("")) {
                Intent intent = new Intent(ctx, WisDirect.class);
                intent.putExtra("session_id",session);
                intent.putExtra("is_publisher","NO");
                intent.putExtra("token",token);
                intent.putExtra("api_key",apikey);
                intent.putExtra("idAct",idAct);
                intent.putExtra("id",direct);



                PendingIntent contentIntent = PendingIntent.getActivity(ctx, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

                NotificationCompat.Builder b = new NotificationCompat.Builder(ctx);

                b.setAutoCancel(true)
                        .setDefaults(Notification.DEFAULT_ALL)
                        .setWhen(System.currentTimeMillis())
                        .setSmallIcon(R.drawable.logo_wis2)
                        .setTicker(sender)
                        .setContentTitle("WISDirect")
                        .setContentText(title)
                        .setDefaults(Notification.DEFAULT_LIGHTS| Notification.DEFAULT_SOUND)
                        .setContentIntent(contentIntent)
                        .setContentInfo("Info");



                NotificationManager notificationManager = (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
                notificationManager.notify(1, b.build());
            }
        }
    }

    public static void setLocale(Context context, String lang) {
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        context.getApplicationContext().getResources().updateConfiguration(config, null);
    }

    public static Boolean showGPSDialog(final Context context) {


        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(R.string.gps_network_not_enabled);
            builder.setPositiveButton(R.string.open_location_settings, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    context.startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                }
            });
            builder.setNegativeButton(R.string.cancel, null);
            builder.create().show();
            return false;
        }else{
            return true;
        }
    }

    public static Bitmap retriveVideoFrameFromVideo(String videoPath) {
        Bitmap bitmap = null;
        MediaMetadataRetriever mediaMetadataRetriever = null;
        try
        {
            mediaMetadataRetriever = new MediaMetadataRetriever();
            //if (Build.VERSION.SDK_INT >= 14)
            //mediaMetadataRetriever.setDataSource(videoPath, new HashMap<String, String>());
            //else
            mediaMetadataRetriever.setDataSource(videoPath);
            //   mediaMetadataRetriever.setDataSource(videoPath);
            bitmap = mediaMetadataRetriever.getFrameAtTime();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            try {
                throw new Throwable(
                        "Exception in retriveVideoFrameFromVideo(String videoPath)"
                                + e.getMessage());
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }

        }
        finally
        {
            if (mediaMetadataRetriever != null)
            {
                mediaMetadataRetriever.release();
            }
        }
        return bitmap;
    }

    public static Location getLastBestLocation(Context mContext) {
        try {
            LocationManager mLocationManager = (LocationManager) mContext
                    .getSystemService(Context.LOCATION_SERVICE);
            Location locationGPS = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            Location locationNet = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

            long GPSLocationTime = 0;
            if (null != locationGPS) {
                GPSLocationTime = locationGPS.getTime();
            }

            long NetLocationTime = 0;

            if (null != locationNet) {
                NetLocationTime = locationNet.getTime();
            }

            if (0 < GPSLocationTime - NetLocationTime) {
                return locationGPS;
            } else {
                return locationNet;
            }
        } catch (Exception e) {
            return null;
        }
    }

    public static Bitmap adjustBitmapResolution(Bitmap bmp) {
        float aspect_ratio = ((float) bmp.getHeight()) / ((float) bmp.getWidth());
        Bitmap scaledBitmap = Bitmap.createBitmap(bmp, 0, 0,
                (int) ((GL10.GL_MAX_TEXTURE_SIZE * 0.5) * aspect_ratio),
                (int) (GL10.GL_MAX_TEXTURE_SIZE * 0.5));
        return scaledBitmap;
    }


    public static void getPrivateChannel(JSONArray privateArray){

        List<String> privatelist = new ArrayList<String>();
        for (int i=0; i<privateArray.length(); i++) {
            try {
                privatelist.add(privateArray.getString(i) );
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        ChatApi.getInstance().addPrivateChannel(privatelist);


    }

    public static void getPublicChannel(JSONArray publicArray){



        ArrayList <String>publicID = new ArrayList<>();

        ArrayList<List>memersID= new ArrayList<>();

        ArrayList<String> groupchannelMember = new ArrayList<>();

        for (int i = 0;i<publicArray.length();i++){

            JSONObject json = null;
            try {
                json = publicArray.getJSONObject(i);

                String publicId = json.getString("public_id");

                publicID.add(publicId);

                JSONArray channelGroupArray = json.getJSONArray("channel_group_members");



//                groupchannelMember.add(String.valueOf(channelGroupArray));

                List<String> memberlist = new ArrayList<String>();
                for (int m=0; m<channelGroupArray.length(); m++) {
                    memberlist.add( channelGroupArray.getString(m) );
                }

                memersID.add(memberlist);



            } catch (JSONException e) {
                e.printStackTrace();
            }


        }



        ChatApi.getInstance().addPublicChannels(memersID,publicID);


    }


    public static void doSubScribe(){

        ChatApi.getInstance().subscribe();


    }



    public static Boolean isStorageAvailable(){

        boolean mExternalStorageAvailable = false;
        boolean mExternalStorageWriteable = false;

        String state = Environment.getExternalStorageState();

        if (Environment.MEDIA_MOUNTED.equals(state)) {
            // We can read and write the media
            mExternalStorageAvailable = mExternalStorageWriteable = true;
        } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            // We can only read the media
            mExternalStorageAvailable = true;
            mExternalStorageWriteable = false;
        } else {
            // Something else is wrong. It may be one of many other states, but all we need
            //  to know is we can neither read nor write
            mExternalStorageAvailable = mExternalStorageWriteable = false;
        }

        return mExternalStorageAvailable;
    }

    public static void loadNotifs(final Context mContext) {
        //notifs = new ArrayList<>();

        //view.findViewById(R.id.loading).setVisibility(View.VISIBLE);

        JSONObject jsonBody = null;

        try {
            jsonBody = new JSONObject("{\"user_id\":\"" + Tools.getData(mContext, "idprofile") + "\"}");
        } catch (JSONException e) {
            e.printStackTrace();
        }


        Log.d("Jsonbody", String.valueOf(jsonBody));
        JsonObjectRequest reqNotifs = new JsonObjectRequest(mContext.getString(R.string.server_url) + mContext.getString(R.string.user_notifcations), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i("Notifications", String.valueOf(response));
                        try {
                            if (response.getBoolean("result")) {
                                JSONObject data = response.getJSONObject("data");



                                int chat = data.getInt("chat");
                                int chatCount = data.getInt("wis_single_chat_total_count");
                                int chatTotal = chat+chatCount;
                                new UserNotification().setWischatcount(chatTotal);

                                int groupchat = data.getInt("group_chat");
                                int groupCount = data.getInt("wis_group_chat_total_count");
                                int groupTotal = groupchat+groupCount;
                                new UserNotification().setWisgroupcount(groupTotal);

                                int phoneCount = data.getInt("wis_phone_totalcount");
                                new UserNotification().setWisphonecount(phoneCount);

//                                int actCount = data.getInt("wis_actuality");
//                                new UserNotification().setWisactualitycount(actCount);

                                int directCount = data.getInt("wis_direct");
                                new UserNotification().setWisdirectcount(directCount);

                                int totalbatch = data.getInt("total_badge_count");

                                int batchTotal = chatTotal+groupTotal;

                                new UserNotification().setBatchcount(totalbatch);







                                Log.i("Notification",String.valueOf(data));
//                                for (int i = 0; i < data.length(); i++) {
//                                    try {
//                                        JSONObject obj = data.getJSONObject(i);
//                                        notifs.add(new WISNotif(obj.getInt("id_pub"), obj.getString("title"), obj.getString("desc"), obj.getString("sent_at"), obj.getString("photo")));
//                                    } catch (Exception e) {
//                                    }
//                                }
//                                lvNotif.setAdapter(new NotifAdapter(getActivity(), notifs));
                            }


                        } catch (JSONException e) {


                            //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //view.findViewById(R.id.loading).setVisibility(View.GONE);
                //if (getActivity() != null)
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(mContext, "token"));
                headers.put("lang", Tools.getData(mContext, "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqNotifs);
    }

    public static JSONObject uploadImage(String sUrl, String sourceImageFile, String token,String userId,String message,String type,String receiver) {

        Log.d("parms url",sUrl);
        Log.d("parsm user",userId);
        Log.d("parsm file",sourceImageFile);

        try {
            File sourceFile = new File(sourceImageFile);

            System.out.println(sourceFile + " : " + sourceFile.exists());

            final MediaType MEDIA_TYPE = sourceImageFile.endsWith("mp3") ?
                    MediaType.parse("audio/mp3") : MediaType.parse("audio/mp3");


            RequestBody requestBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("file","AudioRecording.mp3", RequestBody.create(MEDIA_TYPE, sourceFile))
                    .addFormDataPart("user_id", userId)
                    .addFormDataPart("chat_type","Amis")
                    .addFormDataPart("message_type","voice_recording")
                    .addFormDataPart("receiver",receiver)
//
                    .build();

            Log.e("Jsonbody", String.valueOf(requestBody));

//
//


            Request request = new Request.Builder()
                    .url(sUrl)
                    .post(requestBody)
                    .addHeader("token",token)
                    .build();

            OkHttpClient client = new OkHttpClient();
            okhttp3.Response response = client.newCall(request).execute();
            Log.e("REES", String.valueOf(response));
            return new JSONObject(response.body().string());

        } catch (UnknownHostException | UnsupportedEncodingException e) {
            Log.e("error" ,e.getLocalizedMessage());
        } catch (Exception e) {
            Log.e("error","ERROR EXCEPTION");
            Log.e("error", String.valueOf(e));
        }
        return null;
    }

    public static JSONObject loadingDirect(String archiveId, String directId, String sUrl, String token, String userId, final Context context)
    {
        Log.d("parms url",sUrl);
        Log.d("parsm user",userId);

        Log.d("archiveId",archiveId);
        Log.d("directId",directId);
        try {
            RequestBody requestBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("user_id", userId)
                    .addFormDataPart("archiveId",archiveId)
                    .addFormDataPart("direct_id",directId)
                    .build();
            Request request = new Request.Builder()
                    .url(sUrl)
                    .post(requestBody)
                    .addHeader("token",token)
                    .build();



//            OkHttpClient client = new OkHttpClient();
//            client.newBuilder().readTimeout(50, TimeUnit.SECONDS);
//            client.newBuilder().writeTimeout(60, TimeUnit.SECONDS);
//            client.newBuilder().connectTimeout(50, TimeUnit.SECONDS);
//            okhttp3.Response response = client.newCall(request).execute();
//            Log.e("REES", String.valueOf(response));
//            return new JSONObject(response.body().string());

            OkHttpClient client = new OkHttpClient();
            okhttp3.Response response = client.newCall(request).execute();
            Log.e("REES", String.valueOf(response));
            return new JSONObject(response.body().string());

        } catch (UnknownHostException | UnsupportedEncodingException e) {
            Log.e("error" ,e.getLocalizedMessage());
        } catch (Exception e) {
            Log.e("error","ERROR Tools");
            Log.e("error", String.valueOf(e));

            return null;

        }
        return null;

    }
    public static JSONObject recordVideo(String sUrl,String sessionId,String token,String userId)
    {
        Log.d("parms url",sUrl);
        Log.d("parsm user",userId);
        try {
            RequestBody requestBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("user_id", userId)
                    .addFormDataPart("sessionId",sessionId)
                    .build();
            Request request = new Request.Builder()
                    .url(sUrl)
                    .post(requestBody)
                    .addHeader("token",token)
                    .build();

            OkHttpClient client = new OkHttpClient();
            okhttp3.Response response = client.newCall(request).execute();
            Log.e("REES", String.valueOf(response));
            return new JSONObject(response.body().string());

        } catch (UnknownHostException | UnsupportedEncodingException e) {
            Log.e("error" ,e.getLocalizedMessage());
        } catch (Exception e) {
            Log.e("error","ERROR EXCEPTION");
            Log.e("error", String.valueOf(e));

            return null;

        }
        return null;

    }

    public static JSONObject uploadVoiceToServer(String sUrl, String fPath, String token,String userId,String message,String type,String receiver) {

        Log.d("MUSIC parms url",sUrl);
        Log.d("parsm user",userId);
        Log.d("parsm file", String.valueOf(fPath));

        MultipartBody.Builder multipartBody = new MultipartBody.Builder();
        multipartBody.setType(MultipartBody.FORM);

        try {



                File sourceFile = new File(fPath);
                System.out.println(sourceFile + " : " + sourceFile.exists());
                final MediaType MEDIA_TYPE = fPath.endsWith("mp3") ?
                        MediaType.parse("audio/m4a") : MediaType.parse("audio/mp3");
                RequestBody requestBody = RequestBody.create(MEDIA_TYPE, sourceFile);
                multipartBody.addFormDataPart("file","Audio_record.mp3",requestBody);
                    multipartBody.addFormDataPart("user_id", userId);
                    multipartBody.addFormDataPart("chat_type",type);
                    multipartBody.addFormDataPart("message_type","voice_recording");
                    multipartBody.addFormDataPart("receiver",receiver);

            Request request = new Request.Builder()

                    .url(sUrl)
                    .post(requestBody)
                    .addHeader("token",token)
                    .build();


            OkHttpClient client = new OkHttpClient();
//            OkHttpClient.Builder b = new OkHttpClient.Builder();
//            b.readTimeout(200, TimeUnit.MILLISECONDS);
//            b.writeTimeout(600, TimeUnit.MILLISECONDS);
//            client.newBuilder().build();


            okhttp3.Response response = client.newCall(request).execute();
            Log.e("MUSIC REES=>", String.valueOf(response));
            Log.e("MUSIC RES BODY=>",response.body().string());
            return new JSONObject(response.body().string());

        } catch (UnknownHostException | UnsupportedEncodingException e) {
            Log.e("error" ,e.getLocalizedMessage());
        } catch (Exception e) {
            Log.e("error","ERROR EXCEPTION");
            Log.e("error", String.valueOf(e));
        }
        return null;
    }
    public static JSONObject uploadMusicToServer(List<String>selectImages,List<String> songIcons,String songName,String artist,String songDuration,String type,String songnames,String artists,String durations,String sUrl,String userId, String token) {

        Log.d("MUSIC parms url",sUrl);
        Log.d("parsm user",userId);
        Log.d("parsm file", String.valueOf(selectImages));

        MultipartBody.Builder multipartBody = new MultipartBody.Builder();
        multipartBody.setType(MultipartBody.FORM);

        try {

            for (int i=0;i<selectImages.size();i++){

                File sourceFile = new File(selectImages.get(i));
                System.out.println(sourceFile + " : " + sourceFile.exists());
                final MediaType MEDIA_TYPE = selectImages.get(i).endsWith("mp3") ?
                        MediaType.parse("audio/m4a") : MediaType.parse("audio/mp3");
                RequestBody requestBody = RequestBody.create(MEDIA_TYPE, sourceFile);
                multipartBody.addFormDataPart("file".concat(String.valueOf(i)),"MUSIC.mp3",requestBody);

            }

            for (int j=0;j<songIcons.size();j++){

                File sourceFile = new File(selectImages.get(j));
                System.out.println(sourceFile + " : " + sourceFile.exists());
                final MediaType MEDIA_TYPE = selectImages.get(j).endsWith("image/png") ?
                        MediaType.parse("image/png") : MediaType.parse("image/jpg");
                RequestBody requestBody = RequestBody.create(MEDIA_TYPE, sourceFile);
                multipartBody.addFormDataPart("audio_icons".concat(String.valueOf(j)),"audio.png",requestBody);
            }

//            builder.addTextBody("user_id", userId);
//            builder.addTextBody("album_name", songName);
//            builder.addTextBody("album_artists", artist);
//            builder.addTextBody("type", type);
//            builder.addTextBody("album_duration", songDuration);
//            builder.addTextBody("name",songnames);
//            builder.addTextBody("artists",artists);
//            builder.addTextBody("duration",durations);


            multipartBody.addFormDataPart("user_id", userId);
            multipartBody.addFormDataPart("album_name", songName);
            multipartBody.addFormDataPart("album_artists", artist);
            multipartBody.addFormDataPart("type", type);
            multipartBody.addFormDataPart("album_duration", songDuration);
            multipartBody.addFormDataPart("name",songnames);
            multipartBody.addFormDataPart("artists",artists);
            multipartBody.addFormDataPart("duration",durations);

            RequestBody requestBody = multipartBody.build();



            Log.e("Jsonbody", String.valueOf(requestBody));

//
//


            Request request = new Request.Builder()

                    .url(sUrl)
                    .post(requestBody)
                    .addHeader("token",token)
                    .build();


            OkHttpClient client = new OkHttpClient();
//            OkHttpClient.Builder b = new OkHttpClient.Builder();
//            b.readTimeout(200, TimeUnit.MILLISECONDS);
//            b.writeTimeout(600, TimeUnit.MILLISECONDS);
//            client.newBuilder().build();


            okhttp3.Response response = client.newCall(request).execute();
            Log.e("MUSIC REES=>", String.valueOf(response));
            Log.e("MUSIC RES BODY=>",response.body().string());
            return new JSONObject(response.body().string());

        } catch (UnknownHostException | UnsupportedEncodingException e) {
            Log.e("error" ,e.getLocalizedMessage());
        } catch (Exception e) {
            Log.e("error","ERROR EXCEPTION");
            Log.e("error", String.valueOf(e));
        }
        return null;
    }
    public static JSONObject createDirect(String sUrl, String fPath, String token,String userId,String name,String members,String lat,String lang) {

        Log.d("parms url",sUrl);
        Log.d("parsm user",userId);


        try {
            File sourceFile = new File(fPath);

            MediaType MEDIA_TYPE = null;

            System.out.println(sourceFile + " : " + sourceFile.exists());

            if(sourceFile.exists()){

                MEDIA_TYPE = fPath.endsWith("jpg") ?
                        MediaType.parse("image/jpg") : MediaType.parse("image/jpeg");
            }

            RequestBody requestBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("file","AudioRecording.jpg", RequestBody.create(MEDIA_TYPE, sourceFile))
                    .addFormDataPart("user_id", userId)
                    .addFormDataPart("latitude",lat)
                    .addFormDataPart("langitude",lang)
                    .addFormDataPart("title",name)
                    .addFormDataPart("member_id",members)


//            builder.addTextBody("user_id", userId);
//            builder.addTextBody("latitude", lat);
//            builder.addTextBody("langitude", lang);
//            builder.addTextBody("member_id", members);
//            builder.addTextBody("title", name);
//
                    .build();

//
//


            Request request = new Request.Builder()
                    .url(sUrl)
                    .post(requestBody)
                    .addHeader("token",token)
                    .build();

            OkHttpClient client = new OkHttpClient();
            okhttp3.Response response = client.newCall(request).execute();
            Log.e("REES", String.valueOf(response));
            return new JSONObject(response.body().string());

        } catch (UnknownHostException | UnsupportedEncodingException e) {
            Log.e("error" ,e.getLocalizedMessage());
        } catch (Exception e) {
            Log.e("error","ERROR EXCEPTION");
            Log.e("error", String.valueOf(e));

            return null;

        }
        return null;
    }

    public static void sendMessgaeAlertToServer(final Context context, String groupId,String type) {

        JSONObject jsonBody = new JSONObject();
//        findViewById(R.id.loading).setVisibility(View.VISIBLE);
        try {
            jsonBody.put("id_profil", Tools.getData(context, "idprofile"));
            jsonBody.put("id_profil_dest",groupId);
            jsonBody.put("message",Tools.getData(context, "name")+" "+"Missed a video call");
            jsonBody.put("type_message","text");
            jsonBody.put("chat_type", type);

            Log.i("json", jsonBody.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest reqSend = new JsonObjectRequest(context.getString(R.string.server_url) + context.getString(R.string.sendmsg_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Log.d("SETMESSAGE RES:", String.valueOf(response));
                        try {
                            if (response.getString("result").equals("true")) {

//
                            } else {
                                //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {

                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(context, "token"));
                headers.put("lang", Tools.getData(context, "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqSend);
    }

    public static void updateUserNotification(final Context context, String type, String objectId) {

        JSONObject jsonBody = null;

        try {
            jsonBody = new JSONObject();
            jsonBody.put("user_id",Tools.getData(context,"idprofile"));
            jsonBody.put("type",type);
            jsonBody.put("notification_id",objectId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.i("json body update noti", jsonBody.toString());

        JsonObjectRequest reqActu = new JsonObjectRequest(context.getString(R.string.server_url) + context.getString(R.string.updateNotification), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Log.i("update notification res", String.valueOf(response));

                        try {
                            if (response.getString("result").equals("true")) {




                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(context, "token"));
                headers.put("lang", Tools.getData(context, "lang_pr"));
                return headers;
            }
        };


        ApplicationController.getInstance().addToRequestQueue(reqActu);
    }

    public static JSONObject createSessionAndToken(String sUrl,String token,String userId) {

        Log.d("parms url",sUrl);
        Log.d("parsm user",userId);



        try {
            RequestBody requestBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("user_id", userId)

                    .build();

//
//


            Request request = new Request.Builder()
                    .url(sUrl)
                    .post(requestBody)
                    .addHeader("token",token)
                    .build();

            OkHttpClient client = new OkHttpClient();
            okhttp3.Response response = client.newCall(request).execute();
            Log.e("REES", String.valueOf(response));
            return new JSONObject(response.body().string());

        } catch (UnknownHostException | UnsupportedEncodingException e) {
            Log.e("error" ,e.getLocalizedMessage());
        } catch (Exception e) {
            Log.e("error","ERROR EXCEPTION");
            Log.e("error", String.valueOf(e));
        }
        return null;
    }


    public void showLoader(Context context, String message){

        pd = new ProgressDialog(context);
        pd.setMessage(message);
        pd.show();
    }

    public void dismissLoader(){

        if(pd!=null){

            pd.dismiss();
        }
    }



}
