package com.oec.wis.tools;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.oec.wis.ApplicationController;
import com.oec.wis.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LocationReceiver extends BroadcastReceiver {
    public static double LATTITUDE, LONGITUDE;
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent != null) {
            Bundle bundle = intent.getExtras();

            Location location = (Location) bundle.get(LocationManager.KEY_LOCATION_CHANGED);
            if (location != null) {
                LATTITUDE = location.getLatitude();
                LONGITUDE = location.getLongitude();
                updateLocation(context, location.getLatitude(), location.getLongitude());
            }
        }
    }

    private void updateLocation(final Context context, double lattitude, double longitude) {
        JSONObject jsonBody = null;

        try {
            jsonBody = new JSONObject("{\"id_profil\":" + Tools.getData(context, "idprofile") + ", \"pos_x\":\"" + lattitude + "\",\"pos_y\":\"" + longitude + "\"}");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqLoca = new JsonObjectRequest(context.getString(R.string.server_url) + context.getString(R.string.setcurrlo_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        String x = "";
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String x = "";
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(context, "token"));
                headers.put("lang", Tools.getData(context, "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqLoca);
    }
}
