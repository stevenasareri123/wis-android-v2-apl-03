package com.oec.wis.wisdirect;

import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.InflateException;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.oec.wis.ApplicationController;
import com.oec.wis.R;
import com.oec.wis.Sample;
import com.oec.wis.adapters.CmtAdapter;
import com.oec.wis.adapters.Popupadapter;
import com.oec.wis.chatApi.ChatApi;
import com.oec.wis.entities.Popupelements;
import com.oec.wis.entities.UserNotification;
import com.oec.wis.entities.WISCmt;
import com.oec.wis.tools.Tools;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnItemClickListener;
import com.rockerhieu.emojicon.EmojiconEditText;
import com.rockerhieu.emojicon.EmojiconGridFragment;
import com.rockerhieu.emojicon.EmojiconsFragment;
import com.rockerhieu.emojicon.emoji.Emojicon;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by asareri08 on 20/02/17.
 */

public class WisDirectPublisherView extends Fragment implements EmojiconGridFragment.OnEmojiconClickedListener, EmojiconsFragment.OnEmojiconBackspaceClickedListener {

    View view;

    JSONObject directData;
    ListView lvCmt;
    CmtAdapter adapter;
    List<WISCmt> cmts;
    ImageView sendBtn;
    EmojiconEditText etCmt;

    ProgressDialog dialog;

    ArrayList<Popupelements> popupList;

    Popupadapter simpleAdapter;

    private WisDirect mWisDirect;

    int idAct;

    Boolean doShare = false;
    Boolean disConnect = false;





    public String getArchiveId() {
        return archiveId;
    }

    public void setArchiveId(String archiveId) {
        this.archiveId = archiveId;
    }

    String archiveId;

    public String getDirectId() {
        return directId;
    }

    public void setDirectId(String directId) {
        this.directId = directId;
    }

    String directId;

    TextView tvNLike, tvNView, comment_count,dislike_count, sharecmnt;
    ImageView   ivView, ivCmt,ivShare;
    Button ivLike,dislike;

    List<String> stringList = new ArrayList<>();

    boolean isLike=false;
    boolean isDisLike=false;

    PublisherListener publisherListener;



//    create adapere
//    create list

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);

        }
        try {
            Tools.setLocale(getActivity(), Tools.getData(getActivity(), "lang_pr"));
            view = inflater.inflate(R.layout.direct_publisher_view, container, false);

            setInitControls();
            setListeners();



            cmts = new ArrayList<>();


        } catch (InflateException e) {
        }

        return view;

    }

    public void setPublisherListener(PublisherListener listener)
    {
        this.publisherListener = listener;
    }



    public View getBroadCasterView(){

        return view;
    }

    public void setInitControls()
    {

        lvCmt = (ListView) view.findViewById(R.id.directComment);
        etCmt = (EmojiconEditText) view.findViewById(R.id.etCmt);
        etCmt.setUseSystemDefault(true);
        setEmojiconFragment(true);
        sendBtn = (ImageView)view.findViewById(R.id.ivSend);
        tvNLike = (TextView) view.findViewById(R.id.tvNLike);
        tvNView = (TextView) view.findViewById(R.id.tvNView);
        comment_count = (TextView) view.findViewById(R.id.comment_count);
        dislike_count=(TextView) view.findViewById(R.id.dislike_count);
        sharecmnt = (TextView) view.findViewById(R.id.sharecmnt);
        ivLike = (Button) view.findViewById(R.id.ivLike);
        dislike = (Button) view.findViewById(R.id.dislike);
        ivView = (ImageView) view.findViewById(R.id.ivView);
        ivCmt = (ImageView) view.findViewById(R.id.ivCmt);
        ivShare = (ImageView) view.findViewById(R.id.ivShare);
        ivShare.setEnabled(false);

        ivLike.setEnabled(true);
        dislike.setEnabled(true);
        sendBtn.setEnabled(true);

        etCmt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_SEND) {
                    sendCmt();
                }
                return true;
            }
        });
        etCmt.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (etCmt.getRight() - etCmt.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        if (view.findViewById(R.id.emojicons).getVisibility() == View.VISIBLE)
                            view.findViewById(R.id.emojicons).setVisibility(View.GONE);
                        else
                            view.findViewById(R.id.emojicons).setVisibility(View.VISIBLE);
                        return true;
                    }
                }
                return false;
            }
        });

//        etCmt.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                final int DRAWABLE_RIGHT = 2;
//                if (event.getAction() == MotionEvent.ACTION_UP) {
//                    if (view.findViewById(R.id.emojicons).getVisibility() == View.VISIBLE)
//                        view.findViewById(R.id.emojicons).setVisibility(View.GONE);
//                    else
//                        view.findViewById(R.id.emojicons).setVisibility(View.VISIBLE);
//                    return true;
////                    if (event.getRawX() >= (etCmt.getRight() - etCmt.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
////                        if (view.findViewById(R.id.emojicons).getVisibility() == View.VISIBLE)
////                            view.findViewById(R.id.emojicons).setVisibility(View.GONE);
////                        else
////                            view.findViewById(R.id.emojicons).setVisibility(View.VISIBLE);
////                        return true;
////                    }
//                }
//                else {
//                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
//                        if (view.findViewById(R.id.emojicons).getVisibility() == View.VISIBLE)
//                            view.findViewById(R.id.emojicons).setVisibility(View.GONE);
//                        else
//                            view.findViewById(R.id.emojicons).setVisibility(View.VISIBLE);
//                        return true;
//                    }
//                    return false;
//                }
////                return false;
//            }
//        });



    }
    private void setEmojiconFragment(boolean useSystemDefault) {
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.emojicons, EmojiconsFragment.newInstance(useSystemDefault))
                .commit();
    }





    public void setListeners(){

        ChatApi.getInstance().setPublisherContext(getActivity());
        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendCmt();
            }
        });

        ivLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ivLike.isSelected()){

                    ivLike.setSelected(false);

                    ivLike.setBackgroundResource(R.drawable.like);



                }
                else{

                    ivLike.setSelected(true);
                    ivLike.setBackgroundResource(R.drawable.ilike);

                    if(dislike.isSelected())
                    {
                        dislike.setSelected(false);
                        dislike.setBackgroundResource(R.drawable.dislike_unselect);

                    }

                }

                setLike(ivLike.isSelected());

            }
        });
        dislike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(dislike.isSelected()){

                    dislike.setSelected(false);

                    dislike.setBackgroundResource(R.drawable.dislike_unselect);

                }
                else{

                    dislike.setSelected(true);
                    dislike.setBackgroundResource(R.drawable.dislike_select);

                    if(ivLike.isSelected())
                    {
                        ivLike.setSelected(false);
                        ivLike.setBackgroundResource(R.drawable.like);
                    }
                }

                setDisLike(dislike.isSelected());

            }
        });


        ivShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!doShare) {

                    if (getArchiveId() != null && getDirectId() !=null) {


                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                            Log.i("Calling", "executeOnExecutor()");
                            new LoadDirectData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, getArchiveId(), getDirectId());
                        } else {
                            Log.i("Calling", "execute()");
                            new LoadDirectData().execute(getArchiveId(), getDirectId());
                        }

                    } else {

                        Toast.makeText(getActivity(), "You Cannot share this video", Toast.LENGTH_LONG).show();
                    }
                }
                else{

                    if(directData!=null){
                        showShareView(directData);
                    }
                }
            }
        });
    }

    private void setLike(Boolean selectedStatus) {
        JSONObject jsonBody = null;
        try {
            jsonBody = new JSONObject("{\"id_profil\":\"" + Tools.getData(getActivity(), "idprofile") + "\", \"id_act\":\"" + getActivityId() + "\", \"jaime\":\"" + selectedStatus + "\"}");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("StatusBody", String.valueOf(jsonBody));
        JsonObjectRequest reqLike = new JsonObjectRequest(getActivity().getString(R.string.server_url) + getActivity().getString(R.string.like_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Log.e("StatusDATA", String.valueOf(response));
                        try {
                            if (response.getString("result").equals("true")) {

                                publisherListener.onControlsClicked();


                                //loadDirectStatus();
                            }
                        } catch (JSONException e) {
                            //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                            try {
//                                actuList.get(p).setiLike(false);
//                                actuList.get(p).setnLike(actuList.get(p).getnLike() - 1);
                            } catch (Exception e2) {

                            }
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getActivity(), "token"));
                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqLike);
    }

//    public View getOverView()
//    {
//
//        LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        View view = inflater.inflate(R.layout.display_over_view, null);
//
//        return view;
//    }
    public void loadDirectStatus()
    {

        JSONObject jsonBody = null;
        try {
            jsonBody = new JSONObject("{\"user_id\":\"" + Tools.getData(getActivity(), "idprofile") + "\", \"idAct\":\"" + getActivityId() + "\"}");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e("StatusBodyDirect", String.valueOf(jsonBody));

        JsonObjectRequest reqLike = new JsonObjectRequest(getActivity().getString(R.string.server_url) + getActivity().getString(R.string.get_direct_status), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(final JSONObject response) {

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {


                                try {
                                    JSONObject data = response.getJSONObject("data");
                                    Log.e("DirectStatus", String.valueOf(data));
                                    tvNLike.setText(String.valueOf(data.getInt("like_count")));
                                    dislike_count.setText(String.valueOf(data.getInt("dislike_count")));

                                    tvNView.setText(String.valueOf(data.getInt("views_count")));

                                    comment_count.setText(String.valueOf(data.getInt("comments_count")));

                                    if(data.getInt("views_count")>0)
                                    {
                                        Log.e("Calling","updateViewCount()");
                                        publisherListener.updateViewCount(String.valueOf(data.getInt("views_count")));
                                    }


                                } catch (JSONException e) {
                                    //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                                    try {
//                                actuList.get(p).setiLike(false);
//                                actuList.get(p).setnLike(actuList.get(p).getnLike() - 1);
                                    } catch (Exception e2) {

                                    }
                                }
                            }
                        });


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getActivity(), "token"));
                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqLike);
    }




    public void deleteDirect(String directId)
    {

        JSONObject jsonBody = new JSONObject();

        try {
            jsonBody.put("user_id",Tools.getData(getActivity(), "idprofile"));
            jsonBody.put("idAct",getActivityId());
            jsonBody.put("direct_id",directId);
            Log.i("jsonBody",String.valueOf(jsonBody));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e("BodyDirect", String.valueOf(jsonBody));

        JsonObjectRequest reqLike = new JsonObjectRequest(getActivity().getString(R.string.server_url) + getActivity().getString(R.string.delete_direct), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("DeleteResponse", String.valueOf(response));


                        try {
                            if(response.getString("result").equals("true"))
                            {
                                getActivity().finish();


                                Toast.makeText(getContext(),"direct deleted successfully", Toast.LENGTH_SHORT).show();

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }



                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getActivity(), "token"));
                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqLike);
    }

    public void shareActuality()
    {

        JSONObject jsonBody = null;
        try {
            jsonBody = new JSONObject("{\"user_id\":\"" + Tools.getData(getActivity(), "idprofile") + "\", \"idAct\":\"" + getActivityId() + "\"}");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e("ActuBody", String.valueOf(jsonBody));

//        http://ServerURL/api/activity/shareDirectToAct

        JsonObjectRequest reqLike = new JsonObjectRequest(getActivity().getString(R.string.server_url) + getActivity().getString(R.string.share_direct_actuality), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("ShareResponse", String.valueOf(response));
                        try {
                            if(response.getString("result").equals("true"))
                            {
                                //getActivity().finish();

                                Toast.makeText(getContext(),"successfully shared", Toast.LENGTH_SHORT).show();;

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getActivity(), "token"));
                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqLike);
    }

    private void setDisLike(Boolean selectedStatus) {

        JSONObject jsonBody = null;
        try {
            jsonBody = new JSONObject("{\"id_profil\":\"" + Tools.getData(getActivity(), "idprofile") + "\", \"id_act\":\"" + getActivityId() + "\", \"jaime\":\"" + selectedStatus + "\"}");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("dislike statu", jsonBody.toString());
        JsonObjectRequest reqLike = new JsonObjectRequest(getActivity().getString(R.string.server_url) + getActivity().getString(R.string.dislike), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("StatusDATA", String.valueOf(response));
                        try {
                            if (response.getString("result").equals("true")) {

                                publisherListener.onControlsClicked();

                                //loadDirectStatus();
                            }
                        } catch (JSONException e) {
                            //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                            try {
//                                actuList.get(p).setiLike(false);
//                                actuList.get(p).setnLike(actuList.get(p).getnLike() - 1);
                            } catch (Exception e2) {

                            }
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getActivity(), "token"));
                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqLike);
    }


    private void sendCmt() {
        if (!TextUtils.isEmpty(etCmt.getText().toString())) {
            String msg = etCmt.getText().toString();
            publisherListener.postComments(msg);
            etCmt.setText(null);
            Tools.hideKeyboard(getActivity());
        }
    }


    public void hideKeyboard(){



        InputMethodManager inputManager = (InputMethodManager)
                getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

        inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
    }


    public void addPublisherView(View publisher){

        RelativeLayout broadCasterView = (RelativeLayout)view.findViewById(R.id.broadCasterView);

        broadCasterView.addView(publisher);

    }

    public  void  setActivityId(int idAct)
    {
        this.idAct=idAct;
    }

    public int getActivityId()
    {
        return idAct;
    }


    public void refereshComments(WISCmt comments){

        Log.e("RefereshComents",String.valueOf(comments));

        cmts.add(comments);

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {

                adapter = new CmtAdapter(getActivity(),cmts,null);
                lvCmt.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }
        });



    }

    public void showShareView(final JSONObject directData1){


        Log.e("ShareDirectData", String.valueOf(directData1));

        popupList = new ArrayList<>();
        popupList.add(new Popupelements(R.drawable.wis_photo_picker,getString(R.string.video)));
        popupList.add(new Popupelements(R.drawable.share_profile, getString(R.string.actuality_direct)));
        popupList.add(new Popupelements(R.drawable.copy_chat, getString(R.string.chat)));
        popupList.add(new Popupelements(R.drawable.trash, getString(R.string.Remove)));
        popupList.add(new Popupelements(R.drawable.copy_link, getString(R.string.copy_link)));


        simpleAdapter = new Popupadapter(getActivity(), false, popupList);

        DialogPlus dialog = DialogPlus.newDialog(getActivity())
                .setAdapter(simpleAdapter)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View view, int position) {


                        if(position == 0){
                            dialog.dismiss();

                            Toast.makeText(getContext(),"Video Saved", Toast.LENGTH_SHORT).show();;


                            //showWriteStatusPopup(view);
                        }
                        else if(position ==1){
                            dialog.dismiss();

                            shareActuality();
                            //showWriteStatusPopup(view);

                        }
                        else if(position ==2){
                            dialog.dismiss();
                            try {
                                JSONObject direct =  directData1.getJSONObject("direct");
                                UserNotification.setChatSelected(true);
                                UserNotification.setShareVideo(true);
                                UserNotification.setVideoId(direct.getString("archieveId"));
                                UserNotification.setPublisherid(getDirectId());

                                Intent intent = new Intent(getActivity(), Sample.class);
                                getActivity().startActivity(intent);


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }



                        }
                        else if(position ==3){
                            dialog.dismiss();

                            try {

                                JSONObject direct =  directData1.getJSONObject("direct");

                                if(Tools.getData(getActivity(),"idprofile").equals(direct.getString("created_by")))

                                {

                                    deleteDirect(getDirectId());

                                }else{

                                    Toast.makeText(getActivity(),"You do not perform this action",Toast.LENGTH_LONG).show();;
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }
                        else if(position ==4){


                            dialog.dismiss();
                            try {

                                String link = getActivity().getString(R.string.server_url3).concat(directData1.getString("url"));

                                Log.e("CopiedLink",link);


                                ClipboardManager clipboard = (ClipboardManager)getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
                                ClipData clip = ClipData.newPlainText("Copied", link);
                                clipboard.setPrimaryClip(clip);

                                Toast.makeText(getContext(),"Copied link",Toast.LENGTH_LONG).show();;


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }

                    }
                })
                .setExpanded(false)  // This will enable the expand feature, (similar to android L share dialog)
                .create();
        dialog.show();
    }
    public class LoadDirectData extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(getActivity());
            dialog.setMessage("Please wait...");
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if(dialog!=null){
                dialog.dismiss();
            }

            try {
                if(result!=null) {

                    if (!result.equals("")) {
                        try {
                            JSONObject response = new JSONObject(result);

                            Log.e("LoadingDirectData", String.valueOf(response));

                            JSONObject data = response.getJSONObject("data");
                            Log.e("DirectData", String.valueOf(data));

                            JSONObject direct = data.getJSONObject("direct");
                            if (direct.getString("status").equals("canceled")) {
                                doShare = false;
//                            setDisConnect(false);
                                disConnect = false;
                                Toast.makeText(getContext(), "Video is in progress ,Try again", Toast.LENGTH_SHORT).show();
                            } else {
                                doShare = true;
                                // setDisConnect(true);
                                disConnect = true;

                                directData = data;
                                showShareView(data);
                            }

                        } catch (JSONException e) {

                        }

                    } else {

                    }
                }
                else {
                    Toast.makeText(getContext(),"Connection TimeOut ,Please Try Again",Toast.LENGTH_LONG).show();
                }
            }
            catch (NullPointerException e)
            {

            }
        }


        @Override
        protected String doInBackground(String... urls)
        {
            return Tools.loadingDirectData(urls[0],urls[1],getActivity().getString(R.string.server_url) + getActivity().getString(R.string.get_direct_data),Tools.getData(getActivity(), "token"), Tools.getData(getActivity(), "idprofile"));
        }
    }



    public void showLoader(){

        dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Please wait...");
        dialog.show();
    }

    public void dismissLoader(){

        if(dialog!=null){

            dialog.dismiss();
        }
    }

    @Override
    public void onEmojiconClicked(Emojicon emojicon) {
        EmojiconsFragment.input(etCmt, emojicon);
        view.findViewById(R.id.emojicons).setVisibility(View.GONE);
    }

    @Override
    public void onEmojiconBackspaceClicked(View v) {
        EmojiconsFragment.backspace(etCmt);
    }



    public void emojiClcked(Emojicon emojicon){

        EmojiconsFragment.input(etCmt, emojicon);
        view.findViewById(R.id.emojicons).setVisibility(View.GONE);
    }

    public void emojiBackspaceclicked(View v){

        EmojiconsFragment.backspace(etCmt);
    }


    public void setControls(boolean status){


        ivLike.setEnabled(status);
        dislike.setEnabled(status);
        sendBtn.setEnabled(status);

    }

    public void setshareBtnStatus(boolean status){

        ivShare.setEnabled(status);
    }

}