package com.oec.wis.dialogs;

import android.app.ProgressDialog;
import android.content.ContentUris;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.oec.wis.ApplicationController;
import com.oec.wis.R;
import com.oec.wis.adapters.ContactAdapter;
import com.oec.wis.entities.WISContact;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

public class MyContact extends AppCompatActivity {
    List<WISContact> contacts;
    ListView lvContact;
    TextView headerTitleTV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_contact);
        initControls();
        setListeners();
        new DoLoadContacts().execute();
    }

    private void setListeners() {
        findViewById(R.id.close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void initControls() {
        lvContact = (ListView) findViewById(R.id.lvContacts);
        headerTitleTV=(TextView)findViewById(R.id.headerTitleTV);

        Typeface font=Typeface.createFromAsset(this.getAssets(),"fonts/Harmattan-R.ttf");
        headerTitleTV.setTypeface(font);
    }

    private void loadContacts() {
        contacts = new ArrayList<>();
//        Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
        Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");

        if (phones != null) {

            while (phones.moveToNext()) {


                String contactID = phones.getString(phones.getColumnIndex(ContactsContract.Contacts._ID));
                String name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                String number = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                String email = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                contacts.add(new WISContact(name, number, retrieveContactPhoto(contactID), email));
            }
            phones.close();
        }

    }

    private Bitmap retrieveContactPhoto(String contactID) {
        Bitmap image = null;
        try {
            InputStream inputStream = ContactsContract.Contacts.openContactPhotoInputStream(getContentResolver(),
                    ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, new Long(contactID)));

            if (inputStream != null) {
                image = BitmapFactory.decodeStream(inputStream);
            }

            if (inputStream != null)
                inputStream.close();

        } catch (IOException e) {

        }

        return image;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ApplicationController.getInstance().cancelPendingRequests(ApplicationController.TAG);
    }

    private class DoLoadContacts extends AsyncTask<String, Void, String> {
        final ProgressDialog pDialog = ProgressDialog.show(MyContact.this, "", getString(R.string.pending_operation), true, true);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog.show();
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            pDialog.dismiss();
            lvContact.setAdapter(new ContactAdapter(MyContact.this, contacts));
        }

        @Override
        protected String doInBackground(String... urls) {
            loadContacts();
            return "";
        }
    }

}
