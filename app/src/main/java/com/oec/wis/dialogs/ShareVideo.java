package com.oec.wis.dialogs;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.oec.wis.ApplicationController;
import com.oec.wis.R;
import com.oec.wis.tools.Tools;
import com.rockerhieu.emojicon.EmojiconEditText;
import com.rockerhieu.emojicon.EmojiconGridFragment;
import com.rockerhieu.emojicon.EmojiconsFragment;
import com.rockerhieu.emojicon.emoji.Emojicon;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ShareVideo extends AppCompatActivity implements EmojiconGridFragment.OnEmojiconClickedListener, EmojiconsFragment.OnEmojiconBackspaceClickedListener {
    private FrameLayout emojicons;
    private EmojiconEditText etCmt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_video);
        emojicons = (FrameLayout) findViewById(R.id.emojicons);
        etCmt = (EmojiconEditText) findViewById(R.id.etDesc);
        etCmt.setUseSystemDefault(true);
        setEmojiconFragment(false);
        final ImageView ivVideo = (ImageView) findViewById(R.id.ivVideo);
        Button bShare = (Button) findViewById(R.id.bShare);
        final EditText etCmt = (EditText) findViewById(R.id.etDesc);
        ApplicationController.getInstance().getImageLoader().get(ShareVideo.this.getString(R.string.server_url4) + getIntent().getExtras().getInt("thumb"), new ImageLoader.ImageListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ivVideo.setImageResource(R.drawable.empty);
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    ivVideo.setImageBitmap(response.getBitmap());
                } else {
                    ivVideo.setImageResource(R.drawable.empty);
                }
            }
        });
        bShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareVideo(getIntent().getExtras().getInt("id"), etCmt.getText().toString());
            }
        });

        etCmt.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (etCmt.getRight() - etCmt.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        if (findViewById(R.id.emojicons).getVisibility() == View.VISIBLE)
                            findViewById(R.id.emojicons).setVisibility(View.GONE);
                        else
                            findViewById(R.id.emojicons).setVisibility(View.VISIBLE);
                        return true;
                    }
                }
                return false;
            }
        });

        findViewById(R.id.close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void shareVideo(int idVideo, String cmt) {
        findViewById(R.id.loading).setVisibility(View.VISIBLE);
        JSONObject jsonBody = new JSONObject();

        try {
            jsonBody.put("id_profil", Tools.getData(ShareVideo.this, "idprofile"));
            jsonBody.put("id_video", String.valueOf(idVideo));
            jsonBody.put("commentaire", cmt);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqShare = new JsonObjectRequest(ShareVideo.this.getString(R.string.server_url) + ShareVideo.this.getString(R.string.sharevideo_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {
                                finish();
                                Toast.makeText(ShareVideo.this, ShareVideo.this.getString(R.string.msg_video_shared), Toast.LENGTH_SHORT).show();
                            } else {
                                //Toast.makeText(ShareVideo.this, ShareVideo.this.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                            }
                            findViewById(R.id.loading).setVisibility(View.GONE);
                        } catch (JSONException e) {
                            //Toast.makeText(ShareVideo.this, ShareVideo.this.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                            findViewById(R.id.loading).setVisibility(View.VISIBLE);
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //if (ShareVideo.this != null)
                //Toast.makeText(ShareVideo.this, ShareVideo.this.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                findViewById(R.id.loading).setVisibility(View.VISIBLE);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("lang", Tools.getData(ShareVideo.this, "lang_pr"));
                headers.put("token", Tools.getData(ShareVideo.this, "token"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqShare);
    }

    private void setEmojiconFragment(boolean useSystemDefault) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.emojicons, EmojiconsFragment.newInstance(useSystemDefault))
                .commit();
    }

    @Override
    public void onEmojiconClicked(Emojicon emojicon) {
        EmojiconsFragment.input(etCmt, emojicon);
        emojicons.setVisibility(View.GONE);
    }

    @Override
    public void onEmojiconBackspaceClicked(View v) {
        EmojiconsFragment.backspace(etCmt);
    }
}
