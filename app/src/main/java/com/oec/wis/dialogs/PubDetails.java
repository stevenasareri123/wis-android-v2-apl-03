//package com.oec.wis.dialogs;
//
//import android.content.Intent;
//import android.graphics.Typeface;
//import android.net.Uri;
//import android.os.Bundle;
//import android.support.v7.app.AppCompatActivity;
//import android.view.View;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import com.android.volley.AuthFailureError;
//import com.android.volley.Response;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.ImageLoader;
//import com.android.volley.toolbox.JsonObjectRequest;
//import com.oec.wis.ApplicationController;
//import com.oec.wis.R;
//import com.oec.wis.entities.WISAds;
//import com.oec.wis.tools.Tools;
//
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.util.HashMap;
//import java.util.Map;
//
//public class PubDetails extends AppCompatActivity {
//    public static WISAds ads;
//    ImageView ivPhoto;
//    TextView tvTitle, tvDesc, tvStatus,tvVue,tvNotif,tvStats,headerTitleTV;
//    int id, idAct;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        Tools.setLocale(this, Tools.getData(this, "lang_pr"));
//        setContentView(R.layout.activity_pub_details);
//        loadControls();
//        setListener();
//        reqVueCount();
//    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//        loadData();
//    }
//
//    private void loadControls() {
//        ivPhoto = (ImageView) findViewById(R.id.ivPhoto);
//        tvTitle = (TextView) findViewById(R.id.tvTitle);
//        tvDesc = (TextView) findViewById(R.id.tvDesc);
//        tvStatus = (TextView) findViewById(R.id.tvStatus1);
//        tvVue=(TextView)findViewById(R.id.tvVue);
//        tvNotif=(TextView)findViewById(R.id.tvNotif);
//        tvStats=(TextView)findViewById(R.id.tvStats);
//        headerTitleTV=(TextView)findViewById(R.id.headerTitleTV);
//        Bundle bundle=getIntent().getExtras();
//        if (bundle != null) {
//            id = bundle.getInt("id");
//            idAct = bundle.getInt("idact");
//
//            System.out.println("id and idact" +id  +idAct);
//
//        }
//
//        Typeface font= Typeface.createFromAsset(this.getAssets(), "fonts/Harmattan-Regular.ttf");
//        tvTitle.setTypeface(font);
//        tvDesc.setTypeface(font);
//        tvStatus.setTypeface(font);
//        tvNotif.setTypeface(font);
//        tvVue.setTypeface(font);
//        tvStats.setTypeface(font);
//        headerTitleTV.setTypeface(font);
//
//    }
//
//    private void loadData() {
//        if (ads == null) {
//            findViewById(R.id.loading).setVisibility(View.VISIBLE);
//            findViewById(R.id.scrollView2).setVisibility(View.INVISIBLE);
//        }
//        JSONObject jsonBody = null;
//
//        try {
//            jsonBody = new JSONObject("{\"id_profil\":\"" + Tools.getData(this, "idprofile") + "\",\"id_pub\":\"" + id + "\"}");
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        JsonObjectRequest reqActu = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.getpub_meth), jsonBody,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        try {
//                            if (response.getBoolean("result")) {
//
//                                System.out.println("response====>" +response);
////                                response====>{"Message":"success","data":{"titre":null,"created_by":null,"description":null,"id_pub":"0","distance":null,"cible":null,"created_at":null,"last_update":null,"touriste":null,"sexe":null,"etat":null,"startTime":null,"endTime":null,"banner":null,"type_obj":null,"wis_music":null,"lat":null,"lan":null,"country":null,"duree":null,"date_lancement":null},"result":true}
//
//                                JSONObject obj = response.getJSONObject("data");
//                                String d = "0";
//                                d = obj.getString("distance");
//
//                                boolean t = false;
//                                try {
//                                    if (obj.getInt("touriste") == 1)
//                                        t = true;
//                                } catch (Exception e) {
//                                }
//
//                                boolean b = false;
//                                try {
//                                    if (obj.getInt("banner") == 1)
//                                        b = true;
//                                } catch (Exception e) {
//                                }
//                                String photo = "";
//                                try {
//                                    photo = obj.getString("photo");
//                                } catch (Exception e) {
//                                }
//                                String video_thumb = "";
//                                try {
//                                    video_thumb = obj.getString("photo_video");
//                                } catch (Exception e) {
//                                }
//                                ads = new WISAds(obj.getInt("id_pub"), obj.getString("titre"), obj.getString("description"), obj.getInt("etat"), obj.getInt("duree"), d, obj.getString("cible"), obj.getString("created_at"), obj.getString("date_lancement"), photo, obj.getString("type_obj"), t, b, obj.getString("startTime"), obj.getString("endTime"), video_thumb);
//                                showData(ads);
//                            }
//                            findViewById(R.id.loading).setVisibility(View.GONE);
//
//                        } catch (JSONException e) {
//                            findViewById(R.id.loading).setVisibility(View.GONE);
//                            //Toast.makeText(PubDetails.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
//                        }
//
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                findViewById(R.id.loading).setVisibility(View.GONE);
//                //Toast.makeText(PubDetails.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<>();
//                headers.put("token", Tools.getData(PubDetails.this, "token"));
//                headers.put("lang", Tools.getData(PubDetails.this, "lang_pr"));
//                return headers;
//            }
//        };
//
//        ApplicationController.getInstance().addToRequestQueue(reqActu);
//    }
//
//    private void reqVueCount() {
//        findViewById(R.id.loading).setVisibility(View.VISIBLE);
//        findViewById(R.id.scrollView2).setVisibility(View.INVISIBLE);
//        JSONObject jsonBody = null;
//
//        try {
//            jsonBody = new JSONObject("{\"id_profil\":\"" + Tools.getData(this, "idprofile") + "\",\"id_act\":\"" + idAct + "\"}");
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        JsonObjectRequest reqActu = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.pubvuecount_meth), jsonBody,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        try {
//                            if (response.getBoolean("result")) {
//                                int view = response.getJSONObject("data").getInt("nbr_vue");
//                                int notif = response.getJSONObject("data").getInt("nbr_pub");
//                                ((TextView) findViewById(R.id.tvVue)).setText(view + " " + getString(R.string.views));
//                                ((TextView) findViewById(R.id.tvNotif)).setText(notif + " " + getString(R.string.notifs));
//                            }
//                            findViewById(R.id.loading).setVisibility(View.GONE);
//
//                        } catch (JSONException e) {
//                            findViewById(R.id.loading).setVisibility(View.GONE);
//                            //Toast.makeText(PubDetails.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
//                        }
//
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                findViewById(R.id.loading).setVisibility(View.GONE);
//                //Toast.makeText(PubDetails.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<>();
//                headers.put("token", Tools.getData(PubDetails.this, "token"));
//                headers.put("lang", Tools.getData(PubDetails.this, "lang_pr"));
//                return headers;
//            }
//        };
//
//        ApplicationController.getInstance().addToRequestQueue(reqActu);
//    }
//
//    private void setListener() {
//        findViewById(R.id.close).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                finish();
//            }
//        });
//        findViewById(R.id.bEdit).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i = new Intent(getApplicationContext(), NewAds.class);
//                i.putExtra("edit", "edit");
//                startActivity(i);
//            }
//        });
//        findViewById(R.id.tvStats).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i = new Intent(getApplicationContext(), PubStats.class);
//                i.putExtra("id", id);
//                i.putExtra("date", ads.getcDate());
//                i.putExtra("km", String.valueOf(ads.getVisib()));
//                i.putExtra("duration", String.valueOf(ads.getDuration()));
//                startActivity(i);
//            }
//        });
//        findViewById(R.id.ivVideo).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Uri uri = Uri.parse(getString(R.string.server_url3) + ads.getPhoto());
//                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
//                intent.setDataAndType(uri, "video/mp4");
//                startActivity(intent);
//            }
//        });
//    }
//
//    private void showData(WISAds ads) {
//
//        tvTitle.setText(ads.getTitle());
//        tvDesc.setText(ads.getDesc());
//        if (ads.getTypeObj().equals("video")) {
//            ApplicationController.getInstance().getImageLoader().get(getString(R.string.server_url4) + ads.getThumb(), new ImageLoader.ImageListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    ivPhoto.setImageResource(R.drawable.empty);
//                }
//
//                @Override
//                public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
//                    if (response.getBitmap() != null) {
//                        ivPhoto.setImageBitmap(response.getBitmap());
//                    } else {
//                        ivPhoto.setImageResource(R.drawable.empty);
//                    }
//                }
//            });
//            findViewById(R.id.ivVideo).setVisibility(View.VISIBLE);
//        } else {
//            findViewById(R.id.ivVideo).setVisibility(View.INVISIBLE);
//            ApplicationController.getInstance().getImageLoader().get(getString(R.string.server_url3) + ads.getPhoto(), new ImageLoader.ImageListener() {
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    ivPhoto.setImageResource(R.drawable.empty);
//                }
//
//                @Override
//                public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
//                    if (response.getBitmap() != null) {
//                        ivPhoto.setImageBitmap(response.getBitmap());
//                    } else {
//                        ivPhoto.setImageResource(R.drawable.empty);
//                    }
//                }
//            });
//        }
//        if (ads.getStatus() == 1) {
//            tvStatus.setText(getString(R.string.progress));
//            tvStatus.setBackgroundResource(R.drawable.status1);
//        } else {
//            tvStatus.setText(getString(R.string.expired));
//            tvStatus.setBackgroundResource(R.drawable.status2);
//        }
//        findViewById(R.id.scrollView2).setVisibility(View.VISIBLE);
//    }
//
//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//        ApplicationController.getInstance().cancelPendingRequests(ApplicationController.TAG);
//    }
//}


package com.oec.wis.dialogs;

import android.Manifest;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.oec.wis.ApplicationController;
//import com.oec.wis.CurrentLocation;
import com.oec.wis.R;
//import com.oec.wis.ShowMap;
import com.oec.wis.adapters.ChatAdapter;
import com.oec.wis.adapters.Popupadapter;
//import com.oec.wis.adapters.PubChatAdapter;
import com.oec.wis.adapters.SelectFriendsAdapter;
import com.oec.wis.chatApi.ChatApi;
import com.oec.wis.chatApi.ChatListener;
import com.oec.wis.entities.Popupelements;
import com.oec.wis.entities.UserNotification;
import com.oec.wis.entities.WISAds;
import com.oec.wis.entities.WISChat;
import com.oec.wis.entities.WISPhoto;
import com.oec.wis.entities.WISUser;
import com.oec.wis.fragments.FragMyLocation;
import com.oec.wis.fragments.FragPhoto;
import com.oec.wis.fragments.FragProfile;
import com.oec.wis.tools.TextWatcherAdapter;
import com.oec.wis.tools.Tools;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnItemClickListener;
import com.pubnub.api.models.consumer.PNStatus;
import com.pubnub.api.models.consumer.pubsub.PNMessageResult;
import com.pubnub.api.models.consumer.pubsub.PNPresenceEventResult;
import com.rockerhieu.emojicon.EmojiconEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

public class PubDetails extends AppCompatActivity implements ChatListener, OnMapReadyCallback {
    public static WISAds ads;
    ImageView ivPhoto, sendBtn, showMore;
    TextView tvTitle, tvDesc, tvStatus, address,pubdetailsTV,tvStatics;
    EmojiconEditText messageInput;
    int id, idAct, advertiser_id;
    String advertiserName;
    ArrayList<Popupelements> popupList;
    RelativeLayout address_layout;

    TextView detail_pubmusic;

    private GoogleMap googleMap;

    String coordinates;

    int viewTV, notifTV;

    Geocoder geocoder;

    TextView groupName;
    Button next;
    ImageButton close;

    Boolean is_group;

    ListView groupChatFriendList;
    EditText searchFilter;

    List<WISUser> friendList;

    SelectFriendsAdapter selectFriendsAdapter;

    public static Activity context;

    List<Address> addresses;

    DialogPlus dialog;

    int pub_status;

    String publocation;
    double latitude, langitude;

    private String currentChannel;
    public static List<WISChat> data;

    public static ListView lvChat;
    public static ChatAdapter adapter;
    ImageView music_view;
    ViewGroup myHeader;
    TextView headerTitleTV,descriptionTV;
    Typeface font;
    String parsedString = "De ";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Tools.setLocale(this, Tools.getData(this, "lang_pr"));
        setContentView(R.layout.activity_pub_details);
        loadControls();
        setListener();
        reqVueCount();
        context = this;
        loadFriends(context);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("onResume", "calling");
        ChatApi.getInstance().setIsChatView(Boolean.TRUE);
        loadData();
        lvChat.invalidateViews();
    }

    private void loadControls() {
        lvChat = (ListView) findViewById(R.id.lvChat);

        registerForContextMenu(lvChat);
        data = new ArrayList<>();
        adapter = new ChatAdapter(this, data);
        lvChat.setAdapter(adapter);

        LayoutInflater myinflater = getLayoutInflater();
        myHeader = (ViewGroup) myinflater.inflate(R.layout.pub_header, lvChat, false);
        lvChat.addHeaderView(myHeader, null, false);


        ivPhoto = (ImageView) myHeader.findViewById(R.id.ivPhoto);
        tvTitle = (TextView) myHeader.findViewById(R.id.tvTitle);
        tvDesc = (TextView) myHeader.findViewById(R.id.tvDesc);
        tvStatus = (TextView) myHeader.findViewById(R.id.tvStatus);
        pubdetailsTV =(TextView)myHeader.findViewById(R.id.pubdetailsTV);
        tvStatics=(TextView)myHeader.findViewById(R.id.tvStats);
        id = getIntent().getExtras().getInt("id");
        idAct = getIntent().getExtras().getInt("idact");
        pub_status = getIntent().getExtras().getInt("pub_status");
        advertiser_id = getIntent().getExtras().getInt("advertiser_id");
        advertiserName = getIntent().getExtras().getString("advertiser_name");

//        music_view = (ImageView) myHeader.findViewById(R.id.music_view);

        address_layout = (RelativeLayout) myHeader.findViewById(R.id.address_layout);
        //disp_view = (RelativeLayout) myHeader.findViewById(R.id.disp_view);
        //address_layout.setVisibility(View.GONE);
        //disp_view.setVisibility(View.GONE);
        address = (TextView) myHeader.findViewById(R.id.address);
        // address.setText("167, Kaveri Complex\n" + "Subba Road Avenue, Nungambakkam\n" + "Chennai, Tamil Nadu 600034");


//        detail_pubmusic = (TextView) findViewById(R.id.detail_pubmusic);


        showMore = (ImageView) findViewById(R.id.showMoreIV);

        messageInput = (EmojiconEditText) findViewById(R.id.etMsg);

        sendBtn = (ImageView) findViewById(R.id.ivSend);
//        13.0628115,80.2454184
        String adr = new UserNotification().getAddress();
        address.setText(adr);

        ((TextView) findViewById(R.id.tvVue)).setText(viewTV + " " + getString(R.string.Nombre_de_vues));
        ((TextView) findViewById(R.id.tvNotif)).setText(notifTV + " " + getString(R.string.Nombre_de_clics));

        headerTitleTV =(TextView)findViewById(R.id.headerTitleTV);


        try{
            font=Typeface.createFromAsset(PubDetails.this.getAssets(),"fonts/Harmattan-R.ttf");
            headerTitleTV.setTypeface(font);
            tvTitle.setTypeface(font);
            tvDesc.setTypeface(font);
            tvStatus.setTypeface(font);
            pubdetailsTV.setTypeface(font);
            messageInput.setTypeface(font);
            ((TextView) findViewById(R.id.tvVue)).setTypeface(font);
            ((TextView) findViewById(R.id.tvNotif)).setTypeface(font);
            tvStatics.setTypeface(font);
            address.setTypeface(font);

        }catch (NullPointerException ex){
            System.out.println("Excep" +ex.getMessage());
        }


        address_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(getApplicationContext(), CustomLocationActivity.class);
                UserNotification.setIsFromPubView(Boolean.TRUE);
                i.putExtra("lat", latitude);
                i.putExtra("lng", langitude);
                i.putExtra("advertiser_id", advertiser_id);
                i.putExtra("advertiser_name", advertiserName);
                startActivity(i);

//                "lat":"13.06238531156","lan":"80.245511884571"

                Log.i("lan", String.valueOf(langitude));
                Log.i("lat", String.valueOf(latitude));


            }
        });
    }

    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId() == R.id.lvChat) {
            try {
                AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
                menu.add(Menu.NONE, 100, Menu.NONE, getString(R.string.Remove));
                //menu.add(Menu.NONE, 200, Menu.NONE, getString(R.string.transferer));
                //menu.add(Menu.NONE, 300, Menu.NONE, getString(R.string.copy));

            } catch (Exception e) {
            }
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case 100:
                try {
                    deleteMsg(data.get(info.position - 1).getId());
                    data.remove(info.position - 1);
                    adapter.notifyDataSetChanged();
//                    lvChat.invalidateViews();
//                    lvChat.refreshDrawableState();
//                    lvChat.setSelection(data.size() -1);
                } catch (IndexOutOfBoundsException e) {
                    Log.e("dataSize", String.valueOf(data.size()));
                    Log.e("data", String.valueOf(data));
                    if (data.size() == 1) {
//                        data.clear();
//                        lvChat.setAdapter(null);
                        //lvChat.removeViews(1,lvChat.getChildCount());
                    }
                    //data.clear();
                    //loadChathistoryData(ads.getId(),"pub");

                }

                //lvChat.setSelection(data.size() -1);
                return true;

            default:
                return super.onContextItemSelected(item);
        }
    }

    private void deleteMsg(int id) {
        JSONObject jsonBody = new JSONObject();

        try {
            jsonBody.put("id_profil", Tools.getData(this, "idprofile"));
            jsonBody.put("id_message", String.valueOf(id));
        } catch (JSONException e) {
            e.printStackTrace();
        }


        JsonObjectRequest reqDelete = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.deletechat_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {

                                Log.d("calling", "succuss");

                            }

                        } catch (JSONException e) {
                            //Toast.makeText(ChatApi.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(ChatApi.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("lang", Tools.getData(PubDetails.this, "lang_pr"));
                headers.put("token", Tools.getData(PubDetails.this, "token"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqDelete);
    }

    public void loadFriends(final Context context) {
        friendList = new ArrayList<>();

        JSONObject jsonBody = null;

        try {
            jsonBody = new JSONObject("{\"id_profil\":\"" + Tools.getData(context, "idprofile") + "\"}");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.i("Url", context.getString(R.string.server_url) + context.getString(R.string.friends_meth));
        JsonObjectRequest req = new JsonObjectRequest(context.getString(R.string.server_url) + context.getString(R.string.friends_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("result").equals("true")) {
                                JSONArray data = response.getJSONArray("data");
                                for (int i = 0; i < data.length(); i++) {
                                    try {
                                        JSONObject obj = data.getJSONObject(i);
                                        //                                        friendList.add(new WISUser(obj.getInt("idprofile"), obj.getString("fullName")," ", obj.getString("photo"), obj.getInt("nbr_amis"),obj.getBoolean("activeNewsFeed")));

                                        friendList.add(new WISUser(Boolean.FALSE, obj.getString("blocked_by"), obj.getString("objectId"), obj.getString("activeNewsFeed"), obj.getInt("idprofile"), obj.getString("fullName"), obj.getString("photo"), obj.getInt("nbr_amis")));
                                    } catch (Exception e) {

                                    }
                                }


                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                //if (getActivity() != null)
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(context, "token"));
                headers.put("lang", Tools.getData(context, "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(req);


    }


    private void showGroupChatView(final String chatId, final String text) {


        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.contact_list, null);


        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false).setView(dialogView);

        final AlertDialog alert = builder.create();
        alert.show();


        groupName = (TextView) dialogView.findViewById(R.id.groupName);
        next = (Button) alert.findViewById(R.id.bNext);
        close = (ImageButton) alert.findViewById(R.id.closeGroupchat);
        groupChatFriendList = (ListView) alert.findViewById(R.id.contactList);
        searchFilter = (EditText) alert.findViewById(R.id.searchFilter);
        loadFriendsList(groupChatFriendList);

        groupName.setText("Select Contact");

        dialogView.findViewById(R.id.groupName).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                alert.dismiss();
//                openMedia();


            }

        });


        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });


//        next.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//
//                String message;
//                if(is_group){
//                    message = "Groupe";
//                }
//                else{
//                    message = "Amis";
//                }
//
//                Boolean isSuccess =selectFriendsAdapter.checkMemberExists();
//                Log.i("status", String.valueOf(isSuccess));
//                if (isSuccess == true){
//                    alert.dismiss();
//                    selectFriendsAdapter.transferTextToContacts(message, String.valueOf(chatId),text);
//                    Toast.makeText(context,"Successfully shared", Toast.LENGTH_SHORT).show();
//
//                }else{
//                    Toast.makeText(context,"choose contact", Toast.LENGTH_SHORT).show();
//                }
//
//
//
//
//            }
//
//
//
//
//        });

    }

    private void loadFriendsList(ListView groupChatFriendList) {

        selectFriendsAdapter = new SelectFriendsAdapter(context, friendList);
        groupChatFriendList.setAdapter(selectFriendsAdapter);
        selectFriendsAdapter.notifyDataSetChanged();
        loadFilterConfig(selectFriendsAdapter);


    }

    private void loadFilterConfig(final SelectFriendsAdapter adapter) {
        searchFilter.addTextChangedListener(new TextWatcherAdapter() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                super.beforeTextChanged(s, start, count, after);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                super.onTextChanged(s, start, before, count);
                adapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
                super.afterTextChanged(s);

            }
        });
    }


    private void loadData() {
        if (ads == null) {
            findViewById(R.id.loading).setVisibility(View.VISIBLE);
//            findViewById(R.id.scrollView2).setVisibility(View.INVISIBLE);
        }
        JSONObject jsonBody = null;

        try {
            jsonBody = new JSONObject("{\"id_profil\":\"" + Tools.getData(this, "idprofile") + "\",\"id_pub\":\"" + id + "\"}");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqActu = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.getpub_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {
                                Log.e("pub details", String.valueOf(response));

                                JSONObject obj = response.getJSONObject("data");
                                String d = "0";
                                d = obj.getString("distance");

                                boolean t = false;
                                try {
                                    if (obj.getInt("touriste") == 1)
                                        t = true;
                                } catch (Exception e) {
                                }

                                boolean b = false;
                                try {
                                    if (obj.getInt("banner") == 1)
                                        b = true;
                                } catch (Exception e) {
                                }
                                String photo = "";
                                try {
                                    photo = obj.getString("photo");
                                } catch (Exception e) {
                                }
                                String video_thumb = "";
                                try {
                                    video_thumb = obj.getString("photo_video");
                                } catch (Exception e) {
                                }
                                String photo_audio = "";
                                try {
                                    photo_audio = obj.getString("photo_audio");
                                } catch (Exception e) {
                                }
                                ads = new WISAds(obj.getInt("id_pub"), obj.getString("titre"), obj.getString("description"), obj.getInt("etat"), obj.getInt("duree"), d, obj.getString("cible"), obj.getString("created_at"), obj.getString("date_lancement"), photo, obj.getString("type_obj"), t, b, obj.getString("startTime"), obj.getString("endTime"), video_thumb, photo_audio, obj.getString("country"));
                                showData(ads);

                                Log.i("adsData", String.valueOf(ads));

                                new UserNotification().setAds(ads);

                                loadChathistoryData(ads.getId(), "pub");

                                latitude = obj.getDouble("lat");
                                langitude = obj.getDouble("lan");

                                publocation = getCompleteAddressString(latitude, langitude);
                                Log.d("Location", publocation);
                                if (obj.getString("description") == null) {
                                    String ssds = new UserNotification().getAddress();
                                    address.setText(ssds);
                                } else {
                                    address.setText("Address : " + "\n" + publocation);
                                    String pubAdress = getCompleteAddressString(new UserNotification().getLattitude(), new UserNotification().getLangtitude());
                                    Log.d("pubAddress", pubAdress);
                                }

                            }
                            findViewById(R.id.loading).setVisibility(View.GONE);

                        } catch (JSONException e) {
                            findViewById(R.id.loading).setVisibility(View.GONE);
                            //Toast.makeText(PubDetails.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                findViewById(R.id.loading).setVisibility(View.GONE);
                //Toast.makeText(PubDetails.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(PubDetails.this, "token"));
                headers.put("lang", Tools.getData(PubDetails.this, "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqActu);
    }


    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {


        String strAdd = "";
        geocoder = new Geocoder(this, Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
                Log.i("address", strAdd);

            } else {
                Log.i("address", "no address found");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.i("Current loction", "Canont get Address!");
        }
        return strAdd;
    }

    private void reqVueCount() {
        findViewById(R.id.loading).setVisibility(View.VISIBLE);
//        findViewById(R.id.scrollView2).setVisibility(View.INVISIBLE);
        JSONObject jsonBody = null;

        try {
            jsonBody = new JSONObject("{\"id_profil\":\"" + Tools.getData(this, "idprofile") + "\",\"id_act\":\"" + idAct + "\"}");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqActu = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.pubvuecount_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {
                                viewTV = response.getJSONObject("data").getInt("nbr_vue");
                                notifTV = response.getJSONObject("data").getInt("nbr_pub");


                            }
                            findViewById(R.id.loading).setVisibility(View.GONE);

                        } catch (JSONException e) {
                            findViewById(R.id.loading).setVisibility(View.GONE);
                            //Toast.makeText(PubDetails.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                findViewById(R.id.loading).setVisibility(View.GONE);
                //Toast.makeText(PubDetails.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(PubDetails.this, "token"));
                headers.put("lang", Tools.getData(PubDetails.this, "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqActu);
    }

//    public String parseString() {
//
//        String parsedString = "De ";
//        String startTime = ads.getStartTime();
//        String[] separated = startTime.split(":");
//        String start_hour = separated[0];
//        String start_time = separated[1];
//
//
//        String endTime = ads.getEndTime();
//        String[] separated_endtime = endTime.split(":");
//        String end_hour = separated_endtime[0];
//        String end_time = separated_endtime[1];
//
//
//        Log.e("times :: ", ads.getStartTime());
//        Log.e("h :: ", start_hour);
//        Log.e("t :: ", start_time);
//        parsedString = parsedString.concat(start_hour).concat("h").concat(start_time).concat(" Ã  ").concat(end_hour).concat("h").concat(end_time);
//
//        return parsedString;
//    }

    public String parseString() {



        try{


            String startTime = ads.getStartTime();
            String[] separated = startTime.split(":");
            String start_hour = separated[0];
            String start_time = separated[1];
            String endTime = ads.getEndTime();
            String[] separated_endtime = endTime.split(":");
            String end_hour = separated_endtime[0];
            String end_time = separated_endtime[1];
            Log.e("times :: ", ads.getStartTime());
            Log.e("h :: ", start_hour);
            Log.e("t :: ", start_time);
            parsedString = parsedString.concat(start_hour).concat("h").concat(start_time).concat(" Ã  ").concat(end_hour).concat("h").concat(end_time);
        }catch (IndexOutOfBoundsException exce){
            Log.d("exce" ,exce.getMessage());
        }
        return parsedString;
    }

    private void setListener() {
        findViewById(R.id.close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        findViewById(R.id.bEdit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), NewAds.class);
                i.putExtra("edit", "edit");
                startActivity(i);
            }
        });
        myHeader.findViewById(R.id.tvStats).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), PubStats.class);
                i.putExtra("id", id);
                i.putExtra("date", ads.getcDate());
                i.putExtra("km", String.valueOf(ads.getVisib()));
                i.putExtra("country", ads.getCountry());
                i.putExtra("duration", String.valueOf(ads.getDuration()));
                i.putExtra("expire_time", parseString());
                i.putExtra("pub_state", ads.getStatus());
                Log.d("expire strttime", parseString());
                startActivity(i);

//
//
            }
        });
        myHeader.findViewById(R.id.ivVideo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ads.getAudio().startsWith("wis_music")) {
                    Log.i("items", ads.getAudio());
                    Uri uri = Uri.parse(getString(R.string.server_url3) + ads.getAudio());
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    intent.setDataAndType(uri, "audio/mp3");
                    startActivity(intent);

                } else {
                    Uri uri = Uri.parse(getString(R.string.server_url3) + ads.getPhoto());
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    intent.setDataAndType(uri, "video/mp4");
                    startActivity(intent);
                }

            }
        });

        showMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showOption();
            }
        });

        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!TextUtils.isEmpty(messageInput.getText().toString())) {


                    String chatType;
                    String receiverID;


                    Map<String,String> json = new HashMap<>();
                    json.put("message",messageInput.getText().toString());
                    json.put("chatType","pub");
                    json.put("receiverID", String.valueOf(ads.getId()));
                    json.put("contentType","text");
                    json.put("senderID",Tools.getData(PubDetails.this,"idprofile"));
                    json.put("senderName",Tools.getData(PubDetails.this,"name"));
                    String senderPhoto = Tools.getData(PubDetails.this,"photo");

                    json.put("senderImage",senderPhoto);

                    json.put("actual_channel",currentChannel);
//
                    ChatApi.getInstance().sendMessage(json,currentChannel);

                    hideKeyboard();
                }

            }
        });

        ivPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    if (ads.getAudio().startsWith("wis_music")) {
                        Log.i("items", ads.getAudio());
                        Uri uri = Uri.parse(getString(R.string.server_url3) + ads.getAudio());
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        intent.setDataAndType(uri, "audio/mp3");
                        startActivity(intent);

                    } else {
                        FragPhoto.photos = new ArrayList<>();
                        FragPhoto.photos.add(new WISPhoto(0, "", ads.getPhoto(), ads.getcDate()));
                        Intent i = new Intent(PubDetails.this, PhotoGallery.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        i.putExtra("p", 0);
                        startActivity(i);
                    }

                }catch (NullPointerException ex){
                    System.out.println("Exception" +ex.getMessage());
                }


            }
        });
    }


    public void hideKeyboard() {

        InputMethodManager inputManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);

        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
    }


    public void redirecTOProfileView() {

        Intent i = new Intent(this, ProfileView.class);
        int ids = 225;

        i.putExtra("id", advertiser_id);
        startActivity(i);
    }


    public void redirectToWisLocation() {


        Intent i = new Intent(getApplicationContext(), CustomLocationActivity.class);
        UserNotification.setIsFromPubView(Boolean.TRUE);
        i.putExtra("lat", latitude);
        i.putExtra("lng", langitude);
        i.putExtra("advertiser_id", advertiser_id);
        i.putExtra("advertiser_name", advertiserName);
        startActivity(i);

    }

    public void showOption() {

        popupList = new ArrayList<>();
        Popupadapter simpleAdapter = new Popupadapter(this, false, popupList);

        popupList.add(new Popupelements(R.drawable.user, "Advertiser"));
        popupList.add(new Popupelements(R.drawable.location, "Wis Location"));

        dialog = DialogPlus.newDialog(this)
                .setAdapter(simpleAdapter)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View v, int position) {

                        if (position == 0) {

                            dialog.dismiss();

                            redirecTOProfileView();

                        } else if (position == 1) {
                            dialog.dismiss();

                            redirectToWisLocation();
                        }
                    }
                })
                .setExpanded(false)  // This will enable the expand feature, (similar to android L share dialog)
                .create();
        dialog.show();


    }

    private void showData(WISAds ads) {
        tvTitle.setText(ads.getTitle());
        tvDesc.setText(ads.getDesc());
        if (ads.getTypeObj().equals("video")) {
            ApplicationController.getInstance().getImageLoader().get(getString(R.string.server_url4) + ads.getThumb(), new ImageLoader.ImageListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ivPhoto.setImageResource(R.drawable.empty);
                }

                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                    if (response.getBitmap() != null) {
                        ivPhoto.setImageBitmap(response.getBitmap());
                    } else {
                        ivPhoto.setImageResource(R.drawable.empty);
                    }
                }
            });
            findViewById(R.id.ivVideo).setVisibility(View.VISIBLE);
        } else {
            if (ads.getTypeObj().equals("music")) {
//                detail_pubmusic.setText("Détail Pub Music");
                if (ads.getTypeObj().equals("music")) {
                    ((TextView) findViewById(R.id.tvVue)).setText(viewTV + " " + "Views");
                    ((TextView) findViewById(R.id.tvNotif)).setText(notifTV + " " + "Hearing");

                    new UserNotification().setPubmusic(true);
                }
                ApplicationController.getInstance().getImageLoader().get(getString(R.string.server_url3) + ads.getPhoto(), new ImageLoader.ImageListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        ivPhoto.setImageResource(R.drawable.empty);
                    }

                    @Override
                    public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                        if (response.getBitmap() != null) {
                            ivPhoto.setImageBitmap(response.getBitmap());
                        } else {
                            ivPhoto.setImageResource(R.drawable.empty);
                        }
                    }
                });
                address.setText("Address : " + "\n" + publocation);
                //UserNotification.setLocationSelected(true);
                //UserNotification.setAddress(publocation);
                findViewById(R.id.ivVideo).setVisibility(View.VISIBLE);
//                music_view.setImageResource(R.drawable.music_ear);
//                address_layout.setVisibility(View.VISIBLE);
            } else {
                findViewById(R.id.ivVideo).setVisibility(View.INVISIBLE);
                ApplicationController.getInstance().getImageLoader().get(getString(R.string.server_url3) + ads.getPhoto(), new ImageLoader.ImageListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        ivPhoto.setImageResource(R.drawable.empty);
                    }

                    @Override
                    public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                        if (response.getBitmap() != null) {
                            ivPhoto.setImageBitmap(response.getBitmap());
                        } else {
                            ivPhoto.setImageResource(R.drawable.empty);
                        }
                    }
                });
            }

        }
        System.out.println("------------- sttusdhjbs-------");

        Log.e("status piub", String.valueOf(ads.getStatus()));

        if (ads.getStatus() == 1) {
            tvStatus.setText("TERMINEE");
            tvStatus.setBackgroundResource(R.drawable.status1);
        } else {
            tvStatus.setText(getString(R.string.expired));
            tvStatus.setBackgroundResource(R.drawable.status2);
        }
//        findViewById(R.id.scrollView2).setVisibility(View.VISIBLE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ApplicationController.getInstance().cancelPendingRequests(ApplicationController.TAG);
    }

    @Override
    protected void onPause() {
        super.onPause();

        ChatApi.getInstance().setIsChatView(Boolean.FALSE);
    }


    @Override
    public void didReceiveMessage(PNMessageResult messageResult) {


        String receivedChannel = messageResult.getChannel();

        Log.e("receivedChannel", receivedChannel);

        Log.e("currentChannel", currentChannel);

        String senderId = null;

        String message = null;

        String senderName = null;

        if (receivedChannel.equals(currentChannel)) {

            try {


                JSONObject jsonData = new JSONObject(String.valueOf(messageResult.getMessage()));


                Log.d("json res", String.valueOf(jsonData));


                senderId = jsonData.getString("senderID");

                String receiverId = jsonData.getString("receiverID");

                String currentUserId = Tools.getData(PubDetails.this, "idprofile");

                message = jsonData.getString("message");

                String chatType = jsonData.getString("chatType");

                senderName = jsonData.getString("senderName");

                String senderIcon = jsonData.getString("senderImage");

                String contentType = jsonData.getString("contentType");


                if (senderId.equals(currentUserId)) {


                    Log.e("send message", "send");

//                send message

                    System.out.println("Send message");


                    sendMessageToChatList(message, contentType, chatType);
                } else {

                    Log.e("receive  message", "receive");

//                receive meessage

                    System.out.println("receive meessage");

                    receiveMessage(message, chatType, Integer.parseInt(senderId), senderName, senderIcon, contentType);
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else {

            Log.d("show", "notification");


            showAlert(message, senderName);


        }


    }

    @Override
    public void onPublishSuccess(PNStatus status) {


        Log.d("chat status", status.toString());

    }

    @Override
    public void didReceivePresenceEvent(PNPresenceEventResult eventResult) {


        Log.d("chat preseence event", eventResult.getChannel());

    }


    public void sendMessageToChatList(final String sendMesage, final String contentType, final String type) {


//        override date and time
        Date date = new Date();
        SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sourceFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        sourceFormat.format(date);
        Log.i("smiley", String.valueOf(sourceFormat.format(date)));

        TimeZone tz = TimeZone.getTimeZone(Tools.getData(PubDetails.this, "timezone"));
        SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        destFormat.setTimeZone(tz);

        String result = destFormat.format(date);

        if (ChatApi.getInstance().getIsChatView()) {


            WISChat msg = new WISChat(1, new WISUser(0, PubDetails.this.getString(R.string.me), "", Tools.getData(PubDetails.this, "photo"), 0, "", "", ""), sendMesage, result, true, contentType, sendMesage, "");
            data.add(data.size(), msg);


            lvChat.post(new Runnable() {
                public void run() {
                    findViewById(R.id.loading).setVisibility(View.GONE);
                    messageInput.setText("");
                    adapter.notifyDataSetChanged();
                    lvChat.invalidateViews();
                    lvChat.refreshDrawableState();
                    lvChat.setSelection(data.size() - 1);
                    PubDetails.sendMsg(sendMesage, ads.getId(), contentType, "", type);
                }
            });


        }


    }

    public static void sendMsg(final String msg, int id, final String type, final String thumb, final String chatType) {
        final String lastmessage = msg;
        JSONObject jsonBody = new JSONObject();
        context.findViewById(R.id.loading).setVisibility(View.VISIBLE);
        try {
            jsonBody.put("id_profil", Tools.getData(context, "idprofile"));
            jsonBody.put("id_profil_dest", String.valueOf(id));
            jsonBody.put("message", msg);
            jsonBody.put("type_message", type);
            jsonBody.put("chat_type", chatType);

            Log.i("json", jsonBody.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest reqSend = new JsonObjectRequest(context.getString(R.string.server_url) + context.getString(R.string.sendmsg_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("result").equals("true")) {
                                Calendar c = Calendar.getInstance();
                                Log.i("test image", response.toString());
                                String rDate = DateUtils.getRelativeTimeSpanString(c.getTimeInMillis(), System.currentTimeMillis(), DateUtils.MINUTE_IN_MILLIS).toString();

//                                override date and time
                                Date date = new Date();
                                SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                sourceFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                                sourceFormat.format(date);
                                Log.i("smiley", String.valueOf(sourceFormat.format(date)));

                                TimeZone tz = TimeZone.getTimeZone(Tools.getData(context, "timezone"));
                                SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                destFormat.setTimeZone(tz);

                                String result = destFormat.format(date);
                                Log.i("resilt timezone ", result);

//                                int currentUser= Integer.parseInt(Tools.getData(context, "idprofile"));
                                WISChat msg = new WISChat(response.getInt("data"), new WISUser(0, context.getString(R.string.me), "", Tools.getData(context, "photo"), 0, "", "", ""), lastmessage, result, true, type, thumb, "");
                                data.set(data.size() - 1, msg);
                                adapter.notifyDataSetChanged();
//                              data.add(data.size(), msg);

//                                adapter.notifyDataSetChanged();
//                                lvChat.invalidateViews();
//                                lvChat.refreshDrawableState();
//                                etMsg.setText("");
//                                lvChat.setSelection(data.size() - 1);
                            } else {
                                //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                            }
                            context.findViewById(R.id.loading).setVisibility(View.GONE);
                        } catch (JSONException e) {
                            context.findViewById(R.id.loading).setVisibility(View.GONE);
                            //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                context.findViewById(R.id.loading).setVisibility(View.GONE);
                //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(context, "token"));
                headers.put("lang", Tools.getData(context, "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqSend);
    }


    public void receiveMessage(String receivedMessage, String chatType, int senderId, String senderName, String senderIcon, String contentType) {


        //                                    override date and time
        Date date = new Date();
        SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sourceFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        sourceFormat.format(date);


        TimeZone tz = TimeZone.getTimeZone(Tools.getData(getApplicationContext(), "timezone"));
        SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        destFormat.setTimeZone(tz);

        String result = destFormat.format(date);

        if (ChatApi.getInstance().getIsChatView()) {


            PubDetails.data.add(new WISChat(0, new WISUser(senderId, senderName, senderIcon), receivedMessage, result, false, contentType, receivedMessage, ""));


            runOnUiThread(new Runnable() {
                @Override
                public void run() {

//stuff that updates ui

                    findViewById(R.id.loading).setVisibility(View.GONE);

                    PubDetails.adapter.notifyDataSetChanged();
                    PubDetails.lvChat.invalidateViews();
                    PubDetails.lvChat.setSelection(PubDetails.data.size() - 1);


                }
            });

        }
    }

//    private void scrollMyListViewToBottom() {
//        lvChat.post(new Runnable() {
//            @Override
//            public void run() {
//                // Select the last row so it will scroll into view...
//                lvChat.setSelection(PubDetails.data.size() - 1);
//            }
//        });
//    }

    public void showAlert(String message, String senderName) {

        Log.i("notification", "alert");

        Tools.showNotif2(this, 0, senderName, message);

    }


    public void loadChathistoryData(int id, final String chat_type) {
        findViewById(R.id.loading).setVisibility(View.VISIBLE);
        JSONObject jsonBody = null;

        try {
            String json = "{\"id_profil\":\"" + Tools.getData(PubDetails.this, "idprofile") + "\",\"id_ami\":\"" + id + "\",\"chat_type\":\"" + chat_type + "\"}";
            jsonBody = new JSONObject(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("json parms", String.valueOf(jsonBody));
        String apiUrl = null;
        if (chat_type.equals("Groupe")) {
            apiUrl = getString(R.string.server_url) + getString(R.string.group_history);
        } else {
            apiUrl = getString(R.string.server_url) + getString(R.string.histo_meth);
        }
        JsonObjectRequest reqDisc = new JsonObjectRequest(apiUrl, jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("result").equals("true")) {
//                                data.clear();
//                                adapter.notifyDataSetChanged();
                                JSONArray dataArray = response.getJSONArray("data");
                                for (int i = 0; i < dataArray.length(); i++) {
                                    try {
                                        JSONObject obj = dataArray.getJSONObject(i);
                                        boolean isMe = false;
                                        isMe = Tools.getData(PubDetails.this, "idprofile").equals(obj.getJSONObject("created_by").getString("id_profil"));
                                        String thumb = "";
                                        try {
                                            thumb = obj.getString("photo_video");
                                        } catch (Exception e) {
                                        }

                                        Log.i("new initailzed", obj.toString());

                                        data.add(new WISChat(obj.getInt("id_message"), new WISUser(obj.getJSONObject("created_by").getInt("id_profil"), obj.getJSONObject("created_by").getString("nom"), obj.getJSONObject("created_by").getString("photo")), obj.getString("message"), obj.getString("created_at"), isMe, obj.getString("type_message"), thumb, ""));
                                        adapter.notifyDataSetChanged();
                                        lvChat.invalidateViews();
                                        lvChat.refreshDrawableState();
                                    } catch (Exception e) {

                                    }
                                    lvChat.setSelection(data.size() - 1);
                                }
                            }

                            findViewById(R.id.loading).setVisibility(View.GONE);

                            currentChannel = "pub_banner".concat(String.valueOf(ads.getId()));

                            ChatApi.getInstance().setListener(PubDetails.this);
                            ChatApi.getInstance().subscribe();

                        } catch (JSONException e) {

                            findViewById(R.id.loading).setVisibility(View.GONE);
                            //Toast.makeText(ChatApi.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                findViewById(R.id.loading).setVisibility(View.GONE);
                //Toast.makeText(ChatApi.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(PubDetails.this, "token"));
                headers.put("lang", Tools.getData(PubDetails.this, "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqDisc);
    }

    @Override
    public void onMapReady(GoogleMap map) {

        googleMap = map;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        googleMap.setMyLocationEnabled(true);
        googleMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
            @Override
            public void onMyLocationChange(Location location) {
                try
                {
                    latitude = location.getLatitude();

                    langitude = location.getLongitude();

                    coordinates = String.valueOf(String.valueOf(location.getLatitude()).concat(",").concat(String.valueOf(location.getLongitude())));

                    Log.e("coordinates",coordinates);

                    Log.d("hello","hello");

                    new UserNotification().setTopub(Boolean.TRUE);
                    new UserNotification().setLattitude(latitude);
                    new UserNotification().setLangtitude(langitude);

                }
                catch(NullPointerException ex){

                    ex.printStackTrace();

                }
            }
        });
    }
}