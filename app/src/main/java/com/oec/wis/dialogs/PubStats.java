package com.oec.wis.dialogs;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.CandleDataSet;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.oec.wis.ApplicationController;
import com.oec.wis.R;
import com.oec.wis.tools.Tools;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PubStats extends AppCompatActivity {
    BarChart chart1;
    PieChart chart2;
    int barMode = 0;
    ArrayList<String> times, hours;
    List<BarEntry> yViews;
    List<BarEntry> yClicks;
    int id;
    TextView tvDate, tvKm, tvDuration;
    int tClicks = 0, tVues = 0;
    TextView headerTitle;
    CandleDataSet candleDataSet;
    TextView percentTV,coefficientTV;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Tools.setLocale(this, Tools.getData(this, "lang_pr"));
        setContentView(R.layout.activity_pub_stats);
        id = getIntent().getExtras().getInt("id");
        loadControls();
        loadData();
        setListener();
        setTypeFace();



    }

    private void setTypeFace() {
        Typeface font= Typeface.createFromAsset(PubStats.this.getAssets(),"fonts/Harmattan-R.ttf");
        tvDate.setTypeface(font);
        tvDuration.setTypeface(font);
        tvKm.setTypeface(font);
        headerTitle.setTypeface(font);
        coefficientTV.setTypeface(font);
        percentTV.setTypeface(font);

    }

    private void switchBarView(int xIndex) {
        if (barMode == 1)
            loadBarValueHours(id, times.get(xIndex));
        else {
            loadBarValueDays(times, yViews, yClicks);
        }
    }

    private void setListener() {
        chart1.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
                if (times.size() > 1)
                    switchBarView(e.getXIndex());
            }

            @Override
            public void onNothingSelected() {

            }
        });
        findViewById(R.id.close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void loadControls() {
        chart1 = (BarChart) findViewById(R.id.chart1);
        chart2 = (PieChart) findViewById(R.id.chart2);
        tvDate = (TextView) findViewById(R.id.tvDate);
        tvKm = (TextView) findViewById(R.id.tvKm);
        tvDuration = (TextView) findViewById(R.id.tvDuration);
        headerTitle=(TextView)findViewById(R.id.headerTitleTV);
        percentTV=(TextView)findViewById(R.id.percentTV);
        coefficientTV=(TextView)findViewById(R.id.coefficientTV);



        chart1.setDrawBarShadow(false);
        chart1.setDrawValueAboveBar(true);

//           changed 45

        chart1.setDrawGridBackground(false);   /*set background color*/
        chart1.setDrawBarShadow(false);

//        chart1.getXAxis().setEnabled(false);   /* x axis  line disable*/
//        chart1.getAxisLeft().setEnabled(false);
        chart1.getAxisRight().setEnabled(false);  /* right side enable */


        XAxis xAxis = chart1.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM_INSIDE);

        try{
            candleDataSet.setDrawHorizontalHighlightIndicator(false);

        }catch (NullPointerException ex){
            System.out.println("Exe===>" +ex.getMessage());
        }

//        Legend legend=chart1.getLegend();
//        legend.setTextColor(R.color.gray1);
//        legend.setYOffset(0f);
////        legend.setXEntrySpace(10f);
////        legend.setYEntrySpace(20f);
//        legend.setTextSize(13f);

        Typeface font= Typeface.createFromAsset(this.getAssets(),"fonts/Harmattan-R.ttf");


//        bar chart
        Legend l = chart1.getLegend();
        l.setForm(Legend.LegendForm.SQUARE);
        l.setTextColor(R.color.gray2);

//        l.setVerticalAlignment(LegendVerticalAlignment.BOTTOM);
//        l.setHorizontalAlignment(LegendHorizontalAlignment.LEFT);
//        l.setOrientation(LegendOrientation.HORIZONTAL);
        l.setFormSize(9f);
        l.setTextSize(15f);
//        l.setYEntrySpace(74f);
//        l.setXEntrySpace(40f);
        l.setTypeface(font);
        l.setPosition(Legend.LegendPosition.BELOW_CHART_CENTER);



//         pie chart

        Legend legend1=chart2.getLegend();
        legend1.setTextColor(R.color.gray2);
//        legend1.setYOffset(0f);
        legend1.setTypeface(font);
        legend1.setPosition(Legend.LegendPosition.RIGHT_OF_CHART);
        legend1.setTextSize(15f);
//        legend1.setXEntrySpace(80f);
        legend1.setYEntrySpace(-15f);
//        legend1.setXOffset(0f);

// end 45



//        Legend l = chart2.getLegend();
//        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
//        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
//        l.setOrientation(Legend.LegendOrientation.VERTICAL);
//        l.setDrawInside(false);
//        l.setXEntrySpace(7f);
//        l.setYEntrySpace(0f);
//        l.setYOffset(0f);
    }

    private void loadChart2(int nDays, String Day, int clicks, int vues) {
        List<Entry> yVals2 = new ArrayList<>();
        ArrayList<String> titles = new ArrayList<>();
        titles.add(getString(R.string.clicks));
        titles.add(getString(R.string.views));
        yVals2.add(new Entry(clicks, 0));
        yVals2.add(new Entry(vues, 1));
        PieDataSet dataSet = new PieDataSet(yVals2, "");

//        Color.rgb(245, 124, 114)
//        dataSet.setColors(new int[]{Color.rgb(255, 21, 51), Color.rgb(0, 169, 195)});
        dataSet.setColors(new int[]{Color.rgb(245, 124, 114),Color.rgb(8, 103, 148)});
        dataSet.setValueTextColor(Color.rgb(255, 255, 255));
        dataSet.setValueTextSize(10);
        dataSet.setValueFormatter(new PercentFormatter());
        chart2.setData(new PieData(titles, dataSet));
        chart2.setUsePercentValues(true);
        chart2.setDescription("");
        chart2.setCenterText(Day);
        switch (nDays) {
            case 1:
                chart2.setCenterText(getString(R.string.day1));
                break;
            case 7:
                chart2.setCenterText(getString(R.string.day2));
                break;
            case 30:
                chart2.setCenterText(getString(R.string.day30));
                break;
        }

        chart2.setCenterTextColor(Color.rgb(126, 126, 126));
        chart2.invalidate();
        chart2.animateY(1500, Easing.EasingOption.EaseInOutQuad);


    }



    private void loadData() {
        tvKm.setText(getIntent().getExtras().getString("km") + " KM");
        try {
            tvDate.setText(getIntent().getExtras().getString("date").substring(0, 10));
        } catch (Exception e) {

        }
        findViewById(R.id.loading).setVisibility(View.VISIBLE);
        JSONObject jsonBody = null;

        try {
            String json = "{\"id_pub\":\"" + id + "\"}";
            jsonBody = new JSONObject(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqStats = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.stats_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String x = "";
                            if (response.getBoolean("result")) {
                                System.out.println("respone==>" +response);
                                JSONObject obj = response.getJSONObject("data");
                                //ads = new WISAds(obj.getInt("id_act"), obj.getString("titre"), obj.getString("description"), obj.getInt("etat"), obj.getInt("duree"), obj.getInt("distance"), obj.getString("cible"), obj.getString("created_at"), obj.getString("date_lancement"), obj.getString("photo"));
                                times = new ArrayList<>();
                                JSONArray days = obj.getJSONArray("All-days_pub");
                                for (int i = 0; i < days.length(); i++) {
                                    times.add(days.getString(i).substring(5));
                                }
                                yViews = new ArrayList<>();
                                yClicks = new ArrayList<>();
                                JSONArray data = obj.getJSONArray("All_stats");

                                for (int i = 0; i < data.length(); i++) {
                                    JSONObject dataObj = data.getJSONObject(i);
                                    if (!(dataObj.getInt("vue") == 0 && dataObj.getInt("clicked") == 0)) {
                                        tVues += dataObj.getInt("vue");
                                        tClicks += dataObj.getInt("clicked");
                                        yViews.add(new BarEntry(dataObj.getInt("vue"), i));
                                        yClicks.add(new BarEntry(dataObj.getInt("clicked"), i));
                                    }
                                }

                                loadBarValueDays(times, yViews, yClicks);
                                loadChart2(times.size(), "", tClicks, tVues);
                                switch (times.size()) {
                                    case 1:
                                        tvDuration.setText(getString(R.string.day1));
                                        loadBarValueHours(id, times.get(0));
                                        break;
                                    case 7:
                                        tvDuration.setText(getString(R.string.day2));
                                        break;
                                    case 30:
                                        tvDuration.setText(getString(R.string.day30));
                                        break;
                                }
                            }
                            findViewById(R.id.loading).setVisibility(View.GONE);
                        } catch (JSONException e) {
                            findViewById(R.id.loading).setVisibility(View.GONE);
                            //Toast.makeText(PubStats.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                findViewById(R.id.loading).setVisibility(View.GONE);
                //Toast.makeText(PubStats.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(PubStats.this, "token"));
                headers.put("lang", Tools.getData(PubStats.this, "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqStats);
    }

    private void loadBarValueDays(ArrayList<String> times, List<BarEntry> yViews, List<BarEntry> yClicks) {
        barMode = 1;

        List<BarDataSet> dataSets = new ArrayList<>();

        BarDataSet views = new BarDataSet(yViews, "Number of Views");
        views.setColor(Color.rgb(31, 142, 211));
        views.setValueTextSize(10);
        views.setDrawValues(true);
        dataSets.add(views);

        BarDataSet clicks = new BarDataSet(yClicks, "Number of Clicks");
        clicks.setColor(Color.rgb(245, 124, 114));
        clicks.setValueTextSize(10);
        clicks.setDrawValues(true);
        dataSets.add(clicks);
        BarData data = new BarData(times, dataSets);
        loadChart1(data);
        loadChart2(times.size(), "", tClicks, tVues);
    }

    private void loadBarValueHours(int id, final String day) {
        JSONObject jsonBody = null;
        barMode = 0;
        ArrayList<String> times = new ArrayList<>();
        try {
            int year = Calendar.getInstance().get(Calendar.YEAR);
            String json = "{\"id_pub\":\"" + id + "\",\"day\":\"" + year + "-" + day + "\"}";
            jsonBody = new JSONObject(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest reqStats = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.stats_hours_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String x = "";
                            if (response.getBoolean("result")) {

                                JSONArray array = response.getJSONArray("data");
                                hours = new ArrayList<>();
                                List<BarEntry> yViews = new ArrayList<>();
                                List<BarEntry> yClicks = new ArrayList<>();
                                int tClicks = 0, tVues = 0;
                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject obj = array.getJSONObject(i);
                                    String[] days = obj.getString("day").split("-");
                                    hours.add(days[0].substring(0, 2) + "-" + days[1].substring(0, 2));

                                    if (!(obj.getInt("vue") == 0 && obj.getInt("clicked") == 0)) {
                                        tVues += obj.getInt("vue");
                                        tClicks += obj.getInt("clicked");
                                        yViews.add(new BarEntry(obj.getInt("vue"), i));
                                        yClicks.add(new BarEntry(obj.getInt("clicked"), i));
                                    }
                                }
                                loadHours(yViews, yClicks);
                                loadChart2(0, day, tClicks, tVues);
                            }
                            findViewById(R.id.loading).setVisibility(View.GONE);
                        } catch (JSONException e) {
                            findViewById(R.id.loading).setVisibility(View.GONE);
                            //Toast.makeText(PubStats.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                findViewById(R.id.loading).setVisibility(View.GONE);
                //Toast.makeText(PubStats.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(PubStats.this, "token"));
                headers.put("lang", Tools.getData(PubStats.this, "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqStats);


    }

    private void loadHours(List<BarEntry> yViews, List<BarEntry> yClicks) {
        List<BarDataSet> dataSets = new ArrayList<>();


        BarDataSet clicks = new BarDataSet(yClicks, "Number of Clicks");
        clicks.setColor(Color.rgb(245, 124, 114));
        clicks.setValueTextSize(8);
        clicks.setDrawValues(false);
        dataSets.add(clicks);


        BarDataSet views = new BarDataSet(yViews, "Number of Views");
        views.setColor(Color.rgb(31, 142, 211));
        views.setValueTextSize(8);
        views.setDrawValues(false);
        dataSets.add(views);

        BarData data = new BarData(hours, dataSets);
        loadChart1(data);
    }

    private void loadChart1(BarData data) {
        chart1.setData(data);
        chart1.setDrawHighlightArrow(true);
        chart1.setBackgroundColor(Color.rgb(255, 255, 255));
        chart1.setDescription("");
        chart1.setPinchZoom(false);
        chart1.invalidate();
        chart1.animateY(1500, Easing.EasingOption.EaseInBack);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ApplicationController.getInstance().cancelPendingRequests(ApplicationController.TAG);
    }

    public class PercentFormatter implements ValueFormatter {

        private DecimalFormat mFormat;

        public PercentFormatter() {
            mFormat = new DecimalFormat("###,###,##0");
        }

        @Override
        public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
            // write your logic here
            return mFormat.format(value) + " %";
        }
    }
}





//changed
//
//package com.oec.wis.dialogs;
//
//import android.graphics.Color;
//import android.graphics.Typeface;
//import android.os.Bundle;
//import android.support.v7.app.AppCompatActivity;
//import android.util.Log;
//import android.view.View;
//import android.widget.TextView;
//
//import com.android.volley.AuthFailureError;
//import com.android.volley.Response;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.JsonObjectRequest;
//import com.github.mikephil.charting.animation.Easing;
//import com.github.mikephil.charting.charts.BarChart;
//import com.github.mikephil.charting.charts.BarLineChartBase;
//import com.github.mikephil.charting.charts.PieChart;
//import com.github.mikephil.charting.components.AxisBase;
//import com.github.mikephil.charting.components.Legend;
//import com.github.mikephil.charting.components.XAxis;
//import com.github.mikephil.charting.components.YAxis;
//import com.github.mikephil.charting.data.BarData;
//import com.github.mikephil.charting.data.BarDataSet;
//import com.github.mikephil.charting.data.BarEntry;
//import com.github.mikephil.charting.data.CandleDataSet;
//import com.github.mikephil.charting.data.Entry;
//import com.github.mikephil.charting.data.PieData;
//import com.github.mikephil.charting.data.PieDataSet;
//
//import com.github.mikephil.charting.formatter.IAxisValueFormatter;
//import com.github.mikephil.charting.highlight.Highlight;
//import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
//import com.github.mikephil.charting.utils.ViewPortHandler;
//import com.oec.wis.ApplicationController;
//import com.oec.wis.R;
//import com.oec.wis.tools.Tools;
//
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.text.DecimalFormat;
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//public class PubStats extends AppCompatActivity {
//    BarChart chart1;
//    PieChart chart2;
//    int barMode = 0;
//    ArrayList<String> times, hours;
//    List<BarEntry> yViews;
//    List<BarEntry> yClicks;
//    int id;
//    TextView tvDate, tvKm, tvDuration;
//    int tClicks = 0, tVues = 0;
//    TextView headerTitle;
//    CandleDataSet candleDataSet;
//    TextView percentTV, coefficientTV;
//    protected BarChart mChart;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        Tools.setLocale(this, Tools.getData(this, "lang_pr"));
//        setContentView(R.layout.activity_pub_status_new);
//        id = getIntent().getExtras().getInt("id");
//
//
//
//        mChart = (BarChart) findViewById(R.id.chart1);
//        mChart.setOnChartValueSelectedListener((OnChartValueSelectedListener) PubStats.this);
//
//        mChart.setDrawBarShadow(false);
//        mChart.setDrawValueAboveBar(true);
//
//        mChart.getDescription().setEnabled(false);
//
//        // if more than 60 entries are displayed in the chart, no values will be
//        // drawn
//        mChart.setMaxVisibleValueCount(60);
//
//        // scaling can now only be done on x- and y-axis separately
//        mChart.setPinchZoom(false);
//
//        mChart.setDrawGridBackground(false);
//        // mChart.setDrawYLabels(false);
//
//        IAxisValueFormatter xAxisFormatter = (IAxisValueFormatter) new DayAxisValueFormatter(mChart);
//
//        XAxis xAxis = mChart.getXAxis();
//        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
////        xAxis.setTypeface(mTfLight);
//        xAxis.setDrawGridLines(false);
//        xAxis.setGranularity(1f); // only intervals of 1 day
//        xAxis.setLabelCount(7);
//        xAxis.setValueFormatter((com.github.mikephil.charting.formatter.IAxisValueFormatter) xAxisFormatter);
//
//        IAxisValueFormatter custom = (IAxisValueFormatter) new MyAxisValueFormatter();
//
//        YAxis leftAxis = mChart.getAxisLeft();
//        leftAxis.setLabelCount(8, false);
//        leftAxis.setValueFormatter((com.github.mikephil.charting.formatter.IAxisValueFormatter) custom);
//        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
//        leftAxis.setSpaceTop(15f);
//        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)
//
//        YAxis rightAxis = mChart.getAxisRight();
//        rightAxis.setDrawGridLines(false);
//        rightAxis.setLabelCount(8, false);
//        rightAxis.setValueFormatter((com.github.mikephil.charting.formatter.IAxisValueFormatter) custom);
//        rightAxis.setSpaceTop(15f);
//        rightAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)
//
//    }
//    public class MyAxisValueFormatter implements com.github.mikephil.charting.formatter.IAxisValueFormatter
//    {
//
//        private DecimalFormat mFormat;
//
//        public MyAxisValueFormatter() {
//            mFormat = new DecimalFormat("###,###,###,##0.0");
//        }
//
//        @Override
//        public String getFormattedValue(float value, AxisBase axis) {
//            return mFormat.format(value) + " $";
//        }
//    }
//
//
//    public interface IAxisValueFormatter
//    {
//
//        /**
//         * Called when a value from an axis is to be formatted
//         * before being drawn. For performance reasons, avoid excessive calculations
//         * and memory allocations inside this method.
//         *
//         * @param value the value to be formatted
//         * @param axis  the axis the value belongs to
//         * @return
//         */
//        String getFormattedValue(float value, AxisBase axis);
//    }
//    public class DayAxisValueFormatter implements com.github.mikephil.charting.formatter.IAxisValueFormatter
//    {
//
//        protected String[] mMonths = new String[]{
//                "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
//        };
//
//        private BarLineChartBase<?> chart;
//
//        public DayAxisValueFormatter(BarLineChartBase<?> chart) {
//            this.chart = chart;
//        }
//
//        @Override
//        public String getFormattedValue(float value, AxisBase axis) {
//
//            int days = (int) value;
//
//            int year = determineYear(days);
//
//            int month = determineMonth(days);
//            String monthName = mMonths[month % mMonths.length];
//            String yearName = String.valueOf(year);
//
//            if (chart.getVisibleXRange() > 30 * 6) {
//
//                return monthName + " " + yearName;
//            } else {
//
//                int dayOfMonth = determineDayOfMonth(days, month + 12 * (year - 2016));
//
//                String appendix = "th";
//
//                switch (dayOfMonth) {
//                    case 1:
//                        appendix = "st";
//                        break;
//                    case 2:
//                        appendix = "nd";
//                        break;
//                    case 3:
//                        appendix = "rd";
//                        break;
//                    case 21:
//                        appendix = "st";
//                        break;
//                    case 22:
//                        appendix = "nd";
//                        break;
//                    case 23:
//                        appendix = "rd";
//                        break;
//                    case 31:
//                        appendix = "st";
//                        break;
//                }
//
//                return dayOfMonth == 0 ? "" : dayOfMonth + appendix + " " + monthName;
//            }
//        }
//
//        private int getDaysForMonth(int month, int year) {
//
//            // month is 0-based
//
//            if (month == 1) {
//                boolean is29Feb = false;
//
//                if (year < 1582)
//                    is29Feb = (year < 1 ? year + 1 : year) % 4 == 0;
//                else if (year > 1582)
//                    is29Feb = year % 4 == 0 && (year % 100 != 0 || year % 400 == 0);
//
//                return is29Feb ? 29 : 28;
//            }
//
//            if (month == 3 || month == 5 || month == 8 || month == 10)
//                return 30;
//            else
//                return 31;
//        }
//
//        private int determineMonth(int dayOfYear) {
//
//            int month = -1;
//            int days = 0;
//
//            while (days < dayOfYear) {
//                month = month + 1;
//
//                if (month >= 12)
//                    month = 0;
//
//                int year = determineYear(days);
//                days += getDaysForMonth(month, year);
//            }
//
//            return Math.max(month, 0);
//        }
//
//        private int determineDayOfMonth(int days, int month) {
//
//            int count = 0;
//            int daysForMonths = 0;
//
//            while (count < month) {
//
//                int year = determineYear(daysForMonths);
//                daysForMonths += getDaysForMonth(count % 12, year);
//                count++;
//            }
//
//            return days - daysForMonths;
//        }
//
//        private int determineYear(int days) {
//
//            if (days <= 366)
//                return 2016;
//            else if (days <= 730)
//                return 2017;
//            else if (days <= 1094)
//                return 2018;
//            else if (days <= 1458)
//                return 2019;
//            else
//                return 2020;
//
//        }
//    }
//
//
//}