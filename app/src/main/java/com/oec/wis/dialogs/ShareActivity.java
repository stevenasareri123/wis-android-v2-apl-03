package com.oec.wis.dialogs;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.oec.wis.ApplicationController;
import com.oec.wis.ConnectionDetector;
import com.oec.wis.R;
import com.oec.wis.fragments.FragPhoto;
import com.oec.wis.tools.Tools;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ShareActivity extends Activity {

    ImageView closeBtn,sharedImages;
    Button post;
    EditText text;
    String contentUrl;
    Intent intent;
    String action,type;
    ArrayList<String>MultiplefileData;
    TextView label;

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        super.startActivityForResult(intent, requestCode);

        Log.i("activity called", String.valueOf(requestCode));
    }

    public void initControls(){

        closeBtn = (ImageView)findViewById(R.id.close_btn);
        post = (Button)findViewById(R.id.post_status);
        text = (EditText)findViewById(R.id.message);
        sharedImages = (ImageView)findViewById(R.id.post_image);
        label = (TextView)findViewById(R.id.identifier);

        intent = getIntent();
        action = intent.getAction();
        type = intent.getType();

        Typeface font=Typeface.createFromAsset(this.getAssets(),"fonts/Harmattan-R.ttf");
        text.setTypeface(font);


    }

    public void parseIntentData(){
        // Get intent, action and MIME type

        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if ("text/plain".equals(type)) {
                setIntentData(intent);
            } else if (type.startsWith("image/")) {
               loadSharedData("photo");
            }else if (type.startsWith("video/")) {
//                loadSharedData("video");
            }else if ("text/uri-list".equals(type)){
                handleSendURL(intent);
            }
        } else if (Intent.ACTION_SEND_MULTIPLE.equals(action) && type != null) {
            if (type.startsWith("image/")) {

                handleSendMultipleImages(intent); // Handle multiple images being sent
            }
        } else {
            // Handle other intents, such as being started from the home screen
        }

    }

    public void setIntentData(Intent intent){
        String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);

        if(sharedText!=null){

            text.setText(sharedText);
        }


    }


    public void loadSharedData(String type){
        contentUrl = getFileContent(intent,type);
        MultiplefileData = new ArrayList<>();
        MultiplefileData.add(contentUrl);

        if(contentUrl==null){


            Bitmap bitmap = null;
            Uri imageUri = (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);
            if (imageUri != null) {
                 imageUri = intent.getData();
//                try {
//                   bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//
//                sharedImages.setImageBitmap(bitmap);
//                contentUrl =imageUri.getPath();

                Toast.makeText(ShareActivity.this, "Unable to process your request", Toast.LENGTH_SHORT).show();
                finish();

            }

        }else{
            Bitmap bitmap = BitmapFactory.decodeFile(contentUrl);
            Log.i("image file path", String.valueOf(bitmap));
            sharedImages.setImageBitmap(bitmap);
            String counts = String.valueOf(MultiplefileData.size()).concat(" ").concat("Photos");
            label.setText(counts);
        }





    }

    public void initListener(){

        post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ConnectionDetector internetcheck = new ConnectionDetector(ShareActivity.this);
                boolean is_active = internetcheck.isConnectingToInternet();
                if (is_active) {


                    String id = null;

                    id = Tools.getData(ShareActivity.this, "id_profil");
                    if (id != null) {

                        findViewById(R.id.loading).setVisibility(View.VISIBLE);

                        if (Intent.ACTION_SEND.equals(action) && type != null) {
                            if ("text/plain".equals(type)) {
                                handleSendText(intent);
                            } else if (type.startsWith("image/")) {
                                handleSendImage(MultiplefileData);
                            } else if (type.startsWith("video/")) {
                              handleSendVideo(intent);
                            }
                        }else if (Intent.ACTION_SEND_MULTIPLE.equals(action) && type != null){
                            if (type.startsWith("image/")){
                                handleSendMultipImage();
                            }
                        }
                    } else {

                        Toast.makeText(ShareActivity.this, "Login Required", Toast.LENGTH_SHORT).show();
                        finish();
                    }


                } else {
                    Toast.makeText(ShareActivity.this, "Connection Required", Toast.LENGTH_SHORT).show();
                }
            }

        });

        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    public String getFileContent(Intent intent,String type) {
        String imgPath = "";
        int column_index=0;
        Cursor cursor=null;

        Uri imageUri = (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);
        if (imageUri != null) {
            // Update UI to reflect image being shared

            Log.i("image shared", String.valueOf(imageUri));


            if(type.equals("photo")){
                final String[] proj = {MediaStore.Images.Media.DATA};
                cursor = this.managedQuery(imageUri, proj, null, null,
                        null);
                column_index = cursor
                        .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);

            }else if(type.equals("video")){
                final String[] proj = {MediaStore.Video.Media.DATA};
                cursor = this.managedQuery(imageUri, proj, null, null,
                        null);
                column_index = cursor
                        .getColumnIndexOrThrow(MediaStore.Video.Media.DATA);

            }


            cursor.moveToLast();
            imgPath = cursor.getString(column_index);


        }

        return imgPath;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String id = null;

        id = Tools.getData(ShareActivity.this, "id_profil");
        if (id == null) {

            Toast.makeText(ShareActivity.this, "Login Required", Toast.LENGTH_SHORT).show();
            finish();

        }
        else{

            setContentView(R.layout.activity_share);
            initControls();
            parseIntentData();
            initListener();

        }






    }

    void handleSendText(Intent intent) {
        String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
        if (sharedText != null) {
            // Update UI to reflect text being shared

            Log.i("plain text",sharedText);

            sendUrlPosttoServer(sharedText);
        }
    }


    public void handleSendURL(Intent intent){
        String sharedText = intent.getStringExtra(Intent.EXTRA_ORIGINATING_URI);
        Log.i("url intent", String.valueOf(intent));

    }


public String getMultiplImagePath(Uri imageUri,String type){
    String imgPath=null;
    int column_index = 0;
     Cursor cursor=null;

    if(type.equals("photo")){
        final String[] proj = {MediaStore.Images.Media.DATA};
        cursor = this.managedQuery(imageUri, proj, null, null,
                null);
        column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);

    }else if(type.equals("video")){

        final String[] proj = {MediaStore.Video.Media.DATA};
       cursor = this.managedQuery(imageUri, proj, null, null,
                null);

        column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
    }



    cursor.moveToLast();
    imgPath = cursor.getString(column_index);


    return imgPath;
}
public void loadMultipleImage(ArrayList<Uri>files,Intent intent){
    MultiplefileData = new ArrayList<>();
    for(int i=0;i<files.size();i++){
        MultiplefileData.add(getMultiplImagePath(files.get(i),"photo"));
    }

    Bitmap bitmap = BitmapFactory.decodeFile(MultiplefileData.get(0));
    Log.i("image file path", String.valueOf(bitmap));
    sharedImages.setImageBitmap(bitmap);
    String counts = String.valueOf(MultiplefileData.size()).concat(" ").concat("Photos");
    label.setText(counts);

}

    void handleSendMultipImage() {


        new DoUpload().execute(text.getText().toString(),"partage_photo");

    }
    void handleSendImage(ArrayList filePath) {


            new DoUpload().execute(text.getText().toString(),"partage_photo");

    }

    void handleSendMultipleImages(Intent intent) {
        ArrayList<Uri> imageUris = intent.getParcelableArrayListExtra(Intent.EXTRA_STREAM);
        if (imageUris != null) {

        loadMultipleImage(imageUris,intent);
            Log.i("multiple images", String.valueOf(imageUris));
            // Update UI to reflect multiple images being shared
        }

    }



    void handleSendVideo(Intent intent) {

        Uri videoUri = (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);



        if (videoUri != null) {
            // Update UI to reflect multiple images being shared

            Log.i("video shared", String.valueOf(videoUri));
            new DoUpload().execute(text.getText().toString(),"partage_video");
    }

    }


//    ----------------WEBSERVICE CALLS------------------

private class DoUpload extends AsyncTask<String, Void, String> {
    @Override
    protected void onPreExecute() {
        Log.i("pre","pre excute");
        super.onPreExecute();

    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if (!result.equals("")) {
            try {
                JSONObject img = new JSONObject(result);
                if (img.getBoolean("result"))
                    Log.i("image data set",img.getString("data"));

                findViewById(R.id.loading).setVisibility(View.INVISIBLE);
                //else
                finish();
                Toast.makeText(ShareActivity.this, getString(R.string.share_success_msg), Toast.LENGTH_SHORT).show();

            } catch (JSONException e) {

                Toast.makeText(ShareActivity.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }

        } else {

            //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected String doInBackground(String... urls) {
        return Tools.doMultipleFileUpload(ShareActivity.this.getString(R.string.server_url) + ShareActivity.this.getString(R.string.multipleFileUpload), MultiplefileData, Tools.getData(ShareActivity.this, "token"),Tools.getData(ShareActivity.this, "idprofile"),urls[0],urls[1]);

    }
}

    public void sendUrlPosttoServer(String url_txt){
        JSONObject jsonBody = null;

        try {

            String json = "{\"user_id\": \"" + Tools.getData(ShareActivity.this, "idprofile") + "\",\"url\": \"" + url_txt + "\"}";
            jsonBody = new JSONObject(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqAddPub = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.url_post), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {
                                findViewById(R.id.loading).setVisibility(View.INVISIBLE);
                                Log.i("respones write status",response.toString());
                                finish();
                                Toast.makeText(ShareActivity.this,getString(R.string.share_success_msg),Toast.LENGTH_SHORT).show();


                            }
                        } catch (Exception e) {

                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                findViewById(R.id.loading).setVisibility(View.INVISIBLE);

                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(ShareActivity.this, "token"));
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("lang", "en_US");
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqAddPub);
    }


    private void doUpdate(final String photo,final String desc) {
        JSONObject jsonBody = null;

        try {

            String json = "{\"user_id\": \"" + Tools.getData(ShareActivity.this, "idprofile") + "\",\"name_img\": \"" + photo + "\",\"message\": \"" +desc + "\"}";
            jsonBody = new JSONObject(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqAddPub = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.write_status), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {
                                findViewById(R.id.loading).setVisibility(View.INVISIBLE);

                                Toast.makeText(ShareActivity.this,getString(R.string.share_success_msg),Toast.LENGTH_SHORT).show();

                                Intent result = new Intent("com.example.RESULT_ACTION", Uri.parse("content://result_uri"));
                                 setResult(Activity.RESULT_OK, result);
                                finish();


                            }
                        } catch (Exception e) {

                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                findViewById(R.id.loading).setVisibility(View.INVISIBLE);

                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(ShareActivity.this, "token"));
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("lang", "en_US");
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqAddPub);
    }



}
