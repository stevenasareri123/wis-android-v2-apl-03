//package com.oec.wis.dialogs;
//
//import android.Manifest;
//import android.app.Fragment;
//import android.app.ProgressDialog;
//import android.content.Context;
//import android.content.Intent;
//import android.content.SharedPreferences;
//import android.content.pm.PackageManager;
//import android.graphics.Bitmap;
//import android.graphics.Point;
//import android.location.Address;
//import android.location.Geocoder;
//import android.location.Location;
//import android.os.AsyncTask;
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.support.v4.app.ActivityCompat;
//import android.support.v4.app.FragmentActivity;
//import android.support.v7.app.AlertDialog;
//import android.support.v7.app.AppCompatActivity;
//import android.text.Editable;
//import android.util.Log;
//import android.view.Display;
//import android.view.Gravity;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.ImageView;
//import android.widget.ListView;
//import android.widget.PopupWindow;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.android.volley.AuthFailureError;
//import com.android.volley.Response;
//import com.android.volley.VolleyError;
//import com.android.volley.toolbox.JsonObjectRequest;
//import com.google.android.gms.maps.CameraUpdateFactory;
//import com.google.android.gms.maps.GoogleMap;
//import com.google.android.gms.maps.OnMapReadyCallback;
//import com.google.android.gms.maps.SupportMapFragment;
//import com.google.android.gms.maps.model.BitmapDescriptorFactory;
//import com.google.android.gms.maps.model.LatLng;
//import com.google.android.gms.maps.model.Marker;
//import com.google.android.gms.maps.model.MarkerOptions;
//import com.oec.wis.ApplicationController;
//import com.oec.wis.GPSTracker;
//import com.oec.wis.R;
//import com.oec.wis.adapters.Popupadapter;
//import com.oec.wis.adapters.SelectFriendsAdapter;
//import com.oec.wis.adapters.UserLocationListener;
//import com.oec.wis.entities.EntryItem;
//import com.oec.wis.entities.Popupelements;
//import com.oec.wis.entities.SectionItem;
//import com.oec.wis.entities.UserNotification;
//import com.oec.wis.entities.WISAds;
//import com.oec.wis.entities.WISUser;
//import com.oec.wis.tools.TextWatcherAdapter;
//import com.oec.wis.tools.Tools;
//import com.oec.wis.tools.UserLocation;
//import com.orhanobut.dialogplus.DialogPlus;
//import com.orhanobut.dialogplus.OnItemClickListener;
//
//import org.apache.http.HttpEntity;
//import org.apache.http.HttpResponse;
//import org.apache.http.client.ClientProtocolException;
//import org.apache.http.client.HttpClient;
//import org.apache.http.client.methods.HttpGet;
//import org.apache.http.impl.client.DefaultHttpClient;
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.io.BufferedReader;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.InputStreamReader;
//import java.io.UnsupportedEncodingException;
//import java.net.HttpURLConnection;
//import java.net.URL;
//import java.net.URLEncoder;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Locale;
//import java.util.Map;
//import java.util.Set;
//import java.util.TreeMap;
//
//import javax.net.ssl.HttpsURLConnection;
//
///**
// * Created by asareri08 on 13/07/16.
// */
//public class CustomLocationActivity extends AppCompatActivity implements OnMapReadyCallback {
//
//    public static final String PREFS_NAME = "CustomPermission";
//    SharedPreferences settings;
//    View viewPage;
//
//    ArrayList<LatLng> markerPoints;
//    LatLng myLocation = null;
//    LatLng newlocation = null;
//    private GoogleMap googleMap;
//    Bitmap bitmap;
//    Button addMapOption;
//    ArrayList popupList;
//
//    String publocation;
//    Geocoder geocoder;
//    List<Address> addresses;
//
//
//    private Marker publisher;
//    private Marker subscriber;
//
//    Popupadapter simpleAdapter;
//
//
//    PopupWindow popWindow;
//
//    Context context;
//    double lng, lat;
//    String address;
//    //    GPSTracker tracker;
//    double currentLatitude, currentLongitude;
//
//    ImageView close_btn;
//    Button post_status;
//    ImageView snapShot;
//    TextView message;
//
//    List<WISUser> friendList;
//
//    ArrayList items;
//
//
//    ListView groupChatFriendList;
//
//    EditText searchFilter;
//
//    SelectFriendsAdapter selectFriendsAdapter;
//
//
//    String snapShotFilePath;
//
//    double latitude, langitude;
//
//    String currentLocationName;
//
//
//    String curretnLocationAdress;
//
//    String coordinates;
//
//    String advertiserName;
//
//    @Override
//    protected void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.custom_map);
//        setupMapView();
//    }
//
//    public void setupMapView() {
//        loadFriends();
//
//        Boolean locationEnabled = Tools.showGPSDialog(this);
//
//        if (locationEnabled) {
//            settings = this.getSharedPreferences(PREFS_NAME, 0);
//            boolean dialogShown = settings.getBoolean("locationPermissionDialogShown", false);
//
//            if (!dialogShown) {
//                // AlertDialog code here
//                showMapPermissionAlert();
//
//            } else {
//
//                setUPMap();
////                initGps();
//
//            }
//        } else {
//
//            Log.e("gps", "not activated");
//        }
//
//
////        SupportMapFragment map = ((SupportMapFragment) this.getSupportFragmentManager()
////                .findFragmentById(R.id.mapFragmentTwo)).getMap();
//
//    }
//
////    public void initGps(){
////
////        tracker = new GPSTracker(this);
////
////        if (tracker.canGetLocation()) {
////
////            System.out.println("USER LOCATION CORDINATES");
////
////            Log.e("LAT", String.valueOf(tracker.getLatitude()));
////
////            Log.e("LONG", String.valueOf(tracker.getLongitude()));
////
////            currentLatitude = tracker.getLatitude();
////            currentLongitude = tracker.getLongitude();
////        }
////
////        else{
////
////            tracker.showSettingsAlert();
////        }
////    }
//
//    private void loadFriends() {
//        friendList = new ArrayList<>();
//
//
//        findViewById(R.id.loading).setVisibility(View.VISIBLE);
//
//        JSONObject jsonBody = null;
//
//        try {
//            jsonBody = new JSONObject("{\"id_profil\":\"" + Tools.getData(this, "idprofile") + "\"}");
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        Log.i("Url", getString(R.string.server_url) + getString(R.string.friends_meth));
//        JsonObjectRequest req = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.friends_meth), jsonBody,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//
//                        try {
//
//                            if (response.getString("data").contains("Acun amis"))
//                                Toast.makeText(getApplicationContext(), getString(R.string.no_friend), Toast.LENGTH_SHORT).show();
//                            else {
//                                if (response.getString("result").equals("true")) {
//                                    JSONArray data = response.getJSONArray("data");
//                                    Log.i("contacts list", String.valueOf(response));
//
//
//                                    for (int i = 0; i < data.length(); i++) {
//                                        try {
//                                            JSONObject obj = data.getJSONObject(i);
//                                            Log.i("contacts list elemts", String.valueOf(obj.getString("activeNewsFeed")));
////                                            friendList.add(new WISUser(obj.getInt("idprofile"), obj.getString("fullName"),"", obj.getString("photo"), obj.getInt("nbr_amis"),obj.getBoolean("activeNewsFeed")));
//                                            friendList.add(new WISUser(Boolean.FALSE, obj.getString("blocked_by"), obj.getString("objectId"), obj.getString("activeNewsFeed"), obj.getInt("idprofile"), obj.getString("fullName"), obj.getString("photo"), obj.getInt("nbr_amis")));
//
//
//                                        } catch (Exception e) {
//
//                                        }
////
//
//                                    }
//
//
//                                    Map<String, List<WISUser>> map = new HashMap<String, List<WISUser>>();
//
//                                    for (WISUser student : friendList) {
//                                        String key = student.getFullName().substring(0, 1).toUpperCase();
//                                        if (map.containsKey(key)) {
//                                            List<WISUser> list = map.get(key);
//                                            list.add(student);
//
//                                        } else {
//                                            List<WISUser> list = new ArrayList<WISUser>();
//                                            list.add(student);
//                                            map.put(key, list);
//                                        }
//
//                                    }
//
//                                    Map<String, List<WISUser>> orderMap = new TreeMap<String, List<WISUser>>(map);
//
//
//                                    Log.i("keys and values", String.valueOf(orderMap));
//
//                                    Set<String> keyValues = orderMap.keySet();
//                                    String[] arraykeys = keyValues.toArray(new String[keyValues.size()]);
//                                    items = new ArrayList();
//
//                                    for (int i = 0; i < orderMap.size(); i++) {
//
////
//
//                                        if (orderMap.containsKey(arraykeys[i])) {
//                                            String k = arraykeys[i];
////                                            header
//                                            items.add(new SectionItem(k));
//
//
////                                            items
//
//                                            for (WISUser user : map.get(k)) {
//
//                                                items.add(new EntryItem(user));
//                                            }
//
//
//                                        }
//
//                                    }
//
//
//                                    Log.i("hash map key value", String.valueOf(items));
//
//                                    setupPopUPView();
//
//
//                                }
//                            }
//                            findViewById(R.id.loading).setVisibility(View.GONE);
//
//                        } catch (JSONException e) {
//
//                            findViewById(R.id.loading).setVisibility(View.GONE);
//                            //if (getActivity() != null)
//                            //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
//                        }
//
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                findViewById(R.id.loading).setVisibility(View.GONE);
//                //if (getActivity() != null)
//                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<>();
//                headers.put("token", Tools.getData(getApplicationContext(), "token"));
//                headers.put("lang", Tools.getData(getApplicationContext(), "lang_pr"));
//                return headers;
//            }
//        };
//
//        ApplicationController.getInstance().addToRequestQueue(req);
//    }
//
//    public void setUPMap() {
//
//
//        addMapOption = (Button) findViewById(R.id.addMapOption);
//
//        addMapOption.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//////                viewMoreOption();
////
//                CaptureMapScreen();
//
//                //setupPopUPView();
//
//
//            }
//        });
//        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
//                .findFragmentById(R.id.mapFragmentTwo);
//        mapFragment.getMapAsync(this);
//
//        getAdvertiserCoordinates();
//    }
//
//    @Override
//    public void onMapReady(GoogleMap Map) {
//        googleMap = Map;
//
//        if (UserNotification.getLocationSelected() == Boolean.TRUE) {
//
//            DataLongOperationAsynchTask taks = new DataLongOperationAsynchTask();
//            String add = UserNotification.getAddress();
//            address = add;
//            Log.i("request url ", "http://maps.google.com/maps/api/geocode/json?address=" + address + "&sensor=false");
//            try {
//                taks.execute(add);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//        }
//        //if(UserNotification.getIsFromPubView() == Boolean.TRUE)
//        else {
//
//            Log.d("Advertiser", "show advertiser location");
//
////                       System.out.println("current location");
//
//            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//                // TODO: Consider calling
//                //    ActivityCompat#requestPermissions
//                // here to request the missing permissions, and then overriding
//                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//                //                                          int[] grantResults)
//                // to handle the case where the user grants the permission. See the documentation
//                // for ActivityCompat#requestPermissions for more details.
//                return;
//            }
//            googleMap.setMyLocationEnabled(true);
//            googleMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
//                @Override
//                public void onMyLocationChange(Location location) {
//
//
//                    try {
//
//                        if (myLocation == null || newlocation==null) {
//
//
//
//
//
//                            latitude = getIntent().getExtras().getDouble("lat");
//
//                            langitude = getIntent().getExtras().getDouble("lng");
//                            myLocation = new LatLng(latitude, langitude);
//
//                            //myLocation = new LatLng(-33.87365, 151.20689);
//
//
//
//                            currentLatitude = getIntent().getExtras().getDouble("currentlat");
//                            currentLongitude = getIntent().getExtras().getDouble("currentlang");
//                            newlocation = new LatLng(currentLatitude,currentLongitude);
//
//                            //newlocation = new LatLng(-31.952854, 115.857342);
//
//
//
////                        currentLocationName = getMapUrlLink();
//
//
//                            coordinates = String.valueOf(String.valueOf(latitude).concat(",").concat(String.valueOf(langitude)));
//
//                            if(getIntent().getStringExtra("advertiser_name").equals(Tools.getData(getApplicationContext(), "name")))
//                            {
//                                String titlePublisher =getCompleteAddressString(latitude,langitude);
//
//                                Log.e("Publisher",titlePublisher);
//                                String pname = getIntent().getStringExtra("advertiser_name");
//                                if(pname.equals("")||pname==null)
//                                {
//                                    googleMap.addMarker(new MarkerOptions().position(myLocation).title("Publisher").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)).alpha(0.9f)).showInfoWindow();
//                                }
//                                googleMap.addMarker(new MarkerOptions().position(myLocation).title(getIntent().getStringExtra("advertiser_name")).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)).alpha(0.9f)).showInfoWindow();
//                                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myLocation, 14));
//                            }
//                            else {
//                                String titleSubscriber =getCompleteAddressString(currentLatitude,currentLongitude);
//
//                                Log.e("Subscriber",titleSubscriber);
//                                String sname = Tools.getData(getApplicationContext(), "name");
//                                if(sname.equals("")||sname==null)
//                                {
//                                    googleMap.addMarker(new MarkerOptions().position(newlocation).title("Subscriber").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)).alpha(0.9f)).showInfoWindow();
//                                }
//                                googleMap.addMarker(new MarkerOptions().position(newlocation).title(Tools.getData(getApplicationContext(), "name")).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)).alpha(0.7f)).showInfoWindow();
//                                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(newlocation, 14));
//
//                                String titlePublisher =getCompleteAddressString(latitude,langitude);
//
//                                Log.e("Publisher",titlePublisher);
//
//                                String pname = getIntent().getStringExtra("advertiser_name");
//                                if(pname.equals("")||pname==null)
//                                {
//                                    googleMap.addMarker(new MarkerOptions().position(myLocation).title("Publisher").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)).alpha(0.9f)).showInfoWindow();
//                                }
//                                googleMap.addMarker(new MarkerOptions().position(myLocation).title(getIntent().getStringExtra("advertiser_name")).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)).alpha(0.9f)).showInfoWindow();
//                                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myLocation, 14));
//                            }
//
//
//
//
//                            getCityName(latitude, langitude);
//
//
//                        }
//                    }
//                    catch(NullPointerException ex){
//
//                        ex.printStackTrace();
//
//                    }
//                }
//            });
//
//            googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
//                @Override
//                public boolean onMarkerClick(Marker marker) {
//                    return false;
//                }
//            });
//        }
//
//    }
//
//    public void CaptureMapScreen()
//    {
//        GoogleMap.SnapshotReadyCallback callback = new GoogleMap.SnapshotReadyCallback() {
//            Bitmap bitmap;
//
//            @Override
//            public void onSnapshotReady(Bitmap snapshot) {
//                // TODO Auto-generated method stub
//                bitmap = snapshot;
//
//                Log.d("map snap", String.valueOf(bitmap));
//
//                snapShotFilePath = "/mnt/sdcard/"
//                        + "MyMapScreen" + System.currentTimeMillis()
//                        + ".jpg";
//
//                try {
//                    FileOutputStream out = new FileOutputStream( snapShotFilePath);
//
//                    // above "/mnt ..... png" => is a storage path (where image will be stored) + name of image you can customize as per your Requirement
//
//                    Log.e("mappath", String.valueOf(out));
//
////                    File file = new (filePath);
//
//                    Toast toast= Toast.makeText(getApplicationContext(),
//                            (R.string.capture_success), Toast.LENGTH_SHORT);
//                    toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 0, 0);
//                    toast.show();
//
//
//
//
//
//
//
//
//                    Log.e("map filepath",snapShotFilePath);
//
//
//
//                    bitmap.compress(Bitmap.CompressFormat.PNG, 90, out);
//
//
////                    shareToAmis(view,bitmap);
//                    viewMoreOption(bitmap);
//
//
//
//
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        };
//
//        googleMap.snapshot(callback);
//
//
//    }
//    // complete address
//
//    public void viewMoreOption(final Bitmap snapImageView){
//
//        popupList = new ArrayList<>();
//
//        popupList.add(new Popupelements(R.drawable.copy_chat, getString(R.string.wiscontact)));
//        popupList.add(new Popupelements(R.drawable.share_profile, getString(R.string.actuality)));
//        simpleAdapter = new Popupadapter(this, false, popupList);
//
//        DialogPlus dialog = DialogPlus.newDialog(this)
//                .setAdapter(simpleAdapter)
//                .setOnItemClickListener(new OnItemClickListener() {
//                    @Override
//                    public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
//
//
//                        if(position == 0){
//                            dialog.dismiss();
//
//                            shareToAmis(view,snapImageView);
////                            CaptureMapScreen();
//
//
//
//
//                        }
//                        else if(position ==1){
//                            dialog.dismiss();
//
//
//                            shareLocationToWisActuality();
//
//
//
//
//                        }
//                        else if(position ==2){
//                            dialog.dismiss();
//                            Log.i("WIS chat","chat click");
//
//
//                        }
//                        else if(position ==3){
//                            dialog.dismiss();
//
//
//                        }
//
//                    }
//                })
//                .setExpanded(false)  // This will enable the expand feature, (similar to android L share dialog)
//                .create();
//        dialog.show();
//
//
//
//    }
//    public void shareLocationToWisActuality(){
//
//
//
//
//        sendCurrentLocationToSerer(coordinates,currentLocationName);
//
//
//    }
//    public void sendCurrentLocationToSerer(String cooridinates, String name) {
//
//
//
//        findViewById(R.id.loading).setVisibility(View.VISIBLE);
//
//        JSONObject jsonBody = null;
//
//        try {
////            jsonBody = new JSONObject("{\"user_id\":\"" + Tools.getData(getActivity(), "idprofile") + "\",coordinate\":\"" +cooridinates+ "\",location_name\":\"" + name+ "\"}");
//            jsonBody = new JSONObject();
//            jsonBody.put("user_id", Tools.getData(this, "idprofile"));
//            jsonBody.put("coordinates", cooridinates);
//            jsonBody.put("location_name", name);
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        Log.i("jsone body%@", jsonBody.toString());
//        JsonObjectRequest reqActu = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.share_location), jsonBody,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        findViewById(R.id.loading).setVisibility(View.INVISIBLE);
//                        Log.i("map response", response.toString());
//
//                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.share_success_msg), Toast.LENGTH_SHORT).show();
//
//
//
//
//
////                        tracker.stopUsingGPS();
//
//
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
////                tracker.stopUsingGPS();
//                findViewById(R.id.loading).setVisibility(View.INVISIBLE);
//
//                Toast.makeText(getApplicationContext(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
//
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<>();
//                headers.put("token", Tools.getData(getApplicationContext(), "token"));
//                headers.put("lang", Tools.getData(getApplicationContext(), "lang_pr"));
//                return headers;
//            }
//        };
//
//
//        ApplicationController.getInstance().addToRequestQueue(reqActu);
//
//
//    }
//    public void shareToAmis(final View v,Bitmap snapedImage) {
//        // StartAction
//
//
//
//
//        LayoutInflater layoutInflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//
//        // inflate the custom popup layout
//        final View inflatedView = layoutInflater.inflate(R.layout.wis_amis_view, null, false);
//
//
//        // get device size
//        Display display = getWindowManager().getDefaultDisplay();
//        final Point size = new Point();
//        display.getSize(size);
////        mDeviceHeight = size.y;
//
//
//        // set height depends on the device size
//        popWindow = new PopupWindow(inflatedView, size.x - 35, size.y - 300, true);
//
//        popWindow.setTouchable(true);
//
//
//        popWindow.setOutsideTouchable(false);
//
//        popWindow.setAnimationStyle(R.style.animationName);
//
//
//
//        inflatedView.setAlpha(0.5f);
//
//
//
//        popWindow.showAtLocation(v, Gravity.CENTER, 0, 80);
//
//
//
//
//        close_btn = (ImageView) inflatedView.findViewById(R.id.close_btn);
//        post_status = (Button) inflatedView.findViewById(R.id.post_status);
//        snapShot = (ImageView)inflatedView.findViewById(R.id.snapShot);
//
//        groupChatFriendList = (ListView)inflatedView.findViewById(R.id.groupfriendList);
//        searchFilter = (EditText)inflatedView.findViewById(R.id.searchFilter);
//
//        message = (TextView)inflatedView.findViewById(R.id.map_link);
//
//
//        message.setText(getMapUrlLink());
//
//        currentLocationName = getMapUrlLink();
//
//
//        loadFriendsList(groupChatFriendList);
//
//
//
//
//        snapShot.setImageBitmap(snapedImage);
//
//
//
//        close_btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                popWindow.dismiss();
//                inflatedView.setAlpha(1);
//            }
//        });
//
//
//
//        post_status.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//
//                if (!message.getText().toString().equals("")) {
//                    popWindow.dismiss();
//
//
//                    selectFriendsAdapter.transferTextToContacts("map_link",String.valueOf(""),message.getText().toString());
//
//                    inflatedView.setAlpha(1);
//
//
//
//
//
//
//                }
//
//
//            }
//        });
//
//
//
//    }
//    private void loadFriendsList(ListView groupChatFriendList){
//
//        selectFriendsAdapter = new SelectFriendsAdapter(getApplicationContext(),friendList);
//        groupChatFriendList.setAdapter(selectFriendsAdapter);
//        selectFriendsAdapter.notifyDataSetChanged();
//        loadFilterConfig(selectFriendsAdapter);
//
//
//    }
//    private void loadFilterConfig(final SelectFriendsAdapter adapter){
//        searchFilter.addTextChangedListener(new TextWatcherAdapter(){
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//                super.beforeTextChanged(s, start, count, after);
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                super.onTextChanged(s, start, before, count);
//                adapter.getFilter().filter(s.toString());
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//                super.afterTextChanged(s);
//
//            }
//        });
//    }
//    public String getMapUrlLink(){
//
//        String locationUrl=null;
//        try {
//            locationUrl = curretnLocationAdress.concat(" http://www.google.com/maps/place/".concat(coordinates));
//        }
//        catch (NullPointerException e)
//        {
//            return null;
//        }
//        return locationUrl;
//    }
//
//    public void getCityName(final double latitude, final double longitude) {
//
//        final ProgressDialog dialog = new ProgressDialog(this);
//        dialog.setMessage("Please wait...");
//        dialog.setCanceledOnTouchOutside(false);
//        dialog.show();
//
//        new UserLocation(this, new UserLocationListener() {
//            @Override
//            public void updateUserLocationInfo(String formatted_address) {
//
//                dialog.dismiss();
//
//
//                curretnLocationAdress = formatted_address;
//
//                Log.e("return response",formatted_address);
//            }
//        }).execute(String.valueOf(latitude),String.valueOf(longitude));
//
//
//    }
//
//    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
//
//
//        String strAdd = "";
//        geocoder = new Geocoder(this, Locale.getDefault());
//        try {
//            addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
//            if (addresses != null) {
//                Address returnedAddress = addresses.get(0);
//                StringBuilder strReturnedAddress = new StringBuilder("");
//
//                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
//                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
//                }
//                strAdd = strReturnedAddress.toString();
//                Log.i("address",strAdd);
//
//            } else {
//                Log.i("address","no address found");
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//            Log.i("Current loction", "Canont get Address!");
//        }
//        return strAdd;
//    }
//    public void setupPopUPView() {
//
//        popupList = new ArrayList<>();
//
//        popupList.add(new Popupelements(R.drawable.copy_chat, getString(R.string.wiscontact)));
//        popupList.add(new Popupelements(R.drawable.share_profile, getString(R.string.actuality)));
//
////        popupList.add(new Popupelements(R.drawable.copy_link, getString(R.string.screenShot)));
//        //popupList.add(new Popupelements(R.drawable.copy_chat, getString(R.string.wiscontact)));
//        //popupList.add(new Popupelements(R.drawable.share_profile,"Pub Music"));
//
//        simpleAdapter = new Popupadapter(this, false, popupList);
//
//        DialogPlus dialog = DialogPlus.newDialog(this)
//                .setAdapter(simpleAdapter)
//                .setOnItemClickListener(new OnItemClickListener() {
//                    @Override
//                    public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
//
//
//                        if(position == 0){
//                            dialog.dismiss();
//
//
//
//
//
//                        }
//                        else if(position ==1){
//                            dialog.dismiss();
////
////                            Toast.makeText(getApplicationContext(),"location added to pub", Toast.LENGTH_SHORT).show();
////                            onBackPressed();
//
//
//                            //shareLocationToWisActuality();
//
//
//
//
//                        }
//                        else if(position ==2){
//                            dialog.dismiss();
//                            Log.i("WIS chat","chat click");
//
//
//                        }
//
//                    }
//                })
//                .setExpanded(false)  // This will enable the expand feature, (similar to android L share dialog)
//                .create();
//        dialog.show();
//
//    }
////    private class DataLongOperationAsynchTask extends AsyncTask<String, Void, String[]> {
////        ProgressDialog dialog = new ProgressDialog(CustomLocationActivity.this);
////        @Override
////        protected void onPreExecute() {
////            super.onPreExecute();
////            dialog.setMessage("Please wait...");
////            dialog.setCanceledOnTouchOutside(false);
////            dialog.show();
////        }
////
////        @Override
////        protected String[] doInBackground(String... params) {
////            String response;
////            try {
////                String addr =params[0];
////                response = getLatLongByURL("http://maps.google.com/maps/api/geocode/json?address="+addr+"&sensor=false");
////
////                return new String[]{response};
////            } catch (Exception e) {
////                return new String[]{"error"};
////            }
////        }
////
////        @Override
////        protected void onPostExecute(String... result) {
////            try {
////                JSONObject jsonObject = new JSONObject(result[0]);
////
////                System.out.println("-----");
////                System.out.println(jsonObject.toString());
////
////                lng = ((JSONArray)jsonObject.get("results")).getJSONObject(0)
////                        .getJSONObject("geometry").getJSONObject("location")
////                        .getDouble("lng");
////
////                lat = ((JSONArray)jsonObject.get("results")).getJSONObject(0)
////                        .getJSONObject("geometry").getJSONObject("location")
////                        .getDouble("lat");
////
////                Log.d("latitude", "" + lat);
////                Log.d("longitude", "" + lng);
////                final LatLng testAddr = new LatLng(lat,lng);
////
////                googleMap.addMarker(new MarkerOptions().position(new LatLng(lat,lng)).title(address));
////                googleMap.moveCamera(CameraUpdateFactory.newLatLng(testAddr));
////            } catch (JSONException e) {
////                e.printStackTrace();
////            }
////            if (dialog.isShowing()) {
////                dialog.dismiss();
////            }
////        }
////    }
//
//    private class DataLongOperationAsynchTask extends AsyncTask<String, Void, LatLng> {
//        ProgressDialog dialog = new ProgressDialog(CustomLocationActivity.this);
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            dialog.setMessage("Please wait...");
//            dialog.setCanceledOnTouchOutside(false);
//            dialog.show();
//        }
//
//        @Override
//        protected LatLng doInBackground(String... params) {
//            JSONObject response1;
//            LatLng responsData = null;
//            try {
//                responsData = getLocationFromString(params[0]);
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//
//            return responsData;
//
//        }
//
//
//        @Override
//        protected void onPostExecute(LatLng result) {
//
//            Log.e("resltaas", String.valueOf(result));
//
//
//
//
//
////                JSONObject jsonObject = new JSONObject(result);
////
////                lng = ((JSONArray)jsonObject.get("results")).getJSONObject(0)
////                        .getJSONObject("geometry").getJSONObject("location")
////                        .getDouble("lng");
////
////               lat = ((JSONArray)jsonObject.get("results")).getJSONObject(0)
////                        .getJSONObject("geometry").getJSONObject("location")
////                        .getDouble("lat");
////
////                Log.d("latitude", "" + lat);
////                Log.d("longitude", "" + lng);
////
////
////                Log.i("user address",address);
//
//
//
////            latitude = result.latitude;
////
////            langitude =result.longitude;
//
//            latitude = getIntent().getExtras().getDouble("lat");
//
//            langitude = getIntent().getExtras().getDouble("lng");
//
//            getCityName(latitude,langitude);
//
//            final LatLng testAddr = new LatLng(lat,lng);
//
//
//
//
////                currentLocationName = getMapUrlLink();
//
//            coordinates = String.valueOf(String.valueOf(latitude).concat(",").concat(String.valueOf(langitude)));
//
//            String title =getCompleteAddressString(latitude,langitude);
//
//            googleMap.addMarker(new MarkerOptions().position(new LatLng(latitude,langitude)).title(title)).showInfoWindow();
////                googleMap.moveCamera(CameraUpdateFactory.newLatLng(testAddr));
//            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(result,
//                    14));
//
//            if (dialog.isShowing()) {
//                dialog.dismiss();
//            }
//        }
//    }
//
//    public static LatLng getLocationFromString(String address)
//            throws JSONException {
//
//        HttpGet httpGet = null;
//        try {
//            httpGet = new HttpGet(
//                    "http://maps.google.com/maps/api/geocode/json?address="
//                            + URLEncoder.encode(address, "UTF-8") + "&ka&sensor=false");
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }
//        HttpClient client = new DefaultHttpClient();
//        HttpResponse response;
//        StringBuilder stringBuilder = new StringBuilder();
//
//        try {
//            response = client.execute(httpGet);
//            HttpEntity entity = response.getEntity();
//            InputStream stream = entity.getContent();
//            int b;
//            while ((b = stream.read()) != -1) {
//                stringBuilder.append((char) b);
//            }
//        } catch (ClientProtocolException e) {
//        } catch (IOException e) {
//        }
//
//        JSONObject jsonObject = new JSONObject(stringBuilder.toString());
//
//        double lng = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
//                .getJSONObject("geometry").getJSONObject("location")
//                .getDouble("lng");
//
//        double lat = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
//                .getJSONObject("geometry").getJSONObject("location")
//                .getDouble("lat");
//
//        return new LatLng(lat, lng);
//    }
//
//
//
//    public String getLatLongByURL(String requestURL) {
//        URL url;
//        String response = "";
//        try {
//            url = new URL(requestURL);
//
//            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//            conn.setReadTimeout(15000);
//            conn.setConnectTimeout(15000);
//            conn.setRequestMethod("POST");
//            conn.setDoInput(true);
//            conn.setRequestProperty("Content-Type",
//                    "application/x-www-form-urlencoded");
//            conn.setDoOutput(true);
//            int responseCode = conn.getResponseCode();
//
//            if (responseCode == HttpsURLConnection.HTTP_OK) {
//                String line;
//                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
//                while ((line = br.readLine()) != null) {
//                    response += line;
//                }
//            } else {
//                response = "";
//            }
//
//        }catch (java.net.SocketTimeoutException e) {
//
//            Toast.makeText(getApplicationContext(), getString(R.string.connection_timeout), Toast.LENGTH_SHORT).show();
//            e.printStackTrace();
//        }
//
//        catch (Exception e) {
//            e.printStackTrace();
//        }
//        return response;
//    }
//    @Override
//    public void onPause() {
//        super.onPause();
//
//
//        UserNotification.setLocationSelected(Boolean.FALSE);
//
//        if(UserNotification.getIsFromPubView()){
//
//            UserNotification.setIsFromPubView(Boolean.FALSE);
//        }
//    }
//
//    private void showMapPermissionAlert(){
//        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        builder.setCancelable(false).setView(R.layout.custom_permission_alert);
//
//        final AlertDialog alert = builder.create();
//        alert.show();
//        alert.findViewById(R.id.cancel_alert).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                alert.dismiss();
//            }
//        });
//        alert.findViewById(R.id.accept_alert).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                SharedPreferences.Editor editor = settings.edit();
//                editor.putBoolean("locationPermissionDialogShown", true);
//                editor.commit();
//                alert.dismiss();
//                setUPMap();
//            }
//        });
//    }
//
//
//
//    public void getAdvertiserCoordinates(){
//
//        final ProgressDialog dialog = new ProgressDialog(CustomLocationActivity.this);
//
//        dialog.setMessage("Please wait...");
//        dialog.setCanceledOnTouchOutside(false);
//        dialog.show();
//
//
//
//        int advertiserId= getIntent().getIntExtra("advertiser_id",0);
//        advertiserName = getIntent().getStringExtra("advertiser_name");
//
//        JSONObject jsonBody = new JSONObject();
//
//
//        try {
//            jsonBody.put("user_id",Tools.getData(CustomLocationActivity.this, "idprofile"));
//
//            jsonBody.put("advertiser_Id",advertiserId);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//
//        Log.d("json req", String.valueOf(jsonBody));
//
//        JsonObjectRequest reqActu = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.user_coordinats), jsonBody,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        dialog.dismiss();
//                        try {
//                            Log.i("map", String.valueOf(response));
//                            if (response.getBoolean("result")) {
//                                JSONObject obj = response.getJSONObject("data");
//
////                                latitude = obj.getDouble("latitude");
////                                langitude = obj.getDouble("langitude");
//
//
//                                latitude = getIntent().getExtras().getDouble("lat");
//
//                                langitude = getIntent().getExtras().getDouble("lng");
//
//
//                                getCityName(latitude,langitude);
//
//
//
//                            }
//
//
//                        } catch (JSONException e) {
//
//                            dialog.dismiss();
//
//                            //Toast.makeText(PubDetails.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
//                        }
//
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//
//                dialog.dismiss();
////                findViewById(R.id.loading).setVisibility(View.GONE);
//                //Toast.makeText(PubDetails.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<>();
//                headers.put("token", Tools.getData(CustomLocationActivity.this, "token"));
//                headers.put("lang", Tools.getData(CustomLocationActivity.this, "lang_pr"));
//                return headers;
//            }
//        };
//
//        ApplicationController.getInstance().addToRequestQueue(reqActu);
//
//
//
//
//
//    }
//
//    @Override
//    public void onBackPressed() {
//        super.onBackPressed();
//        finish();
//    }
//
//
//    public void updateMarker(){
//
//
//    }
//
//}



package com.oec.wis.dialogs;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.oec.wis.ApplicationController;
import com.oec.wis.R;
import com.oec.wis.adapters.Popupadapter;
import com.oec.wis.adapters.SelectFriendsAdapter;
import com.oec.wis.adapters.UserLocationListener;
import com.oec.wis.entities.EntryItem;
import com.oec.wis.entities.Popupelements;
import com.oec.wis.entities.SectionItem;
import com.oec.wis.entities.UserNotification;
import com.oec.wis.entities.WISAds;
import com.oec.wis.entities.WISUser;
import com.oec.wis.tools.TextWatcherAdapter;
import com.oec.wis.tools.Tools;
import com.oec.wis.tools.UserLocation;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnItemClickListener;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by asareri08 on 13/07/16.
 */
public class CustomLocationActivity extends AppCompatActivity implements OnMapReadyCallback {

    public static final String PREFS_NAME = "CustomPermission";
    SharedPreferences settings;
    View viewPage;
    LatLng myLocation = null;
    private GoogleMap googleMap;
    Bitmap bitmap;
    Button addMapOption;
    ArrayList popupList;

    String publocation;
    Geocoder geocoder;
    List<Address> addresses;

    Popupadapter simpleAdapter;



    PopupWindow popWindow;

    Context context;
    double lng,lat;
    String address;

    ImageView close_btn;
    Button post_status;
    ImageView snapShot;
    TextView message;

    List<WISUser> friendList;

    ArrayList items;


    ListView groupChatFriendList;

    EditText searchFilter;

    SelectFriendsAdapter selectFriendsAdapter;


    String snapShotFilePath;

    double latitude,langitude;

    String currentLocationName;


    String curretnLocationAdress;

    String coordinates;

    String advertiserName;
    Typeface font;
    TextView locationTV;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.custom_map);
        setupMapView();
    }

    public void setupMapView(){
        loadFriends();

        Boolean locationEnabled = Tools.showGPSDialog(this);

        if(locationEnabled){
            settings = this.getSharedPreferences(PREFS_NAME, 0);
            boolean dialogShown = settings.getBoolean("locationPermissionDialogShown", false);

            if (!dialogShown) {
                // AlertDialog code here
                showMapPermissionAlert();

            }else{

                setUPMap();

            }
        }else{

            Log.e("gps","not activated");
        }



//        SupportMapFragment map = ((SupportMapFragment) this.getSupportFragmentManager()
//                .findFragmentById(R.id.mapFragmentTwo)).getMap();

    }

    private void loadFriends() {
        friendList = new ArrayList<>();


        findViewById(R.id.loading).setVisibility(View.VISIBLE);

        JSONObject jsonBody = null;

        try {
            jsonBody = new JSONObject("{\"id_profil\":\"" + Tools.getData(this, "idprofile") + "\"}");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.i("Url", getString(R.string.server_url) + getString(R.string.friends_meth));
        JsonObjectRequest req = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.friends_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {

                            if (response.getString("data").contains("Acun amis"))
                                Toast.makeText(getApplicationContext(), getString(R.string.no_friend), Toast.LENGTH_SHORT).show();
                            else {
                                if (response.getString("result").equals("true")) {
                                    JSONArray data = response.getJSONArray("data");
                                    Log.i("contacts list", String.valueOf(response));






                                    for (int i = 0; i <data.length(); i++) {
                                        try {
                                            JSONObject obj = data.getJSONObject(i);
                                            Log.i("contacts list elemts", String.valueOf(obj.getString("activeNewsFeed")));
//                                            friendList.add(new WISUser(obj.getInt("idprofile"), obj.getString("fullName"),"", obj.getString("photo"), obj.getInt("nbr_amis"),obj.getBoolean("activeNewsFeed")));
                                            friendList.add(new WISUser(Boolean.FALSE,obj.getString("blocked_by"),obj.getString("objectId"),obj.getString("activeNewsFeed"),obj.getInt("idprofile"), obj.getString("fullName"),obj.getString("photo"), obj.getInt("nbr_amis")));


                                        } catch (Exception e) {

                                        }
//

                                    }




                                    Map<String, List<WISUser>> map = new HashMap<String, List<WISUser>>();

                                    for (WISUser student : friendList) {
                                        String key  = student.getFullName().substring(0,1).toUpperCase();
                                        if(map.containsKey(key)){
                                            List<WISUser> list = map.get(key);
                                            list.add(student);

                                        }else{
                                            List<WISUser> list = new ArrayList<WISUser>();
                                            list.add(student);
                                            map.put(key, list);
                                        }

                                    }

                                    Map<String, List<WISUser>> orderMap = new TreeMap<String,List<WISUser>>(map);


                                    Log.i("keys and values", String.valueOf(orderMap));

                                    Set<String> keyValues = orderMap.keySet();
                                    String [] arraykeys = keyValues.toArray(new String[keyValues.size()]);
                                    items = new ArrayList();

                                    for(int i =0;i<orderMap.size();i++){

//

                                        if(orderMap.containsKey(arraykeys[i])){
                                            String k = arraykeys[i];
//                                            header
                                            items.add(new SectionItem(k));




//                                            items

                                            for (WISUser user:map.get(k)) {

                                                items.add(new EntryItem(user));
                                            }



                                        }

                                    }







                                    Log.i("hash map key value", String.valueOf(items));

                                    setupPopUPView();









                                }
                            }
                            findViewById(R.id.loading).setVisibility(View.GONE);

                        } catch (JSONException e) {

                            findViewById(R.id.loading).setVisibility(View.GONE);
                            //if (getActivity() != null)
                            //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                findViewById(R.id.loading).setVisibility(View.GONE);
                //if (getActivity() != null)
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getApplicationContext(), "token"));
                headers.put("lang", Tools.getData(getApplicationContext(), "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(req);
    }

    public void setUPMap(){


        addMapOption = (Button)findViewById(R.id.addMapOption);

        addMapOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

////                viewMoreOption();
//
                CaptureMapScreen();

                //setupPopUPView();




            }
        });
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapFragmentTwo);
        mapFragment.getMapAsync(this);

        getAdvertiserCoordinates();
    }

    @Override
    public void onMapReady(GoogleMap Map) {
        googleMap = Map;

        if(UserNotification.getLocationSelected() == Boolean.TRUE) {

            DataLongOperationAsynchTask taks = new DataLongOperationAsynchTask();
            String add = UserNotification.getAddress();
            address = add;
            Log.i("request url ", "http://maps.google.com/maps/api/geocode/json?address=" + address + "&sensor=false");
            try{
                taks.execute(add);
            }
            catch (Exception e){
                e.printStackTrace();
            }

        }
        //if(UserNotification.getIsFromPubView() == Boolean.TRUE)
        else{

            Log.d("Advertiser","show advertiser location");

//                       System.out.println("current location");

            googleMap.setMyLocationEnabled(true);
            googleMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
                @Override
                public void onMyLocationChange(Location location) {


                    try {

                        if (myLocation == null) {





                            latitude = getIntent().getExtras().getDouble("lat");

                            langitude = getIntent().getExtras().getDouble("lng");
                            myLocation = new LatLng(latitude, langitude);

//                        currentLocationName = getMapUrlLink();


                            coordinates = String.valueOf(String.valueOf(latitude).concat(",").concat(String.valueOf(langitude)));

                            String title =getCompleteAddressString(latitude,langitude);
                            googleMap.addMarker(new MarkerOptions().position(myLocation).title(title)).showInfoWindow();
//                        googleMap.moveCamera(CameraUpdateFactory.newLatLng(myLocation));

                            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myLocation,
                                    14));


                            getCityName(latitude, langitude);


                        }
                    }
                    catch(NullPointerException ex){

                        ex.printStackTrace();

                    }
                }
            });
        }

    }

    public void CaptureMapScreen()
    {
        GoogleMap.SnapshotReadyCallback callback = new GoogleMap.SnapshotReadyCallback() {
            Bitmap bitmap;

            @Override
            public void onSnapshotReady(Bitmap snapshot) {
                // TODO Auto-generated method stub
                bitmap = snapshot;

                Log.d("map snap", String.valueOf(bitmap));

                snapShotFilePath = "/mnt/sdcard/"
                        + "MyMapScreen" + System.currentTimeMillis()
                        + ".jpg";

                try {
                    FileOutputStream out = new FileOutputStream( snapShotFilePath);

                    // above "/mnt ..... png" => is a storage path (where image will be stored) + name of image you can customize as per your Requirement

                    Log.e("mappath", String.valueOf(out));

//                    File file = new (filePath);

                    Toast toast= Toast.makeText(getApplicationContext(),
                            (R.string.capture_success), Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 0, 0);
                    toast.show();








                    Log.e("map filepath",snapShotFilePath);



                    bitmap.compress(Bitmap.CompressFormat.PNG, 90, out);


//                    shareToAmis(view,bitmap);
                    viewMoreOption(bitmap);





                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

        googleMap.snapshot(callback);


    }
    // complete address

    public void viewMoreOption(final Bitmap snapImageView){

        popupList = new ArrayList<>();

        popupList.add(new Popupelements(R.drawable.copy_chat, getString(R.string.wiscontact)));
        popupList.add(new Popupelements(R.drawable.share_profile, getString(R.string.actuality)));
        simpleAdapter = new Popupadapter(this, false, popupList);

        DialogPlus dialog = DialogPlus.newDialog(this)
                .setAdapter(simpleAdapter)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View view, int position) {


                        if(position == 0){
                            dialog.dismiss();

                            shareToAmis(view,snapImageView);
//                            CaptureMapScreen();




                        }
                        else if(position ==1){
                            dialog.dismiss();


                            shareLocationToWisActuality();




                        }
                        else if(position ==2){
                            dialog.dismiss();
                            Log.i("WIS chat","chat click");


                        }
                        else if(position ==3){
                            dialog.dismiss();


                        }

                    }
                })
                .setExpanded(false)  // This will enable the expand feature, (similar to android L share dialog)
                .create();
        dialog.show();



    }
    public void shareLocationToWisActuality(){




        sendCurrentLocationToSerer(coordinates,currentLocationName);


    }
    public void sendCurrentLocationToSerer(String cooridinates, String name) {



        findViewById(R.id.loading).setVisibility(View.VISIBLE);

        JSONObject jsonBody = null;

        try {
//            jsonBody = new JSONObject("{\"user_id\":\"" + Tools.getData(getActivity(), "idprofile") + "\",coordinate\":\"" +cooridinates+ "\",location_name\":\"" + name+ "\"}");
            jsonBody = new JSONObject();
            jsonBody.put("user_id", Tools.getData(this, "idprofile"));
            jsonBody.put("coordinates", cooridinates);
            jsonBody.put("location_name", name);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("jsone body%@", jsonBody.toString());
        JsonObjectRequest reqActu = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.share_location), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        findViewById(R.id.loading).setVisibility(View.INVISIBLE);
                        Log.i("map response", response.toString());

                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.share_success_msg), Toast.LENGTH_SHORT).show();





//                        tracker.stopUsingGPS();


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                tracker.stopUsingGPS();
                findViewById(R.id.loading).setVisibility(View.INVISIBLE);

                Toast.makeText(getApplicationContext(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getApplicationContext(), "token"));
                headers.put("lang", Tools.getData(getApplicationContext(), "lang_pr"));
                return headers;
            }
        };


        ApplicationController.getInstance().addToRequestQueue(reqActu);


    }
    public void shareToAmis(final View v,Bitmap snapedImage) {
        // StartAction




        LayoutInflater layoutInflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // inflate the custom popup layout
        final View inflatedView = layoutInflater.inflate(R.layout.wis_amis_view, null, false);


        // get device size
        Display display = getWindowManager().getDefaultDisplay();
        final Point size = new Point();
        display.getSize(size);
//        mDeviceHeight = size.y;


        // set height depends on the device size
        popWindow = new PopupWindow(inflatedView, size.x - 35, size.y - 300, true);

        popWindow.setTouchable(true);


        popWindow.setOutsideTouchable(false);

        popWindow.setAnimationStyle(R.style.animationName);



//        inflatedView.setAlpha(0.5f);



        popWindow.showAtLocation(v, Gravity.CENTER, 0, 80);




        close_btn = (ImageView) inflatedView.findViewById(R.id.close_btn);
        post_status = (Button) inflatedView.findViewById(R.id.post_status);
        snapShot = (ImageView)inflatedView.findViewById(R.id.snapShot);

        groupChatFriendList = (ListView)inflatedView.findViewById(R.id.groupfriendList);
        searchFilter = (EditText)inflatedView.findViewById(R.id.searchFilter);

        message = (TextView)inflatedView.findViewById(R.id.map_link);
        locationTV =(TextView) inflatedView.findViewById(R.id.locationTV);




        message.setText(getMapUrlLink());

        currentLocationName = getMapUrlLink();


        loadFriendsList(groupChatFriendList);

        try{
            font=Typeface.createFromAsset(this.getAssets(),"fonts/Harmattan-R.ttf");
            message.setTypeface(font);
            searchFilter.setTypeface(font);
            post_status.setTypeface(font);
            locationTV.setTypeface(font);

        }catch (NullPointerException ex){
            Log.d("Exce",ex.getMessage());
        }


        snapShot.setImageBitmap(snapedImage);



        close_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popWindow.dismiss();
                inflatedView.setAlpha(1);
            }
        });



        post_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (!message.getText().toString().equals("")) {
                    popWindow.dismiss();


                    selectFriendsAdapter.transferTextToContacts("map_link",String.valueOf(""),message.getText().toString());

                    inflatedView.setAlpha(1);






                }


            }
        });



    }
    private void loadFriendsList(ListView groupChatFriendList){

        selectFriendsAdapter = new SelectFriendsAdapter(getApplicationContext(),friendList);
        groupChatFriendList.setAdapter(selectFriendsAdapter);
        selectFriendsAdapter.notifyDataSetChanged();
        loadFilterConfig(selectFriendsAdapter);


    }
    private void loadFilterConfig(final SelectFriendsAdapter adapter){
        searchFilter.addTextChangedListener(new TextWatcherAdapter(){
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                super.beforeTextChanged(s, start, count, after);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                super.onTextChanged(s, start, before, count);
                adapter.getFilter().filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
                super.afterTextChanged(s);

            }
        });
    }
    public String getMapUrlLink(){

        String locationUrl=null;
        try {
            locationUrl = curretnLocationAdress.concat(" http://www.google.com/maps/place/".concat(coordinates));
        }
        catch (NullPointerException e)
        {
            return null;
        }
        return locationUrl;
    }

    public void getCityName(final double latitude, final double longitude) {

        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage("Please wait...");
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();

        new UserLocation(this, new UserLocationListener() {
            @Override
            public void updateUserLocationInfo(String formatted_address) {

                dialog.dismiss();


                curretnLocationAdress = formatted_address;

                Log.e("return response",formatted_address);
            }
        }).execute(String.valueOf(latitude),String.valueOf(longitude));


    }

    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {


        String strAdd = "";
        geocoder = new Geocoder(this, Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
                Log.i("address",strAdd);

            } else {
                Log.i("address","no address found");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.i("Current loction", "Canont get Address!");
        }
        return strAdd;
    }
    public void setupPopUPView() {

        popupList = new ArrayList<>();

        popupList.add(new Popupelements(R.drawable.copy_chat, getString(R.string.wiscontact)));
        popupList.add(new Popupelements(R.drawable.share_profile, getString(R.string.actuality)));

//        popupList.add(new Popupelements(R.drawable.copy_link, getString(R.string.screenShot)));
        //popupList.add(new Popupelements(R.drawable.copy_chat, getString(R.string.wiscontact)));
        //popupList.add(new Popupelements(R.drawable.share_profile,"Pub Music"));

        simpleAdapter = new Popupadapter(this, false, popupList);

        DialogPlus dialog = DialogPlus.newDialog(this)
                .setAdapter(simpleAdapter)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View view, int position) {


                        if(position == 0){
                            dialog.dismiss();





                        }
                        else if(position ==1){
                            dialog.dismiss();
//
//                            Toast.makeText(getApplicationContext(),"location added to pub", Toast.LENGTH_SHORT).show();
//                            onBackPressed();


                            //shareLocationToWisActuality();




                        }
                        else if(position ==2){
                            dialog.dismiss();
                            Log.i("WIS chat","chat click");


                        }

                    }
                })
                .setExpanded(false)  // This will enable the expand feature, (similar to android L share dialog)
                .create();
        dialog.show();

    }
//    private class DataLongOperationAsynchTask extends AsyncTask<String, Void, String[]> {
//        ProgressDialog dialog = new ProgressDialog(CustomLocationActivity.this);
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            dialog.setMessage("Please wait...");
//            dialog.setCanceledOnTouchOutside(false);
//            dialog.show();
//        }
//
//        @Override
//        protected String[] doInBackground(String... params) {
//            String response;
//            try {
//                String addr =params[0];
//                response = getLatLongByURL("http://maps.google.com/maps/api/geocode/json?address="+addr+"&sensor=false");
//
//                return new String[]{response};
//            } catch (Exception e) {
//                return new String[]{"error"};
//            }
//        }
//
//        @Override
//        protected void onPostExecute(String... result) {
//            try {
//                JSONObject jsonObject = new JSONObject(result[0]);
//
//                System.out.println("-----");
//                System.out.println(jsonObject.toString());
//
//                lng = ((JSONArray)jsonObject.get("results")).getJSONObject(0)
//                        .getJSONObject("geometry").getJSONObject("location")
//                        .getDouble("lng");
//
//                lat = ((JSONArray)jsonObject.get("results")).getJSONObject(0)
//                        .getJSONObject("geometry").getJSONObject("location")
//                        .getDouble("lat");
//
//                Log.d("latitude", "" + lat);
//                Log.d("longitude", "" + lng);
//                final LatLng testAddr = new LatLng(lat,lng);
//
//                googleMap.addMarker(new MarkerOptions().position(new LatLng(lat,lng)).title(address));
//                googleMap.moveCamera(CameraUpdateFactory.newLatLng(testAddr));
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//            if (dialog.isShowing()) {
//                dialog.dismiss();
//            }
//        }
//    }

    private class DataLongOperationAsynchTask extends AsyncTask<String, Void, LatLng> {
        ProgressDialog dialog = new ProgressDialog(CustomLocationActivity.this);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.setMessage("Please wait...");
            dialog.setCanceledOnTouchOutside(false);
            dialog.show();
        }

        @Override
        protected LatLng doInBackground(String... params) {
            JSONObject response1;
            LatLng responsData = null;
            try {
                responsData = getLocationFromString(params[0]);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return responsData;

        }


        @Override
        protected void onPostExecute(LatLng result) {

            Log.e("resltaas", String.valueOf(result));





//                JSONObject jsonObject = new JSONObject(result);
//
//                lng = ((JSONArray)jsonObject.get("results")).getJSONObject(0)
//                        .getJSONObject("geometry").getJSONObject("location")
//                        .getDouble("lng");
//
//               lat = ((JSONArray)jsonObject.get("results")).getJSONObject(0)
//                        .getJSONObject("geometry").getJSONObject("location")
//                        .getDouble("lat");
//
//                Log.d("latitude", "" + lat);
//                Log.d("longitude", "" + lng);
//
//
//                Log.i("user address",address);



//            latitude = result.latitude;
//
//            langitude =result.longitude;

            latitude = getIntent().getExtras().getDouble("lat");

            langitude = getIntent().getExtras().getDouble("lng");

            getCityName(latitude,langitude);

            final LatLng testAddr = new LatLng(lat,lng);




//                currentLocationName = getMapUrlLink();

            coordinates = String.valueOf(String.valueOf(latitude).concat(",").concat(String.valueOf(langitude)));

            String title =getCompleteAddressString(latitude,langitude);

            googleMap.addMarker(new MarkerOptions().position(new LatLng(latitude,langitude)).title(title)).showInfoWindow();
//                googleMap.moveCamera(CameraUpdateFactory.newLatLng(testAddr));
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(result,
                    14));

            if (dialog.isShowing()) {
                dialog.dismiss();
            }
        }
    }

    public static LatLng getLocationFromString(String address)
            throws JSONException {

        HttpGet httpGet = null;
        try {
            httpGet = new HttpGet(
                    "http://maps.google.com/maps/api/geocode/json?address="
                            + URLEncoder.encode(address, "UTF-8") + "&ka&sensor=false");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        HttpClient client = new DefaultHttpClient();
        HttpResponse response;
        StringBuilder stringBuilder = new StringBuilder();

        try {
            response = client.execute(httpGet);
            HttpEntity entity = response.getEntity();
            InputStream stream = entity.getContent();
            int b;
            while ((b = stream.read()) != -1) {
                stringBuilder.append((char) b);
            }
        } catch (ClientProtocolException e) {
        } catch (IOException e) {
        }

        JSONObject jsonObject = new JSONObject(stringBuilder.toString());

        double lng = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                .getJSONObject("geometry").getJSONObject("location")
                .getDouble("lng");

        double lat = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                .getJSONObject("geometry").getJSONObject("location")
                .getDouble("lat");

        return new LatLng(lat, lng);
    }



    public String getLatLongByURL(String requestURL) {
        URL url;
        String response = "";
        try {
            url = new URL(requestURL);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");
            conn.setDoOutput(true);
            int responseCode = conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
            } else {
                response = "";
            }

        }catch (java.net.SocketTimeoutException e) {

            Toast.makeText(getApplicationContext(), getString(R.string.connection_timeout), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }

        catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }
    @Override
    public void onPause() {
        super.onPause();


        UserNotification.setLocationSelected(Boolean.FALSE);

        if(UserNotification.getIsFromPubView()){

            UserNotification.setIsFromPubView(Boolean.FALSE);
        }
    }

    private void showMapPermissionAlert(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false).setView(R.layout.custom_permission_alert);

        final AlertDialog alert = builder.create();
        alert.show();
        alert.findViewById(R.id.cancel_alert).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.dismiss();
            }
        });
        alert.findViewById(R.id.accept_alert).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences.Editor editor = settings.edit();
                editor.putBoolean("locationPermissionDialogShown", true);
                editor.commit();
                alert.dismiss();
                setUPMap();
            }
        });
    }



    public void getAdvertiserCoordinates(){

        final ProgressDialog dialog = new ProgressDialog(CustomLocationActivity.this);

        dialog.setMessage("Please wait...");
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();



        int advertiserId= getIntent().getIntExtra("advertiser_id",0);
        advertiserName = getIntent().getStringExtra("advertiser_name");

        JSONObject jsonBody = new JSONObject();


        try {
            jsonBody.put("user_id",Tools.getData(CustomLocationActivity.this, "idprofile"));

            jsonBody.put("advertiser_Id",advertiserId);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        Log.d("json req", String.valueOf(jsonBody));

        JsonObjectRequest reqActu = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.user_coordinats), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dialog.dismiss();
                        try {
                            Log.i("map", String.valueOf(response));
                            if (response.getBoolean("result")) {
                                JSONObject obj = response.getJSONObject("data");

//                                latitude = obj.getDouble("latitude");
//                                langitude = obj.getDouble("langitude");


                                latitude = getIntent().getExtras().getDouble("lat");

                                langitude = getIntent().getExtras().getDouble("lng");


                                getCityName(latitude,langitude);



                            }


                        } catch (JSONException e) {

                            dialog.dismiss();

                            //Toast.makeText(PubDetails.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                dialog.dismiss();
//                findViewById(R.id.loading).setVisibility(View.GONE);
                //Toast.makeText(PubDetails.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(CustomLocationActivity.this, "token"));
                headers.put("lang", Tools.getData(CustomLocationActivity.this, "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqActu);





    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }


    public void updateMarker(){


    }

}
