

package com.oec.wis.dialogs;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.borax12.materialdaterangepicker.time.RadialPickerLayout;
import com.nostra13.universalimageloader.utils.L;
import com.oec.wis.ApplicationController;
import com.oec.wis.R;
import com.oec.wis.adapters.AlbumAdapter;
import com.oec.wis.adapters.AudioAdapter;
import com.oec.wis.adapters.AudioAdapter2;
import com.oec.wis.adapters.ChatIndivisualListener;
import com.oec.wis.adapters.CustomPhotoAdapter;
import com.oec.wis.adapters.CutomVideoAdapter;
import com.oec.wis.adapters.GalleryListener;
import com.oec.wis.adapters.PhotoAdapter2;
import com.oec.wis.adapters.PopupadApterText;
import com.oec.wis.adapters.Popupadapter;
import com.oec.wis.adapters.SongsAdapter;
import com.oec.wis.adapters.VideoAdapter2;
import com.oec.wis.controls.MultiSpinner;
import com.oec.wis.entities.PopupElementsText;
import com.oec.wis.entities.Popupelements;
import com.oec.wis.entities.UserNotification;
import com.oec.wis.entities.WISAlbum;
import com.oec.wis.entities.WISMusic;
import com.oec.wis.entities.WISMusic1;
import com.oec.wis.entities.WISPhoto;
import com.oec.wis.entities.WISSongs;
import com.oec.wis.entities.WISVideo;
import com.oec.wis.tools.Tools;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnItemClickListener;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalItem;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalPaymentDetails;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import com.borax12.materialdaterangepicker.time.TimePickerDialog;


import com.fourmob.datetimepicker.date.DatePickerDialog;
import com.fourmob.datetimepicker.date.DatePickerDialog.OnDateSetListener;

public class NewAds extends AppCompatActivity implements GalleryListener, com.borax12.materialdaterangepicker.time.TimePickerDialog.OnTimeSetListener,OnDateSetListener {
    private static final int REQUEST_CODE_PAYMENT = 2;
    public static ImageView ivPhoto, ivPlay;
    public static Dialog dPickMedia;
    public static String mediaName;
    // PayPal configuration
    private static PayPalConfiguration paypalConfig = new PayPalConfiguration()
            .environment(ApplicationController.PAYPAL_ENVIRONMENT).clientId(
                    ApplicationController.PAYPAL_CLIENT_ID);
    Spinner spDays, spVisib;
    MultiSpinner spTarget, spSexe;
    Spinner spCountry, spFrequency,spFrequency1;
    ViewFlipper vf;
    Button bNext;
    int c = 0;
    EditText etTitle, etDesc;
    TextView distanceid, frequencyid;
    String imgPath = "";
    CheckBox cbTourist, cbBanner, cbNotification, cbPubMusicale;
    //    TimePicker tpFrom, tpTo;
    DatePicker dpDate;
    String paymentId = "";
    Button bPay;
    String vidPath = "";
    Uri fileUri = null;
    Location location;
    ArrayList<Popupelements> popupList;

    private PopupWindow popWindow;
    ImageView close_btn;
    public static List<WISPhoto> photos;
    public static List<WISVideo> video;
    public static List<WISMusic> songs;
    DialogPlus dialog;
    Boolean WISPHOTOSELECTED;
    String WISPHOTOPATH;
    static final int REQUEST_CAMERA = 4;

    static final int REQUEST_VIDEO_CAPTURE = 10;

    static final int REQUEST_VIDEO_PICKER = 1010;

    private final int SELECT_PHOTO = 1;

    String mediaType;

    String audio_path;
    String audio_id;
    EditText etTime,etDate;
    RelativeLayout distanceRelLay ,targetRelLay,durationRelLay,genderRelLay,frequencyRelLay,frequencyRelLayTwo,countryRelay;
    TextView distanceTV,targetTV,descriptionTV,durationTV,genderTV,frequencyTV,frequencyTVTwo,countryTV;

    ImageView distanceImageView,targetImageView,durationImageView,genderImageView,frequencyImageView,frequencyImageViewTwo,countryImageView;
    TextView titleTV;
    String selectedPosition;
    ArrayList popupListDis,popuplistTarget,popuplistGender;
    Typeface font;
    TextView headerTitleTV,tvphone;
    LinearLayout frequencyLLayoutTwo,countryLLayout,frequencyLLayout,distanceLLayout;
    String startTime,endTime;


    public static final String DATEPICKER_TAG = "datepicker";


    String hourStringEnd;
    String minuteStringEnd;

    String hourString;
    String minuteString;
    String time;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Tools.setLocale(this, Tools.getData(this, "lang_pr"));
        setContentView(R.layout.activity_new_ads);

        mediaName = "";
        loadControls();
        setListener();
        loadSpData();
        loadData();
        setTypeFace();
    }



    private void loadData() {
        try{
            if (getIntent().getExtras() != null) {
                etTitle.setText(PubDetails.ads.getTitle());
                etDesc.setText(PubDetails.ads.getDesc());
                if (PubDetails.ads.getTypeObj().equals("video")) {
                    ivPlay.setVisibility(View.VISIBLE);
                    mediaName = PubDetails.ads.getThumb();

                    Log.e("media name", mediaName);
                    ApplicationController.getInstance().getImageLoader().get(getString(R.string.server_url4) + PubDetails.ads.getThumb(), new ImageLoader.ImageListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            ivPhoto.setImageResource(R.drawable.empty);
                        }

                        @Override
                        public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                            if (response.getBitmap() != null) {
                                ivPhoto.setImageBitmap(response.getBitmap());
                            } else {
                                ivPhoto.setImageResource(R.drawable.empty);
                            }
                        }
                    });
                } else {
                    mediaName = PubDetails.ads.getPhoto();
                    ApplicationController.getInstance().getImageLoader().get(getString(R.string.server_url3) + PubDetails.ads.getPhoto(), new ImageLoader.ImageListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }

                        @Override
                        public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                            if (response.getBitmap() != null) {
                                ivPhoto.setImageBitmap(response.getBitmap());
                            }
                        }
                    });
                }
                cbTourist.setChecked(PubDetails.ads.isTourist());
                int s = 0;
                System.out.println("popuplistDisatnce " +popupListDis);

                System.out.println("popuplistDisatnce Value 2 " +popupListDis.indexOf(2));
                if (PubDetails.ads.getVisib().equals("1"))
//                spVisib.setSelection(0);
                    distanceTV.setText(popupListDis.indexOf(0));

                else if (PubDetails.ads.getVisib().equals("5"))
//                spVisib.setSelection(1);
                    distanceTV.setText(popupListDis.indexOf(1));

                else if (PubDetails.ads.getVisib().equals("10"))
//                spVisib.setSelection(2);
                    distanceTV.setText(popupListDis.indexOf(2));

                else if (PubDetails.ads.getVisib().equals("50"))
//                spVisib.setSelection(3);
                    distanceTV.setText(popupListDis.indexOf(3));
                else
//                spVisib.setSelection(4);
                    distanceTV.setText(popupListDis.indexOf(4));


                String[] targets = PubDetails.ads.getTarget().split(",");
                targetTV.setText(String.valueOf(targets));
                if (PubDetails.ads.getDuration() == 1)
                    spDays.setSelection(0);
                else if (PubDetails.ads.getDuration() == 7)
                    spDays.setSelection(1);
                else if (PubDetails.ads.getDuration() == 30)
                    spDays.setSelection(2);
                String date = PubDetails.ads.getlDate();
                if (date.length() > 10)
                    dpDate.updateDate(Integer.parseInt(date.substring(0, 4)), Integer.parseInt(date.substring(5, 7)), Integer.parseInt(date.substring(8, 10)));
                cbBanner.setChecked(PubDetails.ads.isBanner());
//            tpFrom.setCurrentMinute(Integer.parseInt(PubDetails.ads.getStartTime().substring(3, 5)));
//            tpFrom.setCurrentHour(Integer.parseInt(PubDetails.ads.getStartTime().substring(0, 2)));
//            tpTo.setCurrentMinute(Integer.parseInt(PubDetails.ads.getEndTime().substring(3, 5)));
//            tpTo.setCurrentHour(Integer.parseInt(PubDetails.ads.getEndTime().substring(0, 2)));
////            cbBanner.setEnabled(false);
                spDays.setEnabled(false);
                dpDate.setEnabled(false);
//            spVisib.setEnabled(false);
                distanceRelLay.setEnabled(false);
                targetRelLay.setEnabled(false);
                //spCountry.setEnabled(false);
                frequencyRelLay.setEnabled(false);
                spSexe.setEnabled(false);
                cbTourist.setEnabled(false);
//            spVisib.setVisibility(View.VISIBLE);
                distanceRelLay.setVisibility(View.VISIBLE);
                distanceLLayout.setVisibility(View.VISIBLE);
                //spCountry.setVisibility(View.GONE);
                frequencyRelLay.setVisibility(View.GONE);
                distanceid.setText(getResources().getString(R.string.distance));
            }
        }catch (NullPointerException exce){
            System.out.println("Exception" +exce.getMessage());
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, paypalConfig);
        startService(intent);
    }

    private void launchPayPalPayment() {

        PayPalPayment thingsToBuy = prepareFinalCart();

        Intent intent = new Intent(NewAds.this, PaymentActivity.class);

        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, paypalConfig);

        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingsToBuy);

        startActivityForResult(intent, REQUEST_CODE_PAYMENT);
    }

    private PayPalPayment prepareFinalCart() {
        List<PayPalItem> productsInCart = new ArrayList<>();
        productsInCart.add(new PayPalItem("Achat pub", 1, BigDecimal.valueOf(Double.parseDouble(bPay.getText().toString().replace(" euro", ""))), ApplicationController.DEFAULT_CURRENCY, "Pub"));
        PayPalItem[] items = new PayPalItem[1];
        items = productsInCart.toArray(items);

        // Total amount
        BigDecimal subtotal = PayPalItem.getItemTotal(items);

        // If you have shipping cost, add it here
        BigDecimal shipping = new BigDecimal("0.0");

        // If you have tax, add it here
        BigDecimal tax = new BigDecimal("0.0");

        PayPalPaymentDetails paymentDetails = new PayPalPaymentDetails(
                shipping, subtotal, tax);

        BigDecimal amount = subtotal.add(shipping).add(tax);

        PayPalPayment payment = new PayPalPayment(
                amount,
                ApplicationController.DEFAULT_CURRENCY,
                getString(R.string.pub_buy),
                ApplicationController.PAYMENT_INTENT);

        payment.items(items).paymentDetails(paymentDetails);

        // Custom field like invoice_number etc.,
        payment.custom("");

        return payment;
    }

    private String getPath(Uri uri) {
        String[] projection = {MediaStore.Video.Media.DATA, MediaStore.Video.Media.SIZE, MediaStore.Video.Media.DURATION};
        Cursor cursor = this.managedQuery(uri, projection, null, null, null);
        cursor.moveToFirst();
        String filePath = cursor.getString(cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA));
        int fileSize = cursor.getInt(cursor.getColumnIndexOrThrow(MediaStore.Video.Media.SIZE));
        long duration = TimeUnit.MILLISECONDS.toSeconds(cursor.getInt(cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DURATION)));


        //some extra potentially useful data to help with filtering if necessary
        System.out.println("size: " + fileSize);
        System.out.println("path: " + filePath);
        System.out.println("duration: " + duration);

        return filePath;
    }

    public Bitmap getThumbnail(String file) {


        Bitmap bMap = ThumbnailUtils.createVideoThumbnail(file, MediaStore.Video.Thumbnails.MICRO_KIND);

        return bMap;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        Log.e("selected video", String.valueOf(requestCode));


        if (requestCode == REQUEST_CODE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm = data
                        .getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null) {
                    try {
                        paymentId = confirm.toJSONObject()
                                .getJSONObject("response").getString("id");

                        String payment_client = confirm.getPayment()
                                .toJSONObject().toString();

                        //if (!imgPath.equals("") || !vidPath.equals(""))
                        //new DoUpload().execute();
                        //else
                        doAddPub();
                    } catch (JSONException e) {

                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {

            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {

            }
        } else if (requestCode == REQUEST_VIDEO_PICKER && resultCode == RESULT_OK) {
            Uri selectedVideo = data.getData();

            final String[] proj = {MediaStore.Images.Media.DATA};
            final Cursor cursor = this.managedQuery(selectedVideo, proj, null, null,
                    null);
            final int column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToLast();
            String videoPath = cursor.getString(column_index);
            Log.i("video path", videoPath);

            mediaType = "video";

            if (getThumbnail(videoPath) != null)
                ivPhoto.setImageBitmap(getThumbnail(videoPath));


            new DoUpload().execute(videoPath, getString(R.string.upload_video_meth));
        } else if (requestCode == REQUEST_VIDEO_CAPTURE && resultCode == RESULT_OK) {


            Uri videoUri = data.getData();

            final String[] proj = {MediaStore.Images.Media.DATA};
            final Cursor cursor = this.managedQuery(videoUri, proj, null, null,
                    null);
            final int column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToLast();
            String videoPath = cursor.getString(column_index);

            mediaType = "video";

            if (getThumbnail(videoPath) != null)
                ivPhoto.setImageBitmap(getThumbnail(videoPath));

            Log.i("video path", String.valueOf(videoPath));
            new DoUpload().execute(getPath(videoUri), getString(R.string.upload_video_meth));

        } else if (resultCode == RESULT_OK && requestCode == SELECT_PHOTO) {
            Uri selectedPhoto = data.getData();


            final String[] proj = {MediaStore.Images.Media.DATA};
            final Cursor cursor = this.managedQuery(selectedPhoto, proj, null, null,
                    null);
            final int column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToLast();
            String photoPath = cursor.getString(column_index);
            mediaType = "photo";

            Bitmap bitmap = null;
            try {
                bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedPhoto));
                ivPhoto.setImageBitmap(bitmap);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }


            Log.e("selected path", photoPath);

            new DoUpload().execute(photoPath, getString(R.string.upload_profile_meth));
        } else if (resultCode == RESULT_OK && requestCode == REQUEST_CAMERA) {
            System.out.println("finalFile");


            System.out.println("finalFile");

            Log.d("request code", String.valueOf(requestCode));

            Log.d("response", String.valueOf(RESULT_OK));


//            Bitmap thumbnail = (Bitmap)data.getExtras().get("data");


//                    // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
//                    Uri tempUri = getImageUri(getActivity(), thumbnail);
//
//                    // CALL THIS METHOD TO GET THE ACTUAL PATH
//                    File finalFile = new File(getRealPathFromURI(tempUri));
//
//                    System.out.println(finalFile);


            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            ivPhoto.setImageBitmap(thumbnail);
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
            File destination = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
            FileOutputStream fo;
            try {
                destination.createNewFile();
                fo = new FileOutputStream(destination);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            System.out.println("Welcome" + destination);

            //new DoUpload().execute();
            imgPath = String.valueOf(destination);

            mediaType = "photo";

            //new DoUpload().execute();
            new DoUpload().execute(String.valueOf(destination), getString(R.string.upload_profile_meth));
        } else {

            Log.e("image result", "empty mpty");
        }


//        else if (requestCode == 1) {
//
//            if (resultCode == RESULT_OK) {
//                Uri selectedMedia = null;
//                if (data != null) {
//                    selectedMedia = data.getData();
//                    fileUri = null;
//                } else {
//                    selectedMedia = fileUri;
//                }
//                if (selectedMedia != null) {
//                    if (selectedMedia.toString().contains("images") || selectedMedia.toString().contains("Pictures")) {
//                        InputStream imageStream = null;
//                        try {
//                            imageStream = getContentResolver().openInputStream(selectedMedia);
//                        } catch (FileNotFoundException e) {
//                            e.printStackTrace();
//                        }
//
//                        Bitmap sImage = BitmapFactory.decodeStream(imageStream);
//                        ivPhoto.setImageBitmap(sImage);
//                        if (selectedMedia.toString().contains("file:///")) {
//                            imgPath = selectedMedia.toString();
//                        } else {
//                            final String[] proj = {MediaStore.Images.Media.DATA};
//                            final Cursor cursor = managedQuery(selectedMedia, proj, null, null,
//                                    null);
//                            final int column_index = cursor
//                                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
//                            cursor.moveToLast();
//                            imgPath = cursor.getString(column_index);
//                        }
//                        vidPath = "";
//                        if (getIntent().getExtras() != null)
//                            PubDetails.ads.setTypeObj("photo");
//                    }

// else if (selectedMedia.toString().contains("video")) {
//                        final String[] proj = {MediaStore.Video.Media.DATA};
//                        final Cursor cursor = managedQuery(selectedMedia, proj, null, null,
//                                null);
//                        final int column_index = cursor
//                                .getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
//                        cursor.moveToLast();
//                        vidPath = cursor.getString(column_index);
//                        imgPath = "";
//                        ivPhoto.setImageBitmap(Tools.retriveVideoFrameFromVideo(vidPath));
//                        if (getIntent().getExtras() != null)
//                            PubDetails.ads.setTypeObj("video");
//                    }


//                }
//            }
//        }
    }

    private void loadControls() {
        vf = (ViewFlipper) findViewById(R.id.viewFlipper);
        bNext = (Button) findViewById(R.id.bNext);
        ivPhoto = (ImageView) findViewById(R.id.ivPhoto);
        if (new UserNotification().getFROMMUSIC() == Boolean.TRUE) {
            String name = new UserNotification().getAudio();
            name = name.replace("http://146.185.161.92/public/activity/", "");
            String imageIcon = new UserNotification().getSongThumb();
//            Log.i("songIcon",imageIcon);
            Log.e("songIcon", imageIcon);
            //ivPhoto.setImageResource(R.drawable.empty);

            ApplicationController.getInstance().getImageLoader().get(getString(R.string.server_url3) + new UserNotification().getSongThumb(), new ImageLoader.ImageListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ivPhoto.setImageResource(R.drawable.empty);
                }

                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                    if (response.getBitmap() != null) {
                        int nh = (int) (response.getBitmap().getHeight() * (512.0 / response.getBitmap().getWidth()));
                        Bitmap scaled = Bitmap.createScaledBitmap(response.getBitmap(), 512, nh, true);
                        ivPhoto.setImageBitmap(scaled);
                    } else {
                        ivPhoto.setImageResource(R.drawable.empty);
                    }
                }
            });

            mediaName = name;
            audio_id = String.valueOf(new UserNotification().getObject());

            Bitmap image = ((BitmapDrawable) ivPhoto.getDrawable()).getBitmap();
            selectSong(audio_id, image, "audio", mediaName);
            new UserNotification().setFROMMUSIC(Boolean.FALSE);

        } else {
            ivPhoto.setImageResource(R.drawable.load_photo);
        }

//        distanceid = (TextView) findViewById(R.id.distanceid);
//        frequencyid = (TextView) findViewById(R.id.frequency);


        etTitle = (EditText) findViewById(R.id.etTitle);
        etDesc = (EditText) findViewById(R.id.etDesc);
        cbTourist = (CheckBox) findViewById(R.id.cbTourist);
        cbTourist.setSelected(true);
        cbBanner = (CheckBox) findViewById(R.id.cbBanner);
        cbNotification = (CheckBox) findViewById(R.id.cbNotification);
        cbPubMusicale = (CheckBox) findViewById(R.id.cbPubmusical);
//        cbBanner.setEnabled(true);
        cbBanner.setEnabled(false);
        cbNotification.setEnabled(false);
        cbPubMusicale.setEnabled(false);
        //frequencyid.setEnabled(false);
//            frequencyTV.setEnabled(false);
//            frequencyRelLayTwo.setVisibility(View.GONE);
        //spCountry.setVisibility(View.GONE);
//        tpFrom = (TimePicker) findViewById(R.id.tpFrom);
//        tpTo = (TimePicker) findViewById(R.id.tpTo);
//        tpFrom.setIs24HourView(true);
//        tpTo.setIs24HourView(true);
//        tpFrom.setCurrentHour(Calendar.getInstance().get(Calendar.HOUR_OF_DAY));
//        tpFrom.setSaveFromParentEnabled(false);
//        tpFrom.setSaveEnabled(true);
////
//        tpTo.setCurrentHour(Calendar.getInstance().get(Calendar.HOUR_OF_DAY));
//        tpTo.setSaveFromParentEnabled(false);
//        tpTo.setSaveEnabled(true);

//        dpDate = (DatePicker) findViewById(R.id.dpDate);

        bPay = (Button) findViewById(R.id.b_pay);
        ivPlay = (ImageView) findViewById(R.id.ivPlay);

        titleTV=(TextView)findViewById(R.id.titleTV);
        descriptionTV=(TextView)findViewById(R.id.descriptionTV);

        distanceRelLay =(RelativeLayout)findViewById(R.id.distanceRelLay);
        distanceTV=(TextView)findViewById(R.id.distanceTV);
        distanceImageView = (ImageView)findViewById(R.id.distanceImageView);
        headerTitleTV=(TextView)findViewById(R.id.headerTitleTV);

        targetRelLay =(RelativeLayout)findViewById(R.id.targetRelLay);
        targetTV=(TextView)findViewById(R.id.targetTV);
        targetImageView = (ImageView)findViewById(R.id.targetImageView);


        durationRelLay =(RelativeLayout)findViewById(R.id.durationRelLay);
        durationTV=(TextView)findViewById(R.id.durationTV);
        durationImageView = (ImageView)findViewById(R.id.durationImageView);
        genderRelLay =(RelativeLayout)findViewById(R.id.genderRelLay);
        genderTV=(TextView)findViewById(R.id.genderTV);
        genderImageView = (ImageView)findViewById(R.id.genderImageView);
        etTime = (EditText) findViewById(R.id.etTime);
        etDate =(EditText)findViewById(R.id.etDate);
        tvphone=(TextView)findViewById(R.id.tvPhone);

        frequencyRelLay =(RelativeLayout)findViewById(R.id.frequencyRelLay);
        frequencyTV=(TextView)findViewById(R.id.frequencyTV);
        frequencyImageView = (ImageView)findViewById(R.id.frequencyImageView);



        frequencyRelLayTwo =(RelativeLayout)findViewById(R.id.frequencyRelLayTwo);
        frequencyTVTwo=(TextView)findViewById(R.id.frequencyTVTwo);
        frequencyImageViewTwo = (ImageView)findViewById(R.id.frequencyImageViewTwo);

        countryRelay =(RelativeLayout)findViewById(R.id.countryRelay);
        countryTV=(TextView)findViewById(R.id.countryTV);
        countryImageView = (ImageView)findViewById(R.id.countryImageView);
        frequencyLLayoutTwo =(LinearLayout) findViewById(R.id.frequencyLLayoutTwo);
        frequencyLLayout=(LinearLayout)findViewById(R.id.frequencyLLayout);
        countryLLayout=(LinearLayout)findViewById(R.id.countryLLayout);
        distanceLLayout=(LinearLayout)findViewById(R.id.distanceLLayout);
    }
    private void setTypeFace() {
        Typeface font=Typeface.createFromAsset(this.getAssets(),"fonts/Harmattan-R.ttf");
        titleTV.setTypeface(font);
        descriptionTV.setTypeface(font);
        tvphone.setTypeface(font);
        etDate.setTypeface(font);
        etTime.setTypeface(font);
        cbBanner.setTypeface(font);
        cbNotification.setTypeface(font);
        cbTourist.setTypeface(font);
        distanceTV.setTypeface(font);
        durationTV.setTypeface(font);
        targetTV.setTypeface(font);
        genderTV.setTypeface(font);
        etTitle.setTypeface(font);
        etDesc.setTypeface(font);
        cbPubMusicale.setTypeface(font);
        headerTitleTV.setTypeface(font);
        frequencyTV.setTypeface(font);
        frequencyTVTwo.setTypeface(font);
        countryTV.setTypeface(font);
    }
    private void loadSpData() {
        ArrayAdapter<String> dataAdapter;

//        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this,
//                android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.days_list));
//        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spDays.setAdapter(dataAdapter);

//
//        dataAdapter = new ArrayAdapter<>(this,
//                android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.visib_list));
//        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spVisib.setAdapter(dataAdapter);


//        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.country_list,
//                android.R.layout.simple_spinner_item);
//        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spCountry.setAdapter(adapter);


//        dataAdapter = new ArrayAdapter<>(this,
//                android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.frequency_list));
//        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spFrequency.setAdapter(dataAdapter);
//
//        dataAdapter = new ArrayAdapter<>(this,
//                android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.pub_music_frequency_list));
//        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spFrequency1.setAdapter(dataAdapter);


//        ArrayAdapter<CharSequence> adapter1 = ArrayAdapter.createFromResource(this, R.array.frequency_list,
//                android.R.layout.simple_spinner_item);
//        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spFrequency.setAdapter(adapter1);


//        ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<>(this,
//                android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.frequency_list));
//        dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spFrequency.setAdapter(dataAdapter2);

//        spCountry.setItems(new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.country_list))));
//        spFrequency.setItems(new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.frequency_list))));
//        spTarget.setItems(new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.target_list))));
//        spSexe.setItems(new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.sexe_list))));


//
//    if(cbPubMusicale.isChecked())
//    {
//
//        frequencyRelLay.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                openFrequencyDropDown();
//            }
//        });
//    }else
//    {
//        frequencyTV.setTextColor(getResources().getColor(R.color.light_gray_color));
//    }


        targetRelLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTargetDropDown();

            }
        });
        genderRelLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                openGenderDropDown();
            }
        });
        durationRelLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                openDurationDropDown();

                if(cbTourist.isChecked() || cbNotification.isChecked()){

                    openDurationDropDown();

                }else
                {
                    openDurationDropDown1();
                }
            }
        });

        frequencyRelLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFrequencyDropDown();
            }
        });
        frequencyRelLayTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFrequencyDropDownTwo();
            }
        });
        countryRelay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCountryDropDoen();
            }
        });

    }

    private void openCountryDropDoen() {

        final ArrayList popupList = new ArrayList();

        popupList.add(new PopupElementsText("french"));
        popupList.add(new PopupElementsText("english"));
        popupList.add(new PopupElementsText("spanish"));
//        popupList.add(new PopupElementsText("20"));
//        popupList.add(new PopupElementsText("25"));


        PopupadApterText simpleAdapter = new PopupadApterText(this, false, popupList);


        dialog = DialogPlus.newDialog(this)
                .setAdapter(simpleAdapter)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View v, int position) {
                        if(position ==0){
                            dialog.dismiss();

                            countryTV.setText("french");
                        }
                        else if(position ==1){
                            dialog.dismiss();
                            countryTV.setText("english");


                        }
                        else if(position == 2) {
                            dialog.dismiss();
                            countryTV.setText("spanish");

                        }
//                        if(position ==3){
//                            dialog.dismiss();
//
//                            frequencyTV.setText("20");
//                        }
//                        if(position ==4){
//                            dialog.dismiss();
//                            frequencyTV.setText("25");
//                        }

                    }
                })
                .setExpanded(false)  // This will enable the expand feature, (similar to android L share dialog)
                .create();
        dialog.show();

    }

    private void openFrequencyDropDown() {
        final ArrayList popupList = new ArrayList();

        popupList.add(new PopupElementsText("5"));
        popupList.add(new PopupElementsText("10"));
        popupList.add(new PopupElementsText("15"));
        popupList.add(new PopupElementsText("20"));
        popupList.add(new PopupElementsText("25"));


        PopupadApterText simpleAdapter = new PopupadApterText(this, false, popupList);


        dialog = DialogPlus.newDialog(this)
                .setAdapter(simpleAdapter)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View v, int position) {
                        if(position ==0){
                            dialog.dismiss();

                            frequencyTV.setText("5");
                        }
                        else if(position ==1){
                            dialog.dismiss();
                            frequencyTV.setText("10");


                        }
                        else if(position == 2) {
                            dialog.dismiss();
                            frequencyTV.setText("15");

                        }
                        if(position ==3){
                            dialog.dismiss();
                            frequencyTV.setText("20");
                        }
                        if(position ==4){
                            dialog.dismiss();
                            frequencyTV.setText("25");
                        }

                    }
                })
                .setExpanded(false)  // This will enable the expand feature, (similar to android L share dialog)
                .create();
        dialog.show();

    }

    private void openFrequencyDropDownTwo() {
        final ArrayList popupList = new ArrayList();
        popupList.add(new PopupElementsText("20"));
        popupList.add(new PopupElementsText("30"));
        popupList.add(new PopupElementsText("40"));
        popupList.add(new PopupElementsText("50"));

        PopupadApterText simpleAdapter = new PopupadApterText(this, false, popupList);


        dialog = DialogPlus.newDialog(this)
                .setAdapter(simpleAdapter)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View v, int position) {
                        if(position ==0){
                            dialog.dismiss();
                            frequencyTVTwo.setText("20");
                        }
                        else if(position ==1){
                            dialog.dismiss();
                            frequencyTVTwo.setText("30");
                        }
                        else if(position == 2) {
                            dialog.dismiss();
                            frequencyTVTwo.setText("40");
                        }
                        if(position ==3){
                            dialog.dismiss();

                            frequencyTVTwo.setText("50");
                        }

                    }
                })
                .setExpanded(false)  // This will enable the expand feature, (similar to android L share dialog)
                .create();
        dialog.show();

    }

    private void openDurationDropDown() {

        final ArrayList popupList = new ArrayList();

        popupList.add(new PopupElementsText("1days"));
        popupList.add(new PopupElementsText("7 days"));
        popupList.add(new PopupElementsText("30 days"));
        PopupadApterText simpleAdapter = new PopupadApterText(this, false, popupList);


        dialog = DialogPlus.newDialog(this)
                .setAdapter(simpleAdapter)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View v, int position) {
                        if(position ==0){
                            dialog.dismiss();

                            durationTV.setText("1days");
                        }
                        else if(position ==1){
                            dialog.dismiss();
                            durationTV.setText("7days");


                        }
                        else if(position == 2) {
                            dialog.dismiss();
                            durationTV.setText("30days");

                        }

                    }
                })
                .setExpanded(false)  // This will enable the expand feature, (similar to android L share dialog)
                .create();
        dialog.show();

    }
    private void openDurationDropDown1() {

        final ArrayList popupList = new ArrayList();

        popupList.add(new PopupElementsText("7days"));
        popupList.add(new PopupElementsText("30days"));
        PopupadApterText simpleAdapter = new PopupadApterText(this, false, popupList);


        dialog = DialogPlus.newDialog(this)
                .setAdapter(simpleAdapter)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View v, int position) {
                        if(position ==0){
                            dialog.dismiss();
                            durationTV.setText("7days");


                        }
                        else if(position == 1) {
                            dialog.dismiss();
                            durationTV.setText("30days");

                        }

                    }
                })
                .setExpanded(false)  // This will enable the expand feature, (similar to android L share dialog)
                .create();
        dialog.show();

    }





    private void openGenderDropDown() {

        popuplistGender=new ArrayList();
        popuplistGender.add(new PopupElementsText("Male"));
        popuplistGender.add(new PopupElementsText("Female"));
        PopupadApterText adapter=new PopupadApterText(this,false,popuplistGender);

        dialog = DialogPlus.newDialog(this)
                .setAdapter(adapter)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View v, int position) {
                        if(position ==0){
                            dialog.dismiss();
                            genderTV.setText("Male");

                        }
                        else if(position ==1){
                            dialog.dismiss();
                            genderTV.setText("Female");

                        }

                    }
                })
                .setExpanded(false)  // This will enable the expand feature, (similar to android L share dialog)
                .create();
        dialog.show();

    }
//

    private void setListener() {
        bNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (c) {
                    case 0:
                        if (check1()) {
                            vf.showNext();
                            c++;
                        }
                        break;
                    case 1:
                        if (check2()) {
                            if (getIntent().getExtras() != null) {
                                doUpdatePub();
                            } else {
                                vf.showNext();
                                c++;
                                bNext.setVisibility(View.INVISIBLE);
//                                bNext.setText(getString(R.string.finish2));
                                doCalculatePrice();
                            }
                        }
                        break;
                    case 2:
                        if (check3()) {
                            launchPayPalPayment();
                        }
                        break;

                }
                Tools.hideKeyboard(NewAds.this);
            }
        });

        findViewById(R.id.close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (c == 0) {
                    finish();
                } else {
                    vf.showPrevious();
                    c--;
                    if (c == 1) {
//                        bNext.setText(getString(R.string.next));
                        bNext.setVisibility(View.VISIBLE);
                    }
                }
            }
        });

        View.OnClickListener pickPhoto = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*
                Intent pickIntent = new Intent();
                pickIntent.setType("image/* video/*");
                pickIntent.setAction(Intent.ACTION_PICK);
                Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                File file= new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), File.separator + "IMG_" + timeStamp + ".jpg");;

                fileUri = Uri.fromFile(file);
                takePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);


                Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                String pickTitle = "";
                Intent chooserIntent = Intent.createChooser(pickIntent, pickTitle);
                //chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{takePhotoIntent, takeVideoIntent});
                startActivityForResult(pickIntent, 1);
                */
//                pickMedia();

                showMediaOption();
            }
        };



        etTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Calendar now = Calendar.getInstance();
                TimePickerDialog tpd = TimePickerDialog.newInstance(
                        NewAds.this,
                        now.get(Calendar.HOUR_OF_DAY),
                        now.get(Calendar.MINUTE),
                        false
                );
                tpd.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {
                        Log.d("TimePicker", "Dialog was cancelled");
                    }
                });
                tpd.show(getFragmentManager(), "Timepickerdialog");
//                        Calendar now = Calendar.getInstance();
//                        com.borax12.materialdaterangepicker.time.TimePickerDialog tpd = com.borax12.materialdaterangepicker.time.TimePickerDialog.newInstance(
//                                NewAds.this,
//                                now.get(Calendar.HOUR_OF_DAY),
//                                now.get(Calendar.MINUTE),
//                                false
//                        );
//                        tpd.setOnCancelListener(new DialogInterface.OnCancelListener() {
//                            @Override
//                            public void onCancel(DialogInterface dialogInterface) {
//                                Log.d("TimePicker", "Dialog was cancelled");
//                            }
//                        });
//                        tpd.show(getFragmentManager(), "Timepickerdialog");
            }
        });


//        spVisib.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//
//                Log.d("Distance ","spinner");
//                if (i==0)
//                {
//
//                    spFrequency.setSelection(0);
//                }
//                else if(i==1)
//                {
//                    spFrequency.setSelection(1);
//                }
//                else if (i==2)
//                {
//                    spFrequency.setSelection(2);
//                }
//                else if(i==3) {
//                    spFrequency.setSelection(3);
//                } else {
//                    spFrequency.setSelection(4);
//                }
//
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//            }
//        });


//        spDays.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                if (i == 0)
//                    cbTourist.setEnabled(true);
//                else
//                    cbTourist.setEnabled(true);
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//            }
//        });
        ivPhoto.setOnClickListener(pickPhoto);
        findViewById(R.id.tvPhone).setOnClickListener(pickPhoto);

//        final ArrayAdapter<String> dataAdapterBanner = new ArrayAdapter<>(this,
//                android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.days_list_music));
//        dataAdapterBanner.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//
//
//        final ArrayAdapter<String> dataAdapterTourist = new ArrayAdapter<>(this,
//                android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.days_list));
//        dataAdapterTourist.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//
//        final ArrayAdapter<String> dataAdapterPupMusical = new ArrayAdapter<>(this,
//                android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.days_list_music));
//        dataAdapterPupMusical.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


        cbTourist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (cbTourist.isChecked()) {

                    Log.e("T", "check");



                    frequencyRelLay.setVisibility(View.VISIBLE);
                    frequencyLLayout.setVisibility(View.VISIBLE);
                    //frequencyid.setEnabled(true);
                    frequencyRelLay.setEnabled(true);
                    frequencyRelLayTwo.setVisibility(View.GONE);
                    frequencyLLayoutTwo.setVisibility(View.GONE);
                    distanceRelLay.setVisibility(View.VISIBLE);
                    distanceLLayout.setVisibility(View.VISIBLE);
                    countryRelay.setVisibility(View.GONE);
                    countryLLayout.setVisibility(View.GONE);


                    cbBanner.setChecked(false);
                    cbNotification.setChecked(false);
                    cbPubMusicale.setChecked(false);
                    cbNotification.setEnabled(false);

                } else {
                    cbBanner.setChecked(false);
                    cbNotification.setChecked(false);
                    cbPubMusicale.setChecked(false);
                    cbBanner.setEnabled(true);
                    cbNotification.setEnabled(true);
                    cbPubMusicale.setEnabled(true);
                    Log.e("T", "not check");
                }
            }


        });


        cbBanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (cbBanner.isChecked()) {

                    Log.e("b", "checked");
//                    spDays.setAdapter(dataAdapterBanner);
                    // spCountry.setVisibility(View.GONE);
                    // frequencyid.setVisibility(View.VISIBLE);
                    frequencyRelLay.setVisibility(View.VISIBLE);
                    frequencyLLayout.setVisibility(View.VISIBLE);
                    //frequencyid.setEnabled(true);
                    frequencyRelLay.setEnabled(true);
                    frequencyRelLayTwo.setVisibility(View.GONE);
                    frequencyLLayoutTwo.setVisibility(View.GONE);
//                    distanceid.setText(getResources().getString(R.string.distance));
                    cbTourist.setChecked(false);
                    cbNotification.setChecked(false);
                    cbPubMusicale.setChecked(false);
                    distanceRelLay.setVisibility(View.VISIBLE);
                    distanceLLayout.setVisibility(View.VISIBLE);

                    countryRelay.setVisibility(View.GONE);
                    countryLLayout.setVisibility(View.GONE);
                } else {

                    Log.e("b", "not check");


                }
            }
        });

        cbNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (cbNotification.isChecked()) {

//                    spDays.setAdapter(dataAdapterTourist);
                    //spCountry.setVisibility(View.GONE);
                    //frequencyid.setVisibility(View.VISIBLE);
                    frequencyRelLay.setVisibility(View.VISIBLE);
                    frequencyLLayout.setVisibility(View.VISIBLE);

                    //frequencyid.setEnabled(true);
                    frequencyRelLay.setEnabled(true);
                    frequencyRelLayTwo.setVisibility(View.GONE);
                    frequencyLLayoutTwo.setVisibility(View.GONE);
//                    distanceid.setText(getResources().getString(R.string.distance));
                    cbBanner.setChecked(false);
                    cbTourist.setChecked(false);
                    cbPubMusicale.setChecked(false);
                    countryRelay.setVisibility(View.GONE);
                    countryLLayout.setVisibility(View.GONE);
                    distanceRelLay.setVisibility(View.VISIBLE);
                    distanceLLayout.setVisibility(View.VISIBLE);

                }
            }
        });
        cbPubMusicale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (cbPubMusicale.isChecked()) {

//                    spDays.setAdapter(dataAdapterPupMusical);
//                    distanceid.setText(getResources().getString(R.string.countries));
                    //frequencyid.setVisibility(View.VISIBLE);
                    //spCountry.setVisibility(View.VISIBLE);
                    frequencyRelLayTwo.setVisibility(View.VISIBLE);
                    frequencyLLayoutTwo.setVisibility(View.VISIBLE);
                    countryRelay.setVisibility(View.VISIBLE);
                    countryLLayout.setVisibility(View.VISIBLE);
                    //frequencyid.setEnabled(true);
                    frequencyRelLay.setVisibility(View.GONE);
                    frequencyLLayout.setVisibility(View.GONE);

                    distanceRelLay.setVisibility(View.GONE);
                    distanceLLayout.setVisibility(View.GONE);

                    cbBanner.setChecked(false);
                    cbTourist.setChecked(false);
                    cbNotification.setChecked(false);

                }
            }
        });

        bPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (check3()) {
                    if (!bPay.getText().toString().equals("")) {
                        //launchPayPalPayment();
                        doAddPub();
                    }
                }
            }
        });



        etDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                final Calendar calendar = Calendar.getInstance();

                final DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(NewAds.this,
                        calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

//                datePickerDialog.setCloseOnSingleTapDay(isCloseOnSingleTapDay());
                datePickerDialog.show(getSupportFragmentManager(), DATEPICKER_TAG);
            }
        });


    }

//        etTime.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                Calendar now = Calendar.getInstance();
//                TimePickerDialog tpd = TimePickerDialog.newInstance(
//                        NewAds.this,
//                        now.get(Calendar.HOUR_OF_DAY),
//                        now.get(Calendar.MINUTE),
//                        false
//                );
//                tpd.setOnCancelListener(new DialogInterface.OnCancelListener() {
//                    @Override
//                    public void onCancel(DialogInterface dialogInterface) {
//                        Log.d("TimePicker", "Dialog was cancelled");
//                    }
//                });
//                tpd.show(this.getFragmentManager(), "Timepickerdialog");
//            }
//        });
//    }

    private boolean check1() {
        boolean check = true;
        if (TextUtils.isEmpty(etTitle.getText().toString())) {
            check = false;
            etTitle.setError(getString(R.string.msg_empty_title));
        }
        if (TextUtils.isEmpty(etDesc.getText().toString())) {
            check = false;
            etDesc.setError(getString(R.string.msg_empty_desc));
        }
        return check;
    }

    private boolean check2() {
        boolean check = true;
//        if (spTarget.getSelectedIndices().size() == 0) {
//            Toast.makeText(NewAds.this, getString(R.string.err_select_target), Toast.LENGTH_SHORT).show();
//            check = false;
//        }
//        if (spSexe.getSelectedIndices().size() == 0) {
//            Toast.makeText(NewAds.this, getString(R.string.err_select_one_sexe), Toast.LENGTH_SHORT).show();
//            check = false;
//        }
//        if (spCountry.getSelectedIndices().size() == 0) {
//            Toast.makeText(NewAds.this, getString(R.string.err_select_one_country), Toast.LENGTH_SHORT).show();
//            check = false;
//        }
//        if (spFrequency.getSelectedIndices().size() == 0) {
//            Toast.makeText(NewAds.this, getString(R.string.err_select_one_frequency), Toast.LENGTH_SHORT).show();
//            check = false;
//        }

        location = Tools.getLastBestLocation(this);
        if (location == null) {
            Toast.makeText(NewAds.this, getString(R.string.err_location_not_availble), Toast.LENGTH_LONG).show();
            check = false;
        }
        return check;
    }

    private boolean check3() {
        boolean check = true;
        return check;
    }

    private void doCalculatePrice() {
        final ProgressDialog pDialog = ProgressDialog.show(this, "", getString(R.string.pending_operation), true, true);
        pDialog.show();

        JSONObject jsonBody = new JSONObject();

        String distance="";


        String frequency = "";
        String duration;



//        frequency = getResources().getStringArray(R.array.frequency_list)[spFrequency.getSelectedItemPosition()].replace(" passages", "");
        frequency = frequencyTV.getText().toString();

        Log.d("DistanceValue1",frequency);
        if (cbPubMusicale.isChecked() || cbBanner.isChecked()) {
//            duration = getResources().getStringArray(R.array.days_list_music)[spDays.getSelectedItemPosition()].split(" ")[0];
            duration =durationTV.getText().toString().replace("days", "");


        } else {
            duration =durationTV.getText().toString().replace("days", "");

        }
        Log.i("duration", duration);
//        String country = getResources().getStringArray(R.array.country_list)[spCountry.getSelectedItemPosition()].split(" ")[0];
//        Log.i("country", country);
        if(cbPubMusicale.isChecked())
        {
//            distance = getResources().getStringArray(R.array.pub_music_frequency_list)[spFrequency1.getSelectedItemPosition()].replace(" passages", "");
//            distance = frequencyTVTwo.getText().toString().replace("passages", "");
            distance = frequencyTVTwo.getText().toString();

        }
        else {
            distance = distanceTV.getText().toString().replace("KM","");
            if (distance.contains("+50")) {
                distance = "51";
            }
        }
        Log.e("DistanceValue",distance);
        int banner = 0;
        int wis_music = 0;
        if (cbBanner.isChecked())
            banner = 1;

        if (cbPubMusicale.isChecked()) {
            wis_music = 1;
//            distance = frequency;

        }


        try {
            jsonBody.put("id_profil", Tools.getData(NewAds.this, "idprofile"));

            Log.e("duration", duration);
            Log.e("distance" ,distance.replace("KM",""));

            jsonBody.put("duree", duration);
            jsonBody.put("distance",distance);
            jsonBody.put("no_of_passage", frequency);
//            Log.i("frequencys", frequency);
            jsonBody.put("wis_music", wis_music);
//            Log.i("distance", distance);
            jsonBody.put("banner", banner);
            Log.i("bodyjson", String.valueOf(jsonBody));
            Log.e("bodyjson", String.valueOf(jsonBody));

        } catch (JSONException e) {

        }


        JsonObjectRequest request = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.calculprix_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("response1" , String.valueOf(response));
                        try {
                            bPay.setText(response.getString("data"));
                            Log.e("response2" ,response.getString("data"));


                        } catch (Exception e) {

                        }
                        findViewById(R.id.loading).setVisibility(View.GONE);
                        pDialog.dismiss();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                findViewById(R.id.loading).setVisibility(View.GONE);
                //Toast.makeText(NewAds.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(NewAds.this, "token"));
                headers.put("lang", Tools.getData(NewAds.this, "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(request);
    }

    private void doAddPub() {
        final ProgressDialog pDialog = ProgressDialog.show(this, "", getString(R.string.pending_operation), true, true);
        pDialog.show();
        JSONObject jsonBody = new JSONObject();
//        String distance = getResources().getStringArray(R.array.visib_list)[spVisib.getSelectedItemPosition()].replace(" KM", "");
//        if (distance.contains("+50")) {
//            distance = "+50";
//            distance = distance.replace("+50", "51");
//        }
        String target = targetTV.getText().toString();
        if (target.contains("+65")) {
            target = target.replace("+65", "65-99");
        }
        String duration;
        String frequency;
        String distance ="";
        if (cbPubMusicale.isChecked() || cbBanner.isChecked()) {
            duration =durationTV.getText().toString().replace("days", "");
        } else {
            duration =durationTV.getText().toString().replace("days", "");

        }

        //String duration = getResources().getStringArray(R.array.days_list)[spDays.getSelectedItemPosition()].split(" ")[0];
//        String country = getResources().getStringArray(R.array.country_list)[spCountry.getSelectedItemPosition()].split(" ")[0];
//        Log.i("country", country);

        if(cbPubMusicale.isChecked())
        {

            frequency = frequencyTVTwo.getText().toString();

//            frequency = getResources().getStringArray(R.array.pub_music_frequency_list)[spFrequency1.getSelectedItemPosition()].replace(" passages", "");
        }
        else {
            frequency = frequencyTVTwo.getText().toString();        }
        //String frequency = getResources().getStringArray(R.array.frequency_list)[spFrequency.getSelectedItemPosition()].replace(" passages", "");
        String ftFrom, ftTo;
        Format formatter;
//        Calendar calendar = Calendar.getInstance();
//        calendar.set(Calendar.HOUR_OF_DAY, tpFrom.getCurrentHour());
//        calendar.set(Calendar.MINUTE, tpFrom.getCurrentMinute());
//
//        formatter = new SimpleDateFormat("HH:mm");
//        ftFrom = formatter.format(calendar.getTime());
//
//        calendar.set(Calendar.HOUR_OF_DAY, tpTo.getCurrentHour());
//        calendar.set(Calendar.MINUTE, tpTo.getCurrentMinute());
//
//        formatter = new SimpleDateFormat("HH:mm");
//        ftTo = formatter.format(calendar.getTime());

        int notification = 0;
        int toursite = 0;
        int wis_music = 0;
        if (cbTourist.isChecked())
            toursite = 1;
        int banner = 0;
        if (cbBanner.isChecked())
            banner = 1;

        if (cbNotification.isChecked()) {
            notification = 1;
        }
        if (cbPubMusicale.isChecked()) {
            wis_music = 1;
            distance = frequency;

            Log.i("frequencys", distance);
        }

//        int day = dpDate.getDayOfMonth();
//        int month = dpDate.getMonth();
////        int year = dpDate.getYear();
//
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//        String pubDate = sdf.format(new Date(year - 1900, month, day));

        final String amount = bPay.getText().toString().replace(" euro", "");
//
//        String sexe = "H";
//        if (spSexe.getSelectedIndices().size() == 2)
//            sexe = "H/F";
//        else if (spSexe.getSelectedIndices().get(0) == 1)
//            sexe = "F";



        String sexe = "H";
        genderTV.setText("Homme");
        sexe = "H/F";
//        else if (genderTV.getSelectedIndices().get(0) == 1)
        genderTV.setText("Female");
        sexe = "F";

        String address = getCompleteAddressString(location.getLatitude(), location.getLongitude());
        Log.i("address", address);

        Log.i("selected1", mediaName + mediaType + audio_id);
        System.out.println("picked date is" +etDate.getText().toString().replace("Picked Date:",""));

        String getDate  =etDate.getText().toString().trim().replace("Picked Date:","");
        Log.e("getDate", String.valueOf(getDate));

        String gettime = etTime.getText().toString();
        Log.e("getTime", String.valueOf(gettime));
//        String startTime= etTime.getText().toString().replace("Picked time : From - "," ");


        Log.e("startTime11" ,String.valueOf(startTime));
        Log.e("end time11" ,String.valueOf(endTime));

//        Picked time: From - 14h30 To - 16h30
        try {
            jsonBody.put("id_profil", Tools.getData(NewAds.this, "idprofile"));
            jsonBody.put("titre", etTitle.getText().toString());
            jsonBody.put("description", etDesc.getText().toString());
            jsonBody.put("distance",distance);
//            Log.i("finaldistance", distance);
            jsonBody.put("cible", target);
            jsonBody.put("duree", duration);
            jsonBody.put("no_of_passage",frequency);
            jsonBody.put("touriste", toursite);
            jsonBody.put("banner", banner);
            jsonBody.put("sexe", sexe);
            jsonBody.put("photo", mediaName);
            jsonBody.put("startTime",startTime);
            jsonBody.put("endTime",endTime);
            jsonBody.put("date_lanc",getDate);
            jsonBody.put("pos_x", location.getLatitude());
            jsonBody.put("pos_y", location.getLongitude());
            jsonBody.put("wis_music", wis_music);
            jsonBody.put("country", countryTV.getText().toString());
            jsonBody.put("audio_id", audio_id);
            jsonBody.put("address", address);
            //jsonBody.put("currency_code", ApplicationController.DEFAULT_CURRENCY);
            jsonBody.put("type_obj", mediaType);
            jsonBody.put("notification", notification);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e("JsonBodyPub", String.valueOf(jsonBody));

        JsonObjectRequest reqAddPub = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.addpub_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {
                                pDialog.dismiss();
                                Log.i("JsonBodyResponse", String.valueOf(response));
                                String idPub = response.getString("data");
                                Intent i = new Intent(NewAds.this, MyWebView.class);
                                i.putExtra("price", amount);
                                i.putExtra("idPub", idPub);
                                Log.d("price", amount);
                                Log.d("idPub", idPub);
                                startActivity(i);

                                //String url = String.format("http://vps222729.ovh.net/wis/auth/paypub?prix=%1$s&idpub=\'%2$s\'", amount, idPub);
                                //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));

                                finish();
                                //http://vps222729.ovh.net/wis/auth/paypub?prix=29.20&&idpub=158
                                //Toast.makeText(NewAds.this, getString(R.string.paiement_success), Toast.LENGTH_LONG).show();
                                //finish();
                            }
                        } catch (Exception e) {
                            pDialog.dismiss();
                            //Toast.makeText(NewAds.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                //Toast.makeText(NewAds.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(NewAds.this, "token"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqAddPub);
    }

    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {


        String strAdd = "";
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
                Log.i("address", strAdd);

            } else {
                Log.i("address", "no address found");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.i("Current loction", "Canont get Address!");
        }
        return strAdd;
    }

    private void doUpdatePub() {
        final ProgressDialog pDialog = ProgressDialog.show(this, "", getString(R.string.maj_pub), true, true);
        pDialog.show();
        JSONObject jsonBody = new JSONObject();

        String ftFrom, ftTo;
        Format formatter;
//        Calendar calendar = Calendar.getInstance();
//        calendar.set(Calendar.HOUR_OF_DAY, tpFrom.getCurrentHour());
//        calendar.set(Calendar.MINUTE, tpFrom.getCurrentMinute());
//
//        formatter = new SimpleDateFormat("HH:mm:ss");
//        ftFrom = formatter.format(calendar.getTime());
//
//        calendar.set(Calendar.HOUR_OF_DAY, tpTo.getCurrentHour());
//        calendar.set(Calendar.MINUTE, tpTo.getCurrentMinute());

        formatter = new SimpleDateFormat("HH:mm:ss");
//        ftTo = formatter.format(calendar.getTime());

        PubDetails.ads.setTitle(etTitle.getText().toString());
        PubDetails.ads.setDesc(etDesc.getText().toString());
        PubDetails.ads.setPhoto(mediaName);
        PubDetails.ads.setStartTime(etTime.getText().toString());
//        PubDetails.ads.setStartTime(ftTo);
        String typeObj = "photo";
        if (mediaName.contains("video"))
            typeObj = "video";
        PubDetails.ads.setTypeObj(typeObj);
        try {
            jsonBody.put("id", String.valueOf(PubDetails.ads.getId()));
            jsonBody.put("id_profil", Tools.getData(NewAds.this, "idprofile"));
            jsonBody.put("titre", etTitle.getText().toString());
            jsonBody.put("description", etDesc.getText().toString());
            jsonBody.put("photo", mediaName);
            jsonBody.put("startTime", etTime.getText().toString());
//            jsonBody.put("endTime", ftTo);
            jsonBody.put("type_obj", typeObj);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("updated", String.valueOf(jsonBody));
        JsonObjectRequest reqAddPub = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.updatepub_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {

                                System.out.println("response ======>" +response);
                                pDialog.dismiss();
                                finish();
                            }
                        } catch (Exception e) {
                            pDialog.dismiss();
                            //Toast.makeText(NewAds.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pDialog.dismiss();
                //Toast.makeText(NewAds.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(NewAds.this, "token"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqAddPub);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ApplicationController.getInstance().cancelPendingRequests(ApplicationController.TAG);
    }

    private void pickMedia() {
        dPickMedia = new Dialog(this);
        dPickMedia.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dPickMedia.setContentView(R.layout.dialog_pick_media);
        final GridView gvMedia = (GridView) dPickMedia.findViewById(R.id.gvMedia);
        dPickMedia.findViewById(R.id.ivImage).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dPickMedia.findViewById(R.id.llMode).setVisibility(View.GONE);
                dPickMedia.findViewById(R.id.ivBack).setVisibility(View.VISIBLE);
                gvMedia.setVisibility(View.VISIBLE);
                loadPhotos(gvMedia);
            }
        });
        dPickMedia.findViewById(R.id.ivVideo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dPickMedia.findViewById(R.id.llMode).setVisibility(View.GONE);
                dPickMedia.findViewById(R.id.ivBack).setVisibility(View.VISIBLE);
                gvMedia.setVisibility(View.VISIBLE);
                loadVideos(gvMedia);
            }
        });
        dPickMedia.findViewById(R.id.ivBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dPickMedia.findViewById(R.id.llMode).setVisibility(View.VISIBLE);
                dPickMedia.findViewById(R.id.ivBack).setVisibility(View.GONE);
                gvMedia.setVisibility(View.GONE);
            }
        });
        dPickMedia.show();
    }

    private void loadVideos(final GridView grid) {
        final ArrayList<WISVideo> video = new ArrayList<>();
        final VideoAdapter2 adapter = new VideoAdapter2(NewAds.this, video, 2);
        grid.setAdapter(adapter);
        final ProgressDialog progress = ProgressDialog.show(NewAds.this, "", getString(R.string.loading_progress), true, true);
        progress.show();
        JSONObject jsonBody = null;

        try {
            jsonBody = new JSONObject("{\"id_profil\":\"" + Tools.getData(NewAds.this, "idprofile") + "\"}");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqVideos = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.getvideos_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            Log.i("LoadVideos", String.valueOf(response));
                            if (response.getBoolean("result")) {
                                JSONArray data = response.getJSONArray("data");
                                for (int i = 0; i < data.length(); i++) {
                                    try {
                                        JSONObject obj = data.getJSONObject(i);
                                        video.add(new WISVideo(obj.getInt("id"), "", obj.getString("url_video"), obj.getString("created_at"), obj.getString("image_video")));
                                    } catch (Exception e) {
                                    }
                                }
                                if (video.size() == 0)
                                    Toast.makeText(NewAds.this, getString(R.string.no_video_element), Toast.LENGTH_SHORT).show();
                                adapter.notifyDataSetChanged();
                            }
                            progress.dismiss();

                        } catch (JSONException e) {
                            progress.dismiss();
                            //Toast.makeText(NewAds.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.dismiss();
                //Toast.makeText(NewAds.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(NewAds.this, "token"));
                headers.put("lang", Tools.getData(NewAds.this, "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqVideos);

        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (video.get(i).isSelected()) {
                    video.get(i).setSelected(false);
                } else {
                    for (WISVideo item : video) {
                        item.setSelected(false);
                    }
                    video.get(i).setSelected(true);
                }
                adapter.notifyDataSetChanged();
            }
        });
    }


    private void loadPhotos(final GridView grid) {
        final ArrayList<WISPhoto> photos = new ArrayList<>();
        final PhotoAdapter2 adapter = new PhotoAdapter2(NewAds.this, photos, 2);
        grid.setAdapter(adapter);
        final ProgressDialog progress = ProgressDialog.show(NewAds.this, "", getString(R.string.loading_progress), true, true);
        progress.show();
        JSONObject jsonBody = null;

        try {
            jsonBody = new JSONObject("{\"id_profil\":\"" + Tools.getData(NewAds.this, "idprofile") + "\"}");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqPhotos = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.galeriephoto_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("Message").contains("any image")) {
                                Toast.makeText(NewAds.this, getString(R.string.no_photo_element), Toast.LENGTH_SHORT).show();
                            } else {
                                if (response.getString("result").equals("true")) {
                                    JSONArray data = response.getJSONArray("data");
                                    for (int i = 0; i < data.length(); i++) {
                                        try {
                                            JSONObject obj = data.getJSONObject(i);
                                            photos.add(new WISPhoto(obj.getInt("idphoto"), "", obj.getString("pathoriginal"), obj.getString("created_at")));
                                        } catch (Exception e) {
                                        }
                                    }
                                    adapter.notifyDataSetChanged();
                                }
                            }
                            progress.dismiss();
                        } catch (JSONException e) {
                            progress.dismiss();
                            //Toast.makeText(NewAds.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progress.dismiss();
                //Toast.makeText(NewAds.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(NewAds.this, "token"));
                headers.put("lang", Tools.getData(NewAds.this, "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqPhotos);

        grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                if (photos.get(i).isSelected()) {
                    photos.get(i).setSelected(false);
                } else {
                    for (WISPhoto item : photos) {
                        item.setSelected(false);
                    }
                    photos.get(i).setSelected(true);
                }
                adapter.notifyDataSetChanged();
            }
        });
    }


    public void showMediaOption() {


        popupList = new ArrayList<>();
        Popupadapter simpleAdapter = new Popupadapter(this, false, popupList);

        popupList.add(new Popupelements(R.drawable.camera, "Pick Photos"));
        popupList.add(new Popupelements(R.drawable.wis_photo_picker, "Wis Photos"));
        popupList.add(new Popupelements(R.drawable.camera, "Take Photos"));

        popupList.add(new Popupelements(R.drawable.camera, "Pick Videos"));
        popupList.add(new Popupelements(R.drawable.wis_photo_picker, "Wis Videos"));
        popupList.add(new Popupelements(R.drawable.camera, "Take Videos"));
        popupList.add(new Popupelements(R.drawable.wis_photo_picker, "Wis Music"));


        dialog = DialogPlus.newDialog(this)
                .setAdapter(simpleAdapter)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View v, int position) {

                        if (position == 0) {
                            Intent i = new Intent(Intent.ACTION_PICK,
                                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(i, SELECT_PHOTO);
                            dialog.dismiss();

                        } else if (position == 1) {
                            dialog.dismiss();
                            openWisMedia(0, "photo");


                        } else if (position == 2) {
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(intent, REQUEST_CAMERA);
                            dialog.dismiss();


                        } else if (position == 3) {

                            Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(Intent.createChooser(i, getString(R.string.pick_video)), REQUEST_VIDEO_PICKER);
                            dialog.dismiss();

//                        Intent intent = new Intent();
//                        intent.setType("video/*");
//                        intent.setAction(Intent.ACTION_GET_CONTENT);
//                        startActivityForResult(Intent.createChooser(intent,"Select Video"), REQUEST_VIDEO_PICKER);
//                        dialog.dismiss();
                        } else if (position == 4) {
                            dialog.dismiss();
                            openWisMedia(0, "video");

                        } else if (position == 5) {

                            Intent takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                            if (takeVideoIntent.resolveActivity(getPackageManager()) != null) {
                                startActivityForResult(takeVideoIntent, REQUEST_VIDEO_CAPTURE);
                                dialog.dismiss();
                            }

                        } else if (position == 6) {
                            dialog.dismiss();
                            openWisSongs(0, "music");
                        }
                    }
                })
                .setExpanded(false)  // This will enable the expand feature, (similar to android L share dialog)
                .create();
        dialog.show();


    }

    public void openWisMedia(final int position, String type) {

        LayoutInflater layoutInflater = (LayoutInflater) this.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // inflate the custom popup layout
        final View inflatedView = layoutInflater.inflate(R.layout.media_gallery, null, false);


        // get device size
        Display display = this.getWindowManager().getDefaultDisplay();
        final Point size = new Point();
        display.getSize(size);
//        mDeviceHeight = size.y;


        // set height depends on the device size
        popWindow = new PopupWindow(inflatedView, size.x - 35, size.y - 300, true);

        popWindow.setTouchable(true);

        // make it focusable to show the keyboard to enter in `EditText`
//        popWindow.setFocusable(true);
        // make it outside touchable to dismiss the popup window
//        popWindow.setBackgroundDrawable(new BitmapDrawable());
        popWindow.setOutsideTouchable(false);

        popWindow.setAnimationStyle(R.anim.bounce);

//        this.view.setAlpha(0.4f);

//        final View views = this.view;


        close_btn = (ImageView) inflatedView.findViewById(R.id.close_btn);


        TextView header = (TextView) inflatedView.findViewById(R.id.headerTitleForWindow);
        try{

            Typeface typeface =Typeface.createFromAsset(this.getAssets(),"fonts/Harmattan-R.ttf");
            header.setTypeface(typeface);


        }catch (NullPointerException ex){

        }
        close_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popWindow.dismiss();
                dialog.dismiss();
//                view.setAlpha(1);
            }
        });

        // show the popup at bottom of the screen and set some margin at bottom ie,


        final GridView gvMedia = (GridView) inflatedView.findViewById(R.id.media_gallry);


        if (type.equals("photo")) {
            header.setText(getString(R.string.photo));
            getPhotos(gvMedia);
        } else {
            header.setText(getString(R.string.video));
            getVideos(gvMedia);
        }


        popWindow.showAtLocation(findViewById(R.id.baseContent), Gravity.CENTER, 0, 100);

    }

    public void openWisSongs(final int position, String type) {

        LayoutInflater layoutInflater = (LayoutInflater) this.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // inflate the custom popup layout
        //final View inflatedView = layoutInflater.inflate(R.layout.media_gallery, null, false);

        final View inflatedView = layoutInflater.inflate(R.layout.songs_gallery, null, false);
        // get device size
        Display display = this.getWindowManager().getDefaultDisplay();
        final Point size = new Point();
        display.getSize(size);
//        mDeviceHeight = size.y;


        // set height depends on the device size
        popWindow = new PopupWindow(inflatedView, size.x - 35, size.y - 300, true);

        popWindow.setTouchable(true);

        // make it focusable to show the keyboard to enter in `EditText`
//        popWindow.setFocusable(true);
        // make it outside touchable to dismiss the popup window
//        popWindow.setBackgroundDrawable(new BitmapDrawable());
        popWindow.setOutsideTouchable(false);

        popWindow.setAnimationStyle(R.anim.bounce);

//        this.view.setAlpha(0.4f);

//        final View views = this.view;


        close_btn = (ImageView) inflatedView.findViewById(R.id.close_btn);
        TextView header = (TextView) inflatedView.findViewById(R.id.headerTitleForWindow);



        try {
            Typeface typeface =Typeface.createFromAsset(this.getAssets(),"fonts/Harmattan-R.ttf");
            header.setTypeface(typeface);

        }catch (NullPointerException ex){
            Log.d("Exce" ,ex.getMessage());
        }



        close_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popWindow.dismiss();
                dialog.dismiss();
//                view.setAlpha(1);
            }
        });

        // show the popup at bottom of the screen and set some margin at bottom ie,


        //final GridView gvMedia = (GridView) inflatedView.findViewById(R.id.media_gallry);
        final ListView gvMedia1 = (ListView) inflatedView.findViewById(R.id.songs_gallry);

//        if(type.equals("photo")){
//            header.setText(getString(R.string.photo));
//            getPhotos(gvMedia);
//        }
        if (type.equals("music")) {
            header.setText(getString(R.string.music));
            getSongs(gvMedia1);
        }


        popWindow.showAtLocation(findViewById(R.id.baseContent), Gravity.CENTER, 0, 100);

    }

    private void getPhotos(final GridView gridPhoto) {
        photos = new ArrayList<>();

        findViewById(R.id.loading).setVisibility(View.VISIBLE);

        JSONObject jsonBody = null;

        try {
            jsonBody = new JSONObject("{\"id_profil\":\"" + Tools.getData(NewAds.this, "idprofile") + "\"}");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqPhotos = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.galeriephoto_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("Message").contains("any image")) {
                                Toast.makeText(NewAds.this, getString(R.string.no_photo_element), Toast.LENGTH_SHORT).show();
                            } else {
                                if (response.getString("result").equals("true")) {

                                    JSONArray data = response.getJSONArray("data");
                                    for (int i = 0; i < data.length(); i++) {
                                        try {
                                            JSONObject obj = data.getJSONObject(i);
                                            photos.add(new WISPhoto(obj.getInt("idphoto"), "", obj.getString("pathoriginal"), obj.getString("created_at")));
                                        } catch (Exception e) {
                                        }
                                    }
//                                    gridPhoto.setAdapter(new PhotoAdapter((FragmentActivity) getActivity(), photos));
                                    gridPhoto.setAdapter(new CustomPhotoAdapter(photos, NewAds.this));
                                }
                            }
                            findViewById(R.id.loading).setVisibility(View.GONE);

                        } catch (JSONException e) {

//                            view.findViewById(R.id.loading).setVisibility(View.GONE);
                            //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                findViewById(R.id.loading).setVisibility(View.GONE);
                //if (getActivity() != null)
                Toast.makeText(NewAds.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(NewAds.this, "token"));
                headers.put("lang", Tools.getData(NewAds.this, "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqPhotos);
    }

    private void getVideos(final GridView gridVideos) {
        video = new ArrayList<>();

        findViewById(R.id.loading).setVisibility(View.VISIBLE);

        JSONObject jsonBody = null;

        try {
            jsonBody = new JSONObject("{\"id_profil\":\"" + Tools.getData(NewAds.this, "idprofile") + "\"}");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqPhotos = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.getvideos_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {
                                JSONArray data = response.getJSONArray("data");
                                for (int i = 0; i < data.length(); i++) {
                                    try {
                                        JSONObject obj = data.getJSONObject(i);
                                        video.add(new WISVideo(obj.getInt("id"), "", obj.getString("url_video"), obj.getString("created_at"), obj.getString("image_video")));
                                    } catch (Exception e) {
                                    }
                                }
                                gridVideos.setAdapter(new CutomVideoAdapter(video, NewAds.this));
                            }
                            findViewById(R.id.loading).setVisibility(View.GONE);
                        } catch (JSONException e) {

//                            view.findViewById(R.id.loading).setVisibility(View.GONE);
                            //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                findViewById(R.id.loading).setVisibility(View.GONE);
                //if (getActivity() != null)
                Toast.makeText(NewAds.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(NewAds.this, "token"));
                headers.put("lang", Tools.getData(NewAds.this, "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqPhotos);
    }

    private void getSongs(final ListView songsMedia) {
        songs = new ArrayList<>();

        findViewById(R.id.loading).setVisibility(View.VISIBLE);

        JSONObject jsonBody = new JSONObject();

        try {
            jsonBody.put("user_id", Tools.getData(NewAds.this, "idprofile"));
            jsonBody.put("audio_page", 0);
            jsonBody.put("album_page", 0);
            Log.i("jsonBody", String.valueOf(jsonBody));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqMusic = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.getmusic_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i("resonse", response.toString());
                        try {
                            if (response.getBoolean("result")) {


                                JSONArray data1 = response.getJSONArray("album_files");

                                for (int i = 0; i < data1.length(); i++) {
                                    try {
                                        JSONObject obj = data1.getJSONObject(i);
                                        JSONArray songsArry = obj.getJSONArray("songs");
                                        Log.i("AlbumArray", String.valueOf(songsArry));
                                        for (int c = 0; c < songsArry.length(); c++) {
                                            JSONObject obj1 = songsArry.getJSONObject(c);
                                            songs.add(new WISMusic(obj1.getInt("id"), obj1.getString("name"), obj1.getString("artists"), obj1.getString("duration"), obj1.getString("audio_path"), obj1.getString("audio_thumbnail"), obj1.getString("created_at")));
//                                            audio.add(new WISMusic1(obj1.getInt("id"),obj1.getString("name"),obj1.getString("audio_path"),obj1.getString("created_at"),obj1.getString("audio_thumbnail"),chat_type));
                                        }
                                    } catch (Exception e) {
                                    }
                                }

                                JSONArray data = response.getJSONArray("songs_files");

                                for (int i = 0; i < data.length(); i++) {
                                    try {
                                        JSONObject obj = data.getJSONObject(i);

                                        Log.i("songs", obj.toString());

                                        songs.add(new WISMusic(obj.getInt("id"), obj.getString("name"), obj.getString("artists"), obj.getString("duration"), obj.getString("audio_path"), obj.getString("audio_thumbnail"), obj.getString("created_at")));
                                        Log.i("allsongs", String.valueOf(songs));
                                    } catch (Exception e) {
                                    }
                                    songsMedia.setAdapter(new AudioAdapter2(NewAds.this, songs, "songs", new GalleryListener() {
                                        @Override
                                        public void pickedItems(String objectId, Bitmap imageDrawable, String type, String name) {
                                            Log.e("picked image items", name);
                                            Log.e("picked image items", type);
                                            audio_path = name;
                                            audio_id = objectId;
                                            dialog.dismiss();
                                            popWindow.dismiss();

                                            if (type.equals("partage_photo")) {
                                                downloadImage(name);
                                                ivPhoto.setImageBitmap(imageDrawable);
                                                mediaType = "photo";


                                            } else if (type.equals("video")) {

                                                ivPhoto.setImageResource(R.drawable.empty);
                                                mediaType = "video";
                                            } else {
                                                //ivPhoto.setImageResource(R.drawable.empty);
                                                ivPhoto.setImageBitmap(imageDrawable);
                                                mediaType = "audio";
                                            }


                                            mediaName = name;

                                        }
                                    }));
                                    //songs.setAdapter(new CustomPhotoAdapter(photos,NewAds.this));
                                }

                            }
                            findViewById(R.id.loading).setVisibility(View.GONE);

                        } catch (JSONException e) {
                            findViewById(R.id.loading).setVisibility(View.GONE);
                            //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        findViewById(R.id.loading).setVisibility(View.GONE);
                        //if (getActivity() != null)
                        //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(NewAds.this, "token"));
                headers.put("lang", Tools.getData(NewAds.this, "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqMusic);
    }


    @Override
    public void pickedItems(String objectId, Bitmap imageDrawable, String type, String name) {
        Log.e("picked image items", name);
        Log.e("picked image items", type);

        dialog.dismiss();
        popWindow.dismiss();

        if (type.equals("partage_photo")) {
            downloadImage(name);
            ivPhoto.setImageBitmap(imageDrawable);
            mediaType = "photo";


        } else if (type.equals("video")) {
            downloadImage(name);
            //ivPhoto.setImageResource(R.drawable.empty);
            ivPhoto.setImageBitmap(imageDrawable);
            mediaType = "video";
        } else {
            //ivPhoto.setImageResource(R.drawable.empty);
            ivPhoto.setImageBitmap(imageDrawable);
            mediaType = "audio";
        }


        mediaName = name;
    }

    public void selectSong(String objectId, Bitmap imageDrawable, String type, String name) {
        Log.e("picked image items", name);
        Log.e("picked image items", type);


        if (type.equals("partage_photo")) {
            downloadImage(name);
            ivPhoto.setImageBitmap(imageDrawable);
            mediaType = "photo";


        } else if (type.equals("video")) {

            ivPhoto.setImageResource(R.drawable.empty);
            mediaType = "video";
        } else {
            ivPhoto.setImageBitmap(imageDrawable);
            mediaType = "audio";
        }


        mediaName = name;
    }
//     @Override
//    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int hourOfDayEnd, int minuteEnd) {
//        String hourString = hourOfDay < 10 ? "0"+hourOfDay : ""+hourOfDay;
//        String minuteString = minute < 10 ? "0"+minute : ""+minute;
//        String hourStringEnd = hourOfDayEnd < 10 ? "0"+hourOfDayEnd : ""+hourOfDayEnd;
//        String minuteStringEnd = minuteEnd < 10 ? "0"+minuteEnd : ""+minuteEnd;
//         String time = "Picked time: From - "+hourString+"h"+minuteString+" To - "+hourStringEnd+"h"+minuteStringEnd;
////                 String time = "Picked time: From - "+hourString+"h"+minuteString+" To - "+hourStringEnd+"h"+minuteStringEnd;
//
//         etTime.setText(time);
//    }



    @Override
    public void onDateSet(com.fourmob.datetimepicker.date.DatePickerDialog datePickerDialog, int year, int month, int day) {
//        Toast.makeText(NewAds.this, "new date:" + year + "-" + month + "-" + day, Toast.LENGTH_LONG).show();
//         String date="Picked Date:" + day + "-"+ month + "-" + year;

        String date = year + "-" + month + "-" + day;
//        distance.replace("KM","")
        System.out.println("picked date is" +date.replace("Picked Date:",""));
        etDate.setText(date);
    }


    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int hourOfDayEnd, int minuteEnd) {
        hourString = hourOfDay < 10 ? "0"+hourOfDay : ""+hourOfDay;
        minuteString = minute < 10 ? "0"+minute : ""+minute;
        hourStringEnd = hourOfDayEnd < 10 ? "0"+hourOfDayEnd : ""+hourOfDayEnd;
        minuteStringEnd = minuteEnd < 10 ? "0"+minuteEnd : ""+minuteEnd;
        time = "From - "+hourString+"h"+minuteString+" To - "+hourStringEnd+"h"+minuteStringEnd;
        startTime = hourString+":"+minuteString;
        endTime = hourStringEnd+":"+minuteStringEnd;

        Log.e("startTime" ,String.valueOf(startTime));
        Log.e("end time" ,String.valueOf(endTime));

//                 String time = "Picked time: From - "+hourString+"h"+minuteString+" To - "+hourStringEnd+"h"+minuteStringEnd;

        etTime.setText(time);
    }

//    @Override
//    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
//        String hourString = hourOfDay < 10 ? "0"+hourOfDay : ""+hourOfDay;
//        String minuteString = minute < 10 ? "0"+minute : ""+minute;
//        String hourStringEnd = hourOfDay < 10 ? "0"+hourOfDay : ""+hourOfDay;
//        String minuteStringEnd = minute < 10 ? "0"+minute : ""+minute;
//        String time = "Picked time: From - "+hourString+"h"+minuteString+" To - "+hourStringEnd+"h"+minuteStringEnd;
//
//        etTime.setText(time);
//    }

    private class DoUpload extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            findViewById(R.id.loading).setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            findViewById(R.id.loading).setVisibility(View.GONE);

            Log.d("upload results", result);

            if (!result.equals("")) {
                try {
                    JSONObject img = new JSONObject(result);


                    if (img.getBoolean("result")) {
                        Log.d("upload results", img.getString("data"));
                        Log.d("upload image name", img.getString("data"));

                        if (mediaType.equals("video")) {
                            JSONObject videoInfo = img.getJSONObject("data");
                            mediaName = videoInfo.getString("video");
                        }
                        if (mediaType.equals("photo")) {
//                            JSONObject videoInfo = img.getJSONObject("data");

                            mediaName = img.getString("data");

                            Log.d("upload [phot  ok name", mediaName);
                        }


                    }
//                        doUpdate(img.getString("data"));
                    //else
                    //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    findViewById(R.id.loading).setVisibility(View.GONE);
                    Log.e("exception", e.getLocalizedMessage());
                    Toast.makeText(NewAds.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                }

            } else {
                findViewById(R.id.loading).setVisibility(View.GONE);
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }


        @Override
        protected String doInBackground(String... urls) {

            return Tools.doFileUpload(getString(R.string.server_url) + urls[1], urls[0], Tools.getData(NewAds.this, "token"));
        }
    }

    public void downloadImage(String filename) {

        findViewById(R.id.loading).setVisibility(View.VISIBLE);
        ApplicationController.getInstance().getImageLoader().get(getString(R.string.server_url3) + filename, new ImageLoader.ImageListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                findViewById(R.id.loading).setVisibility(View.INVISIBLE);

                ivPhoto.setImageResource(R.drawable.empty);
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                findViewById(R.id.loading).setVisibility(View.INVISIBLE);

                if (response.getBitmap() != null) {
                    ivPhoto.setImageBitmap(response.getBitmap());
                } else {
                    ivPhoto.setImageResource(R.drawable.empty);
                }
            }
        });
    }

    public void selectAudio(String filename) {

        findViewById(R.id.loading).setVisibility(View.VISIBLE);
        ApplicationController.getInstance().getImageLoader().get(filename, new ImageLoader.ImageListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                findViewById(R.id.loading).setVisibility(View.INVISIBLE);

                ivPhoto.setImageResource(R.drawable.empty);
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                findViewById(R.id.loading).setVisibility(View.INVISIBLE);

                if (response.getBitmap() != null) {
                    ivPhoto.setImageBitmap(response.getBitmap());
                } else {
                    ivPhoto.setImageResource(R.drawable.empty);
                }
            }
        });
    }



    private void openDistanceDropDown() {

        popupListDis = new ArrayList();

        popupListDis.add(new PopupElementsText("1KM"));
        popupListDis.add(new PopupElementsText("5KM"));
        popupListDis.add(new PopupElementsText("10KM"));
        popupListDis.add(new PopupElementsText("50KM"));
        popupListDis.add(new PopupElementsText("+50KM"));

        PopupadApterText simpleAdapter = new PopupadApterText(this, false, popupListDis);


        dialog = DialogPlus.newDialog(this)
                .setAdapter(simpleAdapter)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View v, int position) {
                        if(position ==0){
                            dialog.dismiss();
                            distanceTV.setText("1KM");
                            frequencyTV.setText("5");

                        }
                        else if(position ==1){
                            dialog.dismiss();
                            distanceTV.setText("5KM");
                            frequencyTV.setText("10");



                        }
                        else if(position == 2){
                            dialog.dismiss();
                            distanceTV.setText("10KM");
                            frequencyTV.setText("15");


                        }else if(position ==3){
                            dialog.dismiss();
                            distanceTV.setText("50KM");
                            frequencyTV.setText("20");


                        }else if(position ==4){
                            dialog.dismiss();
                            distanceTV.setText("+50KM");
                            frequencyTV.setText("25");


                        }

                    }
                })
                .setExpanded(false)  // This will enable the expand feature, (similar to android L share dialog)
                .create();
        dialog.show();

    }



    private void openTargetDropDown() {

        popuplistTarget=new ArrayList();
        popuplistTarget.add(new PopupElementsText("13-18"));
        popuplistTarget.add(new PopupElementsText("18-34"));
        popuplistTarget.add(new PopupElementsText("35-45"));
        popuplistTarget.add(new PopupElementsText("46-55"));
        popuplistTarget.add(new PopupElementsText("56-65"));
        popuplistTarget.add(new PopupElementsText("+65"));
        PopupadApterText adapter=new PopupadApterText(this,false,popuplistTarget);

        dialog = DialogPlus.newDialog(this)
                .setAdapter(adapter)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View v, int position) {
                        if(position ==0){
                            dialog.dismiss();
                            targetTV.setText("13-18");

                        }
                        else if(position ==1){
                            targetTV.setText("18-34");

                        }
                        else if(position == 2) {
                            dialog.dismiss();
                            targetTV.setText("35-45");
                        }
                        else if(position ==3){
                            dialog.dismiss();
                            targetTV.setText("46-65");

                        }
                        else if(position ==4){
                            dialog.dismiss();
                            targetTV.setText("56-65");

                        }
                        else if(position == 5){
                            dialog.dismiss();
                            targetTV.setText("+65");


                        }

                    }
                })
                .setExpanded(false)  // This will enable the expand feature, (similar to android L share dialog)
                .create();
        dialog.show();

    }


//    private void activityDropDown() {
//
//       final ArrayList popupList = new ArrayList();
//
//       String[] menuArray;
//
//       menuArray = getResources().getStringArray(R.array.activities_list);
//        for(int i=0;i<menuArray.length;i++){
//         //popupList.add(menuArray[i]);
//           popupList.add(new PopupElementsText(menuArray[i]));
//            }
//
//
//
//
//        PopupadApterText simpleAdapter = new PopupadApterText(this, false, popupList);
//
//
//        dialog = DialogPlus.newDialog(this)
//        .setAdapter(simpleAdapter)
//        .setOnItemClickListener(new OnItemClickListener() {
//          @Override
//            public void onItemClick(DialogPlus dialog, Object item, View v, int position) {
//
//
//                ArrayList<PopupElementsText> list = popupList;
//
//                for(int i = 0; i < list.size(); i++)
//                {
//
//                    dialog.dismiss();
//                    activityTV.setText(list.get(position).getTitle());
//                    activityTV.setTextColor(getResources().getColor(R.color.black));
//                    }
//                }
//            })
//       .setExpanded(false)// This will enable the expand feature, (similar to android L share dialog)
//        .create();
//        dialog.show();
//        }

}

//    private int getPostiton(String locationid, Cursor cursor) {
//        int i;
//        cursor.moveToFirst();
//        for (i = 0; i < cursor.getCount() - 1; i++) {
//
//            String locationVal = cursor.getString(cursor.getColumnIndex(RoadMoveDataBase.LT_LOCATION));
//            if (locationVal.equals(locationid)) {
//                position = i + 1;
//                break;
//            } else {
//                position = 0;
//            }
//            cursor.moveToNext();
//        }
//    }
//    private int getIndexByString(Spinner spinner, String string) {
//        int index = 0;
//
//        for (int i = 0; i < spinner.getCount(); i++) {
//            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(string)) {
//                index = i;
//                break;
//            }
//        }
//        return index;
//    }
