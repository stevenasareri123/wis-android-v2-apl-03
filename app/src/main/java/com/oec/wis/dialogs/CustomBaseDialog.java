package com.oec.wis.dialogs;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.widget.TextView;

import com.flyco.animation.Attention.Swing;
import com.flyco.dialog.utils.CornerUtils;
import com.flyco.dialog.widget.base.BaseDialog;
import com.oec.wis.R;

/**
 * Created by asareri08 on 29/09/16.
 */
public class CustomBaseDialog extends BaseDialog<CustomBaseDialog> {
//    private  tv_cancel;
//    private TextView tv_exit;
    Context context;
    public CustomBaseDialog(Context context){

        super(context);

        this.context = context;
    }

    @Override
    public View onCreateView() {


        widthScale(0.85f);
        showAnim(new Swing());

        // dismissAnim(this, new ZoomOutExit());
        View inflate = View.inflate(context, R.layout.event_dialogue, null);
//        tv_cancel = ViewFindUtils.find(inflate, R.id.tv_cancel);
//        tv_exit = ViewFindUtils.find(inflate, R.id.tv_exit);
        inflate.setBackgroundDrawable(
                CornerUtils.cornerDrawable(Color.parseColor("#ffffff"), dp2px(5)));

        return inflate;

    }

    @Override
    public void setUiBeforShow() {

    }
}
