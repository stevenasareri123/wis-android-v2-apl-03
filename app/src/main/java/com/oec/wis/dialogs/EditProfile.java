package com.oec.wis.dialogs;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.borax12.materialdaterangepicker.date.DatePickerDialog;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.oec.wis.ApplicationController;
import com.oec.wis.R;
import com.oec.wis.adapters.CountryAdapter;
import com.oec.wis.adapters.PopupadApterText;
import com.oec.wis.adapters.Popupadapter;
import com.oec.wis.entities.PopupElementsText;
import com.oec.wis.entities.WISActuality;
import com.oec.wis.entities.WISChat;
import com.oec.wis.entities.WISCountry;
import com.oec.wis.entities.WISLanguage;
import com.oec.wis.tools.CustomPhoneNumberFormattingTextWatcher;
import com.oec.wis.tools.OnPhoneChangedListener;
import com.oec.wis.tools.PhoneUtils;
import com.oec.wis.tools.Tools;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnItemClickListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;

public class EditProfile extends AppCompatActivity implements DatePickerDialog.OnDateSetListener,AdapterView.OnItemSelectedListener {
    protected static final TreeSet<String> CANADA_CODES = new TreeSet<>();

    static {
        CANADA_CODES.add("204");
        CANADA_CODES.add("236");
        CANADA_CODES.add("249");
        CANADA_CODES.add("250");
        CANADA_CODES.add("289");
        CANADA_CODES.add("306");
        CANADA_CODES.add("343");
        CANADA_CODES.add("365");
        CANADA_CODES.add("387");
        CANADA_CODES.add("403");
        CANADA_CODES.add("416");
        CANADA_CODES.add("418");
        CANADA_CODES.add("431");
        CANADA_CODES.add("437");
        CANADA_CODES.add("438");
        CANADA_CODES.add("450");
        CANADA_CODES.add("506");
        CANADA_CODES.add("514");
        CANADA_CODES.add("519");
        CANADA_CODES.add("548");
        CANADA_CODES.add("579");
        CANADA_CODES.add("581");
        CANADA_CODES.add("587");
        CANADA_CODES.add("604");
        CANADA_CODES.add("613");
        CANADA_CODES.add("639");
        CANADA_CODES.add("647");
        CANADA_CODES.add("672");
        CANADA_CODES.add("705");
        CANADA_CODES.add("709");
        CANADA_CODES.add("742");
        CANADA_CODES.add("778");
        CANADA_CODES.add("780");
        CANADA_CODES.add("782");
        CANADA_CODES.add("807");
        CANADA_CODES.add("819");
        CANADA_CODES.add("825");
        CANADA_CODES.add("867");
        CANADA_CODES.add("873");
        CANADA_CODES.add("902");
        CANADA_CODES.add("905");
    }

    protected PhoneNumberUtil mPhoneNumberUtil = PhoneNumberUtil.getInstance();
    protected SparseArray<ArrayList<WISCountry>> mCountriesMap = new SparseArray<>();
    protected String mLastEnteredPhone;
    protected AdapterView.OnItemSelectedListener mOnItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            WISCountry c = (WISCountry) spCountry.getItemAtPosition(position);
            if (mLastEnteredPhone != null && mLastEnteredPhone.startsWith(c.getCountryCodeStr())) {
                return;
            }

            //etPhone.getText().clear();
            //etPhone.getText().insert(etPhone.getText().length() > 0 ? 1 : 0, String.valueOf(c.getCountryCode()));
            //etPhone.setSelection(etPhone.length());
            //mLastEnteredPhone = null;



        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };
    protected OnPhoneChangedListener mOnPhoneChangedListener = new OnPhoneChangedListener() {
        @Override
        public void onPhoneChanged(String phone) {
            try {
                mLastEnteredPhone = phone;
                Phonenumber.PhoneNumber p = mPhoneNumberUtil.parse(phone, null);
                ArrayList<WISCountry> list = mCountriesMap.get(p.getCountryCode());
                WISCountry country = null;
                if (list != null) {
                    if (p.getCountryCode() == 1) {
                        String num = String.valueOf(p.getNationalNumber());
                        if (num.length() >= 3) {
                            String code = num.substring(0, 3);
                            if (CANADA_CODES.contains(code)) {
                                for (WISCountry c : list) {
                                    if (c.getPriority() == 1) {
                                        country = c;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    if (country == null) {
                        for (WISCountry c : list) {
                            if (c.getPriority() == 0) {
                                country = c;
                                break;
                            }
                        }
                    }
                }
                if (country != null) {
                    final int position = country.getNum();
                    spCountry.post(new Runnable() {
                        @Override
                        public void run() {
                            spCountry.setSelection(position);
                        }
                    });
                }
            } catch (NumberParseException ignore) {
            }

        }
    };
    Spinner spCountries, spCountry, spLang, spTimezone, spActivity;
    EditText etFName, etLName, etTitle, etState, etPhone, etCName, etApe, etEmail, etPwd, etRName;
    DatePicker dpBirth, dpStart;
    RadioGroup rgSexe;
    CheckBox cbTourist;
    CountryAdapter cAdapter;
    Typeface  font;
    DialogPlus dialog;
    TextView companynameTV,codeAPETV,emailtextTV,passwordTV,activityTV,datestartTV,namerepTV,countryTV,selecttimezoneTV,telephoneTV,dropLanguageTV,languageTV;
    RelativeLayout lanUseRelLay,countryRelLay;
    ImageView lanUseIV,activityIV,countryIV,timezoneImagview;
    TextView activityTextView;
    RelativeLayout activityRl,timezoneRL;
    TextView headerTitleTV,countryNameTV;
    ArrayList<PopupElementsText> list;
    TextView timeZoneTextview;
    EditText etDate;


    protected void initCodes(Context context) {
        new AsyncPhoneInitTask(context).execute();
    }

    protected String validate() {
        String region = null;
        String phone = null;
        if (mLastEnteredPhone != null) {
            try {
                Phonenumber.PhoneNumber p = mPhoneNumberUtil.parse(mLastEnteredPhone, null);
                StringBuilder sb = new StringBuilder(16);
                sb.append('+').append(p.getCountryCode()).append(p.getNationalNumber());
                phone = sb.toString();
                region = mPhoneNumberUtil.getRegionCodeForNumber(p);
            } catch (NumberParseException ignore) {
            }
        }
        if (region != null) {
            return phone;
        } else {
            return null;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        initCodes(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Tools.setLocale(this, Tools.getData(this, "lang_pr"));

        setContentView(R.layout.activity_edit_profile);

        loadControls();
        setListener();
        loadData();
        loadProfilInfo();
        setTypefaceFont();
//        getTimezone();
    }


    private void loadData() {
        etFName.setText(Tools.getData(this, "firstname_prt"));
        etLName.setText(Tools.getData(this, "lastname_prt"));
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this,
                R.layout.edit_profile_spinner_item, getResources().getStringArray(R.array.count_list));
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCountries.setAdapter(dataAdapter);
        dataAdapter = new ArrayAdapter<>(this,
                R.layout.edit_profile_spinner_item, getResources().getStringArray(R.array.activities));
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spActivity.setAdapter(dataAdapter);
//        dataAdapter = new ArrayAdapter<>(this,
//                R.layout.edit_profile_spinner_item, getResources().getStringArray(R.array.new_lang_list));
//        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

//        spLang.setAdapter(dataAdapter);

        dataAdapter = new ArrayAdapter<>(this,
                R.layout.edit_profile_spinner_item, getTimezone());
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spTimezone.setAdapter(dataAdapter);


        cAdapter = new CountryAdapter(this);
        spCountry.setAdapter(cAdapter);


//        spLang.setOnItemSelectedListener(this);
    }



    private ArrayList  getTimezone(){
        ArrayList timeZoneList=new ArrayList<>();
        String[] ids = TimeZone.getAvailableIDs();


        for (String id : ids) {
            timeZoneList.add(displayTimeZone(TimeZone.getTimeZone(id)));

        }
        return timeZoneList;
    }

    private static String displayTimeZone(TimeZone tz) {

        long hours = TimeUnit.MILLISECONDS.toHours(tz.getRawOffset());
        long minutes = TimeUnit.MILLISECONDS.toMinutes(tz.getRawOffset())
                - TimeUnit.HOURS.toMinutes(hours);
        // avoid -4:-30 issue
        minutes = Math.abs(minutes);

        String result = "";
        if (hours > 0) {
//            :%02d
//            :%02d
            result = String.format("(GMT+%d) %s", hours,tz.getID());
        } else {
            result = String.format("(GMT%d) %s", hours,tz.getID());
        }

        return result;

    }

    private static String displayTimeZoneTwo(TimeZone tz) {

        long hours = TimeUnit.MILLISECONDS.toHours(tz.getRawOffset());
        long minutes = TimeUnit.MILLISECONDS.toMinutes(tz.getRawOffset())
                - TimeUnit.HOURS.toMinutes(hours);
        // avoid -4:-30 issue
        minutes = Math.abs(minutes);

        String result = "";
        if (hours > 0) {
//            :%02d
//            :%02d
            result = String.format("%s",tz.getID());
        } else {
            result = String.format("%s",tz.getID());
        }

        return result;

    }



    private void setListener() {

        lanUseRelLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openLanDropDown();
            }
        });
        dropLanguageTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openLanDropDown();

            }
        });
        lanUseIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openLanDropDown();

            }
        });


        activityIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openActivityDropDown();
            }
        });
        activityTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openActivityDropDown();
            }
        });
        activityRl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openActivityDropDown();

            }
        });
//
        etDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = com.borax12.materialdaterangepicker.date.DatePickerDialog.newInstance(
                        EditProfile.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
//                dpd.setAutoHighlight(mAutoHighlight);
                dpd.show(getFragmentManager(), "Datepickerdialog");
            }
        });



        findViewById(R.id.close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        findViewById(R.id.bSave).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkData()) {
                    updateProfile();
                }
            }
        });
        spCountry.setOnItemSelectedListener(mOnItemSelectedListener);
        etPhone.addTextChangedListener(new CustomPhoneNumberFormattingTextWatcher(mOnPhoneChangedListener));
        InputFilter filter = new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end,
                                       Spanned dest, int dstart, int dend) {
                for (int i = start; i < end; i++) {
                    char c = source.charAt(i);
                    if (dstart > 0 && !Character.isDigit(c)) {
                        return "";
                    }
                }
                return null;
            }
        };
        etPhone.setFilters(new InputFilter[]{filter});
       spTimezone.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
           @Override
           public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
               String item = parent.getItemAtPosition(position).toString();
               spTimezone.setSelection(position);

           }

           @Override
           public void onNothingSelected(AdapterView<?> parent) {

           }
       });
//        spLang.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(final AdapterView<?> parent, View view, int position, long id) {
//
//
//                DialogPlus dialog = DialogPlus.newDialog(EditProfile.this)
//                        .setAdapter(dataAdapter1)
//                        .setOnItemClickListener(new OnItemClickListener() {
//                            @Override
//                            public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
//                                String item1 = parent.getItemAtPosition(position).toString();
//                                spLang.setSelection(position);
//
//                            }
//                        })
//                        .setExpanded(true)  // This will enable the expand feature, (similar to android L share dialog)
//                        .create();
//                dialog.show();
//
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });


//         country

        countryRelLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCountryDropDown();
            }
        });
        countryNameTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCountryDropDown();
            }
        });
        countryIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCountryDropDown();
            }
        });
        timezoneRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTimeZoneDropdown();
            }


        });


    }

    private void openTimeZoneDropdown() {

    }

    private void openCountryDropDown() {
        ArrayList popupList =new ArrayList();
        ArrayList newArrayList= new ArrayList();
       String countryVal= String.valueOf(Collections.addAll(newArrayList, getResources().getStringArray(R.array.count_list)));


        PopupElementsText  popupElementsText=new PopupElementsText(countryVal);
        System.out.println("popupelementstext" +popupElementsText);
        System.out.println("countryvallues" +countryVal);

        String val= String.valueOf(popupList.add(new PopupElementsText(countryVal.toString())));

        System.out.println("val" +val);
        final PopupadApterText simpleAdapter = new PopupadApterText(this, false, popupList);

        dialog = DialogPlus.newDialog(this)
                .setAdapter(simpleAdapter)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View v, int position) {

//                        if(position ==0){
//                            dropLanguageTV.setText("French");
//                            dialog.dismiss();
//                        }
//                        else if(position ==1){
//                            dropLanguageTV.setText("English");
//                            dialog.dismiss();
//                        }
//                        else if(position == 2) {
//                            dropLanguageTV.setText("Spanish");
//                            dialog.dismiss();
//                        }

                    }
                })
                .setExpanded(false)  // This will enable the expand feature, (similar to android L share dialog)
                .create();
        dialog.show();



    }

    private void openActivityDropDown() {
        final ArrayList popupList = new ArrayList();
        PopupElementsText popupElementsText = null;

        final String mystring = String.valueOf(getResources().getStringArray(R.array.activities));
//        popupList.add(mystring);
//        System.out.println("added values"  +popupList.add(mystring));
//        popupList.add(new PopupElementsText(mystring));


//        final ArrayList<String> myResArrayList = new ArrayList<String>();
//
//       String arrayList = String.valueOf(Collections.addAll(myResArrayList, getResources().getStringArray(R.array.activities)));
//        popupList.add(arrayList);

//
//        System.out.println("myResult" +myResArrayList);
//        PopupElementsText pop;
//        popupList.add(new PopupElementsText(myResArrayList));
//        String titlePosition=list.get(position).getTitle().position;

//        String lang = PopupElementsText.ge.toString();
//        ArrayList<String[]> list = new ArrayList<>();
////        list.add(getResources().getStringArray(R.array.activities));
//        String data= String.valueOf(Collections.addAll(popupList, getResources().getStringArray(R.array.activities)));
//        System.out.println("popuplist===>" + popupList);
//


//        ArrayList showPopupValues=new ArrayList();
//        popupList.add((new PopupElementsText(getResources().getStringArray(R.array.activities));
//
//        System.out.println("shoepopuplist===>" + showPopupValues);




        popupList.add(new PopupElementsText("Aeronautics and space"));
        popupList.add(new PopupElementsText("Agriculture-food"));
        popupList.add(new PopupElementsText("Food-food Industries"));
        popupList.add(new PopupElementsText("crafts"));
        popupList.add(new PopupElementsText("Autovisual,Cinema"));
        popupList.add(new PopupElementsText("Auditing,accounting,management"));
        popupList.add(new PopupElementsText("Automotive"));
        popupList.add(new PopupElementsText("Bank,insurance"));
        popupList.add(new PopupElementsText("Building public works"));
        popupList.add(new PopupElementsText("Biology,chemistry,pharmacy"));
        popupList.add(new PopupElementsText("Coaching"));
        popupList.add(new PopupElementsText("Trade and distribution"));
        popupList.add(new PopupElementsText("Communication"));
        popupList.add(new PopupElementsText("Creation and crafts"));
        popupList.add(new PopupElementsText("culture,Heritage"));
        popupList.add(new PopupElementsText("Defence, Security"));
        popupList.add(new PopupElementsText("Law"));
        popupList.add(new PopupElementsText("Edition, Book"));
        popupList.add(new PopupElementsText("Education"));
        popupList.add(new PopupElementsText("environment"));
        popupList.add(new PopupElementsText("Train"));
        popupList.add(new PopupElementsText("Fairs and congresses"));
        popupList.add(new PopupElementsText("Public function"));
        popupList.add(new PopupElementsText("Hotel and catering"));
        popupList.add(new PopupElementsText("Humanitarian"));
        popupList.add(new PopupElementsText("Real Estate"));
        popupList.add(new PopupElementsText("Industry"));
        popupList.add(new PopupElementsText("IT,telecm,web"));
        popupList.add(new PopupElementsText("Journalism"));
        popupList.add(new PopupElementsText("Languages"));
        popupList.add(new PopupElementsText("Marketing,Advertising"));
        popupList.add(new PopupElementsText("Medical"));
        popupList.add(new PopupElementsText("Fashion -Textile"));
        popupList.add(new PopupElementsText("Paramedic"));
        popupList.add(new PopupElementsText("Cleanliness and associated services"));
        popupList.add(new PopupElementsText("Psychology"));
        popupList.add(new PopupElementsText("Human Resources"));
        popupList.add(new PopupElementsText("Human and social sciences"));
        popupList.add(new PopupElementsText("Secretariat"));
        popupList.add(new PopupElementsText("Social"));
        popupList.add(new PopupElementsText("Sport"));
        popupList.add(new PopupElementsText("Tourism"));
        popupList.add(new PopupElementsText("Logistic -Transport"));
        popupList.add(new PopupElementsText("Others"));

        ArrayList textValue=new ArrayList();
        textValue.add(popupList);

        final String value= String.valueOf(popupList);
        System.out.println("textValue" +popupList.toString());
        System.out.println("popuplist" +textValue.add(popupList));
        System.out.println("popuplist====>" +popupList);
        System.out.println("popuplist size" +popupList.size());

        final PopupadApterText simpleAdapter = new PopupadApterText(this, false, popupList);



        dialog = DialogPlus.newDialog(this)
                .setAdapter(simpleAdapter)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View v, int position) {

                        System.out.println("position ---->" +position);
//                        activityTextView.setText((CharSequence) myResArrayList);


                        if(position ==0){
                         dialog.dismiss();
                            activityTextView.setText("Aeronautics and space");
                        }
                        else if(position ==1){
                            dialog.dismiss();
                            activityTextView.setText("Agriculture-food");
                        }
                        else if(position == 2) {
                            dialog.dismiss();
                            activityTextView.setText("Food-food Industries");
                         }else if(position ==3){
                            dialog.dismiss();
                          activityTextView.setText("crafts");
                        }else if(position ==4) {
                            dialog.dismiss();
                            activityTextView.setText("Autovisual,Cinema");

                        }else if(position == 5){
                            dialog.dismiss();
                         activityTextView.setText("Auditing,accounting,management");

                        }else if(position == 6){
                            dialog.dismiss();
                            activityTextView.setText("Automotive");
                        }
                        else if(position ==7){
                            dialog.dismiss();
                            activityTextView.setText("Bank,insurance");

                        }else if(position ==8){
                            dialog.dismiss();
                            activityTextView.setText("Building public works");
                        }else if(position ==9){
                            dialog.dismiss();
                            activityTextView.setText("Biology,chemistry,pharmacy");
                        }else if(position ==10){
                            dialog.dismiss();
                            activityTextView.setText("Coaching");
                        }
//                          for (int i=0;i<position;i++){
//
//
//                              activityTextView.setText((CharSequence) popupList);
//
//                          }


//                        String selectedFromList = (popupList.getItemAtPosition(position));

//                        for(int i =0 ; i<position; i++){
//                            for(int j = 0 ; j<=i ; j++){
//
//                            }
//
//                            activityTextView.setText(i);
//
//                        }

                    }
                })
                .setExpanded(false)  // This will enable the expand feature, (similar to android L share dialog)
                .setOverlayBackgroundResource(android.R.color.transparent)
                .create();
        dialog.show();

    }

    private void loadControls() {
        spCountries = (Spinner) findViewById(R.id.spCountryId);
        etFName = (EditText) findViewById(R.id.etFName);
        etLName = (EditText) findViewById(R.id.etLName);
        etTitle = (EditText) findViewById(R.id.etTitle);
        etState = (EditText) findViewById(R.id.etState);
        dpBirth = (DatePicker) findViewById(R.id.dpBirthday);
        rgSexe = (RadioGroup) findViewById(R.id.rgSexe);
        cbTourist = (CheckBox) findViewById(R.id.cbTourist);
//        spLang = (Spinner) findViewById(R.id.spLang);
        spTimezone = (Spinner) findViewById(R.id.spTimezone);
        spActivity = (Spinner) findViewById(R.id.spActivity);
        spCountry = (Spinner) findViewById(R.id.cFlag);
        etPhone = (EditText) findViewById(R.id.etPhone);
        etCName = (EditText) findViewById(R.id.etCName);
        etApe = (EditText) findViewById(R.id.etApe);
        etEmail = (EditText) findViewById(R.id.etEmail);
        etPwd = (EditText) findViewById(R.id.etPwd);
//        dpStart = (DatePicker) findViewById(R.id.dpActivity);
        etRName = (EditText) findViewById(R.id.etRepName);
//        etDate=(EditText)findViewById(R.id.etDate);
        languageTV=(TextView)findViewById(R.id.languageTV);
        companynameTV=(TextView)findViewById(R.id.tvCName);
        codeAPETV=(TextView)findViewById(R.id.tvApe);
        emailtextTV=(TextView)findViewById(R.id.tvEmail);
        passwordTV=(TextView)findViewById(R.id.passwordTV);
        activityTV=(TextView)findViewById(R.id.tvActivity);
        datestartTV=(TextView)findViewById(R.id.tvStart);
        namerepTV=(TextView)findViewById(R.id.tvRep);
        countryTV=(TextView)findViewById(R.id.tvCountry);
        selecttimezoneTV=(TextView)findViewById(R.id.selectTimeZoneTV);
        telephoneTV=(TextView)findViewById(R.id.phoneNumTV);

        lanUseRelLay=(RelativeLayout)findViewById(R.id.lanUseRelLay);
        lanUseIV=(ImageView)findViewById(R.id.lanUseIV);
        dropLanguageTV=(TextView)findViewById(R.id.dropLanguageTV);

//        etDate=(EditText)findViewById(R.id.etDate);
        activityRl=(RelativeLayout)findViewById(R.id.activityRl);
        activityTextView=(TextView)findViewById(R.id.activityTextView);
        activityIV=(ImageView) findViewById(R.id.activityIV);
        headerTitleTV=(TextView)findViewById(R.id.headerTitleTV);

        etDate=(EditText)findViewById(R.id.etDate);


//       country
        countryRelLay=(RelativeLayout)findViewById(R.id.countryRelLay);
        countryNameTV=(TextView)findViewById(R.id.countryNameTV);
        countryIV=(ImageView)findViewById(R.id.countryIV);


//         timezone
        timezoneRL=(RelativeLayout)findViewById(R.id.timezoneRL);
        timeZoneTextview=(TextView)findViewById(R.id.timeZoneTextview);
        timezoneImagview=(ImageView)findViewById(R.id.timezoneImagview);
    }


//
    private void setTypefaceFont() {
        try{
            font=Typeface.createFromAsset(EditProfile.this.getAssets(),"fonts/Harmattan-R.ttf");
            etRName.setTypeface(font);
            etPwd.setTypeface(font);
            etEmail.setTypeface(font);
            etApe.setTypeface(font);
            etCName.setTypeface(font);
            etPhone.setTypeface(font);
            etLName.setTypeface(font);
            etFName.setTypeface(font);
            etTitle.setTypeface(font);
            etState.setTypeface(font);
            companynameTV.setTypeface(font);
            languageTV.setTypeface(font);
            codeAPETV.setTypeface(font);
            emailtextTV.setTypeface(font);
            passwordTV.setTypeface(font);
            activityTV.setTypeface(font);
            datestartTV.setTypeface(font);
            namerepTV.setTypeface(font);
            countryTV.setTypeface(font);
            selecttimezoneTV.setTypeface(font);
            telephoneTV.setTypeface(font);
            etDate.setTypeface(font);
            dropLanguageTV.setTypeface(font);
            countryNameTV.setTypeface(font);

            activityTextView.setTypeface(font);
            headerTitleTV.setTypeface(font);

            timeZoneTextview.setTypeface(font);
        }catch(NullPointerException ex){
            System.out.println("Exception" +ex.getMessage());
        }


    }

    private void openLanDropDown() {
        final ArrayList popupList = new ArrayList();

        popupList.add(new PopupElementsText("French"));
        popupList.add(new PopupElementsText("English"));
        popupList.add(new PopupElementsText("Spanish"));

        PopupadApterText simpleAdapter = new PopupadApterText(this, false, popupList);


        dialog = DialogPlus.newDialog(this)
                .setAdapter(simpleAdapter)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View v, int position) {

                        if(position ==0){
                            dropLanguageTV.setText("French");
                            dialog.dismiss();
                        }
                        else if(position ==1){
                            dropLanguageTV.setText("English");
                            dialog.dismiss();
                        }
                        else if(position == 2) {
                            dropLanguageTV.setText("Spanish");
                            dialog.dismiss();
                        }

                    }
                })
                .setExpanded(false)  // This will enable the expand feature, (similar to android L share dialog)
                .create();
        dialog.show();

    }


    private boolean checkData() {
        boolean check = true;
        if (Tools.getData(EditProfile.this, "profiletype").equals("Particular")) {
            if (TextUtils.isEmpty(etFName.getText().toString())) {
                check = false;
                etFName.setError(getString(R.string.msg_empty_fname));
            }
            if (TextUtils.isEmpty(etLName.getText().toString())) {
                check = false;
                etLName.setError(getString(R.string.msg_empty_name));
            }
        }
        if (Tools.getData(EditProfile.this, "profiletype").equals("Business")) {

        }
        if (TextUtils.isEmpty(etState.getText().toString())) {
            check = false;
            etState.setError(getString(R.string.msg_empty_state));
        }
        return check;
    }

    private void saveUData() throws JSONException {
        Tools.saveData(getApplicationContext(), "firstname_prt", etFName.getText().toString());
        Tools.saveData(getApplicationContext(), "lastname_prt", etLName.getText().toString());
        String sexe = "Homme";
        if (rgSexe.getCheckedRadioButtonId() == R.id.rFemal)
            sexe = "Femme";
        Tools.saveData(getApplicationContext(), "sexe_prt", sexe);
        String lang = WISLanguage.fr_FR.toString();
        if (dropLanguageTV.getImeActionId() == 1)
            lang = WISLanguage.en_US.toString();
        else if (dropLanguageTV.getImeActionId() == 2)
            lang = WISLanguage.es_ES.toString();

        Tools.saveData(getApplicationContext(), "lang_pr", lang.substring(0, lang.indexOf("_")));
        Tools.saveData(getApplicationContext(), "tel_pr", etPhone.getText().toString());
        Tools.saveData(getApplicationContext(), "code_ape_etr", etApe.getText().toString());
        Tools.saveData(getApplicationContext(), "email_pr", etEmail.getText().toString());
        Tools.saveData(getApplicationContext(), "activite", getResources().getStringArray(R.array.activities)[spActivity.getSelectedItemPosition()]);


        int year = 0,month=0,day=0;

//        if (Tools.getData(EditProfile.this, "profiletype").equals("Business")) {
//
//            year = dpStart.getYear();;
//            month = dpStart.getMonth();
//           day = dpStart.getDayOfMonth();
//
//        }

        if (Tools.getData(EditProfile.this, "profiletype").equals("Particular")) {


            year = dpBirth.getYear();;
            month =  dpBirth.getMonth();
            day =  dpBirth.getDayOfMonth();

        }
//        if (Tools.getData(EditProfile.this, "profiletype").equals("Associations")) {
//
//            year = dpStart.getYear();;
//            month = dpStart.getMonth();
//            day = dpStart.getDayOfMonth();
//
//        }




        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month-1, day);

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String birthDate = format.format(calendar.getTime());
        Tools.saveData(getApplicationContext(), "date_birth", birthDate);



        Tools.saveData(getApplicationContext(), "country", getResources().getStringArray(R.array.count_list)[spCountries.getSelectedItemPosition()]);
        Tools.saveData(getApplicationContext(), "state", etState.getText().toString());
        Tools.saveData(getApplicationContext(), "pwd", etPwd.getText().toString());
        int tourist = 0;
        if (cbTourist.isChecked())
            tourist = 1;
        Tools.saveData(getApplicationContext(), "touriste", String.valueOf(tourist));
        Tools.saveData(getApplicationContext(), "name", etCName.getText().toString());

        String CurrentString = spTimezone.getSelectedItem().toString();
        String[] timezone_values = CurrentString.split(" ");

        Tools.saveData(getApplicationContext(),"timezone",timezone_values[1]);
        Tools.saveData(getApplicationContext(),"timezone_format",timezone_values[0]);
        Log.i("user language",lang);

    }

    private void updateProfile() {
        findViewById(R.id.loading).setVisibility(View.VISIBLE);
        JSONObject jsonBody = new JSONObject();

        try {
            jsonBody.put("id_profil", Tools.getData(EditProfile.this, "idprofile"));
            jsonBody.put("firstname_prt", etFName.getText().toString());
            jsonBody.put("lastname_prt", etLName.getText().toString());
            jsonBody.put("special_other", "");
            jsonBody.put("code_ape_etr", etApe.getText().toString());
            jsonBody.put("place_addre", "");
            jsonBody.put("country", getResources().getStringArray(R.array.count_list)[spCountries.getSelectedItemPosition()]);
            jsonBody.put("state", etState.getText().toString());
            jsonBody.put("name", etCName.getText().toString());
            jsonBody.put("name_representant_etr", etRName.getText().toString());
            jsonBody.put("email", etEmail.getText().toString());
            jsonBody.put("password", etPwd.getText().toString());
            jsonBody.put("photo", Tools.getData(this, "photo"));
            jsonBody.put("tel", etPhone.getText().toString());
            jsonBody.put("typeaccount", Tools.getData(this, "profiletype"));
            //timezone
            Object[] spTimezoneArray = getTimezone().toArray();
            String str = spTimezoneArray[spTimezone.getSelectedItemPosition()].toString();
            String answer = str.substring(str.indexOf("(")+1,str.indexOf(")"));
            String[] separated = str.split("");

            String CurrentString = spTimezone.getSelectedItem().toString();
            String[] timezone_values = CurrentString.split(" ");


            jsonBody.put("time_zone",timezone_values[1]);
            jsonBody.put("time_zone_format", timezone_values[0]);


            String lang = WISLanguage.fr_FR.toString();
            if (dropLanguageTV.getImeActionId() == 1)
                lang = WISLanguage.en_US.toString();
            else if (dropLanguageTV.getImeActionId() == 2)
                lang = WISLanguage.es_ES.toString();

            jsonBody.put("language", lang);

            int year = dpBirth.getYear();
            int month = dpBirth.getMonth();
            int day = dpBirth.getDayOfMonth();

            Calendar calendar = Calendar.getInstance();
            calendar.set(year, month, day);

            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            String birthDate = format.format(calendar.getTime());
            jsonBody.put("date_birth", birthDate);
            if (spActivity.getVisibility() == View.VISIBLE)
                jsonBody.put("activite", getResources().getStringArray(R.array.activities)[spActivity.getSelectedItemPosition()]);
            else
                jsonBody.put("activite", "");
//            if (dpStart.getVisibility() == View.VISIBLE) {
//                year = dpStart.getYear();
//                month = dpStart.getMonth();
//                day = dpStart.getDayOfMonth();
//                calendar.set(year, month, day);
//                String startDate = format.format(calendar.getTime());
//                jsonBody.put("date_birth", startDate);
//            }

            String sexe = "Homme";
            if (rgSexe.getCheckedRadioButtonId() == R.id.rFemal)
                sexe = "Femme";
            jsonBody.put("sexe_prt", sexe);
            int tourist = 0;
            if (cbTourist.isChecked())
                tourist = 1;
            jsonBody.put("touriste", String.valueOf(tourist));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("Text-Put", jsonBody.toString());
        Log.i("URL", getString(R.string.server_url) + getString(R.string.profile_update_meth));
        JsonObjectRequest reqUpdate = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.profile_update_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        findViewById(R.id.loading).setVisibility(View.GONE);
                        try {
                            if (response.getString("result").equals("true")) {
                                saveUData();
                                finish();
                                Toast.makeText(EditProfile.this, getString(R.string.msg_profile_updated), Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            findViewById(R.id.loading).setVisibility(View.GONE);
                            //Toast.makeText(EditProfile.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                findViewById(R.id.loading).setVisibility(View.GONE);
                //Toast.makeText(EditProfile.this, getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(EditProfile.this, "token"));
                headers.put("lang", Tools.getData(EditProfile.this, "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqUpdate);
    }

    private void loadProfilInfo() {
        if (Tools.getData(EditProfile.this, "lang_pr").equals("fr"))
            dropLanguageTV.setSelected(true);
        if (Tools.getData(EditProfile.this, "lang_pr").equals("en"))
            dropLanguageTV.setSelected(true);
        if (Tools.getData(EditProfile.this, "lang_pr").equals("es"))
            dropLanguageTV.setSelected(true);

        if (Tools.getData(EditProfile.this, "profiletype").equals("Business")) {
            findViewById(R.id.tvFName).setVisibility(View.GONE);
            etFName.setVisibility(View.GONE);
            findViewById(R.id.tvLName).setVisibility(View.GONE);
            etLName.setVisibility(View.GONE);
            findViewById(R.id.dpBirthday).setVisibility(View.GONE);
            dpBirth.setVisibility(View.GONE);
            findViewById(R.id.tvSexe).setVisibility(View.GONE);
            rgSexe.setVisibility(View.GONE);
            cbTourist.setVisibility(View.GONE);
        }

        if (Tools.getData(EditProfile.this, "profiletype").equals("Particular")) {
//            findViewById(R.id.tvCName).setVisibility(View.GONE);
            etCName.setVisibility(View.GONE);
//            findViewById(R.id.tvApe).setVisibility(View.GONE);
            etApe.setVisibility(View.GONE);
//            findViewById(R.id.tvActivity).setVisibility(View.GONE);
            spActivity.setVisibility(View.GONE);
            etApe.setVisibility(View.GONE);
//            findViewById(R.id.tvStart).setVisibility(View.GONE);
//            dpStart.setVisibility(View.GONE);
//            findViewById(R.id.tvRep).setVisibility(View.GONE);
            etRName.setVisibility(View.GONE);
        }
        if (Tools.getData(EditProfile.this, "profiletype").equals("Associations")) {
//            ((TextView) findViewById(R.id.tvCName)).setText(R.string.name);
            findViewById(R.id.tvFName).setVisibility(View.GONE);
            etFName.setVisibility(View.GONE);
            findViewById(R.id.tvLName).setVisibility(View.GONE);
            etLName.setVisibility(View.GONE);
            findViewById(R.id.dpBirthday).setVisibility(View.GONE);
            dpBirth.setVisibility(View.GONE);
            findViewById(R.id.tvSexe).setVisibility(View.GONE);
            rgSexe.setVisibility(View.GONE);
            cbTourist.setVisibility(View.GONE);
//            findViewById(R.id.tvApe).setVisibility(View.GONE);
            etApe.setVisibility(View.GONE);
//            findViewById(R.id.tvRep).setVisibility(View.GONE);
            etRName.setVisibility(View.GONE);
        }
        etRName.setText(Tools.getData(EditProfile.this, "name_representant_etr"));
        etCName.setText(Tools.getData(EditProfile.this, "name"));
        etApe.setText(Tools.getData(EditProfile.this, "code_ape_etr"));
        etEmail.setText(Tools.getData(EditProfile.this, "email_pr"));
        etPwd.setText(Tools.getData(EditProfile.this, "pwd"));
        int sAct = 0;
        String[] acts = getResources().getStringArray(R.array.activities);
        for (int i = 0; i < acts.length; i++) {
            if (acts[i].equals(Tools.getData(EditProfile.this, "activite")))
                sAct = i;
        }
        spActivity.setSelection(sAct);
        String birthDate = Tools.getData(EditProfile.this, "date_birth");
        dpBirth.updateDate(Integer.parseInt(birthDate.substring(0, 4)), Integer.parseInt(birthDate.substring(5, 7)), Integer.parseInt(birthDate.substring(8, 10)));
//        dpStart.updateDate(Integer.parseInt(birthDate.substring(0, 4)), Integer.parseInt(birthDate.substring(5, 7)), Integer.parseInt(birthDate.substring(8, 10)));
        String startDate = Tools.getData(EditProfile.this, "start_act");
        Log.i("dob",startDate);

        System.out.println("update Date");
        System.out.println(birthDate);
        System.out.println(Integer.parseInt(birthDate.substring(0, 4)));
        System.out.println(Integer.parseInt(birthDate.substring(5, 7)));
        System.out.println(Integer.parseInt(birthDate.substring(8,10)));
        System.out.println("-------------------------");

        if (startDate.length() > 10)
            dpBirth.updateDate(Integer.parseInt(startDate.substring(0, 4)), Integer.parseInt(startDate.substring(5, 7)), Integer.parseInt(startDate.substring(8, 10)));




        int sCount = 0;
        String[] aCount = getResources().getStringArray(R.array.count_list);
        for (int i = 0; i < aCount.length; i++) {
            if (aCount[i].equals(Tools.getData(EditProfile.this, "country")))
                sCount = i;
        }
        spCountries.setSelection(sCount);

        try{
            etState.setText(Tools.getData(EditProfile.this, "state"));

            if (Tools.getData(EditProfile.this, "touriste").equals("1"))
                cbTourist.setChecked(true);

            String phone = Tools.getData(EditProfile.this, "tel_pr");
            etPhone.setText(phone);
            if (Tools.getData(EditProfile.this, "sexe_prt").equals("Femme"))
                ((RadioButton) findViewById(R.id.rFemal)).setChecked(true);

            String timeZone = Tools.getData(getApplicationContext(),"timezone");
            String format =  Tools.getData(getApplicationContext(),"timezone_format");


            Log.i("timezone",timeZone);

//        int id = getTimezone().indexOf(format.concat(" ").concat(timeZone));
            int id2=getTimezone().indexOf(timeZone);

            ArrayList timeZoneListTwo=new ArrayList<>();
            String[] ids = TimeZone.getAvailableIDs();

            for (String id : ids) {
                timeZoneListTwo.add(displayTimeZoneTwo(TimeZone.getTimeZone(id)));

            }
            int id = timeZoneListTwo.indexOf(timeZone);
            Log.i("timezone list", String.valueOf(timeZoneListTwo));
            Log.i("fomrat", String.valueOf(id));
            spTimezone.setSelection(id);

        }catch (NullPointerException ex){
            System.out.println("Null Exce" +ex.getMessage());
        }




    }

    public void openSpinnerLanguage(){

        final ArrayList popupList = new ArrayList();

        popupList.add("FRENCH");
        popupList.add("ENGLISH");
        popupList.add("SPANISH");

        Popupadapter simpleAdapter = new Popupadapter(EditProfile.this, false, popupList);


        dialog = DialogPlus.newDialog(EditProfile.this)
                .setAdapter(simpleAdapter)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View v, int position) {

                        if(position ==0){
//                            Intent i = new Intent(Intent.ACTION_PICK,
//                                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                            startActivityForResult(i, 1);
//                            dialog.dismiss();


                        }
                        else if(position ==1){

//                            openWisMedia(view,0,"photo");


                        }
                        else if(position == 2){
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                            startActivityForResult(intent,REQUEST_CAMERA);
                            dialog.dismiss();

                        }

                    }
                })
                .setExpanded(false)  // This will enable the expand feature, (similar to android L share dialog)
                .create();
        dialog.show();

//        final CharSequence[] items = {"Caméra","Choisir Parmi la galerie","Choisissez parmi le profil de wis","Annuler"};
//        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//        builder.setTitle("Choisissez Photo De");
//        builder.setItems(items, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(
//                    DialogInterface dialog, int item) {
//                if(items[item].equals("Caméra")){
//                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                    startActivityForResult(intent,REQUEST_CAMERA);
//                }
//                else if(items[item].equals("Choisir Parmi la galerie")){
//
//                    Intent intent = new Intent();
//                    intent.setType("image/*");
//                    intent.setAction(Intent.ACTION_GET_CONTENT);//
//                    startActivityForResult(Intent.createChooser(intent, "Select File"),SELECT_FILE);
//                }
//                else if(items[item].equals("Choisissez parmi le profil de wis")){
//                    Intent i = new Intent(Intent.ACTION_PICK,
//                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                    startActivityForResult(i, 1);
//                }
//                else if(items[item].equals("Annuler")){
//                    dialog.dismiss();
//                }
//            }
//
//        });
//        builder.show();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        ApplicationController.getInstance().cancelPendingRequests(ApplicationController.TAG);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth, int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {
        String date = "Picked date: From- "+dayOfMonth+"/"+(++monthOfYear)+"/"+year+" To "+dayOfMonthEnd+"/"+(++monthOfYearEnd)+"/"+yearEnd;
        etDate.setText(date);

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        final ArrayList popupList = new ArrayList();

        popupList.add("FRENCH");
        popupList.add("ENGLISH");
        popupList.add("SPANISH");

        Popupadapter simpleAdapter = new Popupadapter(EditProfile.this, false, popupList);


        dialog = DialogPlus.newDialog(EditProfile.this)
                .setAdapter(simpleAdapter)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View v, int position) {

                        if(position ==0){
//                            Intent i = new Intent(Intent.ACTION_PICK,
//                                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                            startActivityForResult(i, 1);
//                            dialog.dismiss();


                        }
                        else if(position ==1){

//                            openWisMedia(view,0,"photo");


                        }
                        else if(position == 2){
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                            startActivityForResult(intent,REQUEST_CAMERA);
                            dialog.dismiss();

                        }

                    }
                })
                .setExpanded(false)  // This will enable the expand feature, (similar to android L share dialog)
                .create();
        dialog.show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    protected class AsyncPhoneInitTask extends AsyncTask<Void, Void, ArrayList<WISCountry>> {

        private int mSpinnerPosition = -1;
        private Context mContext;

        public AsyncPhoneInitTask(Context context) {
            mContext = context;
        }

        @Override
        protected ArrayList<WISCountry> doInBackground(Void... params) {
            ArrayList<WISCountry> data = new ArrayList<>(233);
            BufferedReader reader = null;
            try {
                reader = new BufferedReader(new InputStreamReader(mContext.getApplicationContext().getAssets().open("countries.dat"), "UTF-8"));

                String line;
                int i = 0;
                while ((line = reader.readLine()) != null) {
                    WISCountry c = new WISCountry(mContext, line, i);
                    data.add(c);
                    ArrayList<WISCountry> list = mCountriesMap.get(c.getCountryCode());
                    if (list == null) {
                        list = new ArrayList<>();
                        mCountriesMap.put(c.getCountryCode(), list);
                    }
                    list.add(c);
                    i++;
                }
            } catch (IOException e) {
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                    }
                }
            }
//            if (!TextUtils.isEmpty(etPhone.getText().toString())) {
//                return data;
//            }
            String countryRegion = PhoneUtils.getCountryRegionFromPhone(mContext);
            int code = mPhoneNumberUtil.getCountryCodeForRegion(countryRegion);
            ArrayList<WISCountry> list = mCountriesMap.get(code);
            if (list != null) {
                for (WISCountry c : list) {
                    if (c.getPriority() == 0) {
                        mSpinnerPosition = c.getNum();
                        break;
                    }
                }
            }
            return data;
        }

        @Override
        protected void onPostExecute(ArrayList<WISCountry> data) {
            cAdapter.addAll(data);
            if (mSpinnerPosition > 0) {
                spCountry.setSelection(mSpinnerPosition);
            }
        }
    }

}
//
//    private void showNoFooterDialog(Holder holder, int gravity, BaseAdapter adapter,
//                                    OnClickListener clickListener, OnItemClickListener itemClickListener,
//                                    OnDismissListener dismissListener, OnCancelListener cancelListener,
//                                    boolean expanded) {
//        final DialogPlus dialog = DialogPlus.newDialog(this)
//                .setContentHolder(holder)
//                .setHeader(R.layout.header)
//                .setCancelable(true)
//                .setGravity(gravity)
//                .setAdapter(adapter)
//                .setOnClickListener(clickListener)
//                .setOnItemClickListener(new OnItemClickListener() {
//                    @Override public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
//                        Log.d("DialogPlus", "onItemClick() called with: " + "item = [" +
//                                item + "], position = [" + position + "]");
//                    }
//                })
//                .setOnDismissListener(dismissListener)
//                .setOnCancelListener(cancelListener)
//                .setExpanded(expanded)
//                .create();
//        dialog.show();
//    }
