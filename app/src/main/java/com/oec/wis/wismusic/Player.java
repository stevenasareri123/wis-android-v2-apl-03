package com.oec.wis.wismusic;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.text.Editable;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.oec.wis.ApplicationController;
import com.oec.wis.CircularSeekBar;
import com.oec.wis.R;
import com.oec.wis.Sample;
import com.oec.wis.adapters.PlayAlbumAdapter;
import com.oec.wis.adapters.PlayPlaylistAlbumAdapter;
import com.oec.wis.adapters.PlayPlaylistSongAdapter;
import com.oec.wis.adapters.PlaySongAdapter;
import com.oec.wis.adapters.Popupadapter;
import com.oec.wis.dialogs.NewAds;
import com.oec.wis.entities.Popupelements;
import com.oec.wis.entities.UserNotification;
import com.oec.wis.entities.WISMusic;
import com.oec.wis.entities.WISPlaylist;
import com.oec.wis.entities.WISPlaylist1;
import com.oec.wis.entities.WISSongs;
import com.oec.wis.tools.TextWatcherAdapter;
import com.oec.wis.tools.Tools;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnItemClickListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

//import com.oec.wis.adapters.SongsAdapter;



public class Player extends Activity implements SeekBar.OnSeekBarChangeListener,MediaPlayer.OnCompletionListener {
    private  MediaPlayer mp;

    GridView navGrid;

    private android.os.Handler myHandler = new android.os.Handler();;
    SeekBar sb;
    ImageButton btFb,btPly,btFf;
    private static final float BLUR_RADIUS = 25f;
    ImageButton btnShuffle,btnRepeat;
    private Uri finalUri;
    ImageView back;

    PlaySongAdapter adapter;
    PlayAlbumAdapter adapter1;
    PlayPlaylistAlbumAdapter adapter3;
    PlayPlaylistSongAdapter adapter4;


    ImageView userNotificationView;


    TextView songName;
    TextView artist;
    TextView startTime,endTime;

    String type;
    TextView headerText;
    ImageView songShare;
    ImageView albumArt;

    EditText songSearch;

    List<WISSongs> songs;
    List<WISMusic> list;
    List<WISPlaylist> wissongs;
    List<WISPlaylist1> wisalbum;
    RelativeLayout songData;
    RelativeLayout searchData;
    private ListView songView;
    private ListView searchList;
    //    private SeekBar volumeSeekbar = null;
    private AudioManager audioManager = null;
    private  boolean isShuffle = false;
    private boolean isRepeat=false;
    private boolean mIsMediaPlaying = false;
    private int currentSongIndex;
    ImageView imageView;
    ArrayList<Popupelements> popupList;
    Popupadapter simpleAdapter;
    ProgressDialog dialog;
    ImageView musicMenuIV,equalizerIV;
    TextView target_view;
    RelativeLayout bottomButtonRL,searchRl;
    CircularSeekBar circularseekbar;
    TextView headerTitleTV;
    ImageView backgroundIV;
    String thumb;
    TextView song_name_two,artist_name_two;
    ImageView bottomBackgroundIV;
    ImageView btRepeat1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);

        //songView = (ListView) findViewById(R.id.song_list);
        songName = (TextView) findViewById(R.id.song_name);
        artist = (TextView) findViewById(R.id.artist_name);
        startTime=(TextView) findViewById(R.id.startTime);
//        endTime = (TextView) findViewById(R.id.endTime);
        back = (ImageView) findViewById(R.id.close);
        songSearch = (EditText) findViewById(R.id.song_search);
        searchList=(ListView) findViewById(R.id.search_list);
        bottomButtonRL =(RelativeLayout)findViewById(R.id.bottomButtonRL);

        headerTitleTV =(TextView)findViewById(R.id.headerTitleTV);
        searchRl=(RelativeLayout)findViewById(R.id.searchRl);
        backgroundIV=(ImageView)findViewById(R.id.backgroundIV);
        bottomBackgroundIV =(ImageView)findViewById(R.id.bottomBackgroundIV);
//        backgroundIV.setBackgroundColor(Color.BLACK);
//        backgroundIV.setAlpha(0.7f);


//        btRepeat1=(ImageView)findViewById(R.id.btRepeat1);

//        btRepeat1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if(btRepeat1.isClickable() == true){
//                    btRepeat1.setImageResource(R.drawable.new_repeat_red);
//                }else
//                {
//                    btRepeat1.setImageResource(R.drawable.new_repeat_icon_one);
//
//                }
//            }
//        });
       /* song_name_two =(TextView)findViewById(R.id.song_name_two);
        artist_name_two=(TextView)findViewById(R.id.artist_name_two);
*/
//        circularSeekbar =new CircularSeekBar(this);
//
//        circularSeekbar.setMaxProgress(100);
//        circularSeekbar.setProgress(100);
//        setContentView(circularSeekbar);
//        circularSeekbar.invalidate();
//        circularSeekbar.setSeekBarChangeListener(new CircularSeekBar.OnSeekChangeListener() {
//
//            @Override
//            public void onProgressChange(CircularSeekBar view, int newProgress) {
//                Log.d("Welcome", "Progress:" + view.getProgress() + "/" + view.getMaxProgress());
//            }
//        });

//              Typeface typeface = Typeface.createFromAsset(Player.this.getAssets(),"fonts/RobotoCondensed-BoldItalic.ttf");
//                 songName.setTypeface(typeface);


// set type face

        try{
            Typeface font=Typeface.createFromAsset(Player.this.getAssets(), "fonts/Harmattan-R.ttf");
            headerTitleTV.setTypeface(font);
            songSearch.setTypeface(font);
            songName.setTypeface(font);
//            song_name_two.setTypeface(font);
//            artist_name_two.setTypeface(font);
            startTime.setTypeface(font);
            artist.setTypeface(font);




        }catch (NullPointerException ex){
            System.out.println("exception "+ex.getMessage());
        }




        //songData = (RelativeLayout) findViewById(R.id.songdata_view);
        try{
            searchData = (RelativeLayout) findViewById(R.id.search_view);
            //songData.setVisibility(View.GONE);
            searchData.setVisibility(View.VISIBLE);
            loadFilterConfig();
            back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new UserNotification().setClicked(false);

                    mp.stop();
                    finish();


                }
            });



//            setVolumeControlStream(AudioManager.STREAM_MUSIC);
//            volumeSeekbar = (SeekBar)findViewById(R.id.media_volume);
//            audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
////            volumeSeekbar.setMax(audioManager
//                    .getStreamMaxVolume(AudioManager.STREAM_MUSIC));
//            volumeSeekbar.setProgress(audioManager
//                    .getStreamVolume(AudioManager.STREAM_MUSIC));

//
////            volumeSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
//            {
//                @Override
//                public void onStopTrackingTouch(SeekBar arg0)
//                {
//                }
//
//                @Override
//                public void onStartTrackingTouch(SeekBar arg0)
//                {
//                }
//
//                @Override
//                public void onProgressChanged(SeekBar arg0, int progress, boolean arg2)
//                {
//                    audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
//                            progress, 0);
//                }
////            });
//            sb = (SeekBar) findViewById(R.id.seekBar);
//            sb.setOnSeekBarChangeListener(this); // Important

//        mp = new MediaPlayer();
//        mp.setOnCompletionListener(this);





//             chnged 45
            circularseekbar = (CircularSeekBar) findViewById(R.id.circularSeekBar1);
            circularseekbar.getProgress();
            circularseekbar.setProgress(0);
            circularseekbar.setMax(100);
            circularseekbar.setVisibility(View.VISIBLE);

//            final ImageView imageView = new ImageView(Player.this);
//            final int dp = (int)pxToDp(5);
//            System.out.println("dp " +dp);
//
//            circularseekbar.post(new Runnable() {
//                @Override
//                public void run() {
//                    imageView.setLayoutParams(new RelativeLayout.LayoutParams(circularseekbar.getWidth() - dp,
//                            circularseekbar.getHeight() - dp));
//
//                    System.out.println("layoutparams" + circularseekbar.getHeight());
//                }
//            });
            circularseekbar.setOnSeekBarChangeListener(new CircularSeekBar.OnCircularSeekBarChangeListener() {
                @Override
                public void onProgressChanged(CircularSeekBar circularSeekBar, int progress, boolean fromUser) {

                }

                @Override
                public void onStopTrackingTouch(CircularSeekBar seekBar) {
                    myHandler.removeCallbacks(mUpdateTimeTask);
                    int totalDuration = mp.getDuration();
                    int currentPosition = progressToTimer(seekBar.getProgress(), totalDuration);

                    // forward or backward to certain seconds
                    mp.seekTo(currentPosition);

                    // update timer progress again
                    updateProgressBar();
                }

                @Override
                public void onStartTrackingTouch(CircularSeekBar seekBar) {
                    // remove message Handler from updating progress bar
                    myHandler.removeCallbacks(mUpdateTimeTask);
                }
            });


            musicMenuIV =(ImageView)findViewById(R.id.musicMenuIV);

//            circularSeekbar.setOnSeekBarChangeListener(this);
            musicMenuIV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(getApplicationContext(),"Menu is",Toast.LENGTH_SHORT).show();
                    searchList.setVisibility(View.VISIBLE);
                    bottomButtonRL.setVisibility(View.GONE);
                    circularseekbar.setVisibility(View.GONE);
                    searchRl.setVisibility(View.VISIBLE);
                    backgroundIV.setVisibility(View.GONE);
                    songName.setVisibility(View.GONE);
                    artist.setVisibility(View.GONE);
                    musicMenuIV.setVisibility(View.GONE);
                    equalizerIV.setVisibility(View.GONE);
                    bottomBackgroundIV.setVisibility(View.GONE);



                }
            });


            equalizerIV =(ImageView)findViewById(R.id.equalizerIV);


        }catch (NullPointerException ex){
            System.out.println("Exception" +ex.getMessage());
        }

        try{
            Intent intent= getIntent();
            type = intent.getStringExtra("type");
            finalUri = intent.getParcelableExtra("uri_Str");
            currentSongIndex = intent.getExtras().getInt("songIndex");
            songName.setText(intent.getStringExtra("songname"));
            artist.setText(intent.getStringExtra("artist"));
//           song_name_two.setText(intent.getStringExtra("songname"));
//           artist_name_two.setText(intent.getStringExtra("artist"));


            thumb = intent.getStringExtra("thumbnail");

            System.out.println("thumbnail" +thumb);

            ApplicationController.getInstance().getImageLoader().get(this.getString(R.string.server_url3) + thumb, new ImageLoader.ImageListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    backgroundIV.setImageResource(R.drawable.empty);
                }

                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                    if (response.getBitmap() != null) {
                        int nh = (int) (response.getBitmap().getHeight() * (512.0 / response.getBitmap().getWidth()));
                        Bitmap scaled = Bitmap.createScaledBitmap(response.getBitmap(), 512, nh, true);
                        backgroundIV.setImageBitmap(scaled);
                    } else {
                        backgroundIV.setImageResource(R.drawable.empty);
                    }
                }
            });

//
//           ApplicationController.getInstance().getImageLoader().get(this.getString(R.string.server_url3) + thumb, new ImageLoader.ImageListener() {
//               @Override
//               public void onErrorResponse(VolleyError error) {
//                   imageView.setImageResource(R.drawable.empty);
//
//                   System.out.println("Imageview calling"  + "Imageview");
//               }
//
//               @Override
//               public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
//                   if (response.getBitmap() != null) {
//                       int nh = (int) (response.getBitmap().getHeight() * (512.0 / response.getBitmap().getWidth()));
//                       Bitmap scaled = Bitmap.createScaledBitmap(response.getBitmap(), 512, nh, true);
//                       imageView.setImageBitmap(scaled);
//                   } else {
//                       imageView.setImageResource(R.drawable.empty);
//                   }
//               }
//           });

//           endTime.setText(intent.getStringExtra("duration"));

            Log.i("urlsong",String.valueOf(finalUri));

            System.out.println("song name" +intent.getStringExtra("songname"));


            //code to add header and footer to listview
            LayoutInflater inflater = getLayoutInflater();
            ViewGroup header = (ViewGroup) inflater.inflate(R.layout.header_album, searchList,
                    false);
            headerText = (TextView) header.findViewById(R.id.album_title);
            songShare = (ImageView) header.findViewById(R.id.song_share);

            albumArt = (ImageView) header.findViewById(R.id.album_art);
//        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.empty);
//
//        albumArt.setImageBitmap(createBitmap_ScriptIntrinsicBlur(bitmap, 25.0f));

            ApplicationController.getInstance().getImageLoader().get(getString(R.string.server_url3) + intent.getStringExtra("songIcon"), new ImageLoader.ImageListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    albumArt.setImageResource(R.drawable.empty);
                }

                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                    if (response.getBitmap() != null) {
                        int nh = (int) (response.getBitmap().getHeight() * (512.0 / response.getBitmap().getWidth()));
                        Bitmap scaled = Bitmap.createScaledBitmap(response.getBitmap(), 512, nh, true);
                        albumArt.setImageBitmap(scaled);
                    } else {
                        albumArt.setImageResource(R.drawable.empty);
                    }
                }
            });


            headerText.setText(intent.getStringExtra("songname"));

            searchList.addHeaderView(header, null, false);

            searchList.invalidateViews();
            if(type.equals("album"))
            {
                //dialog.show();
                //showLoader();
                handleForAlbum(currentSongIndex);
//            AsyncTask.execute(new Runnable() {
//                @Override
//                public void run() {
//
//                }
//            });


            }
            else
            {
                //dialog.show();
                //showLoader();
                handleForSongs(currentSongIndex);
//            AsyncTask.execute(new Runnable() {
//                @Override
//                public void run() {
//
//
//                }
//            });

            }


        }catch(NullPointerException ex){
            System.out.println("Null Exception"+ex.getMessage());
        }





        if(type.equals("songs"))
        {

            list = new UserNotification().getSonglist();
            type = "songs";
            adapter = new PlaySongAdapter(this, list);
            searchList.setAdapter(adapter);
            //searchList.setAdapter(new SongsAdapter(this, list,type));
            searchList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                    int position1 = position - 1;
                    stopRadio();
                    //dialog.show();
                    //showLoader();
                    playSongs(position1);
                    new UserNotification().setClicked(false);
                    searchList.invalidateViews();

                }
            });

        }
//        if(type.equals("album"))
        else
        {
            songs = new UserNotification().getSongs();
            type = "album";
            adapter1 = new PlayAlbumAdapter(this, songs);
            searchList.setAdapter(adapter1);
            searchList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {


                    int position1 = position-1;
                    stopRadio();
                    //dialog.show();
                    //showLoader();

                    playAlbum(position1);
                    new UserNotification().setClicked(false);

                    searchList.invalidateViews();

                }
            });
        }

        btPly = (ImageButton) findViewById(R.id.btPly);
        btFf = (ImageButton) findViewById(R.id.btFf);
        btFb = (ImageButton) findViewById(R.id.btFb);
        btnShuffle = (ImageButton) findViewById(R.id.btRand);
        btnRepeat = (ImageButton) findViewById(R.id.btRepeat);


        try{

            btPly.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(mp!=null){
                        if(mp.isPlaying())
                        {
                            mp.pause();
                            btPly.setImageResource(R.drawable.play);
                            new UserNotification().setClicked(true);
                            searchList.invalidateViews();
                        }
                        else {
                            mp.start();
                            btPly.setImageResource((R.drawable.pause));
                            new UserNotification().setClicked(false);
                            searchList.invalidateViews();
                        }

                        //Toast.makeText(getApplicationContext(), "media player is active", Toast.LENGTH_SHORT).show();
                    }
//                else
//                {
//                    playlist1(0);
//                }



                }
            });
        }catch (NullPointerException ex)
        {
            System.out.println("Exception " +ex.getMessage());
        }



//
//        btnShuffle.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//
//
//                if(isShuffle){
//                    isShuffle = false;
//                    Toast.makeText(getApplicationContext(), "Shuffle is OFF", Toast.LENGTH_SHORT).show();
//                    btnShuffle.setBackgroundColor(Color.TRANSPARENT);
//                }else{
//                    // make repeat to true
//                    isShuffle= true;
//                    Toast.makeText(getApplicationContext(), "Shuffle is ON", Toast.LENGTH_SHORT).show();
////                    btnShuffle.setImageResource(R.drawable.new_forward_red);
//
//                    // make shuffle to false
//                    isRepeat = false;
//                    btnShuffle.setVisibility(View.GONE);
//
////                    btnShuffle.setBackgroundColor(Color.BLUE);
//                    btnRepeat.setBackgroundColor(Color.TRANSPARENT);
//                }
//
//            }
//        });


        btnShuffle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(isShuffle){
                    isShuffle = false;
                    Toast.makeText(getApplicationContext(), "Shuffle is OFF", Toast.LENGTH_SHORT).show();
                    btnShuffle.setBackgroundColor(Color.TRANSPARENT);
                    btnShuffle.setImageResource(R.drawable.shuffle);

                }else{
                    // make repeat to true
                    isShuffle= true;
                    Toast.makeText(getApplicationContext(), "Shuffle is ON", Toast.LENGTH_SHORT).show();
                    // make shuffle to false
                    isRepeat = false;
//                    btnShuffle.setBackgroundColor(Color.BLUE);
                    btnShuffle.setImageResource(R.drawable.color_shuffle);
                    btnRepeat.setBackgroundColor(Color.TRANSPARENT);
                }

            }
        });


        btnRepeat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isRepeat){
                    isRepeat = false;
                    Toast.makeText(getApplicationContext(), "Repeat is OFF", Toast.LENGTH_SHORT).show();
                    btnRepeat.setBackgroundColor(Color.TRANSPARENT);
                    btnRepeat.setImageResource(R.drawable.repeat);

                }else{
                    // make repeat to true
                    isRepeat = true;
                    Toast.makeText(getApplicationContext(), "Repeat is ON", Toast.LENGTH_SHORT).show();
                    // make shuffle to false
                    isShuffle = false;
//                    btnRepeat.setBackgroundColor(Color.BLUE);
                    btnRepeat.setImageResource(R.drawable.color_repeat);
                    btnShuffle.setBackgroundColor(Color.TRANSPARENT);
                }
            }
        });



        if(type.equals("songs"))
        {
            try {
                btFf.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

//                        if(btFf.isClickable() == true){
//                            btFf.setImageResource(R.drawable.color_next);
//
//                        }else
//                        {
//                            btFf.setImageResource(R.drawable.next);
//
//                        }
                        //mp.seekTo(mp.getCurrentPosition()+5000);
//                        btFf.setImageResource(R.drawable.new_next_red);
                        stopRadio();
                        if(currentSongIndex < (list.size() - 1)){
                            playSongs(currentSongIndex + 1);
                            searchList.invalidateViews();
//                            btFf.setImageResource(R.drawable.color_next);
//
//
//                            System.out.println("songs if calling" + "songs btFF");

                        }else{
                            // play first song
                            playSongs(0);
                            searchList.invalidateViews();


                        }
                    }
                });
            }
            catch (NullPointerException e)
            {

            }

            try {
                btFb.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //mp.seekTo(mp.getCurrentPosition()-5000);
//                        btFf.setImageResource(R.drawable.color_previous);

//                         if(btFb .isClickable() == true){
//                             btFb.setImageResource(R.drawable.color_previous);
//
//                         }else {
//                             btFb.setImageResource(R.drawable.previous);
//                         }
                        stopRadio();
//                    position < (list.size() - 1)
                        if(currentSongIndex>0){
                            playSongs(currentSongIndex - 1);

                            System.out.println("if caling" + "btn fb");
                            searchList.invalidateViews();
//                            btFb.setImageResource(R.drawable.color_previous);
//
//                            System.out.println("songs if calling" + "songs btFB==");


                        }else{
                            // play first song
                            playSongs(0);
                            searchList.invalidateViews();
                            System.out.println("else caling" + "btn fb");
//                            btFb.setImageResource(R.drawable.previous);
//                            System.out.println("songs else calling" + "songs btFB==");

                        }
                    }
                });
            }
            catch (NullPointerException e)
            {

            }
        }
        else {
            try {
                btFf.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        stopRadio();
                        if(currentSongIndex < (songs.size() - 1)){
                            playAlbum(currentSongIndex + 1);
                            searchList.invalidateViews();

                        }else{
                            // play first song
                            playAlbum(0);
                            searchList.invalidateViews();
                        }
                    }
                });
            }
            catch (NullPointerException e)
            {

            }


            try {
                btFb.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
//
                        stopRadio();
//                    position < (songs.size() - 1)
                        if(currentSongIndex>0){
                            playAlbum(currentSongIndex - 1);
                            searchList.invalidateViews();

                        }else{
                            // play first song
                            playAlbum(0);
                            searchList.invalidateViews();

                        }
                    }
                });
            }
            catch (NullPointerException e)
            {

            }
        }


    }

    private void displayFrag(int position) {
        Fragment fragment = null;

        switch (position) {
            case 0:
                //fragment = new FragChat();
                startActivity(new Intent(getApplicationContext(), Sample.class));
                UserNotification userNotification = new UserNotification();
                userNotification.setChatSelected(Boolean.TRUE);

                break;
            case 1:
                startActivity(new Intent(getApplicationContext(), Sample.class));
                UserNotification userNotification1 = new UserNotification();
                userNotification1.setFromnotif(Boolean.TRUE);
                break;
            case 2:
                startActivity(new Intent(getApplicationContext(), Sample.class));
                UserNotification userNotification2 = new UserNotification();
                userNotification2.setDISCUSSIONSELECTED(Boolean.TRUE);
                break;
            case 3:
                startActivity(new Intent(getApplicationContext(), Sample.class));
                UserNotification userNotification3 = new UserNotification();
                userNotification3.setFromdirect(Boolean.TRUE);
                //fragment = new FragDirectory();
                break;
            case 4:
                startActivity(new Intent(getApplicationContext(), Sample.class));

                //userNotification.setFROMMUSIC(Boolean.TRUE);
                //fragment = new FragMusic();
                break;
            case 5:
                //fragment = new FragMyLocation();

                break;
            case 6:
                startActivity(new Intent(getApplicationContext(), Sample.class));
                UserNotification userNotification4 = new UserNotification();
                userNotification4.setFromweather(Boolean.TRUE);
                //fragment = new FragWeather();
                break;
            default:
                break;
        }

//        if (fragment != null) {
//            FragmentManager fragmentManager = getFragmentManager();
//            fragmentManager.beginTransaction()
//                    .replace(R.id.frame_container, fragment).commit();
//            //((Dashboard) getActivity()).mDrawerLayout.closeDrawer(GravityCompat.START);
//        }
    }

    private void showLoader() {

        dialog = new ProgressDialog(this);
        dialog.setMessage("Please wait...");
        //dialog.setCanceledOnTouchOutside(true);
        dialog.setCanceledOnTouchOutside(true);
        //dialog.setCancelable(false);
        dialog.show();
    }

    private void loadFilterConfig(){
        songSearch.addTextChangedListener(new TextWatcherAdapter(){
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                super.beforeTextChanged(s, start, count, after);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                super.onTextChanged(s, start, before, count);
                String text = songSearch.getText().toString().toLowerCase(Locale.getDefault());
                if(type.equals("album"))
                {
                    adapter1.filter(text);
                }
                else {
                    adapter.filter(text);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                super.afterTextChanged(s);

            }
        });
    }

    private Bitmap createBitmap_ScriptIntrinsicBlur(Bitmap src, float r) {

        //Radius range (0 < r <= 25)
        if(r <= 0){
            r = 0.1f;
        }else if(r > 25){
            r = 25.0f;
        }

        Bitmap bitmap = Bitmap.createBitmap(
                src.getWidth(), src.getHeight(),
                Bitmap.Config.ARGB_8888);

        RenderScript renderScript = RenderScript.create(this);

        Allocation blurInput = Allocation.createFromBitmap(renderScript, src);
        Allocation blurOutput = Allocation.createFromBitmap(renderScript, bitmap);

        ScriptIntrinsicBlur blur = ScriptIntrinsicBlur.create(renderScript,
                Element.U8_4(renderScript));
        blur.setInput(blurInput);
        blur.setRadius(r);
        blur.forEach(blurOutput);

        blurOutput.copyTo(bitmap);
        renderScript.destroy();
        return bitmap;
    }
    public void  playAlbum(final int position){


        // Play song
        mp = new MediaPlayer();
        try {
            mp.reset();
            String PATH = Environment.getExternalStorageDirectory()
                    + "/wismusic/";

            mp.setDataSource(PATH+songs.get(position).getPath());
            mp.prepare();
            mp.start();




            //set titles
            songName.setText(songs.get(position).getName());
            artist.setText(songs.get(position).getA());
//            endTime.setText(songs.get(position).getDuration());

            btPly.setImageResource(R.drawable.pause);

            try {
                btFf.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //mp.seekTo(mp.getCurrentPosition()+5000);
                        stopRadio();
                        if(position < (songs.size() - 1)){
                            playAlbum(position + 1);
                            searchList.invalidateViews();

                            btFf.setImageResource(R.drawable.color_next);

                            System.out.println("albums if calling" + "albums btFF");
                        }else{
                            // play first song
                            playAlbum(0);
                            searchList.invalidateViews();

                            btFf.setImageResource(R.drawable.next);
                            System.out.println("albums else  calling" + "albums btFF");


                        }


                    }
                });
            }
            catch (NullPointerException e)
            {

            }


            try {
                btFb.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        stopRadio();
//                    position < (songs.size() - 1)
                        if(position>0){
                            playAlbum(position - 1);
                            searchList.invalidateViews();
                            btFb.setImageResource(R.drawable.color_previous);

                            System.out.println(" albums songs if calling" + "songs btFB==");
                        }else{
                            // play first song
                            playAlbum(0);
                            searchList.invalidateViews();
                            btFb.setImageResource(R.drawable.previous);

                            System.out.println("albums songs else calling" + "songs btFB==");
                        }
                    }
                });
            }
            catch (NullPointerException e)
            {

            }

            try {
                ApplicationController.getInstance().getImageLoader().get(getString(R.string.server_url3) + songs.get(position).getThumb(), new ImageLoader.ImageListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        albumArt.setImageResource(R.drawable.empty);
                    }

                    @Override
                    public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                        if (response.getBitmap() != null) {
                            int nh = (int) (response.getBitmap().getHeight() * (512.0 / response.getBitmap().getWidth()));
                            Bitmap scaled = Bitmap.createScaledBitmap(response.getBitmap(), 512, nh, true);
                            albumArt.setImageBitmap(scaled);
                        } else {
                            albumArt.setImageResource(R.drawable.empty);
                        }
                    }
                });
            }
            catch (IndexOutOfBoundsException e)
            {

            }


            //Toast.makeText(getApplicationContext(),songs.get(position).getPath()+songs.get(position).getName()+songs.get(position).getA(),Toast.LENGTH_SHORT).show();
            try {
                songShare.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //Toast.makeText(getApplicationContext(), "called", Toast.LENGTH_SHORT).show();
                        if(songs.size()>0)
                        {
                            String msg = "Name :".concat(songs.get(position).getName()).concat("\n").concat("Artists :").concat(songs.get(position).getA()).concat("\n");
                            UserNotification.setSongThumb(songs.get(position).getThumb());
                            UserNotification.setMusicChatMessage(msg.concat(songs.get(position).getPath()));
                            showsharePopUPView(songs.get(position).getId(),"album",UserNotification.getMusicChatMessage());
                        }


                    }
                });
            }
            catch (IndexOutOfBoundsException ex){

            }



            headerText.setText(songs.get(position).getName());
            // set icon
            new UserNotification().setSelected(position);
//            imageView.setImageResource(android.R.drawable.ic_media_play);
            // set Progress bar values
//            sb.setProgress(0);
//            sb.setMax(100);
            //dialog.dismiss();

            circularseekbar.setProgress(0);
            circularseekbar.setMax(100);



            // Updating progress bar
            updateProgressBar();
            mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {

                    Log.i("onAlbums","completed");
                    if(isRepeat){
                        // repeat is on play same song again
                        playAlbum(position);
                        searchList.invalidateViews();
                    } else if(isShuffle){
                        // shuffle is on - play a random song
                        Random rand = new Random();
                        int suffleposition = (rand.nextInt((songs.size() - 1) - 0 + 1) + 0);
                        playAlbum(suffleposition);
                        searchList.invalidateViews();
                        Log.i("sufflelist", String.valueOf(suffleposition));
                        //playAlbum((rand.nextInt((songs.size() - 1) - 0 + 1) + 0));
                    } else{
                        // no repeat or shuffle ON - play next song
                        if(position < (songs.size() - 1)){
                            playAlbum(position + 1);
                            searchList.invalidateViews();
                        }else{
                            // play first song
                            playAlbum(0);
                            searchList.invalidateViews();
                        }
                    }
                }
            });
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void  playSongs(final int position){

        // Play song
        mp = new MediaPlayer();
        try {
            mp.reset();

            String PATH = Environment.getExternalStorageDirectory()
                    + "/wismusic/";

            mp.setDataSource(PATH+list.get(position).getPath());
            mp.prepare();
            mp.start();



            //set title
            songName.setText(list.get(position).getName());
            artist.setText(list.get(position).getArtists());
//            endTime.setText(list.get(position).getDuration());

            btPly.setImageResource(R.drawable.pause);
//
//            try {
//                btFf.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        //mp.seekTo(mp.getCurrentPosition()+5000);
//                        stopRadio();
//                        if(position < (list.size() - 1)){
//                            playSongs(position + 1);
//                            searchList.invalidateViews();
//
//                            btFf.setImageResource(R.drawable.color_next);
//
//                        }else{
//                            // play first song
//                            playSongs(0);
//                            searchList.invalidateViews();
//                            btFf.setImageResource(R.drawable.next);
//                        }
//                    }
//                });
//            }
//            catch (NullPointerException e)
//            {
//
//            }
//
//
//            try {
//                btFb.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        stopRadio();
////                    position < (songs.size() - 1)
//                        if(position>0){
//                            playSongs(position - 1);
//                            searchList.invalidateViews();
//                            btFf.setImageResource(R.drawable.color_previous);
//
//                        }else{
//                            // play first song
//                            playSongs(0);
//                            searchList.invalidateViews();
//                            btFf.setImageResource(R.drawable.previous);
//
//                        }
//                    }
//                });
//            }
//            catch (NullPointerException e)
//            {
//
//            }

            try {
                btFf.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //mp.seekTo(mp.getCurrentPosition()+5000);
                        stopRadio();
                        if(position < (list.size() - 1)){
                            playSongs(position + 1);
                            searchList.invalidateViews();

                            btFf.setImageResource(R.drawable.color_next);

                            System.out.println("albums if calling" + "albums btFF");
                        }else{
                            // play first song
                            playSongs(0);
                            searchList.invalidateViews();

                            btFf.setImageResource(R.drawable.next);
                            System.out.println("albums else  calling" + "albums btFF");


                        }


                    }
                });
            }
            catch (NullPointerException e)
            {

            }


            try {
                btFb.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        stopRadio();
//                    position < (songs.size() - 1)
                        if(position>0){
                            playSongs(position - 1);
                            searchList.invalidateViews();
                            btFb.setImageResource(R.drawable.color_previous);

                            System.out.println(" albums songs if calling" + "songs btFB==");
                        }else{
                            // play first song
                            playSongs(0);
                            searchList.invalidateViews();
                            btFb.setImageResource(R.drawable.previous);

                            System.out.println("albums songs else calling" + "songs btFB==");
                        }
                    }
                });
            }
            catch (NullPointerException e)
            {

            }

            try {
                ApplicationController.getInstance().getImageLoader().get(getString(R.string.server_url3) + list.get(position).getThumb(), new ImageLoader.ImageListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        albumArt.setImageResource(R.drawable.empty);
                    }

                    @Override
                    public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                        if (response.getBitmap() != null) {
                            int nh = (int) (response.getBitmap().getHeight() * (512.0 / response.getBitmap().getWidth()));
                            Bitmap scaled = Bitmap.createScaledBitmap(response.getBitmap(), 512, nh, true);
                            albumArt.setImageBitmap(scaled);
                        } else {
                            albumArt.setImageResource(R.drawable.empty);
                        }
                    }
                });
            }
            catch (IndexOutOfBoundsException e)
            {

            }
            try {
                songShare.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //Toast.makeText(getApplicationContext(), "called", Toast.LENGTH_SHORT).show();
                        if(list.size()>0)
                        {
                            String msg = "Name :".concat(list.get(position).getName()).concat("\n").concat("Artists :").concat(list.get(position).getArtists()).concat("\n");
                            UserNotification.setSongThumb(list.get(position).getThumb());
                            UserNotification.setMusicChatMessage(msg.concat(list.get(position).getPath()));

                            showsharePopUPView(list.get(position).getId(),"songs",UserNotification.getMusicChatMessage());
                        }



                    }
                });
            }
            catch (IndexOutOfBoundsException ex){

            }

            headerText.setText(list.get(position).getName());
            // set icon
            new UserNotification().setSelected(position);
            // set Progress bar values
//            sb.setProgress(0);
//            sb.setMax(100);
            //dialog.dismiss();


            circularseekbar.setProgress(0);
            circularseekbar.setMax(100);


            // Updating progress bar
            updateProgressBar();
            mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    Log.i("onSongs","completed");
                    if(isRepeat){
                        // repeat is on play same song again
                        playSongs(position);
                        searchList.invalidateViews();
                    } else if(isShuffle){
                        // shuffle is on - play a random song
                        Random rand = new Random();
                        int suffleposition = (rand.nextInt((list.size() - 1) - 0 + 1) + 0);
                        playSongs(suffleposition);
                        searchList.invalidateViews();
                        //playSongs((rand.nextInt((list.size() - 1) - 0 + 1) + 0));
                        Log.i("sufflelist", String.valueOf(suffleposition));

                    } else{
                        // no repeat or shuffle ON - play next song
                        if(position < (list.size() - 1)){
                            playSongs(position + 1);
                            searchList.invalidateViews();
                        }else{
                            // play first song
                            playSongs(0);
                            searchList.invalidateViews();
                        }
                    }
                }
            });
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void handleForAlbum(final int songPosition) {
        if (!mIsMediaPlaying) {
            String streamingUrl = finalUri.toString();
            mp = new MediaPlayer();
            mp.setOnCompletionListener(this);

            try {
                mp.reset();
                mp.setDataSource(streamingUrl);
                mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mp.prepare();
                mp.start();
                // set Progress bar values

                try{
//                    sb.setProgress(0);
//                    sb.setMax(100);


                    circularseekbar.setProgress(0);
                    circularseekbar.setMax(100);
                }catch (NullPointerException ex){
                    System.out.println("EX" +ex.getMessage());
                }


                //dialog.dismiss();


                try {
                    btFf.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            //mp.seekTo(mp.getCurrentPosition()+5000);
                            stopRadio();
                            if(songPosition < (songs.size() - 1)){
                                playAlbum(songPosition + 1);
                                searchList.invalidateViews();
                            }else{
                                // play first song
                                playAlbum(0);
                                searchList.invalidateViews();
                            }
                        }
                    });
                }
                catch (NullPointerException e)
                {

                }


                try {
                    btFb.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            stopRadio();
//                    position < (songs.size() - 1)
                            if(songPosition>0){
                                playAlbum(songPosition - 1);
                                searchList.invalidateViews();
                            }else{
                                // play first song
                                playAlbum(0);
                                searchList.invalidateViews();
                            }
                        }
                    });
                }
                catch (NullPointerException e)
                {

                }


                try {

                    songShare.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if(songs.size()>0) {
                                String msg = "Name :".concat(songs.get(songPosition).getName()).concat("\n").concat("Artists :").concat(songs.get(songPosition).getA()).concat("\n");
                                UserNotification.setMusicChatMessage(msg.concat(finalUri.toString()));
                                UserNotification.setSongThumb(songs.get(songPosition).getThumb());

                                showsharePopUPView(songs.get(songPosition).getId(), "album", UserNotification.getMusicChatMessage());
                            }

                        }
                    });
                }
                catch (IndexOutOfBoundsException ex){

                }

                // Updating progress bar

                updateProgressBar();
                mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        Log.i("goesTo", "onComplete hit");
                        if(songPosition<(songs.size()-1))
                        {
                            playAlbum(songPosition + 1);
                            searchList.invalidateViews();

                        }
                        else {

                            playAlbum(0);
                            searchList.invalidateViews();
                        }



                    }
                });

            } catch (IllegalArgumentException e) {
                e.printStackTrace();

            } catch (IllegalStateException e) {
                e.printStackTrace();

            } catch (IOException e) {
                e.printStackTrace();

            }

        }


    }

    private void handleForSongs(final int songPosition) {
        if (!mIsMediaPlaying) {
            String streamingUrl = finalUri.toString();
            mp = new MediaPlayer();
            mp.setOnCompletionListener(this);
            try {
                mp.reset();
                mp.setDataSource(streamingUrl);
                mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mp.prepare();
                mp.start();


                //dialog.dismiss();
                try {

                    // set Progress bar values
//                    sb.setProgress(0);
//                    sb.setMax(100);


                    circularseekbar.setProgress(0);
                    circularseekbar.setMax(100);

                    btFf.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            //mp.seekTo(mp.getCurrentPosition()+5000);
                            stopRadio();
                            if(songPosition < (list.size() - 1)){
                                playSongs(songPosition + 1);
                                searchList.invalidateViews();
                            }else{
                                // play first song
                                playSongs(0);
                                searchList.invalidateViews();
                            }
                        }
                    });
                }
                catch (NullPointerException e)
                {

                }

                try {
                    btFb.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            //mp.seekTo(mp.getCurrentPosition()-5000);
                            stopRadio();
//                    position < (list.size() - 1)
                            if(songPosition>0){
                                playSongs(songPosition - 1);
                                searchList.invalidateViews();
                            }else{
                                // play first song
                                playSongs(0);
                                searchList.invalidateViews();
                            }
                        }
                    });
                }
                catch (NullPointerException e)
                {

                }

                try {
                    songShare.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            if(list.size()>0)
                            {
                                String msg = "Name :".concat(list.get(songPosition).getName()).concat("\n").concat("Artists :").concat(list.get(songPosition).getArtists()).concat("\n");
                                UserNotification.setMusicChatMessage(msg.concat(finalUri.toString()));
                                UserNotification.setSongThumb(list.get(songPosition).getThumb());

                                showsharePopUPView(list.get(songPosition).getId(), "songs", UserNotification.getMusicChatMessage());
                            }


                        }
                    });
                }
                catch (IndexOutOfBoundsException ex){

                }
                // Updating progress bar

                updateProgressBar();
                mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        Log.i("goesTo", "onComplete hit");
                        if(songPosition<(list.size()-1))
                        {
                            playSongs(songPosition + 1);
                            searchList.invalidateViews();

                        }
                        else {

                            playSongs(0);
                            searchList.invalidateViews();
                        }

                    }
                });
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalStateException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    private void stopRadio(){

        mIsMediaPlaying = false;
        if (mp != null){
            try {
                mp.stop();
                mp.reset();

                //mMediaPlayer.release();
            }catch (Exception e){
                e.printStackTrace();
            }finally {
                Log.e("stop", "Audio stoped");
                mp = null;
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        new UserNotification().setClicked(false);
        mp.stop();
        finish();
    }

    public void updateProgressBar() {
        myHandler.postDelayed(mUpdateTimeTask, 100);
    }
    private Runnable mUpdateTimeTask = new Runnable() {
        public void run() {
            try {
                long totalDuration = mp.getDuration();
                long currentDuration = mp.getCurrentPosition();
                int duration = (int) currentDuration;

                // Displaying Total Duration time
//                endTime.setText(milliSecondsToTimer(totalDuration));
                // Displaying time completed playing
                startTime.setText(milliSecondsToTimer(currentDuration));


                int progress = (int)(getProgressPercentage(currentDuration, totalDuration));
//                Log.d("Progress", ""+progress);
//                sb.setProgress(progress);

                circularseekbar.setProgress(progress);

                // Running this thread after 100 milliseconds
                myHandler.postDelayed(this, 100);
            }
            catch (NullPointerException e)
            {
                e.printStackTrace();
            }

        }
    };
    public String milliSecondsToTimer(long milliseconds){
        String finalTimerString = "";
        String secondsString = "";

        // Convert total duration into time
        int hours = (int)( milliseconds / (1000*60*60));
        int minutes = (int)(milliseconds % (1000*60*60)) / (1000*60);
        int seconds = (int) ((milliseconds % (1000*60*60)) % (1000*60) / 1000);
        // Add hours if there
        if(hours > 0){
            finalTimerString = hours + ":";
        }

        // Prepending 0 to seconds if it is one digit
        if(seconds < 10){
            secondsString = "0" + seconds;
        }else{
            secondsString = "" + seconds;}

        finalTimerString = finalTimerString + minutes + ":" + secondsString;

        // return timer string
        return finalTimerString;
    }
    public int getProgressPercentage(long currentDuration, long totalDuration){
        Double percentage = (double) 0;

        long currentSeconds = (int) (currentDuration / 1000);
        long totalSeconds = (int) (totalDuration / 1000);

        // calculating percentage
        percentage =(((double)currentSeconds)/totalSeconds)*100;

        // return percentage
        return percentage.intValue();
    }

    public int progressToTimer(int progress, int totalDuration) {
        int currentDuration = 0;
        totalDuration = (int) (totalDuration / 1000);
        currentDuration = (int) ((((double)progress) / 100) * totalDuration);

        // return current duration in milliseconds
        return currentDuration * 1000;
    }


    /**
     *
     * */
    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromTouch) {

    }

    /**
     * When user starts moving the progress handler
     * */
    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        // remove message Handler from updating progress bar
        myHandler.removeCallbacks(mUpdateTimeTask);
    }

    /**
     * When user stops moving the progress hanlder
     * */
    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        myHandler.removeCallbacks(mUpdateTimeTask);
        int totalDuration = mp.getDuration();
        int currentPosition = progressToTimer(seekBar.getProgress(), totalDuration);

        // forward or backward to certain seconds
        mp.seekTo(currentPosition);

        // update timer progress again
        updateProgressBar();
    }
    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {
//        // check for repeat is ON or OFF
        //Toast.makeText(getApplicationContext(), "completed", Toast.LENGTH_SHORT).show();
    }
    private void showsharePopUPView(final int object_id,final String type,final String path) {

        mp.stop();

        popupList=new ArrayList<>();
        popupList.add(new Popupelements(R.drawable.copy_chat,getResources().getString(R.string.chat_indivisul)));
        popupList.add(new Popupelements(R.drawable.copy_chat,getResources().getString(R.string.chat_by_group)));
        popupList.add(new Popupelements(R.drawable.write_status,getResources().getString(R.string.field_actuality)));
        popupList.add(new Popupelements(R.drawable.write_status,getResources().getString(R.string.pub)));
        Log.i("popsize",String.valueOf(popupList));
        simpleAdapter = new Popupadapter(this,false,popupList);

        DialogPlus dialog = DialogPlus.newDialog(this)
                .setAdapter(simpleAdapter)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View v, int i) {

                        if(popupList.size()==4){


                            if(i == 0){

                                dialog.dismiss();

                                startActivity(new Intent(getApplicationContext(), Sample.class));

//
                                new UserNotification().setObject(object_id);
                                new UserNotification().setAudio(path);
                                new UserNotification().setType(type);

                                new UserNotification().setFROMMUSIC(true);
                                new UserNotification().setChatSelected(true);
                                new UserNotification().setDISCUSSIONSELECTED(false);

                            }

                            if(i ==1){
                                dialog.dismiss();

                                startActivity(new Intent(getApplicationContext(), Sample.class));


                                new UserNotification().setObject(object_id);
                                new UserNotification().setAudio(path);
                                new UserNotification().setType(type);

                                new UserNotification().setFROMMUSIC(true);
                                new UserNotification().setDISCUSSIONSELECTED(true);
                                new UserNotification().setChatSelected(false);

                            }
                            if(i ==2){
                                dialog.dismiss();
                                shareActuality(object_id,type);
                            }
                            if(i==3)
                            {
                                dialog.dismiss();

                                startActivity(new Intent(getApplicationContext(), NewAds.class));
                                new UserNotification().setFROMMUSIC(true);
                                new UserNotification().setAudio(path);
                                new UserNotification().setObject(object_id);

                            }

                        }
                        else if(popupList.size()==4){
                            dialog.dismiss();

                        }
                    }
                })
                .setExpanded(false)  // This will enable the expand feature, (similar to android L share dialog)
                .create();
        dialog.show();


    }

    @Override
    protected void onStop() {
        super.onStop();
        mp.stop();
        Log.i("calling","clearData");
    }

    @Override
    protected void onDestroy() {
        if (mUpdateTimeTask != null)
            myHandler.removeCallbacks(mUpdateTimeTask );
        super.onDestroy();
    }
    private void shareActuality(int object_id,String type) {
        JSONObject jsonBody = new JSONObject();

        try {
            jsonBody.put("user_id", Tools.getData(this, "idprofile"));
            jsonBody.put("object_id", String.valueOf(object_id));
            jsonBody.put("type",type);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqDelete = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.shareAudioToActuality), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {
                                Log.i("succuss",String.valueOf(response));
                                Toast.makeText(getApplicationContext(),"Audio Succussfully Shared", Toast.LENGTH_SHORT).show();

                            }

                        } catch (JSONException e) {

                            //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //if (context != null)
                //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("lang", Tools.getData(getApplicationContext(), "lang_pr"));
                headers.put("token", Tools.getData(getApplicationContext(), "token"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqDelete);
    }

//     converting dp to pixel


    public int pxToDp(int px) {
        DisplayMetrics displayMetrics = this.getResources().getDisplayMetrics();
        return Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }
}
