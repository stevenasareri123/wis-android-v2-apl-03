
package com.oec.wis.wismusic;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.oec.wis.ApplicationController;
import com.oec.wis.AudioBrowser;
import com.oec.wis.R;
import com.oec.wis.adapters.AlbumAdapter;
import com.oec.wis.adapters.AudioAdapter;
import com.oec.wis.adapters.ChatIndivisualListener;
import com.oec.wis.adapters.Popupadapter;
import com.oec.wis.adapters.RefreshListener;
import com.oec.wis.adapters.ShareAlbumAdapter;
import com.oec.wis.adapters.ShareAlbumListener;
import com.oec.wis.adapters.ShowDialogAdapter;
import com.oec.wis.database.DatabaseHandler;
import com.oec.wis.dialogs.NewAds;
import com.oec.wis.dialogs.PubDetails;
import com.oec.wis.entities.Popupelements;
import com.oec.wis.entities.PubMusic;
import com.oec.wis.entities.UserNotification;
import com.oec.wis.entities.WISAds;
import com.oec.wis.entities.WISAlbum;
import com.oec.wis.entities.WISAlbumPlaylist;
import com.oec.wis.entities.WISMenu;
import com.oec.wis.entities.WISMusic;
import com.oec.wis.entities.WISPlaylist;
import com.oec.wis.entities.WISPlaylist1;
import com.oec.wis.entities.WISPlaylistAlbum;
import com.oec.wis.entities.WISPlaylistSongs;
import com.oec.wis.entities.WISSearch;
import com.oec.wis.entities.WISSearchs;
import com.oec.wis.entities.WISSongs;
import com.oec.wis.fragments.FragChat;
import com.oec.wis.fragments.FragHome;
import com.oec.wis.fragments.FragNotif;
import com.oec.wis.fragments.FragVideo;
import com.oec.wis.fragments.FragWeather;
import com.oec.wis.tools.ProgressHttpEntityWrapper;
import com.oec.wis.tools.TextWatcherAdapter;
import com.oec.wis.tools.Tools;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnItemClickListener;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;


public class FragMusic extends Fragment{
    View view;
    List<WISAlbum> album;
    List<WISMusic> songs;
    GridView navGrid;
    ListView shareSongsList;

    ImageButton close;

    List<WISMusic> allSongs;
    List<WISAlbum> allAlbums;
    ImageView userNotificationView;

    File imageFile;
    GridView gridMusic;
    GridView gridMusic1;
    RelativeLayout album_layout;
    RelativeLayout songs_layout;
    ArrayList<PubMusic> pubMusics;
    List<WISAds> banAds;
    ProgressDialog mProgressDialog;

    private ProgressDialog progress;

    Button suivant,suivant1;


    String progressName;
    String playlistname;

    TextView target_view;
    int progressValue=0;
    Typeface font;

    ArrayAdapter<String> dataAdapter = null;
    String strSong;
    String strAlbum;

    ArrayList<Integer> songIds = new ArrayList<Integer>();
    ArrayList<Integer> albumIds = new ArrayList<Integer>();
    TextView headerTitleTV;


    boolean isTimerRunning = false;
    int cPub = 0;
    DatabaseHandler db;
    public Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            loadPubBanner(cPub);
        }
    };

    TextView tvBanTitle, tvBanDesc;
    ImageView ivBanPhoto;

    ArrayList<Integer> playlistitems = new ArrayList<Integer>();

    DialogPlus dialog;
    static final int REQUEST_ALBUM_CAPTURE = 1;
    public static final String PREFS_NAME = "CustomPermission";
    SharedPreferences settings;

    MediaMetadataRetriever metaRetriver;
    byte[] art;
    EditText albumlist;
    TextView albumTextView,songTextView,playlist;

    ArrayList<Popupelements> popupList;
    Popupadapter simpleAdapter;

    AlbumAdapter adapter;
    AudioAdapter adapter1;
    ArrayList<WISSearch> searchlist = new ArrayList<WISSearch>();
    ArrayList<WISSearchs> searchlist1 = new ArrayList<WISSearchs>();

    private ProgressBar progressBar;
    private int progressStatus = 0;
    private Handler handler = new Handler();

    private Boolean loadAlbums = Boolean.TRUE;

    private MediaPlayer mediaPlayer;

    List<WISSongs> shareAlbumlist;
    SharedPreferences prefs;
    SharedPreferences prefs1;

//    String CurrentAudioPath;


    List<String>CurrentAudioPath = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        UserNotification.setIsAlbumUploaded(Boolean.FALSE);
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }

        try {
            Tools.setLocale(getActivity(), Tools.getData(getActivity(), "lang_pr"));
            view = inflater.inflate(R.layout.frag_music, container, false);
            settings = getActivity().getSharedPreferences(PREFS_NAME, 0);
            boolean dialogShown = settings.getBoolean("dialogShown", false);

            prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
            prefs1 = PreferenceManager.getDefaultSharedPreferences(getActivity());
            if (!dialogShown) {
                // AlertDialog code here
                showcontactAlert();

            }
            else {
                loadMusic();
                initControls();
                setListener();
                loadBannerPub();
                Tools.loadNotifs(getActivity());
                gridMusic.invalidateViews();
                gridMusic.setVisibility(View.VISIBLE);
                suivant.setVisibility(View.GONE);
                suivant1.setVisibility(View.GONE);
                new UserNotification().setCheckboxs(false);
            }





        } catch (InflateException e) {
        }
        return view;
    }

    private void showcontactAlert(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(false).setView(R.layout.wismusic_alert);

        final AlertDialog alert = builder.create();
        alert.show();


        alert.findViewById(R.id.cancel_alert).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.dismiss();
                //target_view.setText("9");
                FragHome fragment = new FragHome();
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame_container, fragment).commit();
            }
        });
        alert.findViewById(R.id.accept_alert).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadMusic();
                initControls();
                setListener();
                loadBannerPub();
                gridMusic.invalidateViews();
                gridMusic.setVisibility(View.VISIBLE);
                suivant.setVisibility(View.GONE);
                suivant1.setVisibility(View.GONE);
                new UserNotification().setCheckboxs(false);
                SharedPreferences.Editor editor = settings.edit();
                editor.putBoolean("dialogShown", true);
                editor.commit();
                //target_view.setText("2");
                //loadMusic();

                alert.dismiss();


//

            }
        });
    }

    private void setListener() {
        view.findViewById(R.id.toggle).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                ((Dashboard) getActivity()).mDrawerLayout.openDrawer(GravityCompat.START);
                ((DrawerLayout) getActivity().findViewById(R.id.drawer_layout)).openDrawer(Gravity.START);
            }
        });
//        albumTextView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                Log.e("Album btn clicked", String.valueOf(album));
//
//                album_layout.setVisibility(View.VISIBLE);
//                gridMusic.setVisibility(View.VISIBLE);
//                songs_layout.setVisibility(View.GONE);
//                albumTextView.setBackgroundColor(getResources().getColor(R.color.trans_white));
//                songTextView.setBackgroundColor(getResources().getColor(R.color.blue4));
//                albumlist.setText("");
//
//                refereshAlbum();
//
//            }
//        });
//        songTextView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                songs_layout.setVisibility(View.VISIBLE);
//                gridMusic1.setVisibility(View.VISIBLE);
//                album_layout.setVisibility(View.GONE);
//                songTextView.setBackgroundColor(getResources().getColor(R.color.trans_white));
//                albumTextView.setBackgroundColor(getResources().getColor(R.color.blue4));
//                albumlist.setText("");
//
//                refereshsongData();
//
//
//
//            }
//        });


        albumTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.e("Album btn clicked", String.valueOf(album));

                album_layout.setVisibility(View.VISIBLE);
                gridMusic.setVisibility(View.VISIBLE);
                songs_layout.setVisibility(View.GONE);
                albumTextView.setBackgroundColor(getResources().getColor(R.color.blue_color));
                songTextView.setBackgroundColor(getResources().getColor(R.color.light_blue_color));
                albumlist.setText("");

                refereshAlbum();

            }
        });
        songTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                songs_layout.setVisibility(View.VISIBLE);
                gridMusic1.setVisibility(View.VISIBLE);
                album_layout.setVisibility(View.GONE);
                songTextView.setBackgroundColor(getResources().getColor(R.color.blue_color));
                albumTextView.setBackgroundColor(getResources().getColor(R.color.light_blue_color));
                albumlist.setText("");

                refereshsongData();



            }
        });
        userNotificationView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //ApplicationController.callDialog(getActivity());
                List<WISMenu> menuList;

                LayoutInflater inflater = getActivity().getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.custom_dialog, null);


                final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setCancelable(true).setView(dialogView);

                final AlertDialog alert = builder.create();
                alert.show();
//                next = (Button)alert.findViewById(R.id.bNext);
//                close = (ImageButton)alert.findViewById(R.id.closeGroupchat);
                navGrid = (GridView)alert.findViewById(R.id.nav_grid);
                menuList = new ArrayList<>();
                menuList.add(new WISMenu(getString(R.string.chat_circle), R.drawable.chat,new UserNotification().getChatcount()));
                menuList.add(new WISMenu(getString(R.string.notifictions), R.drawable.bell,0));
                menuList.add(new WISMenu(getString(R.string.chat_group), R.drawable.user_groups,new UserNotification().getGroupcount()));
                menuList.add(new WISMenu(getString(R.string.live_direct), R.drawable.user,new UserNotification().getDirectcount()));
                menuList.add(new WISMenu(getString(R.string.chat_music), R.drawable.music,0));
                menuList.add(new WISMenu(getString(R.string.demandes), R.drawable.profile,0));
                menuList.add(new WISMenu(getString(R.string.weather), R.drawable.weather_logo,0));

                navGrid.setAdapter(new ShowDialogAdapter(getActivity(), menuList));
                //loadFriendsList(groupChatFriendList);

                navGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        if(position==4)
                        {
                            alert.dismiss();
                        }
                        else {
                            displayFrag(position);
                            alert.dismiss();
                        }
                    }
                });
            }
        });
        gridMusic.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                        List<WISSongs> array = album.get(position).getListdata();
//                        Log.i("songsData",String.valueOf(array));
//                        Uri uri = Uri.parse(album.get(position).getListdata().get(0).getPath());
//                        new UserNotification().setSongs(array);
//                        new UserNotification().setSelected(position);
//                        Intent intent = new Intent(getActivity(),Player.class);
//                        intent .putExtra("uri_Str", uri);
//                        intent.putExtra("songIcon",album.get(position).getListdata().get(0).getThumb());
//                        intent.putExtra("songIndex",album.get(position).getListdata().get(0).toString());
//                        intent.putExtra("type","album");
//                        intent.putExtra("songname",album.get(position).getName());
//                        intent.putExtra("artist",album.get(position).getListdata().get(0).getA());
//                        intent.putExtra("duration",album.get(position).getListdata().get(0).getDuration());
//
//                        Log.d("Pass Data","ok");
//
//                        startActivity(intent);

                List<WISSongs> array = db.getAlbumSongs(allAlbums.get(position).getId());
                Log.i("songsData",String.valueOf(array));
                String PATH = Environment.getExternalStorageDirectory()
                        + "/wismusic/";
                Uri uri = Uri.parse(PATH+array.get(0).getPath());
                Log.i("albumUri", String.valueOf(uri));
                new UserNotification().setSongs(array);
                new UserNotification().setSelected(0);
                Intent intent = new Intent(getActivity(),Player.class);
                intent .putExtra("uri_Str", uri);
                intent.putExtra("songIcon",array.get(0).getThumb());
                intent.putExtra("songIndex",0);
                intent.putExtra("type","album");
                intent.putExtra("songname",allAlbums.get(position).getName());
                //intent.putExtra("artist",allAlbums.get(position).getArtists());
                intent.putExtra("songIcon",array.get(0).getThumb());
                intent.putExtra("artist",array.get(0).getA());
                intent.putExtra("duration",array.get(0).getDuration());
//                try{
//
//                    intent.putExtra("thumbnail",array.get(position).getThumb());
//
//                }catch (IndexOutOfBoundsException ex){
//                    System.out.println("Exce" +ex.getMessage());
//                }


                Log.d("Pass Data","ok");

                startActivity(intent);





            }
        });

        gridMusic1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

//                Uri uri = Uri.parse(songs.get(position).getPath());
//                new UserNotification().setSonglist(songs);
//                new UserNotification().setSelected(position);
//                Intent intent = new Intent(getActivity(),Player.class);
//                intent .putExtra("uri_Str", uri);
//                intent.putExtra("songIcon",songs.get(position).getThumb());
//                intent.putExtra("songIndex",position);
//                intent.putExtra("type","songs");
//                intent.putExtra("songname",songs.get(position).getName());
//                intent.putExtra("artist",songs.get(position).getArtists());
//                intent.putExtra("duration",songs.get(position).getDuration());
//
//                Log.d("Pass Data","ok");
//
//                startActivity(intent);

                String PATH = Environment.getExternalStorageDirectory()
                        + "/wismusic/";
                Uri uri = Uri.parse(PATH+allSongs.get(position).getPath());
                new UserNotification().setSonglist(allSongs);
                new UserNotification().setSelected(position);
                Intent intent = new Intent(getActivity(),Player.class);
                intent .putExtra("uri_Str", uri);
                intent.putExtra("songIcon",allSongs.get(position).getThumb());
                intent.putExtra("songIndex",position);
                intent.putExtra("type","songs");
                intent.putExtra("songname",allSongs.get(position).getName());
                intent.putExtra("artist",allSongs.get(position).getArtists());
                intent.putExtra("duration",allSongs.get(position).getDuration());
                intent.putExtra("thumbnail",allSongs.get(position).getThumb());


                Log.d("Pass Data","ok");

                startActivity(intent);





            }
        });
        view.findViewById(R.id.bAdd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showMediaoption();
            }
        });

        suivant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                songIds = new UserNotification().getSongids();
                albumIds = new UserNotification().getAlbumids();
                Integer[] myArray = new Integer[songIds.size()];
                for (int i = 0; i < songIds.size(); i++) {
                    myArray[i] = Integer.valueOf(songIds.get(i));
                }
                Integer[] myArray1 = new Integer[albumIds.size()];
                for (int j = 0; j < albumIds.size(); j++) {
                    myArray1[j] = Integer.valueOf(albumIds.get(j));
                }
                strSong = Arrays.toString(myArray).replaceAll("[\\[\\]\\ ]", "");
                strAlbum = Arrays.toString(myArray1).replaceAll("[\\[\\]\\ ]", "");
                createPlaylist(strSong,strAlbum,playlistname);


            }
        });

        view.findViewById(R.id.bAdd1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showMediaoption();
            }
        });

        suivant1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                songIds = new UserNotification().getSongids();
                albumIds = new UserNotification().getAlbumids();
                Integer[] myArray = new Integer[songIds.size()];
                for (int i = 0; i < songIds.size(); i++) {
                    myArray[i] = Integer.valueOf(songIds.get(i));
                }
                Integer[] myArray1 = new Integer[albumIds.size()];
                for (int j = 0; j < albumIds.size(); j++) {
                    myArray1[j] = Integer.valueOf(albumIds.get(j));
                }
                strSong = Arrays.toString(myArray).replaceAll("[\\[\\]\\ ]", "");
                strAlbum = Arrays.toString(myArray1).replaceAll("[\\[\\]\\ ]", "");
                createPlaylist(strSong,strAlbum,playlistname);
                //Toast.makeText(getActivity(),"LIST" +strSong+"and"+strAlbum+"received", Toast.LENGTH_LONG).show();
            }
        });

        playlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                playlist.setText(getString(R.string.playlist_search));
                playlist.setTextColor(Color.WHITE);
                suivant.setVisibility(View.VISIBLE);
                suivant1.setVisibility(View.VISIBLE);

                //new UserNotification().setCheckboxs(true);

                AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                alert.setTitle(getString(R.string.playlist_search));
//                alert.setMessage("Message");
                final EditText input = new EditText (getActivity());
                alert.setView(input);

                alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        playlistname = input.getText().toString();
                        new UserNotification().setCheckboxs(true);
                        gridMusic.invalidateViews();
                        gridMusic1.invalidateViews();
                    }
                });

                alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        new UserNotification().setCheckboxs(false);
                        playlist.setText(getString(R.string.create_pllist));
                        playlist.setTextColor(Color.WHITE);

                        suivant.setVisibility(View.GONE);
                        suivant1.setVisibility(View.GONE);
                        dialog.cancel();
                    }
                });
                alert.show();



            }
        });

    }
    private void displayFrag(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = new FragChat();
                UserNotification userNotification = new UserNotification();
                userNotification.setChatSelected(Boolean.FALSE);
                break;
            case 1:
                fragment = new
                        FragNotif();
                break;
            case 2:
                fragment = new FragChat();
                UserNotification userNotification1 = new UserNotification();
                userNotification1.setDISCUSSIONSELECTED(Boolean.TRUE);
                break;
            case 3:

//                fragment = new FragDirectory();
                break;
            case 4:
                fragment = new FragMusic();
                break;
            case 5:
                //fragment = new FragMyLocation();

                break;
            case 6:

                fragment = new FragWeather();
                break;
            default:
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.frame_container, fragment).commit();
            //((Dashboard) getActivity()).mDrawerLayout.closeDrawer(GravityCompat.START);
        }
    }
    public void showMediaoption(){


        final ArrayList popupList = new ArrayList();

        popupList.add(new Popupelements(R.drawable.wis_photo_picker,"Pick Songs"));
        popupList.add(new Popupelements(R.drawable.wis_photo_picker,"Pick Album"));
        popupList.add(new Popupelements(R.drawable.wis_photo_picker,"WIS Playlist"));
        popupList.add(new Popupelements(R.drawable.wis_photo_picker,"WIS Music Pub"));
        popupList.add(new Popupelements(R.drawable.wis_photo_picker,"WISVideo"));

        Popupadapter simpleAdapter = new Popupadapter(getActivity(), false, popupList);


        dialog = DialogPlus.newDialog(getActivity())
                .setAdapter(simpleAdapter)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View v, int position) {

                        if(position ==0){
                            Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(Intent.createChooser(i, getString(R.string.pick_songs)), 1);
                        }

                        else if(position == 1){
                            new UserNotification().setUploadSong(true);
                            Intent intent = new Intent(getActivity(), AudioBrowser.class);
                            startActivity(intent);
                            dialog.dismiss();
                            gridMusic.invalidateViews();
                        }
                        else if(position==2)
                        {
                            FragPlaylist fragment = new FragPlaylist();
                            FragmentManager fragmentManager = getFragmentManager();
                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.frame_container, fragment).commit();
                            dialog.dismiss();
                        }
                        else if(position==3)
                        {
                            startActivity(new Intent(getActivity(), NewAds.class));
                            dialog.dismiss();
                        }
                        else if(position==4)
                        {
                            FragVideo fragment = new
                                    FragVideo();
                            FragmentManager fragmentManager = getFragmentManager();
                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.frame_container, fragment).commit();
                            dialog.dismiss();
                        }
                        else {
                            dialog.dismiss();
                        }


                    }
                })
                .setExpanded(false)  // This will enable the expand feature, (similar to android L share dialog)
                .create();
        dialog.show();
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);

        dialog.dismiss();

        switch (requestCode) {
            case 1:
                if (resultCode == getActivity().RESULT_OK) {
                    Uri selectedSong = imageReturnedIntent.getData();
                    final String[] proj = {MediaStore.Images.Media.DATA,MediaStore.Audio.Media.TITLE,MediaStore.Audio.Media.ARTIST,MediaStore.Audio.Media.SIZE, MediaStore.Audio.Media.DURATION};
                    final Cursor cursor = getActivity().managedQuery(selectedSong, proj, null, null, null);
                    final int column_index = cursor.getColumnIndex(MediaStore.Images.Media.DATA);
                    cursor.moveToLast();
                    String musicPath = cursor.getString(column_index);
                    String nameSong = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.TITLE));
                    String artist = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST));

                    long duration = (cursor.getInt(cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DURATION)));;
                    long durationSec =TimeUnit.MILLISECONDS.toSeconds(duration);
                    long minutes,seconds,hours;
                    seconds = durationSec % 60;
                    durationSec/= 60;
                    minutes = durationSec % 60;
                    durationSec /= 60;
                    hours = durationSec % 24;
                    String  durationStr=String.valueOf(hours)+":"+String.valueOf(minutes)+":"+String.valueOf(seconds);
                    metaRetriver = new MediaMetadataRetriever(); metaRetriver.setDataSource(musicPath);
                    try {

                        art = metaRetriver.getEmbeddedPicture();

                        Log.e("Art Icon", String.valueOf(art));

                        if(art!=null)
                        {
                            Bitmap songImage1 = BitmapFactory .decodeByteArray(art, 0, art.length);
                            //persistImage(songImage1,"wis_music");

                            String PATH = Environment.getExternalStorageDirectory()
                                    + "/wismusic/";
                            File file = new File(PATH);
                            if (!file.exists()) {
                                file.mkdirs();
                            }
                            Random generator = new Random();
                            int n = 10000;
                            n = generator.nextInt(n);
                            String fname = "wis_music-"+ n + ".jpg";
                            imageFile = new File(PATH, fname);

                            OutputStream os;
                            try {
                                os = new FileOutputStream(imageFile);
                                songImage1.compress(Bitmap.CompressFormat.JPEG, 100, os);
                                os.flush();
                                os.close();
                            } catch (Exception e) {

                            }

                            Log.i("imageIcon",String.valueOf(imageFile));
                        }
                        else{
                            Bitmap songImage1 = BitmapFactory.decodeResource( getResources(), R.drawable.empty);

                            String PATH = Environment.getExternalStorageDirectory()
                                    + "/wismusic/";
                            File file = new File(PATH);
                            if (!file.exists()) {
                                file.mkdirs();
                            }
                            Random generator = new Random();
                            int n = 10000;
                            n = generator.nextInt(n);
                            String fname = "wis_music-"+ n + ".jpg";
                            imageFile = new File(PATH, fname);

                            OutputStream os;
                            try {
                                os = new FileOutputStream(imageFile);
                                songImage1.compress(Bitmap.CompressFormat.JPEG, 100, os);
                                os.flush();
                                os.close();
                            } catch (Exception e) {

                            }
                            Log.i("imageNull",String.valueOf(imageFile));
                        }

                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                    if(artist==null||artist.equals("")){

                        artist = "wismusic";
                    }

                    if(nameSong ==null||nameSong.equals("")){

                        nameSong = "wismusic";
                    }

                    if(durationStr==null||durationStr.equals(""))
                    {
                        durationStr="00:00:04";
                    }


                    String typeSongs = "songs";

                    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                    {
                        Log.i("Calling","executeOnExecutor()");
                        new DoUpload().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,musicPath,String.valueOf(imageFile),nameSong,artist,durationStr,typeSongs);
                    }
                    else {
                        Log.i("Calling","execute()");
                        new DoUpload().execute(musicPath,String.valueOf(imageFile),nameSong,artist,durationStr,typeSongs);
                    }


                }
                break;
            case 2:
                if (resultCode == getActivity().RESULT_OK) {
                    Uri selectedSong = imageReturnedIntent.getData();

                    final String[] proj = {MediaStore.Images.Media.DATA, MediaStore.Audio.Media.TITLE,MediaStore.Audio.Media.ALBUM,MediaStore.Audio.Media.ARTIST, MediaStore.Audio.Media.SIZE, MediaStore.Audio.Media.DURATION};
                    final Cursor cursor = getActivity().managedQuery(selectedSong, proj, null, null, null);
                    final int column_index = cursor.getColumnIndex(MediaStore.Images.Media.DATA);
                    cursor.moveToLast();
                    String musicPath = cursor.getString(column_index);
                    String album_name = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM));
                    String album_artists = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST));

                    long duration = (cursor.getInt(cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DURATION)));
                    long durationSec = TimeUnit.MILLISECONDS.toSeconds(duration);
                    long minutes, seconds, hours;
                    seconds = durationSec % 60;
                    durationSec /= 60;
                    minutes = durationSec % 60;
                    durationSec /= 60;
                    hours = durationSec % 24;
                    String durationStr = String.valueOf(hours) + ":" + String.valueOf(minutes) + ":" + String.valueOf(seconds);
                    String albumIcon = getRealPathFromURI(selectedSong);

                    String typeAlbum = "album";

                    new DoUpload().execute(musicPath,albumIcon,album_name, album_artists, durationStr, typeAlbum);


                }
                break;
            default:
                break;
        }
    }

    //Bitmap to String:
    public String BitMapToString(Bitmap bitmap){
        ByteArrayOutputStream baos=new  ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,100, baos);
        byte [] arr=baos.toByteArray();
        String result= Base64.encodeToString(arr, Base64.DEFAULT);
        return result;
    }

    //String to Bitmap:
    public Bitmap StringToBitMap(String image){
        try{
            byte [] encodeByte=Base64.decode(image,Base64.DEFAULT);
            Bitmap bitmap=BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        }catch(Exception e){
            e.getMessage();
            return null;
        }
    }

    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = { MediaStore.Images.Media.DATA };
        CursorLoader cursorLoader = new CursorLoader(getActivity(),contentUri, proj, null, null, null);
        Cursor cursor = cursorLoader.loadInBackground();

        int column_index =
                cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }
    private void createPlaylist(String song_ids, String album_ids,String title) {
        JSONObject jsonBody = new JSONObject();

        try {
            jsonBody.put("user_id", Tools.getData(getActivity(), "idprofile"));
            jsonBody.put("songs_ids", song_ids);
            jsonBody.put("album_ids", album_ids);
            jsonBody.put("title",title);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.i("JsongBody", String.valueOf(jsonBody));

        JsonObjectRequest reqDelete = new JsonObjectRequest(getActivity().getString(R.string.server_url) + getActivity().getString(R.string.create_playlist), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {


                                if(db!=null){

                                    db.deleteAllPlaylist();
                                }

                                Log.i("playlist", String.valueOf(response));

                                JSONArray data = response.getJSONArray("album_files");

                                Log.i("albumData", String.valueOf(data));

                                for (int i = 0; i < data.length(); i++) {
                                    try {
                                        JSONObject obj1 = data.getJSONObject(i);
                                        //create albumlist array
                                        JSONArray albumArray = obj1.getJSONArray("albums");

                                        List<WISAlbumPlaylist> albumlist = new ArrayList<>();

                                        for (int pa=0;pa<albumArray.length();pa++)
                                        {

                                            JSONObject albumdict = albumArray.getJSONObject(pa);

//                                            create songlist array

                                            JSONArray songArray = albumdict.getJSONArray("songs");

                                            List<WISPlaylist1> songsList = new ArrayList<>();

                                            for(int ps=0;ps<songArray.length();ps++)
                                            {

                                                JSONObject songDict = songArray.getJSONObject(ps);
//                                                save songs

                                                songsList.add(new WISPlaylist1(songDict.getInt("id"),songDict.getString("thumbnail"),songDict.getString("title"),songDict.getString("artists"),songDict.getString("created_at"),songDict.getString("audio_path"),songDict.getString("duration"),songDict.getString("type"),songDict.getInt("album_id")));
                                                db.putPlaylistAlbumSongs(new WISPlaylist1(songDict.getInt("id"),songDict.getString("thumbnail"),songDict.getString("title"),songDict.getString("artists"),songDict.getString("created_at"),songDict.getString("audio_path"),songDict.getString("duration"),songDict.getString("type"),songDict.getInt("album_id")));
                                                Log.i("songsList",String.valueOf(songsList));
                                            }
//                                            save albums
                                            albumlist.add(new WISAlbumPlaylist(obj1.getInt("id"),albumdict.getString("album_name"),albumdict.getInt("album_id"),albumdict.getString("album_artists"),albumdict.getString("created_at"),songsList));
                                            db.addPlaylistAddAlbums(new WISAlbumPlaylist(obj1.getInt("id"),albumdict.getString("album_name"),albumdict.getInt("album_id"),albumdict.getString("album_artists"),albumdict.getString("created_at"),songsList));

                                            Log.i("albumlist",String.valueOf(albumlist));
                                        }

//                                        save playlist
                                        //album.add(new WISPlaylistAlbum(obj1.getInt("id"),obj1.getString("type"),obj1.getString("created_at"),obj1.getString("created_by"),obj1.getString("name"),albumlist));
                                        db.addPlaylistAllAlbums(new WISPlaylistAlbum(obj1.getInt("id"),obj1.getString("type"),obj1.getString("created_at"),obj1.getString("created_by"),obj1.getString("name"),albumlist));




                                    } catch (Exception e) {
                                    }


                                }

                            }

                        } catch (JSONException e) {

                            //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }
                        try {
                            if (response.getBoolean("result")) {

                                JSONArray data = response.getJSONArray("songs_files");

                                Log.i("songsData", String.valueOf(data));

                                for (int i = 0; i < data.length(); i++) {
                                    try {
                                        JSONObject obj = data.getJSONObject(i);


                                        JSONArray songArray = obj.getJSONArray("songs");

                                        List<WISPlaylist> songsList = new ArrayList<>();

                                        for(int ps=0;ps<songArray.length();ps++)
                                        {

                                            JSONObject songDict = songArray.getJSONObject(ps);
//                                                save songs

                                            songsList.add(new WISPlaylist(songDict.getInt("id"),songDict.getString("thumbnail"),songDict.getString("title"),songDict.getString("artists"),songDict.getString("created_at"),getString(R.string.server_url3)+songDict.getString("audio_path"),songDict.getString("duration"),songDict.getString("type"),songDict.getInt("album_id")));
                                            db.putPlaylistSongs(new WISPlaylist(songDict.getInt("id"),songDict.getString("thumbnail"),songDict.getString("title"),songDict.getString("artists"),songDict.getString("created_at"),songDict.getString("audio_path"),songDict.getString("duration"),songDict.getString("type"),obj.getInt("id")));
                                            Log.i("songsList",String.valueOf(songsList));
                                        }
                                        // save playlist song
                                        db.addPlaylistSongs(new WISPlaylistSongs(obj.getInt("id"),obj.getString("type"),obj.getString("created_at"),obj.getString("created_by"),obj.getString("name"),songsList));
                                    } catch (Exception e) {
                                    }
                                }
//                                if (songs.size() == 0)
//                                    Toast.makeText(getActivity(), getString(R.string.no_song_element), Toast.LENGTH_SHORT).show();
//                                adapter1 = new PlaylistAudioAdapter(getActivity(), songs, new ChatIndivisualListener() {
//                                    @Override
//                                    public void showPopUP(int id, int position, String type, String path) {
//                                        showsharePopUPView(id,position,type,UserNotification.getMusicChatMessage());
//                                    }
//                                });
//                                gridMusic1.setAdapter(adapter1);
//                                adapter1.notifyDataSetChanged();
//                                loadFilterConfig();
                            }


//                            referesh Playlist Data




                        } catch (JSONException e) {

                            //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }


                        Log.i("playlist",String.valueOf(response));
                        FragPlaylist fragment = new FragPlaylist();
                        FragmentManager fragmentManager = getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.frame_container, fragment).commit();

                        new UserNotification().setCheckboxs(false);
                        songIds.clear();
                        albumIds.clear();
                        //dialog.dismiss();
                        Toast.makeText(getActivity(),"Playlist succussfully created", Toast.LENGTH_SHORT).show();

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //if (context != null)
                //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
                headers.put("token", Tools.getData(getActivity(), "token"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqDelete);
    }

    private void loadFilterConfig(){

        albumlist.addTextChangedListener(new TextWatcherAdapter(){
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                super.beforeTextChanged(s, start, count, after);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                super.onTextChanged(s, start, before, count);
                String text = albumlist.getText().toString().toLowerCase(Locale.getDefault());
                try{
                    adapter.filter(text);
                    adapter1.filter(text);
                }
                catch (Exception e)
                {
                    //Toast.makeText(getActivity(), getString(R.string.no_song_element), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                super.afterTextChanged(s);

            }
        });
    }

    private void initControls() {

        gridMusic = (GridView) view.findViewById(R.id.gridPhoto);
        gridMusic1 = (GridView) view.findViewById(R.id.gridPhoto1);
        playlist = (TextView) view.findViewById(R.id.playlist_serach);
        albumlist = (EditText) view.findViewById(R.id.album_search);
        album_layout = (RelativeLayout) view.findViewById(R.id.type_album);
        songs_layout = (RelativeLayout) view.findViewById(R.id.type_songs);
        albumTextView= (TextView) view.findViewById(R.id.album);
        songTextView= (TextView) view.findViewById(R.id.songs);
        target_view = (TextView) view.findViewById(R.id.target_view);

        suivant = (Button) view.findViewById(R.id.suivant);
        suivant1 = (Button) view.findViewById(R.id.suivant1);

        //progressBar = (ProgressBar) view.findViewById(R.id.loading);
        tvBanTitle = (TextView) view.findViewById(R.id.tvPubTitle);
        tvBanDesc = (TextView) view.findViewById(R.id.tvPubDesc);
        ivBanPhoto = (ImageView) view.findViewById(R.id.ivPubPhoto);
        headerTitleTV =(TextView)view.findViewById(R.id.headerTitleTV);

        userNotificationView = (ImageView) view.findViewById(R.id.userNotificationView);

        if(new UserNotification().getBatchcount()==0)
        {
            target_view.setVisibility(View.INVISIBLE);
        }
        else {
            target_view.setText(String.valueOf(new UserNotification().getBatchcount()));
        }

        ApplicationController.getInstance().getImageLoader().get(getString(R.string.server_url3) + Tools.getData(getActivity(), "photo"), new ImageLoader.ImageListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                userNotificationView.setImageResource(R.drawable.logo_wis);
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    userNotificationView.setImageBitmap(response.getBitmap());
                }
            }
        });
//        textView = (TextView) view.findViewById(R.id.textView1);


        try {
//
////        set typeface
            font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Harmattan-R.ttf");
            playlist.setTypeface(font);
            albumlist.setTypeface(font);
            albumTextView.setTypeface(font);
            songTextView.setTypeface(font);
            target_view.setTypeface(font);
            tvBanTitle.setTypeface(font);
            tvBanDesc.setTypeface(font);
            headerTitleTV.setTypeface(font);

        }catch (NullPointerException ex){
            System.out.println("Exception" +ex.getMessage());
        }

        refereshMusicData();

        mediaPlayer=new MediaPlayer();
        if(mediaPlayer.isPlaying())
        {
            mediaPlayer.stop();
            mediaPlayer.release();
        }




    }


    public void refereshMusicData(){

        db = new DatabaseHandler(getActivity());

        allAlbums = db.getAllAlbums();
        allSongs = db.getAllSongs();

        if(allAlbums.size()>0){

            refereshAlbum();

        }
        else if(allSongs.size()>0){


            refereshsongData();
        }

        else{

            loadMusic();
        }

    }


    public void fetchMusicFromLocalData(){

        db = new DatabaseHandler(getActivity());

        allAlbums = db.getAllAlbums();
        allSongs = db.getAllSongs();

        if(allAlbums.size()>0){

            refereshAlbum();

        }
        else if(allSongs.size()>0){

            refereshsongData();
        }

        if(loadAlbums==Boolean.FALSE){

            Log.i("calling","refereshsongData()");
            refereshsongData();
        }

    }

    public void refereshAlbum(){

        try {
            adapter = new AlbumAdapter(getActivity(), allAlbums, new ChatIndivisualListener() {
                @Override
                public void showPopUP(int id, int position, String type, String path) {
                    showsharePopAlbum(id, position, type, UserNotification.getMusicChatMessage());



                }
            }, new RefreshListener() {
                @Override
                public void refreshData() {
                    db.deleteAlumbs();
                    allAlbums.clear();
                    loadMusic();
                    adapter.notifyDataSetChanged();

                }


            });
            if(adapter!=null){

                gridMusic.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                loadFilterConfig();
            }
        }
        catch (IndexOutOfBoundsException e)
        {

        }
        catch (NullPointerException es)
        {

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }



    }

    public void fetchSongsFromLocalDb(){
        Log.i("calling","fetchSongsFromLocalDb");
        db = new DatabaseHandler(getActivity());
        allSongs = db.getAllSongs();
        if(allSongs.size()>0){
            refereshsongData();
        }

    }

    public void refereshsongData(){

        try {
            adapter1 = new AudioAdapter(getActivity(), allSongs, new ChatIndivisualListener() {
                @Override
                public void showPopUP(int id, int position, String type, String path) {

                    showsharePopUPView(id, position, type, UserNotification.getMusicChatMessage());
                }
            }, new RefreshListener() {
                public void refreshData() {

                    db.deleteSongs();
                    allSongs.clear();
//                fetchSongsFromLocalDb();
                    loadMusic();
                    adapter1.notifyDataSetChanged();



                }
            });

            if(adapter1!=null){

                gridMusic1.setAdapter(adapter1);
                adapter1.notifyDataSetChanged();
                loadFilterConfig();
            }
        }
        catch (IndexOutOfBoundsException e)
        {

        }
        catch (NullPointerException es)
        {

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

    }

    private void loadBannerPub() {
        JSONObject jsonBody = null;
        try {
            jsonBody = new JSONObject("{\"id_profil\":\"" + Tools.getData(getActivity(), "idprofile") + "\"}");
            jsonBody.put("type","music");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        banAds = new ArrayList<>();
        pubMusics = new ArrayList<>();
        JsonObjectRequest reqBan = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.banner_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {
                                try {
                                    JSONArray data = response.getJSONArray("data");

                                    Log.i("pubdata",String.valueOf(data));

                                    for (int i = 0; i < data.length(); i++) {
                                        JSONObject obj = data.getJSONObject(i);
                                        String photo = "";
                                        try {
                                            photo = obj.getString("photo");
                                        } catch (Exception e) {
                                        }
                                        String video_thumb = "";
                                        try {
                                            video_thumb = obj.getString("photo_video");
                                        } catch (Exception e) {
                                        }
                                        banAds.add(new WISAds(obj.getInt("id"), obj.getString("title"), obj.getString("description"), 0, 0, "", "", "", "", photo, "", false, false, "", obj.getString("type_obj"), video_thumb));
                                        pubMusics.add(new PubMusic(obj.getInt("id"), obj.getString("title"), obj.getString("description")));
                                        //pubList.add(new WISAds(obj.getInt("created_by"),obj.getString("creater_name"),obj.getInt("id_pub"), obj.getInt("id_act"), obj.getString("titre"), obj.getInt("etat"), obj.getInt("duree"), obj.getString("created_at"), obj.getString("date_lancement"), photo, obj.getString("type_obj"), video_thumb));

                                    }
                                    Log.i("pubmusiclist",String.valueOf(banAds));

                                    if (banAds.size() > 0) {
                                        showBanContent(0);
                                        view.findViewById(R.id.llPubBanner).setVisibility(View.VISIBLE);
                                        if (banAds.size() > 1)
                                            startBannerShow();
                                    } else {
                                        view.findViewById(R.id.llPubBanner).setVisibility(View.GONE);
                                    }

                                } catch (Exception e) {
                                }
                            }
                        } catch (JSONException e) {
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String x = "";
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getActivity(), "token"));
                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));

                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqBan);
    }

    private void loadPubBanner(final int i) {
        if (getActivity() != null) {
            Animation a = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_out);
            a.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    showBanContent(i);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            view.findViewById(R.id.llPubBanner).startAnimation(a);
        }
    }

    private void startBannerShow() {
        isTimerRunning = true;
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                if (cPub < (banAds.size() - 1))
                    cPub++;
                else
                    cPub = 0;
                mHandler.obtainMessage(cPub).sendToTarget();
            }
        }, 0, 8000);
    }

    private void showBanContent(int i) {
        tvBanTitle.setText(banAds.get(i).getTitle());
        tvBanDesc.setText(banAds.get(i).getDesc());

        final int id = banAds.get(i).getId();
        final int idact = banAds.get(i).getIdAct();
        final int advid = banAds.get(i).getAdvertiser();
        final String advname = banAds.get(i).getAdvertiserName();

        Log.i("going","pubdetails :"+id+","+idact+","+advid+","+advname);


        tvBanTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), PubDetails.class);

                i.putExtra("id",id);
                i.putExtra("idact", idact);
                i.putExtra("advertiser_id", advid);
                i.putExtra("advertiser_name", advname);

                startActivity(i);


            }
        });
        tvBanDesc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), PubDetails.class);

                i.putExtra("id",id);
                i.putExtra("idact", idact);
                i.putExtra("advertiser_id", advid);
                i.putExtra("advertiser_name", advname);

                startActivity(i);


            }
        });


        if (!banAds.get(i).getTypeObj().equals("video")) {
            ApplicationController.getInstance().getImageLoader().get(getString(R.string.server_url3) + banAds.get(i).getPhoto(), new ImageLoader.ImageListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ivBanPhoto.setImageResource(R.drawable.empty);
                }

                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                    if (response.getBitmap() != null) {
                        ivBanPhoto.setImageBitmap(response.getBitmap());
                    } else {
                        ivBanPhoto.setImageResource(R.drawable.empty);
                    }
                }
            });
        } else {
            ApplicationController.getInstance().getImageLoader().get(getString(R.string.server_url4) + banAds.get(i).getThumb(), new ImageLoader.ImageListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ivBanPhoto.setImageResource(R.drawable.empty);
                }

                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                    if (response.getBitmap() != null) {
                        ivBanPhoto.setImageBitmap(response.getBitmap());
                    } else {
                        ivBanPhoto.setImageResource(R.drawable.empty);
                    }
                }
            });
        }
    }



    public void loadMusic() {
        album = new ArrayList<>();
        songs = new ArrayList<>();

        view.findViewById(R.id.loading).setVisibility(View.VISIBLE);

        JSONObject jsonBody = new JSONObject();

        try {
            jsonBody.put("user_id", Tools.getData(getActivity(), "idprofile"));
            jsonBody.put("audio_page",0);
            jsonBody.put("album_page",0);
            Log.i("jsonBody",String.valueOf(jsonBody));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqMusic = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.getmusic_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i("resonse",response.toString());
                        try {
                            if (response.getBoolean("result")) {
                                final JSONArray data = response.getJSONArray("album_files");
                                for (int i = 0; i < data.length(); i++) {
                                    try {
                                        JSONObject obj = data.getJSONObject(i);
                                        JSONArray songsArry = obj.getJSONArray("songs");
                                        Log.i("songArray",String.valueOf(songsArry));
                                        List<WISSongs> arrayList = new ArrayList<>();

                                        List<String> pathArray = new ArrayList<>();


                                        db.addAlbums(new WISAlbum(obj.getInt("album_id"),obj.getString("album_name"),obj.getString("album_artists"),obj.getString("album_created_at")));

                                        int albumid = obj.getInt("album_id");

                                        for(int c =0;c<songsArry.length();c++)
                                        {
                                            JSONObject obj1 = songsArry.getJSONObject(c);
                                            arrayList.add(new WISSongs(obj1.getInt("id"),obj1.getString("name"),obj1.getString("artists"),obj1.getString("duration"),obj1.getString("audio_path"),obj1.getString("audio_thumbnail"),obj1.getString("created_at")));
                                            db.putSongs(new WISSongs(obj1.getInt("id"),obj1.getString("name"),obj1.getString("artists"),obj1.getString("duration"),obj1.getString("audio_path"),obj1.getString("audio_thumbnail"),obj1.getString("created_at"),albumid));

                                            Log.i("files",obj1.getString("audio_path"));
                                            //saveFile(obj1.getString("audio_path"));

                                            pathArray.add(getString(R.string.server_url3)+obj1.getString("audio_path"));

                                        }
//                                            if(!prefs.getBoolean("firstTime", false)) {
//
//
//                                                Log.i("pathArray", String.valueOf(pathArray.size()));
//
//                                                SharedPreferences.Editor editor = prefs.edit();
//                                                editor.putBoolean("firstTime", true);
//                                                editor.commit();
//                                            }
                                        for(int j=0;j<pathArray.size();j++)
                                        {
                                            new DownloadFile().execute(pathArray.get(j));
                                            //Log.i("callingAlbum",j+1+"time");
                                        }



                                        album.add(new WISAlbum(obj.getString("album_name"),obj.getInt("album_id"),obj.getString("album_artists"),obj.getString("album_created_at"),arrayList));


                                    } catch (Exception e) {
                                    }
                                }
//                                        adapter = new AlbumAdapter(getActivity(), album, new ChatIndivisualListener() {
//                                            @Override
//                                            public void showPopUP(int id, int position, String type, String path) {
//                                                showsharePopUPView(id, position, type, UserNotification.getMusicChatMessage());
//                                            }
//                                        }, new RefreshListener() {
//                                            @Override
//                                            public void refreshData() {
//                                                album.clear();
//                                                loadMusic();
//                                                adapter.notifyDataSetChanged();
//
//                                            }
//
//
//                                        });
//                                        if(adapter!=null){
//
//                                            gridMusic.setAdapter(adapter);
//                                            adapter.notifyDataSetChanged();
//                                            loadFilterConfig();
//                                        }
//                                        else {
//                                            Toast.makeText(getActivity(), getString(R.string.no_song_element), Toast.LENGTH_SHORT).show();
//                                        }


                            }
                            view.findViewById(R.id.loading).setVisibility(View.GONE);

                        } catch (JSONException e) {
                            view.findViewById(R.id.loading).setVisibility(View.GONE);
                            //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                        try {
                            if (response.getBoolean("result")) {



                                JSONArray data = response.getJSONArray("songs_files");
                                List<String> pathArray = new ArrayList<>();
                                for (int i = 0; i < data.length(); i++) {
                                    try {
                                        JSONObject obj = data.getJSONObject(i);

                                        Log.i("songs",obj.toString());

                                        songs.add(new WISMusic(obj.getInt("id"),obj.getString("name"),obj.getString("artists"),obj.getString("duration"),obj.getString("audio_path"),obj.getString("audio_thumbnail"),obj.getString("created_at")));
                                        db.addSongs(new WISMusic(obj.getInt("id"),obj.getString("name"),obj.getString("artists"),obj.getString("duration"),obj.getString("audio_path"),obj.getString("audio_thumbnail"),obj.getString("created_at")));
                                        Log.i("files",obj.getString("audio_path"));
                                        pathArray.add(getString(R.string.server_url3)+obj.getString("audio_path"));
                                    } catch (Exception e) {
                                    }
                                }
//                                    if(!prefs1.getBoolean("firstTime1", false)) {
//                                        // run your one time code here
//
//                                        Log.i("pathArray", String.valueOf(pathArray.size()));
//
//                                        SharedPreferences.Editor editor = prefs1.edit();
//                                        editor.putBoolean("firstTime1", true);
//                                        editor.commit();
//                                    }
                                for(int j=0;j<pathArray.size();j++)
                                {
                                    new DownloadFile1().execute(pathArray.get(j));
                                    //Log.i("callingMusic",j+1+"time");
                                }

//                                        adapter1 = new AudioAdapter(getActivity(), songs, new ChatIndivisualListener() {
//                                            @Override
//                                            public void showPopUP(int id, int position, String type, String path) {
//                                                showsharePopUPView(id, position, type, UserNotification.getMusicChatMessage());
//                                            }
//                                        }, new RefreshListener() {
//                                            public void refreshData() {
//                                                Log.i("calling","refreshData");
//                                                songs.clear();
//                                                loadMusic();
//                                                adapter1.notifyDataSetChanged();
//
//
//
//                                            }
//                                        });
//
//                                        if(adapter1!=null){
//
//                                            gridMusic1.setAdapter(adapter1);
//                                            adapter1.notifyDataSetChanged();
//                                            loadFilterConfig();
//                                        }
//                                        else {
//                                            Toast.makeText(getActivity(), getString(R.string.no_song_element), Toast.LENGTH_SHORT).show();
//                                        }

                                // SONGS DATA


                            }
                            view.findViewById(R.id.loading).setVisibility(View.GONE);

                        } catch (JSONException e) {
                            view.findViewById(R.id.loading).setVisibility(View.GONE);
                        }


                        System.out.println("referesh Local data");

                        fetchMusicFromLocalData();

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        view.findViewById(R.id.loading).setVisibility(View.GONE);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(getActivity(), "token"));
                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
                return headers;
            }
        };
        ApplicationController.getInstance().addToRequestQueue(reqMusic);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ApplicationController.getInstance().cancelPendingRequests(ApplicationController.TAG);
    }

    private class DownloadFile extends AsyncTask<String, Integer, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... url) {

            int count;
            String imageURL = url[0];
            try {
                URL urls = new URL(imageURL);

                URLConnection conexion = urls.openConnection();

                conexion.connect();

                int lenghtOfFile = conexion.getContentLength();
                InputStream input = new BufferedInputStream(urls.openStream());
                imageURL = imageURL.replace(getString(R.string.server_url3),"");
                String fname = imageURL;
                String PATH = Environment.getExternalStorageDirectory()
                        + "/wismusic/";
                //Log.v("log_tag", "PATH: " + PATH);
                File file = new File(PATH);
                if (!file.exists()) {
                    file.mkdirs();
                }
                File outputFile = new File(file, fname);
                if (!outputFile.exists ())
                {
                    try {
                        OutputStream output = new FileOutputStream(outputFile);
                        byte data[] = new byte[1024];
                        long total = 0;
                        while ((count = input.read(data)) != -1) {

                            total += count;
                            publishProgress((int) (total * 100 / lenghtOfFile));

                            output.write(data, 0, count);

                        }
                        output.flush();

                        output.close();

                        input.close();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                else {
                    //Log.e("FileExists", String.valueOf(outputFile));
                }
                //outputFile.delete ();


            } catch (Exception e) {
            }

            return null;

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }
    }

    public void copy(File src, File dst) throws IOException {

    }

    public void saveAudioToLocalPath(String fname){

        if(Tools.isStorageAvailable()){

            System.out.println("Save AudioPath=>");

            Log.e("Audio path", String.valueOf(CurrentAudioPath));

            File localAudioFile = new File(CurrentAudioPath.get(0));

            try {


                String sep = File.separator; // Use this instead of hardcoding the "/"
                String newFolder = "wismusic";
                String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
                File myNewFolder = new File(extStorageDirectory + sep + newFolder);
                if(!myNewFolder.exists()){

                    myNewFolder.mkdir();
                }

                String mediaFile = Environment.getExternalStorageDirectory().toString() + sep + newFolder + sep + fname;

                Log.e("Local media file path",mediaFile);
                InputStream in = new FileInputStream(localAudioFile);
                File file = new File(mediaFile);
                OutputStream out = new FileOutputStream(file);

                // Transfer bytes from in to out
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);

                    CurrentAudioPath.clear();


                }
                in.close();
                out.close();

//                    }
//                }
            } catch (Exception e) {}


        }
        else{

            Log.e("Space not available","***");
        }

    }


    private class DownloadFile1 extends AsyncTask<String, Integer, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... url) {

            int count;
            String imageURL = url[0];
            try {
                URL urls = new URL(imageURL);

                URLConnection conexion = urls.openConnection();

                conexion.connect();

                int lenghtOfFile = conexion.getContentLength();

                InputStream input = new BufferedInputStream(urls.openStream());

                imageURL = imageURL.replace(getString(R.string.server_url3),"");

                String fname = imageURL;

                String PATH = Environment.getExternalStorageDirectory()
                        + "/wismusic/";
                //Log.v("log_tag", "PATH: " + PATH);
                File file = new File(PATH);
                if (!file.exists()) {
                    file.mkdirs();
                }
                File outputFile = new File(file, fname);
                if (!outputFile.exists ())
                {
                    try {
                        OutputStream output = new FileOutputStream(outputFile);
                        byte data[] = new byte[1024];
                        long total = 0;
                        while ((count = input.read(data)) != -1) {

                            total += count;
                            publishProgress((int) (total * 100 / lenghtOfFile));

                            output.write(data, 0, count);

                        }
                        output.flush();

                        output.close();

                        input.close();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                //outputFile.delete ();


            } catch (Exception e) {
            }

            return null;

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }
    }
    public String uploadMusic(String fPath,String icons,String songName,String artist,String songDuration,String type,String url,String userId, String token) throws ParseException, IOException {

        HttpClient http = AndroidHttpClient.newInstance("WIS");
        HttpPost method = new HttpPost(url);
        String BOUNDARY = "--wisboundry--";
        method.setHeader("Content-Type", "multipart/form-data; boundary=" + BOUNDARY);
        method.addHeader("token", token);

        ProgressHttpEntityWrapper.ProgressCallback progressCallback = new ProgressHttpEntityWrapper.ProgressCallback() {

            @Override
            public void progress(float progress) {
                //Use the progress


                progressValue = (int) Math.round(progress);

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        //Log.e("progressValue", String.valueOf(progressValue));
                        //mProgressDialog.setTitle("Please Wait");
                        mProgressDialog.setMessage("Uploading"+"\n"+String.valueOf(progressValue).concat(" %"));

                    }
                });

            }

        };



        ArrayList<String>filedata = new ArrayList<>();
        filedata.add(fPath);

        ArrayList<String>audioIcons = new ArrayList<>();
        audioIcons.add(icons);


        Log.i("file name", String.valueOf(fPath));
        Log.i("audio_icons", String.valueOf(icons));
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        builder.setBoundary(BOUNDARY);
        builder.setCharset(Charset.defaultCharset());
        for(int i=0;i<filedata.size();i++) {
            FileBody fileBody = new FileBody(new File(filedata.get(i)));
            builder.addPart("file".concat(String.valueOf(i)), fileBody);
            Log.i("file name","file".concat(String.valueOf(i)));

        }
        for(int i=0;i<audioIcons.size();i++) {
            FileBody iconsBody = new FileBody(new File(audioIcons.get(i)));
            builder.addPart("audio_icons".concat(String.valueOf(i)),iconsBody);
            Log.i("audio_icons","audio_icons".concat(String.valueOf(i)));

        }
        builder.addTextBody("user_id", userId);

        //builder.addTextBody("audio_icons", songIcon);

        if(type.equals("album"))
        {
            String typeAlbum = "album";
            builder.addTextBody("album_name", songName);
            builder.addTextBody("album_artists", artist);
            builder.addTextBody("type", typeAlbum);
            builder.addTextBody("album_duration", songDuration);


        }
        else {
            String typeSongs = "songs";
            builder.addTextBody("name", songName);
            builder.addTextBody("artists", artist);
            builder.addTextBody("type", typeSongs);
            builder.addTextBody("duration", songDuration);
        }
        HttpEntity entity = builder.build();



        method.setHeader("Accept", "application/json");
        method.setHeader("Content-Type", "multipart/form-data; boundary=" + BOUNDARY);

        method.setEntity(new ProgressHttpEntityWrapper(entity, progressCallback));



        HttpResponse response = null;
        try {
            response = http.execute(method);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String res = "";
        HttpEntity resEntity = response.getEntity();
        try {
            res = EntityUtils.toString(resEntity);
            Log.i("muliple response", String.valueOf(res));

//            EntityUtils.consume(resEntity);
            if( response.getEntity() != null ) {
                response.getEntity().consumeContent();
            }






        } catch (IOException e) {
            e.printStackTrace();
        }
        return res;
    } // end of upl


    private void showsharePopUPView(final int object_id, final int position, final String type, final String path) {

        popupList=new ArrayList<>();
        popupList.add(new Popupelements(R.drawable.copy_chat,getActivity().getResources().getString(R.string.chat_indivisul)));
        popupList.add(new Popupelements(R.drawable.copy_chat,getActivity().getResources().getString(R.string.chat_by_group)));
        popupList.add(new Popupelements(R.drawable.write_status,getActivity().getResources().getString(R.string.field_actuality)));
        popupList.add(new Popupelements(R.drawable.write_status,getActivity().getResources().getString(R.string.pub)));
        Log.i("popsize",String.valueOf(popupList));
        simpleAdapter = new Popupadapter(getActivity(),false,popupList);

        DialogPlus dialog = DialogPlus.newDialog(getActivity())
                .setAdapter(simpleAdapter)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View v, int i) {


                        if(popupList.size()==4){

                            if(i == 0){


                                dialog.dismiss();

                                FragChat fragment = new FragChat();
                                FragmentManager fragmentManager = getFragmentManager();
                                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                fragmentTransaction.replace(R.id.frame_container, fragment).commit();

                                //sendMsg(audio_path,receiverId,type,"Amis");

                                new UserNotification().setObject(object_id);
                                new UserNotification().setAudio(path);
                                new UserNotification().setType(type);

                                new UserNotification().setFROMMUSIC(true);
                                new UserNotification().setChatSelected(true);
                                new UserNotification().setDISCUSSIONSELECTED(false);



                            }

                            if(i ==1){
                                dialog.dismiss();

                                FragChat fragment = new FragChat();
                                FragmentManager fragmentManager = getFragmentManager();
                                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                                fragmentTransaction.replace(R.id.frame_container, fragment).commit();

                                //sendMsg(audio_path,receiverId,type,"Groupe");

                                new UserNotification().setObject(object_id);
                                new UserNotification().setAudio(path);
                                new UserNotification().setType(type);

                                new UserNotification().setFROMMUSIC(true);
                                new UserNotification().setDISCUSSIONSELECTED(true);
                                new UserNotification().setChatSelected(false);


//                                Toast.makeText(context, data.get(position).getId()+data.get(position).getPath()+type, Toast.LENGTH_SHORT).show();

                            }
                            if(i ==2){
                                dialog.dismiss();


                                shareActuality(object_id,type);


                            }
                            if(i==3)
                            {
                                dialog.dismiss();

                                startActivity(new Intent(getActivity(), NewAds.class));
                                new UserNotification().setFROMMUSIC(true);
                                new UserNotification().setAudio(path);

                                Log.i("selectedPath",path);
                                new UserNotification().setObject(object_id);


                            }

                        }
                        else if(popupList.size()==4){
                            dialog.dismiss();

                        }
                    }
                })
                .setExpanded(false)  // This will enable the expand feature, (similar to android L share dialog)
                .create();
        dialog.show();


    }
    private void showsharePopAlbum(final int object_id, final int position, final String type, final String path) {

        popupList=new ArrayList<>();
        popupList.add(new Popupelements(R.drawable.copy_chat,getActivity().getResources().getString(R.string.chat_indivisul)));
        popupList.add(new Popupelements(R.drawable.copy_chat,getActivity().getResources().getString(R.string.chat_by_group)));
        popupList.add(new Popupelements(R.drawable.write_status,getActivity().getResources().getString(R.string.field_actuality)));
        popupList.add(new Popupelements(R.drawable.write_status,getActivity().getResources().getString(R.string.pub)));
        Log.i("popsize",String.valueOf(popupList));
        simpleAdapter = new Popupadapter(getActivity(),false,popupList);

        DialogPlus dialog = DialogPlus.newDialog(getActivity())
                .setAdapter(simpleAdapter)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View v, int i) {


                        if(popupList.size()==4){

                            if(i == 0){


                                dialog.dismiss();

                                showGroupChatView(object_id,"single");




                            }

                            if(i ==1){
                                dialog.dismiss();

                                showGroupChatView(object_id,"group");



//                                Toast.makeText(context, data.get(position).getId()+data.get(position).getPath()+type, Toast.LENGTH_SHORT).show();

                            }
                            if(i ==2){
                                dialog.dismiss();
                                showGroupChatView(object_id,"shareActuality");




                            }
                            if(i==3)
                            {
                                dialog.dismiss();
                                showGroupChatView(object_id,"pub");




                            }

                        }
                        else if(popupList.size()==4){
                            dialog.dismiss();

                        }
                    }
                })
                .setExpanded(false)  // This will enable the expand feature, (similar to android L share dialog)
                .create();
        dialog.show();


    }


    private void shareActuality(int song_id,String type) {
        JSONObject jsonBody = new JSONObject();

        try {
            jsonBody.put("user_id", Tools.getData(getActivity(), "idprofile"));
            jsonBody.put("object_id", String.valueOf(song_id));
            jsonBody.put("type",type);
            Log.i("user_id", Tools.getData(getActivity(), "idprofile"));
            Log.i("object_id",String.valueOf(song_id));
            Log.i("type",type);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqDelete = new JsonObjectRequest(getActivity().getString(R.string.server_url) + getActivity().getString(R.string.shareAudioToActuality), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {
                                Log.i("succuss",String.valueOf(response));
                                Toast.makeText(getActivity(),"Audio Succussfully Shared", Toast.LENGTH_SHORT).show();

                            }

                        } catch (JSONException e) {

                            //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("lang", Tools.getData(getActivity(), "lang_pr"));
                headers.put("token", Tools.getData(getActivity(), "token"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqDelete);
    }
    public class DoUpload extends AsyncTask<String, Integer, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setTitle("Please Wait");
            mProgressDialog.setCanceledOnTouchOutside(false);
            mProgressDialog.setCancelable(false);
            mProgressDialog.show();

        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            //dialog.dismiss();
            if (!result.equals("")) {
                try {
                    JSONObject response = new JSONObject(result);
                    Log.i("response",result);
                    if (response.getBoolean("result")) {
                        progressValue=0;
                        mProgressDialog.dismiss();

                        JSONArray song_data = response.getJSONArray("songs_files");
                        JSONArray album_data = response.getJSONArray("album_files");

                        List<String> pathArray = new ArrayList<>();
                        for (int i = 0; i < song_data.length(); i++) {
                            try {
                                JSONObject obj = song_data.getJSONObject(i);

                                Log.i("songs",obj.toString());

                                songs.add(new WISMusic(obj.getInt("id"),obj.getString("name"),obj.getString("artists"),obj.getString("duration"),obj.getString("audio_path"),obj.getString("audio_thumbnail"),obj.getString("created_at")));
                                db.addSongs(new WISMusic(obj.getInt("id"),obj.getString("name"),obj.getString("artists"),obj.getString("duration"),obj.getString("audio_path"),obj.getString("audio_thumbnail"),obj.getString("created_at")));
                                Log.i("files",obj.getString("audio_path"));
                                pathArray.add(getString(R.string.server_url3)+obj.getString("audio_path"));
                                saveAudioToLocalPath(obj.getString("audio_path"));
                            } catch (Exception e) {
                            }
                        }

                        Log.i("pathArray", String.valueOf(pathArray.size()));

                        if(song_data.length()>0){

                            loadAlbums = Boolean.FALSE;

                        }else if(album_data.length()>0){

                            loadAlbums = Boolean.TRUE;
                        }

                        fetchSongsFromLocalDb();

                        Toast.makeText(getActivity(),"audio song is uploaded", Toast.LENGTH_LONG).show();

//                        loadMusic();

                        //refereshMusicData();
                        //
                    }
                    else {
                        Toast.makeText(getActivity(),"audio extension not supported", Toast.LENGTH_LONG).show();
                        mProgressDialog.dismiss();
                    }
                } catch (JSONException e) {
                }

            }
            else {
                Toast.makeText(getActivity(),"audio extension not supported", Toast.LENGTH_LONG).show();
                mProgressDialog.dismiss();
            }
        }

        @Override
        protected String doInBackground(String... urls) {
            try {

                Log.e("calling","uploadMusic");
                Log.i("filePath",urls[0]);
                Log.i("icons",urls[1]);
                Log.i("songName",urls[2]);
                Log.i("artist",urls[3]);
                Log.i("songDuration",urls[4]);
                Log.i("type",urls[5]);

                CurrentAudioPath.clear();
                CurrentAudioPath.add(urls[0]);




                return uploadMusic(urls[0],urls[1],urls[2],urls[3],urls[4],urls[5],getActivity().getString(R.string.server_url) + getActivity().getString(R.string.upload_music_meth), Tools.getData(getActivity(), "idprofile"), Tools.getData(getActivity(), "token"));

            } catch (Exception e) {
                return "";
            }
        }
    }
    private void showGroupChatView(final int objectid, final String share){

        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.share_songs, null);


        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setCancelable(false).setView(dialogView);

        final AlertDialog alert = builder.create();
        alert.show();
        close = (ImageButton)alert.findViewById(R.id.closeGroupchat);
        shareSongsList = (ListView)alert.findViewById(R.id.groupfriendList);
        shareAlbumlist=db.getAlbumSongs(objectid);
        shareSongsList.setAdapter(new ShareAlbumAdapter(getActivity(), shareAlbumlist, new ShareAlbumListener() {
            @Override
            public void showlist(int id, int position, String type, String path) {
                alert.dismiss();
                if(share.equals("single"))
                {
                    FragChat fragment = new FragChat();
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.frame_container, fragment).commit();

                    new UserNotification().setObject(id);
                    new UserNotification().setAudio(path);
                    new UserNotification().setType(type);

                    new UserNotification().setFROMMUSIC(true);
                    new UserNotification().setChatSelected(true);
                    new UserNotification().setDISCUSSIONSELECTED(false);
                }
                else if(share.equals("group"))
                {
                    FragChat fragment = new FragChat();
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.frame_container, fragment).commit();

                    new UserNotification().setObject(id);
                    new UserNotification().setAudio(path);
                    new UserNotification().setType(type);

                    new UserNotification().setFROMMUSIC(true);
                    new UserNotification().setDISCUSSIONSELECTED(true);
                    new UserNotification().setChatSelected(false);
                }
                else if(share.equals("shareActuality"))
                {
                    shareActuality(id,type);
                }
                else {
                    startActivity(new Intent(getActivity(), NewAds.class));
                    new UserNotification().setFROMMUSIC(true);
                    new UserNotification().setAudio(path);

                    Log.i("selectedPath",path);
                    new UserNotification().setObject(id);
                }
                //showsharePopUPView(id,position,type,UserNotification.getMusicChatMessage());
            }
        }));
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });



    }
    @Override
    public void onResume() {
        super.onResume();

        if(UserNotification.getIsAlbumUploaded()){
            UserNotification.setIsAlbumUploaded(Boolean.FALSE);
            Log.i("OnResume","calling");
            album.clear();
            songs.clear();
            allAlbums.clear();
            allSongs.clear();
            db.deleteSongs();
            db.deleteAlumbs();
            loadMusic();
        }

//        if(mediaPlayer.isPlaying())
//        {
//            mediaPlayer.stop();
//            mediaPlayer.release();
//        }

    }

}
