package com.oec.wis.chatApi;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.animation.TranslateAnimation;

import com.google.android.gms.cast.CastRemoteDisplayLocalService;
import com.google.android.gms.drive.events.ChangeListener;
import com.nostra13.universalimageloader.utils.L;
import com.oec.wis.Dashboard;
import com.oec.wis.R;
import com.oec.wis.SplashSreen;
import com.oec.wis.entities.WISCmt;
import com.oec.wis.fragments.FragHome;
import com.oec.wis.tools.Tools;

import com.oec.wis.wisdirect.WisDirect;
import com.pubnub.api.PNConfiguration;
import com.pubnub.api.PubNub;
import com.pubnub.api.PubNubError;
import com.pubnub.api.callbacks.PNCallback;
import com.pubnub.api.callbacks.SubscribeCallback;
import com.pubnub.api.enums.PNHeartbeatNotificationOptions;
import com.pubnub.api.enums.PNLogVerbosity;
import com.pubnub.api.enums.PNStatusCategory;
import com.pubnub.api.models.consumer.PNPublishResult;
import com.pubnub.api.models.consumer.PNStatus;
import com.pubnub.api.models.consumer.channel_group.PNChannelGroupsAddChannelResult;
import com.pubnub.api.models.consumer.presence.PNSetStateResult;
import com.pubnub.api.models.consumer.pubsub.PNMessageResult;
import com.pubnub.api.models.consumer.pubsub.PNPresenceEventResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.pubnub.api.callbacks.*;

/**
 * Created by asareri08 on 14/09/16.
 */
public class ChatApi {
    ChatListener listener;
    CallListener callListener;


    public JSONObject getDirectResponse() {
        return directResponse;
    }

    public void setDirectResponse(JSONObject directResponse) {
        this.directResponse = directResponse;
    }


    JSONObject directResponse;



//    PublisherListener publisherListener;

    Context publisherContext;

    ChatTextListener chatTextListener;

    public String OPENTOKAPIKEY;

    public String getOPENTOKAPIKEY() {
        return OPENTOKAPIKEY;
    }

    public void setOPENTOKAPIKEY(String OPENTOKAPIKEY) {
        this.OPENTOKAPIKEY = OPENTOKAPIKEY;
    }

    public String OPENTOKSESSIONID;

    public String getOPENTOKSESSIONID() {
        return OPENTOKSESSIONID;
    }

    public void setOPENTOKSESSIONID(String OPENTOKSESSIONID) {
        this.OPENTOKSESSIONID = OPENTOKSESSIONID;
    }

    public String getOPENTOKTOKEN() {
        return OPENTOKTOKEN;
    }

    public void setOPENTOKTOKEN(String OPENTOKTOKEN) {
        this.OPENTOKTOKEN = OPENTOKTOKEN;
    }

    public String OPENTOKTOKEN;

    public ChatTextListener getChatTextListener() {
        return chatTextListener;
    }

    public void setChatTextListener(ChatTextListener chatTextListener) {
        this.chatTextListener = chatTextListener;
    }

    public Context getPublisherContext() {
        return publisherContext;
    }

    public void setPublisherContext(Context publisherContext) {
        this.publisherContext = publisherContext;
    }

    public CallListener getCallListener() {
        return callListener;
    }

    public void setCallListener(CallListener callListener) {

        System.out.print("******************");
        if(callListener!=null){

            Log.d("CallerListener Called", String.valueOf(callListener));
        }

        else{

            Log.d("CallerListener Called", "IS NULL");
        }

        this.callListener = callListener;
    }

//    public PublisherListener getPublisherListener() {
//        return publisherListener;
//    }
//
//    public void setPublisherListener(PublisherListener publisherListener) {
//        this.publisherListener = publisherListener;
//    }

    private static ChatApi ourInstance;

    public ArrayList<String>rejectedMemberArry;
    //
    private static final String PUBLISH_KEY = "pub-c-7049380a-6618-4f7c-990b-d9e776b5d24f";

    private static final String SUBSCRIBER_KEY = "sub-c-e4f6a77c-7688-11e6-8d11-02ee2ddab7fe";
    MediaPlayer mediaPlayer;

    public static boolean isStreamingconnected;

    public static boolean nowReceiverAvailabel;

    public static boolean isNowReceiverAvailabel() {
        return nowReceiverAvailabel;
    }

    public static void setNowReceiverAvailabel(boolean nowReceiverAvailabel) {
        ChatApi.nowReceiverAvailabel = nowReceiverAvailabel;
    }

    public static boolean isStreamingconnected() {
        return isStreamingconnected;
    }

    public static void setIsStreamingconnected(boolean isStreamingconnected) {
        ChatApi.isStreamingconnected = isStreamingconnected;
    }

    ArrayList<String> privatechannel;

    List<String> publicChannel;

    public HashMap<String, ArrayList<String>> getGroupMembersDict() {
        return groupMembersDict;
    }

    public void setGroupMembersDict(HashMap<String, ArrayList<String>> groupMembersDict) {
        this.groupMembersDict = groupMembersDict;
    }



    ArrayList<List> publicChannelMember;

    List<String> channels;

    HashMap<String,ArrayList<String>>groupMembersDict=new HashMap<>();

    HashMap<String,ArrayList<String>>groupMembersDetails=new HashMap<>();

    public HashMap<String, ArrayList<String>> getGroupMembersDetails() {
        return groupMembersDetails;
    }


    public ArrayList<String>getGroupMembersBychannel(String key){

        return getGroupMembersDetails().get(key);
    }



    public void setGroupMembersDetails(String keys,JSONArray jsonObject) {

        ArrayList<String> listdata = new ArrayList<String>();
        JSONArray jArray = (JSONArray)jsonObject;
        if (jArray != null) {
            listdata.add(String.valueOf(jArray));

        }
        this.groupMembersDetails.put(keys,listdata);
    }


    public ArrayList<String>convertJsonToArray(JSONArray jsonObject){

        ArrayList<String> listdata = new ArrayList<String>();
        JSONArray jArray = (JSONArray)jsonObject;
        if (jArray != null) {
            listdata.add(String.valueOf(jArray));

        }

        return listdata;

    }


    PNConfiguration pnConfiguration;

    PubNub pubnub;

    Boolean isChatView;

    Context context;

    Boolean userIsAvailable = Boolean.TRUE;

    List<WISCmt> data;


    public Boolean getUserIsAvailable() {
        return userIsAvailable;
    }

    public void setUserIsAvailable(Boolean userIsAvailable) {
        this.userIsAvailable = userIsAvailable;
    }



    private static String userCurrentChannel;

    public static String getUserCurrentChannel() {
        return userCurrentChannel;
    }

    public static void setUserCurrentChannel(String userCurrentChannel) {
        ChatApi.userCurrentChannel = userCurrentChannel;
    }

    public static ChatApi getInstance() {
        if (ourInstance == null) {

            ourInstance = new ChatApi();


        }
        return ourInstance;
    }

    private ChatApi() {


        privatechannel = new ArrayList<>();

        publicChannel = new ArrayList<>();

        publicChannelMember = new ArrayList<>();

        isChatView = Boolean.FALSE;

        isStreamingconnected = Boolean.FALSE;

        rejectedMemberArry=new ArrayList<>();

        setUserIsAvailable(Boolean.TRUE);

        setUpChatConfig();
        addListener();



    }

    public void setListener(ChatListener listener) {

        this.listener = listener;


    }

    public void setListenerMain(ChatListener listener) {

        this.listener = listener;
    }


//    public void setListenerMain(CallListener callListener) {
//
//        this.callListener = callListener;
//    }


    public void setUpChatConfig() {

        PNConfiguration pnConfiguration = new PNConfiguration();

        pnConfiguration.setSubscribeKey(SUBSCRIBER_KEY);
        pnConfiguration.setPublishKey(PUBLISH_KEY);

        pnConfiguration.setLogVerbosity(PNLogVerbosity.BODY);


        String uniqueID = UUID.randomUUID().toString();
        pnConfiguration.setUuid(uniqueID);

        pubnub = new PubNub(pnConfiguration);

        pubnub.reconnect();


    }


    public void addListener() {

        pubnub.addListener(new SubscribeCallback() {
            @Override
            public void status(PubNub pubnub, PNStatus status) {

                Log.e("PUNUB", status.toString());


                if (status.getCategory() == PNStatusCategory.PNUnexpectedDisconnectCategory) {
                    // This event happens when radio / connectivity is lost

                    pubnub.reconnect();

                    Log.e("PUNUB", String.valueOf(status.getErrorData()));
                } else if (status.getCategory() == PNStatusCategory.PNConnectedCategory) {

                    // Connect event. You can do stuff like publish, and know you'll get it.
                    // Or just use the connected event to confirm you are subscribed for
                    // UI / internal notifications, etc

                    if (status.getCategory() == PNStatusCategory.PNConnectedCategory) {

                        Log.e("PUNUB CONECT", String.valueOf(pubnub.getConfiguration()));

//                        pubnub.


                    }
                } else if (status.getCategory() == PNStatusCategory.PNReconnectedCategory) {

                    // Happens as part of our regular operation. This event happens when
                    // radio / connectivity is lost, then regained.

                    Log.e("connectivity is lost", "PNReconnectedCategory");

                    pubnub.reconnect();
                } else if (status.getCategory() == PNStatusCategory.PNDecryptionErrorCategory) {

                    // Handle messsage decryption error. Probably client configured to
                    // encrypt messages and on live data feed it received plain text.
                }
            }

            @Override
            public void message(PubNub pubnub, PNMessageResult message) {
                // Handle new message stored in message.data.message
                if (message.getActualChannel() != null) {
                    // Message has been received on channel group stored in
                    // message.getActualChannel()
                } else {
                    // Message has been received on channel stored in
                    // message.getSubscribedChannel()
                }

            /*
                log the following items with your favorite logger
                    - message.getMessage()
                    - message.getSubscribedChannel()
                    - message.getTimetoken()
            */



                Log.i("received message", String.valueOf(message.getMessage()));








                try {
                    final JSONObject jsonData = new JSONObject(message.getMessage().toString());




                    Log.e("JsonDataWis", String.valueOf(jsonData));

                    try {


//                        if(Tools.getData(context, "idprofile").equals("")){
//
                        Log.d("User Status", "inside try catch");


                        if (!TextUtils.isEmpty(Tools.getData(getContext(), "token"))){

                            if (jsonData.has("opentok_token") && jsonData.has("opentok_session_id") && jsonData.has("opentok_apikey")) {


                                System.out.print("YOU ARE HERE @@ 2 ***");

                                Log.e("IOS CALLINg0", "HERE");


//
                                if (jsonData.has("Name")) {


                                    if (jsonData.getString("Name").equals("WISDirectInvite")) {


                                        if (ChatApi.getInstance().getUserIsAvailable()) {

//

                                            String directid = jsonData.getString("direct_id");
//

                                            String senderIcon = jsonData.getString("senderImage");
                                            String senderName = jsonData.getString("senderName");

                                            String title = jsonData.getString("title");
                                            String senderId = jsonData.getString("senderID");

//

                                            String sessionId = jsonData.getString("opentok_session_id");
                                            String openTok_token = jsonData.getString("opentok_token");

                                            String apiKey = jsonData.getString("opentok_apikey");

//

                                            String direct = null;
                                            if (jsonData.has("id")) {

                                                direct = jsonData.getString("id");
                                            }


                                            setDirectResponse(jsonData);

                                            Tools.wisNotify(getPublisherContext(), sessionId, openTok_token, directid, apiKey, title, senderName, senderId, direct);


                                        }
                                    } else {

                                        Log.e("USER NOT AVAILABLe", "USER STATUS");
                                    }


                                    if (jsonData.has("isVideoCalling")) {

                                        if (jsonData.getString("isVideoCalling").equals("yes")) {


                                            System.out.print("YOU ARE HERE isVideoCalling ++++=>***");


                                            listener.didReceiveMessage(message);
//                                    2_MX40NTc3MjA5Mn5-MTQ4NzY3NTg0MzI0N35UQzFxR2tHRTlIVEZyZHdTdGJPdWxHNGd-QX4

                                        }


                                    }
                                }


                            } else {

                                chatTextListener.didReceiveTextMessage(message);

                            }

                    }
                    else{

                        Log.e("USER NOT LOGGED IN","USER AUTH");

                    }








                    } catch (NullPointerException excep) {
                        Log.e("exce", excep.getLocalizedMessage());
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void presence(PubNub pubnub, PNPresenceEventResult presence) {

                Log.i("presence", presence.toString());

                Log.i("presence channel", presence.getChannel());

//                presence.getUserMetadata();

                listener.didReceivePresenceEvent(presence);
//                callListener.didReceivePresenceEvent(presence);

            }
        });


    }


    public void subscribe() {

        channels = new ArrayList<>();

        channels.addAll(privatechannel);

        channels.addAll(publicChannel);

        Log.i("globale channels", String.valueOf(channels));

        pubnub.unsubscribeAll();

        pubnub.subscribe().channels(channels).execute();


    }

    public void unSubscribeChannels(){

        pubnub.unsubscribeAll();
    }


    public void addPrivateChannel(List<String> channel) {

        for (int i = 0; i < channel.size(); i++) {

            privatechannel.add(channel.get(i));

        }


        System.out.println("private:::::");
        Log.i("private_mem::", String.valueOf(privatechannel));


    }


    public void addPublicChannels(ArrayList channelMebers, ArrayList channelGroup) {


        for (int i = 0; i < channelGroup.size(); i++) {

            Log.i("channel meb", String.valueOf(channelMebers.get(i)));


            groupMembersDict.put(channelGroup.get(i).toString(), (ArrayList<String>) channelMebers.get(i));




            pubnub.addChannelsToChannelGroup()
                    .channelGroup(String.valueOf(channelGroup.get(i)))
                    .channels((List<String>) channelMebers.get(i))
                    .async(new PNCallback<PNChannelGroupsAddChannelResult>() {
                        @Override
                        public void onResponse(PNChannelGroupsAddChannelResult result, PNStatus status) {

                            if (!status.isError()) {

                                Log.i("channel_group_res", String.valueOf(status));


                            } else {

                                status.retry();
                            }

                        }
                    });


        }

        publicChannel.addAll(channelGroup);

        setGroupMembersDict(groupMembersDict);


        System.out.println("public:::::");
        Log.i("public_mem::", String.valueOf(publicChannel));


        Log.i("groupMembersDict::", String.valueOf(groupMembersDict));


    }

    public void sendMessageForGroupVideo(Map<String, String> messageData, String channel, final CallListener calllistener) {

        Log.d("sendMessage GroupVideo", String.valueOf(messageData));




        pubnub.publish()
                .message(messageData)
                .channel(channel)
                .shouldStore(true)
                .usePOST(true)
                .async(new PNCallback<PNPublishResult>() {
                    @Override
                    public void onResponse(PNPublishResult result, PNStatus status) {
                        // handle publish result, status always present, result if successful
                        // status.isError to see if error happened


                        System.out.println("status" +status);

                        if (!status.isError()) {
                            try{
                                listener.onPublishSuccess(status);

                                setCallListener(calllistener);



                            }catch (NullPointerException ex){
                                System.out.println("Null Exception==>" +ex.getMessage());
                            }


                        } else {

                            status.retry();

                        }

                        Log.d("message status", String.valueOf(status));

                        Log.e("message status", String.valueOf(status));


                    }
                });


        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                //TODO your background code


            }
        });


    }


    public void sendMessage(Map<String, String> messageData, String channel) {

        Log.d("sende message called", String.valueOf(messageData));




        pubnub.publish()
                .message(messageData)
                .channel(channel)
                .shouldStore(true)
                .usePOST(true)
                .async(new PNCallback<PNPublishResult>() {
                    @Override
                    public void onResponse(PNPublishResult result, PNStatus status) {
                        // handle publish result, status always present, result if successful
                        // status.isError to see if error happened


                        System.out.println("status" +status);

                        if (!status.isError()) {
                            try{
                                listener.onPublishSuccess(status);

                                if(chatTextListener!=null){

                                    chatTextListener.onTextPublishSuccess(status);
                                }



//                                callListener.onPublishSuccess(status);

                            }catch (NullPointerException ex){
                                System.out.println("Null Exception==>" +ex.getMessage());
                            }


                        } else {

                            status.retry();

                        }

                        Log.d("message status", String.valueOf(status));

                        Log.e("message status", String.valueOf(status));


                    }
                });


        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                //TODO your background code


            }
        });


    }

    public void removePrivatechannels() {


        privatechannel.clear();
    }

    public void removePublicchannels() {


        publicChannel.clear();
    }


    public void setIsChatView(Boolean isChatView) {

        this.isChatView = isChatView;


    }


    public Boolean getIsChatView() {

        return this.isChatView;
    }

    public void setUpRingTone(Context context) {

        AudioManager audioManager = (AudioManager)context.getSystemService(Context.AUDIO_SERVICE);
//        RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE))

//        String mp3File = "raw/default_ringtone.mp3";
//        AssetManager assetMan = getAssets();
//        FileInputStream mp3Stream = assetMan.openFd(mp3File).createInputStream();

//        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//        Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
//        r.play();

        int resID=context.getResources().getIdentifier("tes", "raw", context.getPackageName());
        mediaPlayer = MediaPlayer.create(context.getApplicationContext(),resID);

        try {

            mediaPlayer.setVolume((float) (audioManager.getStreamVolume(AudioManager.STREAM_NOTIFICATION) / 7.0), (float) (audioManager.getStreamVolume(AudioManager.STREAM_NOTIFICATION) / 7.0));
            mediaPlayer.setLooping(true);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    public void playRingTone() {
        Log.e("calling", "called");

        if (!mediaPlayer.isPlaying()) {
            mediaPlayer.start();
        }
    }





    public  void stopRingTone() {
        Log.e("calling", "called");

        try{
            if (mediaPlayer.isPlaying() && (mediaPlayer != null)) {
                mediaPlayer.stop();
            }
        }catch (NullPointerException ex){
            System.out.println("Null exception==>" +ex.getMessage());
        }



    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }


    public void addbusyModeUser(String id){

        this.rejectedMemberArry.add(id);
    }

    public void claerBusyModeUsers(){

        this.rejectedMemberArry.clear();
    }

    public int  getBusyModeUsersLength(){

        return this.rejectedMemberArry.size();
    }

    public void addComments(List<WISCmt> commentlist)
    {
        this.data = commentlist;
    }

}
