package com.oec.wis.chatApi;

import com.pubnub.api.models.consumer.PNStatus;
import com.pubnub.api.models.consumer.pubsub.PNMessageResult;
import com.pubnub.api.models.consumer.pubsub.PNPresenceEventResult;

/**
 * Created by asareri08 on 14/09/16.
 */
public interface ChatListener {


    public void didReceiveMessage(PNMessageResult messageResult);

    public void onPublishSuccess(PNStatus status);

    public void didReceivePresenceEvent(PNPresenceEventResult eventResult);
}
