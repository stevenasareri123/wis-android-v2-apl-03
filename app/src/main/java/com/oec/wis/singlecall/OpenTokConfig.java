package com.oec.wis.singlecall;

public class OpenTokConfig {

    // *** Fill the following variables using your own Project info from the OpenTok dashboard  ***
    // ***                      https://dashboard.tokbox.com/projects                           ***
    // Replace with a generated Session ID
    public static final String SESSION_ID = "2_MX40NTc3MjA5Mn5-MTQ4NzIzNDU2NDI5OH5QZUpnU0FERzh4ZE5kWVA5TnRhNDlqZ0J-fg";
    // Replace with a generated token (from the dashboard or using an OpenTok server SDK)
    public static final String TOKEN ="T1==cGFydG5lcl9pZD00NTc3MjA5MiZzaWc9YjZlYTc0MjFlNzA3MWFkNmY3Mjc2ZjE3MDA2MzU4ZjU3NGU4MTM0MTpzZXNzaW9uX2lkPTJfTVg0ME5UYzNNakE1TW41LU1UUTROekl6TkRVMk5ESTVPSDVRWlVwblUwRkVSemg0WkU1a1dWQTVUblJoTkRscVowSi1mZyZjcmVhdGVfdGltZT0xNDg3MjM0NTk1Jm5vbmNlPTAuOTg1NjE4NTI3NDUyMDA4NCZyb2xlPXB1Ymxpc2hlciZleHBpcmVfdGltZT0xNDg5ODI2NTkz";
    // Replace with your OpenTok API key
    public static final String API_KEY = "45772092";

    // Subscribe to a stream published by this client. Set to false to subscribe
    // to other clients' streams only.
    public static final boolean SUBSCRIBE_TO_SELF = false;

    // For internal use only. Please do not modify or remove this code.
    public static final String LOG_CLIENT_VERSION = "android-vsol-1.1.0";
    public static final String LOG_COMPONENTID = "oneToOneSample";
    public static final String LOG_ACTION_INITIALIZE = "Init";
    public static final String LOG_ACTION_START_COMM = "Start";
    public static final String LOG_ACTION_END_COMM = "End";

    public static final String LOG_VARIATION_ATTEMPT = "Attempt";
    public static final String LOG_VARIATION_ERROR = "Failure";
    public static final String LOG_VARIATION_SUCCESS = "Success";

}