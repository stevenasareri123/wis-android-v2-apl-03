package com.oec.wis.singlecall;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.oec.wis.R;
import com.oec.wis.tools.Tools;
import com.squareup.picasso.Picasso;

import java.util.Calendar;


public class PreviewCameraFragment extends Fragment {

    private static final String LOGTAG = PreviewCameraFragment.class.getSimpleName();
    private SingleCallMainActivity mActivity;

    private RelativeLayout mContainer;
    private View mRootView;
    private ImageButton mCameraBtn;
    private LinearLayout previewView;
    private RelativeLayout phonePreviewView;
    String senderId,OwnerId,receiverFirstName,currentChannel,receiverAmisId,receiverImage;
    TextView registerUserTV,registerUserCountryTv,otherUserNameTv,otherUserCountryTV,dateTV;
    CircularImageView ivRegisterProfile,ivReceiverProfile;
    ImageView greenArrowIV,redArrowIV;
    Typeface font;

    Boolean is_group,isConnectedToStream=Boolean.FALSE;

    int fId;

    private PreviewCameraCallbacks mCameraCallbacks = cameraCallbacks;

    public interface PreviewCameraCallbacks {
        public void onCameraSwap();
    }

    private static PreviewCameraCallbacks cameraCallbacks = new PreviewCameraCallbacks() {
        @Override
        public void onCameraSwap() {}

    };

    private View.OnClickListener mBtnClickListener = new View.OnClickListener() {
        public void onClick(View v) {

            cameraSwap();
        }
    };

    @Override
    public void onAttach(Context context) {
        Log.i(LOGTAG, "OnAttach PreviewCameraFragment");

        super.onAttach(context);

        this.mActivity = (SingleCallMainActivity) context;
        this.mCameraCallbacks = (PreviewCameraCallbacks) context;
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        Log.i(LOGTAG, "OnAttached PreviewCameraFragment");



        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {

            this.mActivity = (SingleCallMainActivity) activity;
            this.mCameraCallbacks = (PreviewCameraCallbacks) activity;
        }

    }

    @Override
    public void onDetach() {
        Log.i(LOGTAG, "OnDetach PreviewCameraFragment");

        super.onDetach();

        mCameraCallbacks = cameraCallbacks;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.i(LOGTAG, "onCreate PreviewCameraFragment");

        mRootView = inflater.inflate(R.layout.preview_camera_fragment, container, false);

        mContainer = (RelativeLayout) this.mActivity.findViewById(R.id.camera_preview_fragment_container);
        mCameraBtn = (ImageButton) mRootView.findViewById(R.id.camera);

        previewView =  (LinearLayout)mRootView.findViewById(R.id.preview);
        phonePreviewView=(RelativeLayout)mRootView.findViewById(R.id.phonePreview);
        registerUserTV=(TextView)mRootView.findViewById(R.id.registerUserTV);
        registerUserCountryTv=(TextView)mRootView.findViewById(R.id.registerUserCountryTv);
        otherUserNameTv=(TextView)mRootView.findViewById(R.id.otherUserNameTv);

        otherUserCountryTV=(TextView)mRootView.findViewById(R.id.otherUserCountryTV);
        ivRegisterProfile=(CircularImageView)mRootView.findViewById(R.id.ivRegisterProfile);
        ivReceiverProfile=(CircularImageView)mRootView.findViewById(R.id.ivReceiverProfile);
        dateTV=(TextView)mRootView.findViewById(R.id.dateTV);

        registerUserTV.setText(Tools.getData(getActivity(), "name"));

        registerUserCountryTv.setText(Tools.getData(getActivity(), "country"));
        greenArrowIV=(ImageView)mRootView.findViewById(R.id.greenarrowIV);
        redArrowIV=(ImageView)mRootView.findViewById(R.id.redarrowIV);


        font=Typeface.createFromAsset(getActivity().getAssets(),"fonts/Harmattan-R.ttf");
        registerUserTV.setTypeface(font);
        registerUserCountryTv.setTypeface(font);
        otherUserNameTv.setTypeface(font);
        otherUserCountryTV.setTypeface(font);
        dateTV.setTypeface(font);
//        downloadProfileImageReceiver();
        parseValues();

        Calendar c = Calendar.getInstance();

        int seconds = c.get(Calendar.SECOND);
        int minutes = c.get(Calendar.MINUTE);
        int hour = c.get(Calendar.HOUR);
        String time = hour + "h"+ ":" + minutes +"m" + ":" + seconds +"s";

        Log.i("time", time);

        int day = c.get(Calendar.DAY_OF_MONTH);
        int month = c.get(Calendar.MONTH);
//         months 0 to 11
        int year = c.get(Calendar.YEAR);
        String date = day + "/" + month + "/" + year;
        Log.i("date", date);


//        dateTV.setText(date + " " + time);

        dateTV.setText(time);

        otherUserNameTv.setText(receiverFirstName);
        otherUserCountryTV.setText("India");


        mCameraBtn.setOnClickListener(mBtnClickListener);

        return mRootView;
    }

    public void cameraSwap() {


        Log.e("ON cameraSwap","CLICKEE");

        mCameraCallbacks.onCameraSwap();
    }

    public void disableCustomPreview(){

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {

//stuff that updates ui

                mRootView.setBackgroundColor(Color.parseColor("#00ffffff"));

                previewView.setVisibility(View.INVISIBLE);
                phonePreviewView.setVisibility(View.INVISIBLE);

            }
        });



    }


    public void downloadRegistermage() {

        Picasso.with(getActivity())
                .load(this.getString(R.string.server_url2) + Tools.getData(getActivity(), "photo"))
                .placeholder(R.drawable.empty)
                .error(R.drawable.empty)
                .into(ivRegisterProfile);


    }




    private void downloadProfileImageReceiver(String image) {


        Picasso.with(getActivity())
                .load(this.getString(R.string.server_url2) + image)
                .placeholder(R.drawable.empty)
                .error(R.drawable.empty)
                .into(ivReceiverProfile);


        //        img_user1450170726_566fd966017c1.jpg

//        img_user1477998338.jpg


//


    }
    public void parseValues(){

        OwnerId=Tools.getData(getActivity(), "idprofile");

        fId =  getArguments().getInt("id");
        receiverFirstName =  getArguments().getString("receiver_name");
//        receiverImage = getArguments().getString("image");



//        Log.e("receiver Image" ,receiverImage);

        currentChannel =  getArguments().getString("actual_channel");
        is_group = getArguments().getBoolean("is_group");

        if(getArguments().getString("senderID",null)!=null){

            senderId=getArguments().getString("senderID");
            System.out.println("senderId  if "  + senderId);

        }else{

            senderId = Tools.getData(getActivity(), "idprofile");
            System.out.println("senderId  else "  + senderId);

            downloadRegistermage();

//            if(getArguments().getString("senderImage")!=null){
//
//                downloadProfileImageReceiver(getArguments().getString("senderImage"));
//            }



        }

        if(getArguments().getString("ReceiverImage",null)!=null){

            downloadProfileImageReceiver(getArguments().getString("ReceiverImage"));
        }
    }


}
