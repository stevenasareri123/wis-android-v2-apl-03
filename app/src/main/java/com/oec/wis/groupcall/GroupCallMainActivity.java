package com.oec.wis.groupcall;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.DateUtils;
import android.text.format.Time;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.oec.wis.ApplicationController;
import com.oec.wis.R;
import com.oec.wis.chatApi.CallListener;
import com.oec.wis.chatApi.ChatApi;
import com.oec.wis.dialogs.Chat;
import com.oec.wis.entities.WISChat;
import com.oec.wis.entities.WISUser;
import com.oec.wis.singlecall.OpenTokConfig;
import com.oec.wis.tools.Tools;
import com.opentok.android.BaseVideoRenderer;
import com.opentok.android.Connection;
import com.opentok.android.OpentokError;
import com.opentok.android.Publisher;
import com.opentok.android.PublisherKit;
import com.opentok.android.Session;
import com.opentok.android.Stream;
import com.opentok.android.Subscriber;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

public class GroupCallMainActivity extends AppCompatActivity
        implements EasyPermissions.PermissionCallbacks,
        Publisher.PublisherListener,
        Session.SessionListener,GroupCallPreviewFragment.OnFragmentInteractionListener,CallListener,Session.SignalListener {



    private static final String TAG = "simple-multiparty " + GroupCallMainActivity.class.getSimpleName();

    private final int MAX_NUM_SUBSCRIBERS = 4;

    private static final int RC_SETTINGS_SCREEN_PERM = 123;
    private static final int RC_VIDEO_APP_PERM = 124;

    private Session mSession;
    private Publisher mPublisher;

    private ArrayList<Subscriber> mSubscribers = new ArrayList<Subscriber>();
    private HashMap<Stream, Subscriber> mSubscriberStreams = new HashMap<Stream, Subscriber>();

    private RelativeLayout mPublisherViewContainer;

    private GroupCallPreviewFragment mPreviewFragment;
    private FragmentTransaction mFragmentTransaction;

    private ImageView callBtn;
    private ImageView rejectBtn;

    public boolean makeRejectCall=false;

    LinearLayout videoFrameView;

    Chronometer myChronometer;

    Boolean SessionStarted=Boolean.FALSE;

    ProgressDialog pd;






    Boolean hasPermission=Boolean.FALSE,hasAdmin = Boolean.FALSE,btnClicked = Boolean.FALSE;

    ArrayList<String> groupMemberArray;


    String senderId,OwnerId,receiverFirstName,currentChannel,receiverAmisId,group_mebers;

    int fId;

    Bundle b;

    RelativeLayout groupcall_preview_fragment_container;


    Boolean isStreamRecieved = Boolean.FALSE;

    String memberDetails;

    JSONArray memberArray;

    String [] stringArray;


    TextView  headerTitleTV;

    ImageView backBtn;


    public void setupTimer(){

        final long timeBeforeThreadStart = System.currentTimeMillis();

        final boolean isInputEventOccurred = false;
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub
                while ((System.currentTimeMillis() - timeBeforeThreadStart) < (30 * 1000)) {
                    // you add sleep here if you want instead of loop

                    Log.d("TIMER INIT","TIMER START");
                }
                if (!isInputEventOccurred) {
                    // write code to close app

                    Log.d("TIMER STOPED","TIMER STOPED");

//                    disconnectSession();

                }

            }
        });
        t.start();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group_call_main);

        ChatApi.getInstance().setPublisherContext(this);
        ChatApi.getInstance().setUserIsAvailable(Boolean.FALSE);
        ApplicationController.getInstance().isInputEventOccurred = false;


        headerTitleTV =(TextView)findViewById(R.id.headerTitleTV);
        backBtn = (ImageView)findViewById(R.id.close);

        Typeface font= Typeface.createFromAsset(getAssets(),"fonts/Harmattan-R.ttf");

        headerTitleTV.setTypeface(font);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showAlert();
            }
        });


//        ApplicationController.getInstance().checkPermissionFor();


        parseValues();

        initView();

        if(savedInstanceState==null){


            Log.d("savedInstanceState","Called here");

            mFragmentTransaction = getSupportFragmentManager().beginTransaction();
            initViewPreviewfragemnts();
            mFragmentTransaction.commitAllowingStateLoss();


        }



        mPublisherViewContainer = (RelativeLayout) findViewById(R.id.publisherview);

        final ImageView swapCamera = (ImageView) findViewById(R.id.swapCamera);
        swapCamera.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (mPublisher == null) {
                    return;
                }
                mPublisher.swapCamera();
            }
        });


        final ImageView audioBtn = (ImageView)findViewById(R.id.toggleAudio);

        audioBtn.setSelected(true);

        audioBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mPublisher == null) {
                    return;
                }

                if(audioBtn.isSelected()){

                    audioBtn.setSelected(false);

                    audioBtn.setImageResource(R.drawable.no_microphone_thumb_thumb);

                    mPublisher.setPublishAudio(false);

                }

                else{

                    audioBtn.setSelected(true);

                    audioBtn.setImageResource(R.drawable.mic_icon);

                    mPublisher.setPublishAudio(true);


                }
            }
        });

        final ImageView videoBtn = (ImageView)findViewById(R.id.toggleVideo);

        videoBtn.setSelected(true);

        videoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mPublisher == null) {
                    return;
                }

                if(videoBtn.isSelected()){

                    videoBtn.setSelected(false);

                    videoBtn.setImageResource(R.drawable.no_video_icon);

                    mPublisher.setPublishVideo(false);
                }

                else{

                    videoBtn.setSelected(true);

                    videoBtn.setImageResource(R.drawable.video_icon);

                    mPublisher.setPublishVideo(true);
                }
            }
        });

//        final ToggleButton toggleAudio = (ToggleButton) findViewById(R.id.toggleAudio);
//        toggleAudio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if (mPublisher == null) {
//                    return;
//                }
//                if (isChecked) {
//                    mPublisher.setPublishAudio(true);
//                } else {
//                    mPublisher.setPublishAudio(false);
//                }
//            }
//        });

//        final ToggleButton toggleVideo = (ToggleButton) findViewById(R.id.toggleVideo);
//        toggleVideo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if (mPublisher == null) {
//                    return;
//                }
//                if (isChecked) {
//                    mPublisher.setPublishVideo(true);
//                } else {
//                    mPublisher.setPublishVideo(false);
//                }
//            }
//        });

        requestPermissions();

    }

    public void initView(){

        callBtn = (ImageView) findViewById(R.id.callBtn);
        rejectBtn = (ImageView)findViewById(R.id.rejectBtn);


//        if(senderId.equals(OwnerId)) {
//
//            System.out.println("OWNER");
//
//            rejectBtn.setEnabled(false);
//
//        }else{
//
//            System.out.println("Receiver");
//
//            rejectBtn.setEnabled(true);
//        }



        videoFrameView = (LinearLayout)findViewById(R.id.videoFrameView);


        groupcall_preview_fragment_container =  (RelativeLayout)findViewById(R.id.groupcall_preview_fragment_container);


        myChronometer = (Chronometer)findViewById(R.id.chronometer);



        myChronometer.setBase(SystemClock.elapsedRealtime());

        myChronometer.setTextColor(Color.WHITE);

//        myChronometer.setFormat("HH:MM:SS");




        setActionListener();



    }

    public void parseValues(){

        b = getIntent().getExtras();

        if(b!=null){

            OwnerId=Tools.getData(GroupCallMainActivity.this, "idprofile");

            fId =  b.getInt("id");
            receiverFirstName =  b.getString("receiver_name");

            currentChannel =  b.getString("actual_channel");

            ApplicationController.getInstance().setUserCurrentChannel(currentChannel);


            if(b.getStringArrayList("group_mebers")!=null){

                groupMemberArray = b.getStringArrayList("group_mebers");
            }

            if(getIntent().getStringArrayExtra("members_details")!=null){

                stringArray = getIntent().getStringArrayExtra("members_details");



                ArrayList<String>tempArray = new ArrayList<>(Arrays.asList(stringArray));

                try {
                    JSONArray jsonArray  =new JSONArray(tempArray.get(0));

                    Log.d("jsonArray", String.valueOf(jsonArray));

                    memberArray = jsonArray;

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }else{

                String mememberArray = ChatApi.getInstance().getGroupMembersBychannel(currentChannel).get(0);

                stringArray = new String[] {mememberArray};


                ArrayList<String>tempArray = new ArrayList<>(Arrays.asList(stringArray));

                try {
                    JSONArray jsonArray  =new JSONArray(tempArray.get(0));

                    Log.d("jsonArray", String.valueOf(jsonArray));

                    memberArray = jsonArray;

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }


            receiverAmisId =b.getString("receiverId");

            if(b.getString("senderID",null)!=null){

                senderId=b.getString("senderID");
                System.out.println("senderId  if "  + senderId);

            }else{

                senderId = Tools.getData(GroupCallMainActivity.this, "idprofile");
                System.out.println("senderId  else "  + senderId);

            }
        }


        if(senderId.equals(OwnerId)){


        }else{

            ApplicationController.getInstance().startPreviewScreenTimer();
        }




    }


    public void setActionListener(){

        callBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (ApplicationController.getInstance().isNetworkConnected()) {

                    ChatApi.getInstance().setUserIsAvailable(false);

                    ApplicationController.getInstance().isInputEventOccurred = false;

                    if (hasPermission) {

                        if (!btnClicked) {

                            btnClicked = Boolean.TRUE;

                            makePubClls();
                        }


                    }


                }
                else{

                    Toast.makeText(GroupCallMainActivity.this,"Check your Internet Connection",Toast.LENGTH_SHORT);
                }
            }

        });

        rejectBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ApplicationController.getInstance().setShowVoicePopup(Boolean.FALSE);


                if (ApplicationController.getInstance().isNetworkConnected()) {

                    ChatApi.getInstance().setUserIsAvailable(true);


                    ApplicationController.getInstance().setInputEventOccurred(Boolean.TRUE);

                    ApplicationController.getInstance().stopPreviewScreenTimer();

                    ApplicationController.getInstance().setShowVoicePopup(Boolean.FALSE);


                    if (hasAdmin) {

//                    disconnectSession();

//                    finish();

                        if (makeRejectCall) {

                            makePublisherCalsForStopVideo();

                        } else {

                            onBackPressed();
                        }


                    } else {

//                    disconnectSession();

//                    isStreamRecieved

                        onBackPressed();
                    }


                }
                else{

                    Toast.makeText(GroupCallMainActivity.this,"Check your Internet Connection",Toast.LENGTH_SHORT);
                }

            }
        });
    }

    public  void makePublisherCalsForStopVideo() {

        try {

            Log.e("SENDER ID", senderId);

            Log.e("OWNERID", OwnerId);

            Map<String, String> json = new HashMap<>();
            json.put("message", "RejectConferenceVideoCall");
            json.put("Name", "RejectConferenceVideoCall");
            json.put("actual_channel", currentChannel);

            json.put("ReceiverName", "dummy");

            json.put("senderID", senderId);
            json.put("receiverID", String.valueOf(fId));

            json.put("contentType", "text");

            json.put("senderName", Tools.getData(this, "name"));

            json.put("chatType", "Groupe");
            json.put("isVideoCalling", "yes");
            json.put("type", "Call Connecting");
            json.put("isVideoCalling", "yes");
            String senderPhoto = Tools.getData(this, "photo");
            json.put("senderImage", senderPhoto);
            json.put("fromMe", "yes");
            json.put("ReceiverImage", senderPhoto);
            json.put("senderCountry", Tools.getData(this, "country"));
            json.put("ReceiverCountry", "India");

            json.put("opentok_session_id", ChatApi.getInstance().getOPENTOKSESSIONID());
            json.put("opentok_token",ChatApi.getInstance().getOPENTOKTOKEN());
            json.put("opentok_apikey",ChatApi.getInstance().getOPENTOKAPIKEY());


            String mem = android.text.TextUtils.join(",", groupMemberArray);

            mem = mem.replace("M_", "");

            json.put("group_mebers", mem);
            json.put("GroupCount", String.valueOf(groupMemberArray.size()));
            json.put("currenIDs", Tools.getData(GroupCallMainActivity.this, "idprofile"));

            json.put("receiverGroup", memberDetails);


//            json.put("group_mebers", "0");
//            json.put("GroupCount", "");
            json.put("currenIDs", Tools.getData(GroupCallMainActivity.this, "idprofile"));
//            json.put("receiverGroup", "");

            ChatApi.getInstance().sendMessageForGroupVideo(json, currentChannel, GroupCallMainActivity.this);

//            ApplicationController.getInstance().setShowVoicePopup(true);


        }

        catch (NullPointerException ex){

            Log.e("NULL EXcep",ex.getLocalizedMessage());
        }




    }

    public  void makePubClls(){

        Log.e("SENDER ID",senderId);

        Log.e("OWNERID",OwnerId);

        if(senderId.equals(OwnerId)){

            Log.e("OWNER",senderId);

            ChatApi.getInstance().setCallListener(this);

            Toast.makeText(this, "Call Connecting.", Toast.LENGTH_LONG).show();

            ChatApi.getInstance().setUserIsAvailable(Boolean.FALSE);

            ChatApi.getInstance().setIsStreamingconnected(Boolean.TRUE);

            ApplicationController.getInstance().startTimer(GroupCallMainActivity.this,"Groupe",currentChannel.replace("Public_",""),currentChannel);

            hasAdmin = true;

            memberDetails = ChatApi.getInstance().getGroupMembersBychannel(currentChannel).get(0);

            Map<String, String> json = new HashMap<>();
            json.put("message", "MultiCall");
            json.put("Name","MultiCall");
            json.put("actual_channel", currentChannel);

            json.put("ReceiverName","dummy");

            json.put("senderID", senderId);
            json.put("receiverID",Tools.getData(GroupCallMainActivity.this, "idprofile"));

            json.put("contentType", "text");

            json.put("senderName", Tools.getData(this, "name"));
//                        json.put("ReceiverName", receiverFirstName);
            json.put("chatType", "Groupe");
            json.put("isVideoCalling","yes");
            json.put("type","Call Connecting");
            json.put("isVideoCalling","yes");
            String senderPhoto = Tools.getData(this, "photo");
            json.put("senderImage", senderPhoto);
            json.put("fromMe","yes");
            json.put("ReceiverImage", senderPhoto);
            json.put("senderCountry", Tools.getData(this, "country"));
            json.put("ReceiverCountry", "India");


            json.put("opentok_session_id", ChatApi.getInstance().getOPENTOKSESSIONID());
            json.put("opentok_token",ChatApi.getInstance().getOPENTOKTOKEN());
            json.put("opentok_apikey",ChatApi.getInstance().getOPENTOKAPIKEY());


            String mem = android.text.TextUtils.join(",", groupMemberArray);

            mem = mem.replace("M_","");

            json.put("group_mebers",mem);
            json.put("GroupCount", String.valueOf(groupMemberArray.size()));
            json.put("currenIDs",Tools.getData(GroupCallMainActivity.this,"idprofile"));

            json.put("receiverGroup",memberDetails);

            ChatApi.getInstance().sendMessageForGroupVideo(json, currentChannel,GroupCallMainActivity.this);

//            sendMessgaeToServer(currentChannel.replace("Public_",""));

            Tools.sendMessgaeAlertToServer(GroupCallMainActivity.this,currentChannel.replace("Public_",""),"Groupe_video");


            startVideoSession();




        }else{

            hasAdmin = false;

            ApplicationController.getInstance().setCallConnected(Boolean.TRUE);

//            ApplicationController.getInstance().isInputEventOccurred = false;

            ApplicationController.getInstance().setInputEventOccurred(Boolean.TRUE);

            ChatApi.getInstance().setIsStreamingconnected(Boolean.TRUE);

            ApplicationController.getInstance().stopPreviewScreenTimer();

            Log.e("Subscriber connecting",senderId);

            Log.e("OWNER",senderId);

            ChatApi.getInstance().stopRingTone();

//            updatenotification(currentChannel.replace("Public_",""));

            Toast.makeText(this, "Call Connected.", Toast.LENGTH_LONG).show();

            Map<String, String> json = new HashMap<>();
//            Tools.getData(GroupCallMainActivity.this, "idprofile")
            json.put("receiver_name", "senderName");
            json.put("receiverID",senderId);
            json.put("actual_channel", currentChannel);
            json.put("senderID",OwnerId);
            json.put("Name","connecting Multiple Call");

            json.put("message", "connecting Multiple Call");
            json.put("contentType", "text");
            json.put("senderName", senderId);
//                        json.put("ReceiverName", receiverFirstName);
            json.put("chatType", "Groupe");
            json.put("isVideoCalling", "yes");
            json.put("type", "Connecting");
            String senderPhoto = Tools.getData(this, "photo");
            json.put("senderImage", senderPhoto);

//            String[] myarray = new String[];
            json.put("receiverGroup", String.valueOf(memberArray));
            json.put("GroupNewCount", String.valueOf(memberArray.length()));


            ArrayList gr =ChatApi.getInstance().getGroupMembersDict().get(currentChannel);
            json.put("group_mebers",android.text.TextUtils.join(",",gr));
            json.put("senderImage", senderPhoto);
            json.put("ReceiverImage", senderPhoto);
            json.put("senderCountry", Tools.getData(this, "country"));
            json.put("ReceiverCountry", "India");

            json.put("opentok_session_id", ChatApi.getInstance().getOPENTOKSESSIONID());
            json.put("opentok_token",ChatApi.getInstance().getOPENTOKTOKEN());
            json.put("opentok_apikey",ChatApi.getInstance().getOPENTOKAPIKEY());


            ChatApi.getInstance().sendMessageForGroupVideo(json, currentChannel,GroupCallMainActivity.this);

            Tools.updateUserNotification(GroupCallMainActivity.this,"Groupe_video",currentChannel.replace("Public_",""));


//stuff that updates ui

//                    startVideoSession();







        }




    }


    public void initSession(){

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {

                final JSONObject sessionResponse = Tools.createSessionAndToken(GroupCallMainActivity.this.getString(R.string.server_url)+GroupCallMainActivity.this.getString(R.string.create_opentok_session),Tools.getData(GroupCallMainActivity.this, "idprofile"),Tools.getData(GroupCallMainActivity.this, "token"));


                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {



                        if(sessionResponse!=null){

                            dismissLoader();

                            try {
                                if(sessionResponse.getBoolean("result")){

                                    JSONObject opentok_response = sessionResponse.getJSONObject("data");

                                    ChatApi.getInstance().setOPENTOKSESSIONID(opentok_response.getString("session_id"));

                                    ChatApi.getInstance().setOPENTOKTOKEN(opentok_response.getString("token"));

                                    ChatApi.getInstance().setOPENTOKAPIKEY(opentok_response.getString("api_key"));



//                                    startVideoSession();






                                }else{

                                    Toast.makeText(GroupCallMainActivity.this,"Unable to Create session Please try again",Toast.LENGTH_SHORT);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        else{

                            dismissLoader();

                            Toast.makeText(GroupCallMainActivity.this,"Unable to Create session Please try again",Toast.LENGTH_SHORT);
                        }

                    }
                });
            }
        });
    }


    public void startVideoSession(){

        runOnUiThread(new Runnable() {
            @Override
            public void run() {


                mSession = new Session(GroupCallMainActivity.this,ChatApi.getInstance().getOPENTOKAPIKEY(),ChatApi.getInstance().getOPENTOKSESSIONID());
                mSession.setSessionListener(GroupCallMainActivity.this);
                mSession.connect(ChatApi.getInstance().getOPENTOKTOKEN());

                videoFrameView.setVisibility(View.INVISIBLE);

            }
        });




    }


    public void initViewPreviewfragemnts(){


        mPreviewFragment = new GroupCallPreviewFragment();
        mPreviewFragment.setArguments(b);

        getSupportFragmentManager().beginTransaction()
                .add(R.id.groupcall_preview_fragment_container, mPreviewFragment).commit();





    }

    @Override
    protected void onStart() {
        Log.d(TAG, "onStart");

        super.onStart();
    }

    @Override
    protected void onRestart() {
        Log.d(TAG, "onRestart");

        super.onRestart();
    }

    @Override
    protected void onResume() {
        Log.d(TAG, "onResume");

        super.onResume();

        if (mSession == null) {
            return;
        }
        mSession.onResume();
    }

    @Override
    protected void onPause() {
        Log.d(TAG, "onPause");

        super.onPause();

        if (mSession == null) {
            return;
        }
        mSession.onPause();

        if (isFinishing()) {
            disconnectSession();
        }


    }

    @Override
    protected void onStop() {
        Log.d(TAG, "onPause");
//        ApplicationController.getInstance().stopTimer();
        disconnectSession();

        super.onStop();
    }
    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy");

//       ApplicationController.getInstance().stopTimer();

        disconnectSession();

        super.onDestroy();
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        Log.d(TAG, "onPermissionsGranted:" + requestCode + ":" + perms.size());

//        if(senderId.equals(OwnerId)){
//
//            showLoader();
//            initSession();
//
//        }
//



    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        Log.d(TAG, "onPermissionsDenied:" + requestCode + ":" + perms.size());

        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            new AppSettingsDialog.Builder(this, getString(R.string.rationale_ask_again))
                    .setTitle(getString(R.string.title_settings_dialog))
                    .setPositiveButton(getString(R.string.setting))
                    .setNegativeButton(getString(R.string.cancel), null)
                    .setRequestCode(RC_SETTINGS_SCREEN_PERM)
                    .build()
                    .show();
        }
    }

    @AfterPermissionGranted(RC_VIDEO_APP_PERM)
    private void requestPermissions() {
        String[] perms = {
                Manifest.permission.INTERNET,
                Manifest.permission.CAMERA,
                Manifest.permission.RECORD_AUDIO
        };
        if (EasyPermissions.hasPermissions(this, perms)) {

            hasPermission =Boolean.TRUE;

            if(senderId.equals(OwnerId)){

                showLoader();
                initSession();

            }else{

//                startVideoSession();

//                initSession();
            }



//            mSession = new Session(GroupCallMainActivity.this, OpenTokConfig.API_KEY, OpenTokConfig.SESSION_ID);
//            mSession.setSessionListener(this);
//            mSession.connect(OpenTokConfig.TOKEN);
        } else {
            EasyPermissions.requestPermissions(this, getString(R.string.rationale_video_app), RC_VIDEO_APP_PERM, perms);
        }
    }
    @Override
    public void onConnected(Session session) {
        Log.d(TAG, "onConnected: Connected to session " + session.getSessionId());

        Toast.makeText(GroupCallMainActivity.this,"Connected to session",Toast.LENGTH_SHORT);

//        ApplicationController.getInstance().setCallConnected(Boolean.TRUE);


        mPublisher = new Publisher(GroupCallMainActivity.this,currentChannel.concat(String.valueOf(new DateTime())));



        mPublisher.setPublisherListener(this);
        mPublisher.setStyle(BaseVideoRenderer.STYLE_VIDEO_SCALE, BaseVideoRenderer.STYLE_VIDEO_FILL);

        mPublisherViewContainer.addView(mPublisher.getView());

        mSession.publish(mPublisher);
    }

    @Override
    public void onDisconnected(Session session) {

        ApplicationController.getInstance().setCallConnected(Boolean.FALSE);

        Log.d(TAG, "onDisconnected: disconnected from session " + session.getSessionId());

        mSession = null;

        finish();
    }

    @Override
    public void onError(Session session, OpentokError opentokError) {
        Log.d(TAG, "onError: Error (" + opentokError.getMessage() + ") in session " + session.getSessionId());

        String errMessage = "onError: Error (" + opentokError.getMessage() + ") in session " + session.getSessionId();

        Toast.makeText(this, errMessage, Toast.LENGTH_LONG).show();

        finish();
    }
    @Override
    public void onStreamReceived(Session session, Stream stream) {
        Log.d(TAG, "onStreamReceived: New stream " + stream.getStreamId() + " in session " + session.getSessionId());



        if(session.getSessionId().equals(ChatApi.getInstance().getOPENTOKSESSIONID())) {


            Toast.makeText(GroupCallMainActivity.this, "onStreamReceived", Toast.LENGTH_SHORT);

            ChatApi.getInstance().setUserIsAvailable(Boolean.FALSE);

            ApplicationController.getInstance().setCallConnected(Boolean.TRUE);

            if (mSubscribers.size() + 1 > MAX_NUM_SUBSCRIBERS) {

                Toast.makeText(this, "New subscriber ignored. MAX_NUM_SUBSCRIBERS limit reached.", Toast.LENGTH_LONG).show();
                return;
            }

            isStreamRecieved = Boolean.TRUE;


            if (mSession != null) {

                final Subscriber subscriber = new Subscriber(GroupCallMainActivity.this, stream);
                mSession.subscribe(subscriber);
                mSubscribers.add(subscriber);
                mSubscriberStreams.put(stream, subscriber);

                int position = mSubscribers.size() - 1;
                int id = getResources().getIdentifier("subscriberview" + (new Integer(position)).toString(), "id", GroupCallMainActivity.this.getPackageName());
                RelativeLayout subscriberViewContainer = (RelativeLayout) findViewById(id);


                subscriber.setStyle(BaseVideoRenderer.STYLE_VIDEO_SCALE, BaseVideoRenderer.STYLE_VIDEO_FILL);

                Log.d("stream view::", String.valueOf(subscriber.getView()));

                subscriberViewContainer.addView(subscriber.getView());

                id = getResources().getIdentifier("toggleAudioSubscriber" + (new Integer(position)).toString(), "id", GroupCallMainActivity.this.getPackageName());

                final ImageView toggleAudio = (ImageView) findViewById(id);

                toggleAudio.setSelected(true);

                toggleAudio.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        if (toggleAudio.isSelected()) {

                            toggleAudio.setSelected(false);
                            toggleAudio.setImageResource(R.drawable.no_microphone_thumb_thumb);
                            subscriber.setSubscribeToAudio(false);

                        } else {
                            toggleAudio.setSelected(true);
                            toggleAudio.setImageResource(R.drawable.mic_icon);
                            subscriber.setSubscribeToAudio(true);
                        }
                    }
                });

//        final ToggleButton toggleAudio = (ToggleButton) findViewById(id);
//        toggleAudio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if (isChecked) {
//                    subscriber.setSubscribeToAudio(true);
//                } else {
//                    subscriber.setSubscribeToAudio(false);
//                }
//            }
//        });
                toggleAudio.setVisibility(View.VISIBLE);


                groupcall_preview_fragment_container.setVisibility(View.INVISIBLE);

                videoFrameView.setVisibility(View.VISIBLE);

                rejectBtn.setEnabled(true);

//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//
//
//            }
//        });

                myChronometer.setVisibility(View.VISIBLE);
                myChronometer.start();

            } else {

                Log.d("Session Error", "Error");
            }

        }
        else{

            Toast.makeText(this, "Session Closed", Toast.LENGTH_LONG).show();

            finish();

        }


    }

    @Override
    public void onStreamDropped(Session session, Stream stream) {

        Log.d(TAG, "onStreamDropped: Stream " + stream.getStreamId() + " dropped from session " + session.getSessionId());

        Subscriber subscriber = mSubscriberStreams.get(stream);
        if (subscriber == null) {
            return;
        }

        int position = mSubscribers.indexOf(subscriber);
        int id = getResources().getIdentifier("subscriberview" + (new Integer(position)).toString(), "id", GroupCallMainActivity.this.getPackageName());

        mSubscribers.remove(subscriber);
        mSubscriberStreams.remove(stream);

        RelativeLayout subscriberViewContainer = (RelativeLayout) findViewById(id);
        subscriberViewContainer.removeView(subscriber.getView());

        id = getResources().getIdentifier("toggleAudioSubscriber" + (new Integer(position)).toString(), "id", GroupCallMainActivity.this.getPackageName());

        final ImageView toggleAudio = (ImageView)findViewById(id);

        toggleAudio.setSelected(true);
//        final ToggleButton toggleAudio = (ToggleButton) findViewById(id);
//        toggleAudio.setOnCheckedChangeListener(null);
        toggleAudio.setVisibility(View.INVISIBLE);


        Log.d("Subscriber size", String.valueOf(mSubscribers.size()));

        if(isStreamRecieved){

            Log.d("isStreamRecieved", String.valueOf(isStreamRecieved));

            Log.d("Subscriber size", String.valueOf(mSubscribers.size()));

            if(mSubscribers.size()==0){

                ApplicationController.getInstance().setCallConnected(Boolean.FALSE);

                myChronometer.setVisibility(View.GONE);

                myChronometer.stop();

                disconnectSession();
            }
        }

    }


    @Override
    public void onStreamCreated(PublisherKit publisherKit, Stream stream) {
        Log.d(TAG, "onStreamCreated: Own stream " + stream.getStreamId() + " created");
    }

    @Override
    public void onStreamDestroyed(PublisherKit publisherKit, Stream stream) {
        Log.d(TAG, "onStreamDestroyed: Own stream " + stream.getStreamId() + " destroyed");
    }

    @Override
    public void onError(PublisherKit publisherKit, OpentokError opentokError) {
        Log.d(TAG, "onError: Error (" + opentokError.getMessage() + ") in publisher");

        Toast.makeText(this, "Session error. See the logcat please.", Toast.LENGTH_LONG).show();
        finish();
    }

    private void disconnectSession() {


        Log.e("DisConnect","GroupCall Called");

//        Toast.makeText(this, "disconnectSession Called.", Toast.LENGTH_LONG).show();



        makeRejectCall = false;
        ChatApi.getInstance().setUserIsAvailable(Boolean.TRUE);

//        ApplicationController.getInstance().setUserCurrentChannel("");

        if (mSession == null) {
            return;
        }

        if (mSubscribers.size() > 0) {
            for (Subscriber subscriber : mSubscribers) {
                if (subscriber != null) {
                    mSession.unsubscribe(subscriber);
                    subscriber.destroy();
                }
            }
        }

        if (mPublisher != null) {
            mPublisherViewContainer.removeView(mPublisher.getView());
            mSession.unpublish(mPublisher);
            mPublisher.destroy();
            mPublisher = null;
        }

        mSession.disconnect();

        ChatApi.getInstance().setUserIsAvailable(Boolean.TRUE);

        ChatApi.getInstance().setIsStreamingconnected(Boolean.FALSE);

        ApplicationController.getInstance().stopPreviewScreenTimer();

        ApplicationController.getInstance().setCallConnected(Boolean.FALSE);

        finish();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void connectStreamingToOwner() {

        Log.e("GROUOCALL ACCEPTED","connectStreamingToOwner");

//        startVideoSession();

    }

    @Override
    public void groupCallAcceptedFromReceiver(final String receiverId) {

        Log.d("GROUOCALL ACCEPTED","ACCESPTED");

        ApplicationController.getInstance().setCallConnected(Boolean.TRUE);

        ApplicationController.getInstance().setShowVoicePopup(Boolean.FALSE);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                System.out.println("***********");
                System.out.println(memberArray);
                Log.e("Sender",senderId);
                Log.e("REceiver",receiverId);

                Log.e("Owner ID",OwnerId);

                startVideoSession();


                Log.e("This Session", String.valueOf(SessionStarted));

//                if(SessionStarted==Boolean.FALSE){
//
//                    SessionStarted = Boolean.TRUE;
//
//
//                }
//                else{
//
//                    Log.e("This Session", "STARTED");
//                }
//


//                Toast.makeText(GroupCallMainActivity.this, "groupCallAcceptedFromReceive", Toast.LENGTH_LONG).show();



                String currentUserId = Tools.getData(GroupCallMainActivity.this, "idprofile");

                if(senderId.equals(OwnerId)){

                    Log.e("Call ","Owner");



                    if(!ApplicationController.getInstance().getCallConnected()){

                        Log.e("Call ","startSession");

                        ApplicationController.getInstance().setCallConnected(Boolean.FALSE);

//                        BF9F887A-9BBC-4241-9D94-91EA018299E1
//                        B4B76B38-5148-4323-AA63-98B08D032ADE
//                        1416AA85-907A-43CB-B76D-3449F413E198

                        makeRejectCall = true;



                    }else{

                        Log.e("Call ","Live in Progress");
                    }


                    ChatApi.getInstance().setIsStreamingconnected(Boolean.TRUE);


                }
                else if(currentUserId.equals(receiverId)){

                    Log.e("Call","Receiver");

//                    ChatApi.getInstance().setIsStreamingconnected(Boolean.TRUE);
//
//                    ApplicationController.getInstance().stopPreviewScreenTimer();


                }else{

                    Log.e("Already Connection","Came bro");
                }


            }
        });



    }


    @Override
    public void onBackPressed(){


        ApplicationController.getInstance().setShowVoicePopup(Boolean.FALSE);

        if(ApplicationController.getInstance().isNetworkConnected()){

            showAlert();

        }else{
            Toast.makeText(GroupCallMainActivity.this,"Check your Internet Connection",Toast.LENGTH_SHORT);
        }

    }

    public void showAlert(){

        ApplicationController.getInstance().setShowVoicePopup(Boolean.FALSE);

        new AlertDialog.Builder(this)
                .setTitle("Wis")
                .setMessage("Are you sure you want to exit this session?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete

                        ChatApi.getInstance().stopRingTone();

//                        disconnectSession();

                        ChatApi.getInstance().setIsStreamingconnected(Boolean.FALSE);

                        try {

//                            disconnectSession();
                        }
                        catch (NullPointerException ex)
                        {

                            Log.e("Call disConnect","Groupe");
                        }



                        if(hasAdmin){

                            makePublisherCalsForStopVideo();
                        }else{

                            finish();
                        }




                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();

    }


    public  void sendMessgaeToServer(String groupId) {

        JSONObject jsonBody = new JSONObject();
//        findViewById(R.id.loading).setVisibility(View.VISIBLE);
        try {
            jsonBody.put("id_profil", Tools.getData(GroupCallMainActivity.this, "idprofile"));
            jsonBody.put("id_profil_dest",groupId);
            jsonBody.put("message",Tools.getData(GroupCallMainActivity.this, "name")+" "+"Missed a video call");
            jsonBody.put("type_message","text");
            jsonBody.put("chat_type", "Groupe_video");

            Log.i("json", jsonBody.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest reqSend = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.sendmsg_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Log.d("SETMESSAGE RES:", String.valueOf(response));
                        try {
                            if (response.getString("result").equals("true")) {

//
                            } else {
                                //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {

                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(GroupCallMainActivity.this, "token"));
                headers.put("lang", Tools.getData(GroupCallMainActivity.this, "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqSend);
    }


    public  void updatenotification(String groupId) {

        JSONObject jsonBody = new JSONObject();
//        findViewById(R.id.loading).setVisibility(View.VISIBLE);
        try {
            jsonBody.put("user_id", Tools.getData(GroupCallMainActivity.this, "idprofile"));
            jsonBody.put("sender_id",groupId);
            jsonBody.put("type","Groupe");



            Log.i("json", jsonBody.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest reqSend = new JsonObjectRequest(getString(R.string.server_url) + getString(R.string.updateNotification), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Log.d("SETMESSAGE RES:", String.valueOf(response));
                        try {
                            if (response.getString("result").equals("true")) {

//
                            } else {
                                //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {

                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("token", Tools.getData(GroupCallMainActivity.this, "token"));
                headers.put("lang", Tools.getData(GroupCallMainActivity.this, "lang_pr"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqSend);
    }


    @Override
    public void onSignalReceived(Session session, String type, String data, Connection connection) {

        System.out.println("________________");
        Log.e("signal","received");

        String myConnectionId = session.getConnection().getConnectionId();
        if (connection != null && connection.getConnectionId().equals(myConnectionId)) {
            // Signal received from another client
        }
    }


    //    show loader

    public void showLoader(){

        pd = new ProgressDialog(this);
        pd.setMessage("loading");
        pd.show();
    }

    public void dismissLoader(){

        if(pd!=null){

            pd.dismiss();
        }
    }


}
