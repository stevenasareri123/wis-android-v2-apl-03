package com.oec.wis.adapters;

import android.app.Activity;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.oec.wis.R;

/**
 * Created by asareri-07 on 1/18/2017.
 */

public class SampleImageListAdapter extends ArrayAdapter<String> {

    private final Activity context;
    private final String[] web;
    private final Integer[] imageId;
    RelativeLayout dashboardRL;
    TextView txtTitle;
    public SampleImageListAdapter(Activity context,
                                  String[] web, Integer[] imageId) {
        super(context, R.layout.list_single, web);
        this.context = context;
        this.web = web;
        this.imageId = imageId;

    }

    public View getView(final int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.list_single, null, true);
        try{
              txtTitle = (TextView) rowView.findViewById(R.id.txt);

            Typeface font = Typeface.createFromAsset(context.getAssets(),"fonts/Harmattan-R.ttf");
            txtTitle.setTypeface(font);

        }catch (NullPointerException ex){
            Log.i("Exception",ex.getMessage());
        }


        ImageView imageView = (ImageView) rowView.findViewById(R.id.img);
//        dashboardRL=(RelativeLayout)rowView.findViewById(R.id.dashboardRL);
//
//        dashboardRL.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if(we){
//                    dashboardRL.setBackgroundColor(R.color.light_gray);
//                }
//                Toast.makeText(context,"clickable",Toast.LENGTH_SHORT).show();
//            }
//        });



        txtTitle.setText(web[position]);
            try{
                imageView.setImageResource(imageId[position]);
            }catch(ArrayIndexOutOfBoundsException ex){
                System.out.println("exce" +ex.getMessage());
            }


        return rowView;
    }

}