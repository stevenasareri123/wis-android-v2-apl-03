package com.oec.wis.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.oec.wis.ApplicationController;
import com.oec.wis.R;
import com.oec.wis.chatApi.ChatApi;
import com.oec.wis.dialogs.Chat;
import com.oec.wis.dialogs.NewAds;
import com.oec.wis.entities.WISVideo;
import com.oec.wis.tools.Tools;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VideoAdapter2 extends BaseAdapter {
    Context context;
    List<WISVideo> data;
    int mode;

    public VideoAdapter2(Context context, List<WISVideo> data, int mode) {
        this.context = context;
        this.data = data;
        this.mode = mode;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return data.get(position).getId();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.row_video_2, null);
            holder = new ViewHolder(convertView);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.ivVideo.setImageResource(0);
        if (data.get(position).isSelected()) {
            holder.rlSend.setVisibility(View.VISIBLE);
        } else {
            holder.rlSend.setVisibility(View.INVISIBLE);
        }
        holder.rlSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                data.get(position).setSelected(false);
                notifyDataSetChanged();
            }
        });

        holder.ivSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mode == 1) {
                    Chat.rootView.setAlpha(1);
                    Chat.disMissPopMenus();
                    Chat.makePubCalls(data.get(position).getPath(),data.get(position).getChat_type(),Tools.getData(context, "idprofile"),Tools.getData(context, "name"),Tools.getData(context, "photo"),"video");
//                    Chat.sendMsg(data.get(position).getPath(), Chat.fId, "video", data.get(position).getThumb(),data.get(position).getChat_type());
//                    Chat.dPickMedia.dismiss();


                } else {
                    NewAds.ivPlay.setVisibility(View.VISIBLE);
                    ApplicationController.getInstance().getImageLoader().get(context.getString(R.string.server_url4) + data.get(position).getThumb(), new ImageLoader.ImageListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }

                        @Override
                        public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                            if (response.getBitmap() != null) {
                                NewAds.ivPhoto.setImageBitmap(response.getBitmap());
                            }
                        }
                    });
                    NewAds.dPickMedia.dismiss();
                    NewAds.mediaName = data.get(position).getPath();
                }
            }
        });

        ApplicationController.getInstance().getImageLoader().get(context.getString(R.string.server_url4) + data.get(position).getThumb(), new ImageLoader.ImageListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                holder.ivVideo.setImageResource(R.drawable.empty);
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    holder.ivVideo.setImageBitmap(response.getBitmap());
                } else {
                    holder.ivVideo.setImageResource(R.drawable.empty);
                }
            }
        });

        return convertView;
    }

    @Override
    public int getViewTypeCount() {
        if (data.size() > 1)
            return data.size();
        else
            return 1;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    private static class ViewHolder {
        ImageView ivVideo, ivSend;
        RelativeLayout rlSend;

        public ViewHolder(View view) {
            ivVideo = (ImageView) view.findViewById(R.id.ivVideo);
            ivSend = (ImageView) view.findViewById(R.id.ivSend);
            rlSend = (RelativeLayout) view.findViewById(R.id.rlSend);
            view.setTag(this);
        }
    }
}
