package com.oec.wis.adapters;

/**
 * Created by asareri08 on 12/19/16.
 */

public class SelectCountry {


    private String mName;
    private String mCountryISO;
    private int mCountryCode;
    private String mCountryCodeStr;
    private int mPriority;
    private int mResId;
    private int mNum;

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getmCountryISO() {
        return mCountryISO;
    }

    public void setmCountryISO(String mCountryISO) {
        this.mCountryISO = mCountryISO;
    }

    public int getmCountryCode() {
        return mCountryCode;
    }

    public void setmCountryCode(int mCountryCode) {
        this.mCountryCode = mCountryCode;
    }

    public String getmCountryCodeStr() {
        return mCountryCodeStr;
    }

    public void setmCountryCodeStr(String mCountryCodeStr) {
        this.mCountryCodeStr = mCountryCodeStr;
    }

    public int getmPriority() {
        return mPriority;
    }

    public void setmPriority(int mPriority) {
        this.mPriority = mPriority;
    }

    public int getmResId() {
        return mResId;
    }

    public void setmResId(int mResId) {
        this.mResId = mResId;
    }

    public int getmNum() {
        return mNum;
    }

    public void setmNum(int mNum) {
        this.mNum = mNum;
    }

    public SelectCountry(String mName, String mCountryISO, int mCountryCode, String mCountryCodeStr, int mPriority, int mResId, int mNum) {
        this.mName = mName;
        this.mCountryISO = mCountryISO;
        this.mCountryCode = mCountryCode;
        this.mCountryCodeStr = mCountryCodeStr;
        this.mPriority = mPriority;
        this.mResId = mResId;
        this.mNum = mNum;
    }
}


