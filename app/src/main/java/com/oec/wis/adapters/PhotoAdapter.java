package com.oec.wis.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.util.SparseArray;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.etsy.android.grid.util.DynamicHeightImageView;
import com.oec.wis.ApplicationController;
import com.oec.wis.R;
import com.oec.wis.dialogs.SharePhoto;
import com.oec.wis.entities.WISPhoto;
import com.oec.wis.tools.Tools;

import org.joda.time.DateTime;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static com.google.android.gms.wearable.DataMap.TAG;

public class PhotoAdapter extends BaseAdapter {
    static FragmentActivity context;
    static List<WISPhoto> data;
    private PopupWindow popWindow;
    private final Random mRandom;
    String dataPath;
    int dataId,dataPosition;

    private static final SparseArray<Double> sPositionHeightRatios = new SparseArray<Double>();


    public PhotoAdapter(FragmentActivity context, List<WISPhoto> data) {
        PhotoAdapter.context = context;
        PhotoAdapter.data = data;
        mRandom = new Random();

    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return data.get(position).getId();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.row_photo, null);
            holder = new ViewHolder(convertView);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.ivPhoto.setImageResource(0);
        holder.tvTitle.setText(data.get(position).getTitle());

        dataPath = data.get(position).getPath();
        dataId=data.get(position).getId();

        System.out.println("data path and dataId" +dataPath +dataId);

        double positionHeight = getPositionRatio(position);

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date d = null;
        try {
            d = format.parse(data.get(position).getDateTime());
        } catch (Exception e) {
            e.printStackTrace();
        }
        String rDate = net.danlew.android.joda.DateUtils.getRelativeTimeSpanString(context, new DateTime(d), true).toString();
        holder.tvDate.setText(rDate);

        holder.tvDate.setText(rDate);

        ApplicationController.getInstance().getImageLoader().get(context.getString(R.string.server_url3) + data.get(position).getPath(), new ImageLoader.ImageListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                holder.ivPhoto.setImageResource(R.drawable.empty);
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    int nh = (int) (response.getBitmap().getHeight() * (512.0 / response.getBitmap().getWidth()));
                    Bitmap scaled = Bitmap.createScaledBitmap(response.getBitmap(), 512, nh, true);
                    holder.ivPhoto.setImageBitmap(scaled);
                } else {
                    holder.ivPhoto.setImageResource(R.drawable.empty);
                }
            }
        });

//        holder.bDelete.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                deleteDialog(position);
//            }
//        });
//
//        holder.bShare.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent i = new Intent(context, SharePhoto.class);
//                i.putExtra("path", data.get(position).getPath());
//                i.putExtra("id", data.get(position).getId());
//                context.startActivity(i);
//            }
//        });

        holder.dotsTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//               showPopUP();
//                replyCommentsView(view,position);


//                PopupMenu popup = new PopupMenu(context, view);
//                Menu m = popup.getMenu();
//                MenuInflater inflater = popup.getMenuInflater();
//                inflater.inflate(R.menu.diplay_photo_menu, popup.getMenu());
//
//                if (audio.getDownload().equals("0")) {
//
//                    m.removeItem(R.id.add_download);
//
//                }

                PopupMenu popup = new PopupMenu(context, view);
                MenuInflater inflater = popup.getMenuInflater();
                inflater.inflate(R.menu.diplay_photo_menu, popup.getMenu());
                popup.setOnMenuItemClickListener(new MyMenuItemClickListener());
                popup.show();



            }
        });
        return convertView;

    }

    private double getPositionRatio(final int position) {
        double ratio = sPositionHeightRatios.get(position, 0.0);
        // if not yet done generate and stash the columns height
        // in our real world scenario this will be determined by
        // some match based on the known height and width of the image
        // and maybe a helpful way to get the column height!
        if (ratio == 0) {
            ratio = getRandomHeightRatio();
            sPositionHeightRatios.append(position, ratio);
            Log.d(TAG, "getPositionRatio:" + position + " ratio:" + ratio);
        }
        return ratio;
    }

    private double getRandomHeightRatio() {
        return (mRandom.nextDouble() / 2.0) + 1.0; // height will be 1.0 - 1.5 the width
    }

    private void showPopUP() {

        // custom dialog
        final Dialog dialog = new Dialog(context);

        dialog.setContentView(R.layout.photo_pop_up);
//        dialog.setTitle("Title...");

        // set the custom dialog components - text, image and button
        TextView shareTV = (TextView) dialog.findViewById(R.id.shareTV);
        shareTV.setText("Share");
       TextView deleteTV=(TextView)dialog.findViewById(R.id.deleteTV);
        deleteTV.setText("Delete");

        dialog.show();
    }

    public void replyCommentsView(final View v,final int position){
        LayoutInflater layoutInflater = (LayoutInflater) context.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // inflate the custom popup layout
        final View inflatedView = layoutInflater.inflate(R.layout.photo_pop_up, null, false);


        // get device size
        Display display = ((Activity)context).getWindowManager().getDefaultDisplay();
        final Point size = new Point();
        display.getSize(size);
//        mDeviceHeight = size.y;


        // set height depends on the device size
        popWindow = new PopupWindow(inflatedView, size.x-500, size.y -1000, true);

        popWindow.setTouchable(true);

        v.setAlpha(0.5f);
//        backgroundView.setAlpha(0.4f);

        // make it focusable to show the keyboard to enter in `EditText`
//        popWindow.setFocusable(true);
        // make it outside touchable to dismiss the popup window
//        popWindow.setBackgroundDrawable(new BitmapDrawable());
        popWindow.setOutsideTouchable(false);

//        popWindow.setAnimationStyle(R.style.animationName);

        popWindow.showAtLocation(v, Gravity.LEFT, 150, 100);

        TextView shareTV = (TextView) inflatedView.findViewById(R.id.shareTV);
        shareTV.setText("Share");
        TextView deleteTV=(TextView)inflatedView.findViewById(R.id.deleteTV);
        deleteTV.setText("Delete");
        shareTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(context, SharePhoto.class);
                i.putExtra("path", data.get(position).getPath());
                i.putExtra("id", data.get(position).getId());
                context.startActivity(i);
            }
        });
        deleteTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteDialog(position);
            }
        });


    }

    private void deleteDialog(final int position) {
        new AlertDialog.Builder(context)
                .setMessage(context.getString(R.string.msg_confirm_delete_photo))
                .setPositiveButton(context.getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteVideo(data.get(position).getId(), position);
                    }

                })
                .setNegativeButton(context.getString(R.string.no), null)
                .show();
    }

    private void deleteVideo(int idPhoto, final int position) {
        JSONObject jsonBody = new JSONObject();

        try {
            jsonBody.put("id_profil", Tools.getData(context, "idprofile"));
            jsonBody.put("id_photo", String.valueOf(idPhoto));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqDelete = new JsonObjectRequest(context.getString(R.string.server_url) + context.getString(R.string.delphoto_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {
                                data.remove(position);
                                notifyDataSetChanged();
                            }

                        } catch (JSONException e) {
                            //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //if (context != null)
                //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("lang", Tools.getData(context, "lang_pr"));
                headers.put("token", Tools.getData(context, "token"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqDelete);
    }

    @Override
    public int getViewTypeCount() {
        if (data.size() > 1)
            return data.size();
        else
            return 1;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    private static class ViewHolder {
        DynamicHeightImageView ivPhoto;
        TextView tvTitle, tvDate,dotsTV;
        Button bDelete, bShare;

        public ViewHolder(View view) {
            ivPhoto = (DynamicHeightImageView) view.findViewById(R.id.ivPhoto);
            tvTitle = (TextView) view.findViewById(R.id.tvTitle);
            tvDate = (TextView) view.findViewById(R.id.tvDate);
//            bDelete = (Button) view.findViewById(R.id.bDelete);
//            bShare = (Button) view.findViewById(R.id.bShare);
            dotsTV=(TextView)view.findViewById(R.id.dotsTV);
            view.setTag(this);
        }
    }

    private class MyMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {
        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            switch (menuItem.getItemId()) {
                case R.id.shareMenu:
                    Intent i = new Intent(context, SharePhoto.class);
                    i.putExtra("path", dataPath);
                    i.putExtra("id", dataId);
                    context.startActivity(i);
//                    Toast.makeText(context, "Add to favourite", Toast.LENGTH_SHORT).show();
                    return true;
                case R.id.deleteMenu:
                   final int position = 0;
                    new AlertDialog.Builder(context)
                            .setMessage(context.getString(R.string.msg_confirm_delete_photo))
                            .setPositiveButton(context.getString(R.string.yes), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    deleteVideo(dataId, position);
                                }

                            })
                            .setNegativeButton(context.getString(R.string.no), null)
                            .show();
                

//                    Toast.makeText(context, "Play next", Toast.LENGTH_SHORT).show();
                    return true;
                default:
            }
            return false;
        }


    }


//    @Override
//    public boolean onOptionsItemSelected(MenuItem item, int position) {
//        // Handle item selection
//        switch (item.getItemId()) {
//            case R.id.shareMenu:
//                Intent i = new Intent(context, SharePhoto.class);
//                i.putExtra("path", data.get(position).getPath());
//                i.putExtra("id", data.get(position).getId());
//                context.startActivity(i);
//                return true;
//            case R.id.deleteMenu:
//                deleteDialog(position);
//                return true;
//            default:
//                return super.onOptionsItemSelected(item);
//        }
//    }

}
