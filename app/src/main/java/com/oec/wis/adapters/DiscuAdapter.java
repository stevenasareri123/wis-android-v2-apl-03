package com.oec.wis.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.oec.wis.ApplicationController;
import com.oec.wis.R;
import com.oec.wis.dialogs.Phone;
import com.oec.wis.dialogs.ProfileView;
import com.oec.wis.entities.WISChat;
import com.rockerhieu.emojicon.EmojiconTextView;

import org.joda.time.DateTime;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class DiscuAdapter extends BaseAdapter {
    Context context;
    List<WISChat> data;

    public DiscuAdapter(Context context, List<WISChat> data) {
        this.data = data;
        this.context = context;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return data.get(position).getId();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.row_discussion, null);
            holder = new ViewHolder(convertView);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
//        holder.llContent.setBackgroundColor(context.getResources().getColor(R.color.white));
//        if (position % 2 == 0) {
//            holder.llContent.setBackgroundColor(context.getResources().getColor(R.color.blue5));
//            holder.tvLastMsg.setTextColor(context.getResources().getColor(R.color.white));
//            holder.tvDate.setTextColor(context.getResources().getColor(R.color.white));
//            holder.time.setTextColor(context.getResources().getColor(R.color.white));
//        }
        holder.ivPic.setImageResource(0);

        holder.ivPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(context, ProfileView.class);
                i.putExtra("id",data.get(position).getUser().getId());
                context.startActivity(i);


            }
        });


        holder.tvName.setText(data.get(position).getUser().getFirstName() + " " + data.get(position).getUser().getLastName());
        Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/Harmattan-Regular.ttf");
        holder.tvName.setTypeface(font);
        holder.time.setTypeface(font);
        holder.tvDate.setTypeface(font);
        holder.tvLastMsg.setTypeface(font);
        if (data.get(position).getTypeMsg().equals("photo")) {
            holder.tvLastMsg.setText(context.getString(R.string.type_photo));
        } else if (data.get(position).getTypeMsg().equals("video")) {
            holder.tvLastMsg.setText(context.getString(R.string.type_video));
        } else {
            if (data.get(position).getMsg().length() < 50)
                holder.tvLastMsg.setText(data.get(position).getMsg());
            else
                holder.tvLastMsg.setText(data.get(position).getMsg().substring(0, 50) + "...");
        }
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat format_two = new SimpleDateFormat("dd/MM/yy");
        SimpleDateFormat format_three = new SimpleDateFormat("dd/MMMM/yyyy");
        SimpleDateFormat hour = new SimpleDateFormat("HH");
        SimpleDateFormat min = new SimpleDateFormat("mm");
        SimpleDateFormat sec = new SimpleDateFormat("ss");
        Date d = null;
        String h="",m="",s="";
        String created_at = null;
        try {
            d = format.parse(data.get(position).getThumb());
            created_at  =format_two.format(d);
            h = hour.format(d);
            m=min.format(d);
            s=sec.format(d);
        } catch (Exception e) {
            e.printStackTrace();
        }

//
        String rDate = net.danlew.android.joda.DateUtils.getRelativeTimeSpanString(context, new DateTime(d), true).toString();
        Log.i("pass date",data.get(position).getDateTime());
        holder.time.setText(rDate);
        holder.tvDate.setText(created_at);
        String text = h.concat("H").concat(m).concat("'").concat(s);
        holder.time.setText(text);
        ApplicationController.getInstance().getImageLoader().get(context.getString(R.string.server_url2) + data.get(position).getUser().getPic(), new ImageLoader.ImageListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                holder.ivPic.setImageResource(R.drawable.profile);
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    holder.ivPic.setImageBitmap(response.getBitmap());
                } else {
                    holder.ivPic.setImageResource(R.drawable.profile);
                }
            }
        });
        return convertView;
    }

    @Override
    public int getViewTypeCount() {
        if (data.size() > 1)
            return data.size();
        else
            return 1;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    private static class ViewHolder {
        TextView tvName, tvDate,time;
        EmojiconTextView tvLastMsg;
        ImageView ivPic;
        LinearLayout llContent;

        public ViewHolder(View view) {
            tvName = (TextView) view.findViewById(R.id.tvUName);
            tvLastMsg = (EmojiconTextView) view.findViewById(R.id.tvLastMsg);
            //tvLastMsg.setUseSystemDefault(true);
            tvDate = (TextView) view.findViewById(R.id.date);
            time = (TextView) view.findViewById(R.id.time);
            ivPic = (ImageView) view.findViewById(R.id.ivPic);
            llContent = (LinearLayout) view.findViewById(R.id.llContent);
            view.setTag(this);
        }
    }
}
