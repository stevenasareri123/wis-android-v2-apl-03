package com.oec.wis.adapters;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.oec.wis.ApplicationController;
import com.oec.wis.ConnectionDetector;
import com.oec.wis.R;
import com.oec.wis.entities.Popupelements;
import com.oec.wis.entities.UserNotification;
import com.oec.wis.entities.WISAds;
import com.oec.wis.entities.WISUser;
import com.oec.wis.tools.Tools;
import com.oec.wis.wisdirect.DirectListener;
import com.oec.wis.wisdirect.WisDirect;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnItemClickListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by asareri08 on 25/07/16.
 */
public class SelectFriendsAdapter extends BaseAdapter implements Filterable {

    Context context;
    List<WISUser> data;
    List<WISAds> ads;
    private List<WISUser>filteredData = null;
    private ItemFilter mFilter = new ItemFilter();
    DialogPlus dialog;
    DirectListener listener;
    static final int REQUEST_CAMERA = 2;
    ArrayList<Integer>groupMembers;
    ProgressDialog pd;



    public SelectFriendsAdapter(Context context, List<WISUser> data) {
        this.data = data;
        this.context = context;
        this.filteredData =  data ;
        groupMembers = new ArrayList<>();


    }

    public void setDirectListener(DirectListener listener){

        this.listener  = listener;
    }


    @Override
    public int getCount() {
        return filteredData.size();
    }

    @Override
    public Object getItem(int position) {
        return filteredData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return filteredData.get(position).getId();
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;


        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.group_chat_items, null);
            holder = new ViewHolder(convertView);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.ivPic.setImageResource(0);



        if(filteredData.get(position).getFullName()!=null){
            holder.tvName.setText(filteredData.get(position).getFullName());
        }else{
            holder.tvName.setText(filteredData.get(position).getFirstName() + " " + filteredData.get(position).getLastName());
        }


//
//        if(groupMembers.get(position)==data.get(position).getId()){
//
//
//        }

        holder.selecteFriends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("check staes", String.valueOf(holder.selecteFriends));
                holder.selecteFriends.setSelected(!holder.selecteFriends.isSelected());
                if(holder.selecteFriends.isSelected()){
                    holder.selecteFriends.setImageResource(R.drawable.done);

                    groupMembers.add(filteredData.get(position).getId());
                    Log.i("group mebers", String.valueOf(groupMembers));

//
                }
                else{
                    holder.selecteFriends.setImageResource(R.drawable.unchecked);

                    Log.i("check staes", String.valueOf(holder.selecteFriends.getId()));
//                   groupMembers.remove(position);
                    groupMembers.remove(filteredData.get(position));
                }


            }
        });

        if(checkMemberSelected(data.get(position).getId())){

            holder.selecteFriends.setImageResource(R.drawable.done);

        }else{

            holder.selecteFriends.setImageResource(R.drawable.unchecked);
        }


        holder.tvNFriend.setText(String.valueOf(filteredData.get(position).getnFriend()) + " Amis");
        ApplicationController.getInstance().getImageLoader().get(context.getString(R.string.server_url2) + filteredData.get(position).getPic(), new ImageLoader.ImageListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                holder.ivPic.setImageResource(R.drawable.empty);
//                retryImageDownload(holder,position);
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    holder.ivPic.setImageBitmap(response.getBitmap());
                } else {
                    holder.ivPic.setImageResource(R.drawable.empty);
                }
            }
        });
        if (position % 2 == 0) {
            holder.llContent.setBackgroundColor(context.getResources().getColor(R.color.blue5));
            holder.tvNFriend.setTextColor(context.getResources().getColor(R.color.white));
        }else{
            holder.llContent.setBackgroundColor(context.getResources().getColor(R.color.white));
            holder.tvNFriend.setTextColor(context.getResources().getColor(R.color.gray3));
        }

        return convertView;
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }

    public void createGroupchat(String name,String imagePath){
        Integer userId = Integer.parseInt(Tools.getData(context, "idprofile"));
//        groupMembers.add(userId);


        String members = groupMembers.toString().replace("[", "").replace("]", "");



        Log.i("group",members);



        new DoUpload().execute(imagePath,name,members);

    }


    private class DoUpload extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            view.findViewById(R.id.loading).setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (!result.equals("")) {
                try {
                    JSONObject img = new JSONObject(result);
                    if (img.getBoolean("result"))
                        Log.i("image data set",img.getString("data"));
//                    doUpdate(img.getString("data"));
                    //else
                    //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
//                    view.findViewById(R.id.loading).setVisibility(View.GONE);
                    Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                }

            } else {
//                view.findViewById(R.id.loading).setVisibility(View.GONE);
                //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected String doInBackground(String... urls) {
            return Tools.createGroup(context.getString(R.string.server_url) + context.getString(R.string.create_group), urls[0], Tools.getData(context, "token"),Tools.getData(context, "idprofile"),urls[1],urls[2]);
        }
    }

    public boolean checkMemberExists(){

        if(groupMembers.size()>0){
            return Boolean.TRUE;
        }else{
            return Boolean.FALSE;
        }


    }

    public boolean checkMemberSelected(int userId){

        for (int i=0;i<groupMembers.size();i++){

            if(groupMembers.get(i)==userId){

                return true;
            }
        }

        return false;
    }

    public void transferTextToContacts(String type,String chatId,String msg){

        String members = groupMembers.toString().replace("[", "").replace("]", "");
        Log.i("contacts", String.valueOf(groupMembers));
        Log.i("contacts user,msg",chatId);

        sendText(chatId,type,members,msg);

        groupMembers.clear();

    }


    private void sendText(final String chatId, final String chat_type,final String to,String text) {
        JSONObject jsonBody = new JSONObject();

        try {
            jsonBody.put("user_id", Tools.getData(context, "idprofile"));
            jsonBody.put("chat_type", "Amis");
            jsonBody.put("chat_id", chatId);
            jsonBody.put("to",to);
            jsonBody.put("message",text);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqDelete = new JsonObjectRequest(context.getString(R.string.server_url) + context.getString(R.string.transferText), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {

                                Toast.makeText(context, context.getString(R.string.share_success_msg), Toast.LENGTH_SHORT).show();

//
                            }

                        } catch (JSONException e) {
                            //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //if (context != null)
                Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("lang", Tools.getData(context, "lang_pr"));
                headers.put("token", Tools.getData(context, "token"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqDelete);
    }




    public void retryImageDownload(final ViewHolder holder,int position){

        try {

            ApplicationController.getInstance().getImageLoader().get(context.getString(R.string.server_url3) + filteredData.get(position).getPic(), new ImageLoader.ImageListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    holder.ivPic.setImageResource(R.drawable.empty);
                }

                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                    if (response.getBitmap() != null) {
                        holder.ivPic.setImageBitmap(response.getBitmap());
                    } else {
                        holder.ivPic.setImageResource(R.drawable.empty);
                    }
                }
            });

        }
        catch (IndexOutOfBoundsException ex){
            ex.printStackTrace();
        }


    }





    private static class ViewHolder {
        TextView tvName, tvNFriend,time,createdDate;
        ImageView ivPic, bChat, bNotif;
        ImageButton showStatus;
        LinearLayout llContent;
        ImageButton  selecteFriends;


        public ViewHolder(View view) {
            tvName = (TextView) view.findViewById(R.id.uName);
            tvNFriend = (TextView) view.findViewById(R.id.fCount);
            ivPic = (ImageView) view.findViewById(R.id.ivPic);
            bChat = (ImageView) view.findViewById(R.id.bChat);
            bNotif = (ImageView) view.findViewById(R.id.bNotif);
            llContent = (LinearLayout) view.findViewById(R.id.llContent);
//            showStatus = (ImageButton)view.findViewById(R.id.statusOption);
            time = (TextView)view.findViewById(R.id.time);
            createdDate = (TextView)view.findViewById(R.id.tvDate);
            selecteFriends = (ImageButton)view.findViewById(R.id.selectFriends);


            view.setTag(this);
        }
    }



    // Filter Class
    private class ItemFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults filterResults = new FilterResults();
            if (constraint!=null && constraint.length()>0) {
                ArrayList<WISUser> tempList = new ArrayList<WISUser>();

                // search content in friend list
                for (WISUser user : data) {
                    if (user.getFullName().toLowerCase().contains(constraint.toString().toLowerCase())) {
                        tempList.add(user);
                    }
                }

                filterResults.count = tempList.size();
                filterResults.values = tempList;
            } else {
                filterResults.count = data.size();
                filterResults.values = data;
            }

            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredData = (ArrayList<WISUser>) results.values;
            notifyDataSetChanged();
        }

    }
//    create pub add all

//    groupMembers.add(userId);


    public void clearGroupMember(){

        groupMembers.clear();

        notifyDataSetChanged();
    }

    public void addAll()
    {
        for(int i=0;i<data.size();i++)
        {

            groupMembers.add(data.get(i).getId());
        }

        Log.e("Slected FRiends", String.valueOf(groupMembers));


//        String members = groupMembers.toString().replace("[", "").replace("]", "").replace(",","||");

//        Log.i("groupAll",members);
    }














    public void createWisDirect(String name,String logo_path,Double latitude,Double longtitude){




        Integer userId = Integer.parseInt(Tools.getData(context, "idprofile"));
//        groupMembers.add(userId);

        String members = groupMembers.toString().replace("[", "").replace("]", "").replace(",", "||");

        Log.i("group", members);


        if (new ConnectionDetector(context).isConnectingToInternet() && groupMembers.size() > 0) {

            showLoader();

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                Log.i("Calling", "executeOnExecutor()");
                new DoCreateDirect().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, logo_path, name, members, String.valueOf(latitude), String.valueOf(longtitude));
            } else {
                Log.i("Calling", "execute()");
                new DoCreateDirect().execute(logo_path, name, members, String.valueOf(latitude), String.valueOf(longtitude));
            }


        } else {

            dismissLoader();

            if (groupMembers.size() == 0) {

                Toast.makeText(context, context.getString(R.string.select_friends_alert), Toast.LENGTH_SHORT).show();

            } else {

                Toast.makeText(context, context.getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
            }


        }




    }

    public class DoCreateDirect extends AsyncTask<String,Void,String>{

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            dismissLoader();


            try {
                if(result!=null) {

                    if (!result.equals("")) {
                        try {
                            JSONObject response_data = new JSONObject(result);
                            if (response_data.getBoolean("result"))
                                Log.i("direct created", String.valueOf(response_data));

//                    JSONObject direct_data

                            JSONObject opentok_res = response_data.getJSONObject("opentok_data");

//                    JSONArray wis_direct_mebers = response_data.getJSONArray("wis_direct_mebers");

                            Log.i("opentok_res", String.valueOf(opentok_res));

//                    Log.i("wis_direct_mebers", String.valueOf(wis_direct_mebers));

                            listener.directCreated(opentok_res.getString("api_key"), opentok_res.getString("session_id"), opentok_res.getString("token"), response_data);

//                    JSONObject memberList = response_data.getJSONObject("");


                        } catch (JSONException e) {
//                    view.findViewById(R.id.loading).setVisibility(View.GONE);
                            Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    } else {
//                view.findViewById(R.id.loading).setVisibility(View.GONE);
                        //Toast.makeText(getActivity(), getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                    }
                }else{

                    Toast.makeText(context,"Connection TimeOut ,Please Try Again",Toast.LENGTH_LONG).show();
                }
            }
            catch (NullPointerException w)
            {

            }
        }


        @Override
        protected String doInBackground(String... params) {

            JSONObject response = null;
            try {

                Log.e("Prts",params[0]);
                Log.e("Prts1",params[1]);
                Log.e("Prts2",params[2]);
                Log.e("Prts3",params[3]);
                Log.e("Prts3",params[4]);

                response = Tools.createDirect(context.getString(R.string.server_url) + context.getString(R.string.create_direct),params[0],Tools.getData(context,"token"),Tools.getData(context, "idprofile"),params[1],params[2],params[3],params[4]);
                if(response!=null){
                    return response.toString();
                }else{
                    return null;
                }

            }
            catch (Exception e)
            {

            }
            return null;

        }
    }

    //    show loader

    public void showLoader(){

        pd = new ProgressDialog(context);
        pd.setMessage("loading");
        pd.show();
    }

    public void dismissLoader(){

        if(pd!=null){

            pd.dismiss();
        }
    }

}
