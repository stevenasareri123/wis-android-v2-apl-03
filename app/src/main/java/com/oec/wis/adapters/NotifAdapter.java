
package com.oec.wis.adapters;

        import android.app.Activity;
        import android.content.Context;
        import android.graphics.Typeface;
        import android.util.Log;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.BaseAdapter;
        import android.widget.Button;
        import android.widget.ImageView;
        import android.widget.TextView;

        import com.android.volley.AuthFailureError;
        import com.android.volley.Response;
        import com.android.volley.VolleyError;
        import com.android.volley.toolbox.ImageLoader;
        import com.android.volley.toolbox.JsonObjectRequest;
        import com.github.siyamed.shapeimageview.CircularImageView;
        import com.oec.wis.ApplicationController;
        import com.oec.wis.R;
        import com.oec.wis.entities.WISNotif;
        import com.oec.wis.tools.Tools;

        import org.joda.time.DateTime;
        import org.json.JSONException;
        import org.json.JSONObject;

        import java.text.SimpleDateFormat;
        import java.util.Date;
        import java.util.HashMap;
        import java.util.List;
        import java.util.Map;

public class NotifAdapter extends BaseAdapter {
    Context context;
    List<WISNotif> data;

    public NotifAdapter(Context context, List<WISNotif> data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return data.get(position).getId();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.row_notif, null);
            holder = new ViewHolder(convertView);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        try{

            holder.ivPhoto.setImageResource(0);
            holder.tvTitle.setText(data.get(position).getTitle());
            holder.tvDesc.setText(data.get(position).getDesc());

            System.out.println("Description" +data.get(position).getDesc());

            Typeface font=Typeface.createFromAsset(context.getAssets(),"fonts/Harmattan-R.ttf");
            holder.tvDate.setTypeface(font);
            holder.tvDesc.setTypeface(font);
            holder.tvTitle.setTypeface(font);

        }catch(NullPointerException ex){
            Log.d("Null Exception" ,ex.getMessage());
        }

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date d = null;
        try {
            d = format.parse(data.get(position).getDateTime());
        } catch (Exception e) {
            e.printStackTrace();
        }
        String rDate = net.danlew.android.joda.DateUtils.getRelativeTimeSpanString(context, new DateTime(d), true).toString();
        holder.tvDate.setText(rDate);

        ApplicationController.getInstance().getImageLoader().get(context.getString(R.string.server_url3) + data.get(position).getPhoto(), new ImageLoader.ImageListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                holder.ivPhoto.setImageResource(R.drawable.empty);
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    holder.ivPhoto.setImageBitmap(response.getBitmap());
                } else {
                    holder.ivPhoto.setImageResource(R.drawable.empty);
                }
            }
        });
        holder.bDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteNotif(data.get(position).getId(), position);
            }
        });
        return convertView;
    }

    private void deleteNotif(int idNotif, final int position) {
        JSONObject jsonBody = new JSONObject();

        try {
            jsonBody.put("id_profil", Tools.getData(context, "idprofile"));
            jsonBody.put("id_notif", String.valueOf(idNotif));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqDelete = new JsonObjectRequest(context.getString(R.string.server_url) + context.getString(R.string.deletenotif_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {
                                data.remove(position);
                                notifyDataSetChanged();
                            }

                        } catch (JSONException e) {
                            //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //if (context != null)
                //Toast.makeText(context, context.getString(R.string.msg_server_errormsg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("lang", Tools.getData(context, "lang_pr"));
                headers.put("token", Tools.getData(context, "token"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqDelete);
    }
    @Override
    public int getViewTypeCount() {
        if (data.size() > 1)
            return data.size();
        else
            return 1;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    private static class ViewHolder {
        TextView tvTitle, tvDesc, tvDate;
        CircularImageView ivPhoto;
        Button bDelete;
        public ViewHolder(View view) {
            tvTitle = (TextView) view.findViewById(R.id.tvTitle);
            tvDesc = (TextView) view.findViewById(R.id.tvDesc);
            tvDate = (TextView) view.findViewById(R.id.tvDate);
            ivPhoto = (CircularImageView) view.findViewById(R.id.ivPhoto);
            bDelete = (Button) view.findViewById(R.id.bDelete);
            view.setTag(this);
        }
    }
}