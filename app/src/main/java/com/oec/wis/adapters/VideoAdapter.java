package com.oec.wis.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.oec.wis.ApplicationController;
import com.oec.wis.R;
import com.oec.wis.dialogs.ShareVideo;
import com.oec.wis.entities.WISVideo;
import com.oec.wis.tools.Tools;
import com.squareup.picasso.Picasso;

import org.joda.time.DateTime;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import tcking.github.com.giraffeplayer.GiraffePlayer;

//import fm.jiecao.jcvideoplayer_lib.JCUserAction;
//import fm.jiecao.jcvideoplayer_lib.JCUserActionStandard;
//import fm.jiecao.jcvideoplayer_lib.JCVideoPlayer;
//import fm.jiecao.jcvideoplayer_lib.JCVideoPlayerStandard;

public class VideoAdapter extends BaseAdapter {
    Context context;
    List<WISVideo> data;

    public VideoAdapter(Context context, List<WISVideo> data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return data.get(position).getId();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.row_video, null);
            holder = new ViewHolder(convertView);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }



        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date d = null;
        try {
            d = format.parse(data.get(position).getDateTime());
        } catch (Exception e) {
            e.printStackTrace();
        }
        String rDate = net.danlew.android.joda.DateUtils.getRelativeTimeSpanString(context, new DateTime(d), true).toString();
        holder.tvDate.setText(rDate);

        try{
            holder.ivVideo.setImageResource(0);

            ApplicationController.getInstance().getImageLoader().get(context.getString(R.string.server_url4) + data.get(position).getThumb(), new ImageLoader.ImageListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    holder.ivVideo.setImageResource(R.drawable.empty);
                }

                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                    if (response.getBitmap() != null) {
                        holder.ivVideo.setImageBitmap(response.getBitmap());
                    } else {
                        holder.ivVideo.setImageResource(R.drawable.empty);
                    }
                }
            });
        }catch (NullPointerException ex){
            System.out.println("Nullpointerexception"+ex.getMessage());
        }




//        holder.mJcVideoPlayerStandard.setUp("http://video.jiecao.fm/11/23/xin/%E5%81%87%E4%BA%BA.mp4"
//                , JCVideoPlayerStandard.SCREEN_LAYOUT_NORMAL, "嫂子不信");
//        Picasso.with(context)
//                .load("http://img4.jiecaojingxuan.com/2016/11/23/00b026e7-b830-4994-bc87-38f4033806a6.jpg@!640_360")
//                .into(holder.mJcVideoPlayerStandard.thumbImageView);
//
//        JCVideoPlayer.setJcUserAction(new MyUserActionStandard());
        holder.bDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteDialog(position);
            }
        });
        holder.bShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(context, ShareVideo.class);
                i.putExtra("thumb", data.get(position).getThumb());
                i.putExtra("id", data.get(position).getId());
                context.startActivity(i);
            }
        });
        return convertView;
    }

    private void deleteDialog(final int position) {
        new AlertDialog.Builder(context)
                .setMessage(context.getString(R.string.msg_confirm_delete_video))
                .setPositiveButton(context.getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteVideo(data.get(position).getId(), position);
                    }

                })
                .setNegativeButton(context.getString(R.string.no), null)
                .show();
    }

    private void deleteVideo(int idVideo, final int position) {
        JSONObject jsonBody = new JSONObject();

        try {
            jsonBody.put("id_profil", Tools.getData(context, "idprofile"));
            jsonBody.put("id_video", String.valueOf(idVideo));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqDelete = new JsonObjectRequest(context.getString(R.string.server_url) + context.getString(R.string.delvideos_meth), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {
                                data.remove(position);
                                notifyDataSetChanged();
                            }

                        } catch (JSONException e) {

                            //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //if (context != null)
                //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("lang", Tools.getData(context, "lang_pr"));
                headers.put("token", Tools.getData(context, "token"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqDelete);
    }


//    class MyUserActionStandard implements JCUserActionStandard {
//
//        @Override
//        public void onEvent(int type, String url, int screen, Object... objects) {
//            switch (type) {
//                case JCUserAction.ON_CLICK_START_ICON:
//                    Log.i("USER_EVENT", "ON_CLICK_START_ICON" + " title is : " + (objects.length == 0 ? "" : objects[0]) + " url is : " + url + " screen is : " + screen);
//                    break;
//                case JCUserAction.ON_CLICK_START_ERROR:
//                    Log.i("USER_EVENT", "ON_CLICK_START_ERROR" + " title is : " + (objects.length == 0 ? "" : objects[0]) + " url is : " + url + " screen is : " + screen);
//                    break;
//                case JCUserAction.ON_CLICK_START_AUTO_COMPLETE:
//                    Log.i("USER_EVENT", "ON_CLICK_START_AUTO_COMPLETE" + " title is : " + (objects.length == 0 ? "" : objects[0]) + " url is : " + url + " screen is : " + screen);
//                    break;
//                case JCUserAction.ON_CLICK_PAUSE:
//                    Log.i("USER_EVENT", "ON_CLICK_PAUSE" + " title is : " + (objects.length == 0 ? "" : objects[0]) + " url is : " + url + " screen is : " + screen);
//                    break;
//                case JCUserAction.ON_CLICK_RESUME:
//                    Log.i("USER_EVENT", "ON_CLICK_RESUME" + " title is : " + (objects.length == 0 ? "" : objects[0]) + " url is : " + url + " screen is : " + screen);
//                    break;
//                case JCUserAction.ON_SEEK_POSITION:
//                    Log.i("USER_EVENT", "ON_SEEK_POSITION" + " title is : " + (objects.length == 0 ? "" : objects[0]) + " url is : " + url + " screen is : " + screen);
//                    break;
//                case JCUserAction.ON_AUTO_COMPLETE:
//                    Log.i("USER_EVENT", "ON_AUTO_COMPLETE" + " title is : " + (objects.length == 0 ? "" : objects[0]) + " url is : " + url + " screen is : " + screen);
//                    break;
//                case JCUserAction.ON_ENTER_FULLSCREEN:
//                    Log.i("USER_EVENT", "ON_ENTER_FULLSCREEN" + " title is : " + (objects.length == 0 ? "" : objects[0]) + " url is : " + url + " screen is : " + screen);
//                    break;
//                case JCUserAction.ON_QUIT_FULLSCREEN:
//                    Log.i("USER_EVENT", "ON_QUIT_FULLSCREEN" + " title is : " + (objects.length == 0 ? "" : objects[0]) + " url is : " + url + " screen is : " + screen);
//                    break;
//                case JCUserAction.ON_ENTER_TINYSCREEN:
//                    Log.i("USER_EVENT", "ON_ENTER_TINYSCREEN" + " title is : " + (objects.length == 0 ? "" : objects[0]) + " url is : " + url + " screen is : " + screen);
//                    break;
//                case JCUserAction.ON_QUIT_TINYSCREEN:
//                    Log.i("USER_EVENT", "ON_QUIT_TINYSCREEN" + " title is : " + (objects.length == 0 ? "" : objects[0]) + " url is : " + url + " screen is : " + screen);
//                    break;
//                case JCUserAction.ON_TOUCH_SCREEN_SEEK_VOLUME:
//                    Log.i("USER_EVENT", "ON_TOUCH_SCREEN_SEEK_VOLUME" + " title is : " + (objects.length == 0 ? "" : objects[0]) + " url is : " + url + " screen is : " + screen);
//                    break;
//                case JCUserAction.ON_TOUCH_SCREEN_SEEK_POSITION:
//                    Log.i("USER_EVENT", "ON_TOUCH_SCREEN_SEEK_POSITION" + " title is : " + (objects.length == 0 ? "" : objects[0]) + " url is : " + url + " screen is : " + screen);
//                    break;
//
//                case JCUserActionStandard.ON_CLICK_START_THUMB:
//                    Log.i("USER_EVENT", "ON_CLICK_START_THUMB" + " title is : " + (objects.length == 0 ? "" : objects[0]) + " url is : " + url + " screen is : " + screen);
//                    break;
//                case JCUserActionStandard.ON_CLICK_BLANK:
//                    Log.i("USER_EVENT", "ON_CLICK_BLANK" + " title is : " + (objects.length == 0 ? "" : objects[0]) + " url is : " + url + " screen is : " + screen);
//                    break;
//                default:
//                    Log.i("USER_EVENT", "unknow");
//                    break;
//            }
//        }
//    }

    @Override
    public int getViewTypeCount() {
        if (data.size() > 1)
            return data.size();
        else
            return 1;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    private static class ViewHolder {
        ImageView ivVideo;
        TextView tvDate;
        Button bDelete;
        Button bShare;
//        JCVideoPlayerStandard mJcVideoPlayerStandard;



        public ViewHolder(View view) {
            ivVideo = (ImageView) view.findViewById(R.id.playIV);
            bShare = (Button) view.findViewById(R.id.bShare);
            tvDate = (TextView) view.findViewById(R.id.tvDate);
            bDelete = (Button) view.findViewById(R.id.bDelete);
//            mJcVideoPlayerStandard=(JCVideoPlayerStandard)view.findViewById(R.id.jc_video);
            view.setTag(this);
        }
    }
}
