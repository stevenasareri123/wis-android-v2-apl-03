package com.oec.wis.adapters;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.oec.wis.ApplicationController;
import com.oec.wis.R;
import com.oec.wis.chatApi.ChatApi;
import com.oec.wis.dialogs.Chat;
import com.oec.wis.dialogs.NewAds;
import com.oec.wis.entities.WISPhoto;
import com.oec.wis.tools.Tools;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PhotoAdapter2 extends BaseAdapter {
    Context context;
    List<WISPhoto> data;
    int mode;

    public PhotoAdapter2(Context context, List<WISPhoto> data, int mode) {
        this.context = context;
        this.data = data;
        this.mode = mode;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return data.get(position).getId();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.row_photo2, null);
            holder = new ViewHolder(convertView);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.ivPhoto.setImageResource(0);
        ApplicationController.getInstance().getImageLoader().get(context.getString(R.string.server_url3) + data.get(position).getPath(), new ImageLoader.ImageListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                holder.ivPhoto.setImageResource(R.drawable.empty);
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    holder.ivPhoto.setImageBitmap(response.getBitmap());
                } else {
                    holder.ivPhoto.setImageResource(R.drawable.empty);
                }
            }
        });

        holder.rlSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                data.get(position).setSelected(false);
                notifyDataSetChanged();
            }
        });

//        Chat.dialoge.findViewById(R.id.buttonsendfile).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
////                Toast.makeText(this,"xfsdfsag",Toast.LENGTH_LONG).show();
//
//
//                System.out.println("dialog clickable ");
//
//                Chat.sendAudioFileToChat(data.get(position).getPath(), String.valueOf(Chat.fId), "Audio",data.get(position).getChat_type());
//
//
//            }
//        });
        holder.ivSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mode == 1) {
                    Log.i("paht----",data.get(position).getPath());
//                    Chat.sendMsg(data.get(position).getPath(), Chat.fId, "photo", "",data.get(position).getChat_type());
//                    Chat.dPickMedia.dismiss();
//                    Chat.popWindow.dismiss();
//                    Chat.dialog.dismiss();

                    Chat.rootView.setAlpha(1);
                    Chat.disMissPopMenus();
                    Chat.makePubCalls(data.get(position).getPath(),data.get(position).getChat_type(),Tools.getData(context, "idprofile"),Tools.getData(context, "name"),Tools.getData(context, "photo"),"photo");
//                    Chat.sendM







//                   Chat.sendAudioFileToChat(data.get(position).getPath(), String.valueOf(Chat.fId), "Audio",data.get(position).getChat_type());


                } else {
                    NewAds.mediaName = data.get(position).getPath();
                    NewAds.ivPlay.setVisibility(View.GONE);
                    ApplicationController.getInstance().getImageLoader().get(context.getString(R.string.server_url3) + data.get(position).getPath(), new ImageLoader.ImageListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }

                        @Override
                        public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                            if (response.getBitmap() != null) {
                                NewAds.ivPhoto.setImageBitmap(response.getBitmap());
                            }
                        }
                    });
                    NewAds.dPickMedia.dismiss();
                }
            }
        });


        if (data.get(position).isSelected()) {
            holder.rlSend.setVisibility(View.VISIBLE);
        } else {
            holder.rlSend.setVisibility(View.INVISIBLE);
        }

        return convertView;
    }

    @Override
    public int getViewTypeCount() {
        if (data.size() > 1)
            return data.size();
        else
            return 1;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    private static class ViewHolder {
        ImageView ivPhoto, ivSend;
        RelativeLayout rlSend;

        public ViewHolder(View view) {
            ivPhoto = (ImageView) view.findViewById(R.id.ivPhoto);
            ivSend = (ImageView) view.findViewById(R.id.ivSend);
            rlSend = (RelativeLayout) view.findViewById(R.id.rlSend);
            view.setTag(this);
        }
    }
}
