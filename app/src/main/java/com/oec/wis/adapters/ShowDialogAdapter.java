package com.oec.wis.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.oec.wis.Interfaces.MenuClickListener;
import com.oec.wis.R;
import com.oec.wis.entities.WISMenu;

import java.util.List;

/**
 * Created by asareri12 on 23/02/17.
 */

public class ShowDialogAdapter extends BaseAdapter {
    private Context context;
    private List<WISMenu> items;


    public MenuClickListener menuClickListener;

    public ShowDialogAdapter(Context context, List<WISMenu> items) {
        this.context = context;
        this.items = items;


    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.dialog_items, null);
        }

        TextView tvTitle = (TextView) convertView.findViewById(R.id.title);
        ImageView ivLogo = (ImageView) convertView.findViewById(R.id.logo);
        TextView target = (TextView) convertView.findViewById(R.id.target_view);
        tvTitle.setText(items.get(position).getTitle());
        ivLogo.setImageResource(items.get(position).getLogo());
        if(items.get(position).getBatch()==0)
        {
            target.setVisibility(View.INVISIBLE);
        }
        else {
            target.setText(String.valueOf(items.get(position).getBatch()));
        }

        //target.setText(String.valueOf(items.get(position).getBatch()));


//        convertView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                menuClickListener.onMenuItemClick(position);
//
//            }
//        });

        return convertView;
    }
}