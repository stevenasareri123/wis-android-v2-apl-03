package com.oec.wis.adapters;

import android.view.View;

import com.oec.wis.entities.WISActuality;

/**
 * Created by asareri08 on 13/06/16.
 */
public interface ImagePickerListener {

    void showImageGallery();
    void showCustomPopup(int index,int act_id,String desc);

    void showViewMoreOptionView(int position,int act_id);

    void showCustomPopup();
}
