package com.oec.wis.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.oec.wis.ApplicationController;
import com.oec.wis.R;
import com.oec.wis.dialogs.Comments;
import com.oec.wis.dialogs.ProfileView;
import com.oec.wis.dialogs.ProfileViewTwo;
import com.oec.wis.entities.Popupelements;
import com.oec.wis.entities.WISCmt;
import com.oec.wis.tools.Tools;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnItemClickListener;
import com.rockerhieu.emojicon.EmojiconTextView;

import org.joda.time.DateTime;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CmtAdapter extends BaseAdapter {
    Context context;
    List<WISCmt> data;
    private static final int REGULAR = 0;
    private static final int HEADER = 1;
    ArrayList<Popupelements> popupList;
    Popupadapter simpleAdapter;
    Button post_status;
    ImageView  camera_btn, close_btn, post_image;
    private PopupWindow popWindow;
    EditText message;
    TextView comment_userName;
    EditText replyComments;
    List<WISCmt> cmts;
    View backgroundView;
    Typeface font;

//    ListView replyView;

    ReplyCommentsListener replyCommentsListener;


    public CmtAdapter(Context context, List<WISCmt> data,ReplyCommentsListener listener) {
        this.data = data;
        this.context = context;
        this.replyCommentsListener = listener;
    }

    @Override
    public   int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return data.get(position).getId();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        convertView = ((LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.row_cmt, null);
        holder = new ViewHolder(convertView);
        holder.ivUPic.setImageResource(0);
        holder.tvUName.setText(data.get(position).getUser().getFirstName());
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        font=Typeface.createFromAsset(context.getAssets(),"fonts/Harmattan-R.ttf");
        holder.tvCmt.setTypeface(font);
        holder.created_at.setTypeface(font);
        holder.tvDate.setTypeface(font);
        holder.tvUName.setTypeface(font);


        SimpleDateFormat format_hour =new SimpleDateFormat("HH");
        SimpleDateFormat format_min =new SimpleDateFormat("mm");
        SimpleDateFormat format_sec =new SimpleDateFormat("ss");

//        Log.i("created_at",actuList.get(position).getDate());
//
//

        SimpleDateFormat create_date_format = new SimpleDateFormat("dd/MMM/yyyy");
        String created_at="",h = null,m = null,s = null;
        Date d = null;
        try {
            d = format.parse(data.get(position).getDate());
            created_at = create_date_format.format(d);
            h = format_hour.format(d);
            m =format_min.format(d);
            s = format_sec.format(d);
//
        } catch (Exception e) {
            e.printStackTrace();
        }
        String rDate = net.danlew.android.joda.DateUtils.getRelativeTimeSpanString(context, new DateTime(d), true).toString();
        holder.tvDate.setText(h.concat("h").concat(m).concat("'").concat(s));

        holder.tvCmt.setText(data.get(position).getCmt());

        holder.created_at.setText(created_at);
        if(data.get(position).getCommentview_type().equals("true")){
            holder.tooltip.setVisibility(View.VISIBLE);
        }else{
            holder.tooltip.setVisibility(View.INVISIBLE);
        }

        holder.tooltip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                setupPopUPView(String.valueOf(data.get(position).getUser().getId()));

                showCommentOptionView(position);

            }
        });


        holder.ivUPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                data.get(position).getUser().getId()
                Log.i("user", String.valueOf(data.get(position).getUser().getId()));
                String user_id = Tools.getData(context, "idprofile");
                String amis_id = String.valueOf(data.get(position).getUser().getId());
                if(user_id.contains(amis_id))
                {
                    Log.i(" amis", String.valueOf(Integer.parseInt(amis_id)));
                    Comments cmnt = new Comments();

                    Intent i = new Intent(context, ProfileViewTwo.class);
                    i.putExtra("id",Integer.parseInt(user_id));
                    context.startActivity(i);




//                    FragProfile fragment = new FragProfile();
//                    FragmentManager fragmentManager =((Activity) context).getFragmentManager();
//
//                    fragmentManager.beginTransaction().add(R.id.frame_container,fragment).commit();
////




                }else{
                    Log.i("amis", String.valueOf(Integer.parseInt(amis_id)));
                    Intent i = new Intent(context, ProfileView.class);
                    i.putExtra("id",Integer.parseInt(amis_id));
                    context.startActivity(i);
                }
            }
        });

        ApplicationController.getInstance().getImageLoader().get(context.getString(R.string.server_url2) + data.get(position).getUser().getPic(), new ImageLoader.ImageListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                holder.ivUPic.setImageResource(R.drawable.empty);
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    holder.ivUPic.setImageBitmap(response.getBitmap());
                } else {
                    holder.ivUPic.setImageResource(R.drawable.empty);
                }
            }
        });
        backgroundView=convertView;
        return convertView;
    }



    @Override
    public int getViewTypeCount() {
        if (data.size() > 1)
            return data.size();
        else
            return 1;
    }

    @Override
    public int getItemViewType(int position) {
        if(position!=data.size()){
            return REGULAR;
        }
        else {
            return position;
        }
    }

    public void showCommentOptionView(final int p){

        DialogPlus dialog = DialogPlus.newDialog(context)
                .setAdapter(simpleAdapter)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
                        if(popupList.size()==2){

                            if(position == 0){

                                showWriteStatusPopup(view,p);

                            }
                            if(position == 1){
                                deleteDialog(p);

                            }

                        }

                        else{

                            replyCommentsView(view,p);

                        }


                        dialog.dismiss();
                    }
                })
                .setExpanded(false)  // This will enable the expand feature, (similar to android L share dialog)
                .create();
        dialog.show();

    }
    public void replyCommentsView(final View v,final int p){
        LayoutInflater layoutInflater = (LayoutInflater) context.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // inflate the custom popup layout
        final View inflatedView = layoutInflater.inflate(R.layout.reply_commentsview, null, false);


        // get device size
        Display display = ((Activity)context).getWindowManager().getDefaultDisplay();
        final Point size = new Point();
        display.getSize(size);
//        mDeviceHeight = size.y;


        // set height depends on the device size
        popWindow = new PopupWindow(inflatedView, size.x-30, size.y -300, true);

        popWindow.setTouchable(true);

//        v.setAlpha(0.5f);
        backgroundView.setAlpha(0.4f);

        // make it focusable to show the keyboard to enter in `EditText`
//        popWindow.setFocusable(true);
        // make it outside touchable to dismiss the popup window
//        popWindow.setBackgroundDrawable(new BitmapDrawable());
        popWindow.setOutsideTouchable(false);

        popWindow.setAnimationStyle(R.style.animationName);

        popWindow.showAtLocation(v, Gravity.CENTER, 0, 100);



        post_status = (Button) inflatedView.findViewById(R.id.update_comment);

        replyComments = (EditText)inflatedView.findViewById(R.id.replyComments);
//        replyView = (ListView)inflatedView.findViewById(R.id.replyCmt);
        close_btn = (ImageView) inflatedView.findViewById(R.id.popup_close);

        close_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popWindow.dismiss();
                backgroundView.setAlpha(1f);

            }
        });

        post_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               addComments(replyComments.getText().toString(),p,null, String.valueOf(data.get(p).getId()));

                popWindow.dismiss();
                backgroundView.setAlpha(1f);

            }
        });


    }

    public void setupReplyCommentView(){





    }

    public void addComments(String message,final int p,ListView replyView,String comment_id){

        replyCommentsListener.addComments(p,message,replyView,comment_id);



    }



    public void showWriteStatusPopup(View v,final int p) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // inflate the custom popup layout
        final View inflatedView = layoutInflater.inflate(R.layout.comment_editview, null, false);


        // get device size
        Display display = ((Activity)context).getWindowManager().getDefaultDisplay();
        final Point size = new Point();
        display.getSize(size);
//        mDeviceHeight = size.y;


        // set height depends on the device size
        popWindow = new PopupWindow(inflatedView, size.x-30, size.y -300, true);

        popWindow.setTouchable(true);

        backgroundView.setAlpha(0.5f);

        // make it focusable to show the keyboard to enter in `EditText`
//        popWindow.setFocusable(true);
        // make it outside touchable to dismiss the popup window
//        popWindow.setBackgroundDrawable(new BitmapDrawable());
        popWindow.setOutsideTouchable(false);

        popWindow.setAnimationStyle(R.style.animationName);




        final View views = inflatedView;

        // show the popup at bottom of the screen and set some margin at bottom ie,
        popWindow.showAtLocation(v, Gravity.CENTER, 0, 100);


        close_btn = (ImageView) inflatedView.findViewById(R.id.popup_close);
        post_status = (Button) inflatedView.findViewById(R.id.update_comment);
        post_image = (ImageView)inflatedView.findViewById(R.id.ivUPic);
        message = (EditText) inflatedView.findViewById(R.id.edit_comment);
        comment_userName = (TextView)inflatedView.findViewById(R.id.tvUName);

        comment_userName.setText(Tools.getData(context,"name"));

        message.setText(data.get(p).getCmt().toString());



        ApplicationController.getInstance().getImageLoader().get(context.getString(R.string.server_url2) + data.get(p).getUser().getPic(), new ImageLoader.ImageListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                post_image.setImageResource(R.drawable.empty);
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    post_image.setImageBitmap(response.getBitmap());
                } else {
                    post_image.setImageResource(R.drawable.empty);
                }
            }
        });


//
        close_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popWindow.dismiss();

                backgroundView.setAlpha(1);

            }
        });
//

        post_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                updateComments(p,String.valueOf(data.get(p).getId()),message.getText().toString());
                backgroundView.setAlpha(1);
                popWindow.dismiss();

            }
        });




    }

    public void updateComments(final int position,String id_cmnt,String message){
        JSONObject jsonBody = new JSONObject();

        try {
            jsonBody.put("user_id", Tools.getData(context, "idprofile"));
            jsonBody.put("comment_id", String.valueOf(id_cmnt));
            jsonBody.put("comments", message);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqDelete = new JsonObjectRequest(context.getString(R.string.server_url) + context.getString(R.string.update_comments), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {

                                replyCommentsListener.replyListener();

                                notifyDataSetChanged();
                            }

                        } catch (JSONException e) {
                            //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //if (context != null)
                //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("lang", Tools.getData(context, "lang_pr"));
                headers.put("token", Tools.getData(context, "token"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqDelete);

    }

    private void deleteDialog(final int position) {
        new AlertDialog.Builder(context)
                .setMessage(context.getString(R.string.msg_confirm_delete_post))
                .setPositiveButton(context.getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteActu(data.get(position).getId(), position);
                    }

                })
                .setNegativeButton(context.getString(R.string.no), null)
                .show();
    }

    private void deleteActu(int idCmnt, final int position) {
        JSONObject jsonBody = new JSONObject();

        try {
            jsonBody.put("user_id", Tools.getData(context, "idprofile"));
            jsonBody.put("comment_id", String.valueOf(idCmnt));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest reqDelete = new JsonObjectRequest(context.getString(R.string.server_url) + context.getString(R.string.remove_comments), jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getBoolean("result")) {

                                data.remove(position);
                                notifyDataSetChanged();
                            }

                        } catch (JSONException e) {
                            //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //if (context != null)
                //Toast.makeText(context, context.getString(R.string.msg_server_error), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("lang", Tools.getData(context, "lang_pr"));
                headers.put("token", Tools.getData(context, "token"));
                return headers;
            }
        };

        ApplicationController.getInstance().addToRequestQueue(reqDelete);
    }



    public void setupPopUPView(String comment_user) {



        String current_user = Tools.getData(context,"idprofile");

        popupList = new ArrayList<>();



        if(current_user.equals(comment_user)){
            popupList.add(new Popupelements(R.drawable.write_status,context.getString(R.string.Edit)));
            popupList.add(new Popupelements(R.drawable.trash,context.getString(R.string.Remove)));
        }

       else{
            popupList.add(new Popupelements(R.drawable.reply,context.getString(R.string.Reply)));
        }

        simpleAdapter = new Popupadapter(context, false, popupList);


        Log.i("list",comment_user);



    }

    private static class ViewHolder {
        TextView tvUName, tvDate,created_at;
        EmojiconTextView tvCmt;
        ImageView ivUPic,tooltip;

        public ViewHolder(View view) {
            tvUName = (TextView) view.findViewById(R.id.tvUName);
            tvDate = (TextView) view.findViewById(R.id.tvDate);
            tvCmt = (EmojiconTextView) view.findViewById(R.id.tvCmt);
            ivUPic = (ImageView) view.findViewById(R.id.ivUPic);
            created_at = (TextView)view.findViewById(R.id.created_date);
            tooltip = (ImageView)view.findViewById(R.id.tooltip);
            view.setTag(this);
        }
    }
}
