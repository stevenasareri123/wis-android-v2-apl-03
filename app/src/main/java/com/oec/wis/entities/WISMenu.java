package com.oec.wis.entities;

import android.app.Fragment;

public class WISMenu {
    private String title;
    private int logo;
    private int type;

    public int getBatch() {
        return batch;
    }

    public void setBatch(int batch) {
        this.batch = batch;
    }

    private int batch;
    private Fragment fregment;
    private Class nclass;

    public WISMenu(String title, int logo,int batch) {
        this.title = title;
        this.logo = logo;
        this.batch=batch;
    }
    public WISMenu(String title, int logo) {
        this.title = title;
        this.logo = logo;
    }

    public WISMenu(String title, int type, Fragment fregment, Class nclass) {
        this.title = title;
        this.type = type;
        this.fregment = fregment;
        this.nclass = nclass;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getLogo() {
        return logo;
    }

    public void setLogo(int logo) {
        this.logo = logo;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Fragment getFregment() {
        return fregment;
    }

    public void setFregment(Fragment fregment) {
        this.fregment = fregment;
    }

    public Class getNclass() {
        return nclass;
    }

    public void setNclass(Class nclass) {
        this.nclass = nclass;
    }
}
