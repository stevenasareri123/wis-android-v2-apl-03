//package com.oec.wis.entities;
//
//public class WISAds {
//    private int id;
//    private String title;
//    private String desc;
//    private int status;
//    private int duration;
//    private String visib;
//    private String target;
//    private String cDate;
//    private String lDate;
//    private String photo;
//    private int idAct;
//    private boolean tourist;
//    private boolean banner;
//    private String startTime;
//    private String endTime;
//    private String typeObj;
//    private String thumb;
//    private boolean isGroup;
//    public WISAds(int id, String title, int status, int duration, String cDate, String lDate, String photo) {
//        this.id = id;
//        this.title = title;
//        this.status = status;
//        this.duration = duration;
//        this.cDate = cDate;
//        this.lDate = lDate;
//        this.photo = photo;
//    }
//
//    public WISAds(int id, String title, String desc, int status, int duration, String visib, String target, String cDate, String lDate, String photo, String typeObj, boolean tourist, boolean banner, String startTime, String endTime, String thumb) {
//        this.id = id;
//        this.title = title;
//        this.desc = desc;
//        this.status = status;
//        this.duration = duration;
//        this.visib = visib;
//        this.target = target;
//        this.cDate = cDate;
//        this.lDate = lDate;
//        this.photo = photo;
//        this.tourist = tourist;
//        this.banner = banner;
//        this.startTime = startTime;
//        this.endTime = endTime;
//        this.typeObj = typeObj;
//        this.thumb = thumb;
//    }
//
//    public WISAds(int id, int idAct, String title, int status, int duration, String cDate, String lDate, String photo) {
//        this.id = id;
//        this.idAct = idAct;
//        this.title = title;
//        this.status = status;
//        this.duration = duration;
//        this.cDate = cDate;
//        this.lDate = lDate;
//        this.photo = photo;
//    }
//
//    public WISAds(int id, int idAct, String title, int status, int duration, String cDate, String lDate, String photo, String typeObj, String thumb) {
//        this.id = id;
//        this.idAct = idAct;
//        this.title = title;
//        this.status = status;
//        this.duration = duration;
//        this.cDate = cDate;
//        this.lDate = lDate;
//        this.photo = photo;
//        this.typeObj = typeObj;
//        this.thumb = thumb;
//    }
//
//
//    public boolean isGroup() {
//        return isGroup;
//    }
//
//    public void setGroup(boolean group) {
//        isGroup = group;
//    }
//
//    public WISAds(int id, int idAct, String title, String desc, String photo, String typeObj, String thumb, boolean isGroup) {
//        this.id = id;
//        this.idAct = idAct;
//        this.title = title;
//        this.desc = desc;
//
//        this.photo = photo;
//
//        this.typeObj = typeObj;
//        this.thumb = thumb;
//        this.isGroup = isGroup;
//    }
//
//    public int getId() {
//        return id;
//    }
//
//    public void setId(int id) {
//        this.id = id;
//    }
//
//    public String getTitle() {
//        return title;
//    }
//
//    public void setTitle(String title) {
//        this.title = title;
//    }
//
//    public String getDesc() {
//        return desc;
//    }
//
//    public void setDesc(String desc) {
//        this.desc = desc;
//    }
//
//    public int getStatus() {
//        return status;
//    }
//
//    public void setStatus(int status) {
//        this.status = status;
//    }
//
//    public int getDuration() {
//        return duration;
//    }
//
//    public void setDuration(int duration) {
//        this.duration = duration;
//    }
//
//    public String getVisib() {
//        return visib;
//    }
//
//    public void setVisib(String visib) {
//        this.visib = visib;
//    }
//
//    public String getTarget() {
//        return target;
//    }
//
//    public void setTarget(String target) {
//        this.target = target;
//    }
//
//    public String getcDate() {
//        return cDate;
//    }
//
//    public void setcDate(String cDate) {
//        this.cDate = cDate;
//    }
//
//    public String getlDate() {
//        return lDate;
//    }
//
//    public void setlDate(String lDate) {
//        this.lDate = lDate;
//    }
//
//    public String getPhoto() {
//        return photo;
//    }
//
//    public void setPhoto(String photo) {
//        this.photo = photo;
//    }
//
//    public int getIdAct() {
//        return idAct;
//    }
//
//    public void setIdAct(int idAct) {
//        this.idAct = idAct;
//    }
//
//    public boolean isTourist() {
//        return tourist;
//    }
//
//    public void setTourist(boolean tourist) {
//        this.tourist = tourist;
//    }
//
//    public boolean isBanner() {
//        return banner;
//    }
//
//    public void setBanner(boolean banner) {
//        this.banner = banner;
//    }
//
//    public String getStartTime() {
//        return startTime;
//    }
//
//    public void setStartTime(String startTime) {
//        this.startTime = startTime;
//    }
//
//    public String getEndTime() {
//        return endTime;
//    }
//
//    public void setEndTime(String endTime) {
//        this.endTime = endTime;
//    }
//
//    public String getTypeObj() {
//        return typeObj;
//    }
//
//    public void setTypeObj(String type_obj) {
//        this.typeObj = type_obj;
//    }
//
//    public String getThumb() {
//        return thumb;
//    }
//
//    public void setThumb(String thumb) {
//        this.thumb = thumb;
//    }
//}
package com.oec.wis.entities;

public class WISAds {
    private int id;
    private String title;
    private String desc;
    private int status;
    private int duration;
    private String visib;
    private String target;
    private String cDate;
    private String lDate;
    private String photo;
    private int idAct;
    private boolean tourist;
    private boolean banner;
    private String startTime;
    private String endTime;
    private String typeObj;
    private String thumb;
    private boolean isGroup;
    private  int advertiser;
    private String advertiserName;



    private String country;



    private String audio;

    public WISAds(int id, String title, int status, int duration, String cDate, String lDate, String photo) {
        this.id = id;
        this.title = title;
        this.status = status;
        this.duration = duration;
        this.cDate = cDate;
        this.lDate = lDate;
        this.photo = photo;
    }

    public WISAds(int id, String title, String desc, int status, int duration, String visib, String target, String cDate, String lDate, String photo, String typeObj, boolean tourist, boolean banner, String startTime, String endTime, String thumb,String audio,String country) {
        this.id = id;
        this.title = title;
        this.desc = desc;
        this.status = status;
        this.duration = duration;
        this.visib = visib;
        this.target = target;
        this.cDate = cDate;
        this.lDate = lDate;
        this.photo = photo;
        this.tourist = tourist;
        this.banner = banner;
        this.startTime = startTime;
        this.endTime = endTime;
        this.typeObj = typeObj;
        this.thumb = thumb;
        this.audio = audio;
        this.country=country;
    }

    public WISAds(int id, String title, String desc, int status, int duration, String visib, String target, String cDate, String lDate, String photo, String typeObj, boolean tourist, boolean banner, String startTime, String endTime, String thumb) {
        this.id = id;
        this.title = title;
        this.desc = desc;
        this.status = status;
        this.duration = duration;
        this.visib = visib;
        this.target = target;
        this.cDate = cDate;
        this.lDate = lDate;
        this.photo = photo;
        this.tourist = tourist;
        this.banner = banner;
        this.startTime = startTime;
        this.endTime = endTime;
        this.typeObj = typeObj;
        this.thumb = thumb;
    }


    public WISAds(int id, int idAct, String title, int status, int duration, String cDate, String lDate, String photo) {
        this.id = id;
        this.idAct = idAct;
        this.title = title;
        this.status = status;
        this.duration = duration;
        this.cDate = cDate;
        this.lDate = lDate;
        this.photo = photo;
    }

    public WISAds(int id, int idAct, String title, int status, int duration, String cDate, String lDate, String photo, String typeObj, String thumb) {
        this.id = id;
        this.idAct = idAct;
        this.title = title;
        this.status = status;
        this.duration = duration;
        this.cDate = cDate;
        this.lDate = lDate;
        this.photo = photo;
        this.typeObj = typeObj;
        this.thumb = thumb;
    }

    public String getAdvertiserName() {
        return advertiserName;
    }

    public void setAdvertiserName(String advertiserName) {
        this.advertiserName = advertiserName;
    }

    public int getAdvertiser() {
        return advertiser;
    }

    public void setAdvertiser(int advertiser) {
        this.advertiser = advertiser;
    }

    public WISAds(int advertiser, String advertiserName, int id, int idAct, String title, int status, int duration, String cDate, String lDate, String photo, String typeObj, String thumb) {
        this.id = id;
        this.idAct = idAct;
        this.title = title;
        this.status = status;
        this.duration = duration;
        this.cDate = cDate;
        this.lDate = lDate;
        this.photo = photo;
        this.typeObj = typeObj;
        this.thumb = thumb;

        this.advertiser = advertiser;
        this.advertiserName = advertiserName;

    }


    public boolean isGroup() {
        return isGroup;
    }

    public void setGroup(boolean group) {
        isGroup = group;
    }

    public WISAds(int id, int idAct, String title, String desc, String photo, String typeObj, String thumb, boolean isGroup) {
        this.id = id;
        this.idAct = idAct;
        this.title = title;
        this.desc = desc;

        this.photo = photo;

        this.typeObj = typeObj;
        this.thumb = thumb;
        this.isGroup = isGroup;
    }
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getVisib() {
        return visib;
    }

    public void setVisib(String visib) {
        this.visib = visib;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getcDate() {
        return cDate;
    }

    public void setcDate(String cDate) {
        this.cDate = cDate;
    }

    public String getlDate() {
        return lDate;
    }

    public void setlDate(String lDate) {
        this.lDate = lDate;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public int getIdAct() {
        return idAct;
    }

    public void setIdAct(int idAct) {
        this.idAct = idAct;
    }

    public boolean isTourist() {
        return tourist;
    }

    public void setTourist(boolean tourist) {
        this.tourist = tourist;
    }

    public boolean isBanner() {
        return banner;
    }

    public void setBanner(boolean banner) {
        this.banner = banner;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getTypeObj() {
        return typeObj;
    }

    public void setTypeObj(String type_obj) {
        this.typeObj = type_obj;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }
}
