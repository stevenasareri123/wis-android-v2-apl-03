package com.oec.wis.entities;

public class WISUser {
    private int id;
    private String firstName;
    private String lastName;
    private String pic;
    private int nFriend;
    private String pType;
    private String email;
    private String sexe;
    private int nCFriend;
    private int lInvit;
    private String fullName;
    private String objectId;
    private String unfollowerId;

    private  String publishNewsFeed;

    private Boolean isGroupChatalive;

    public WISUser(int id, String firstName, String lastName, String pic, int nFriend, String pType, String email, String sexe) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.pic = pic;
        this.nFriend = nFriend;
        this.pType = pType;
        this.email = email;
        this.sexe = sexe;
    }

    public WISUser(int id, String firstName, String lastName, String pic, int nFriend) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.pic = pic;
        this.nFriend = nFriend;
    }

    public WISUser(int id, String name, String pic) {
        this.id = id;
        this.firstName = name;
        this.pic = pic;
    }

    public WISUser(int id, String name, String pic, int nCFriend, int lInvit) {
        this.id = id;
        this.firstName = name;
        this.pic = pic;
        this.nCFriend = nCFriend;
        this.lInvit = lInvit;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String isPublishNewsFeed() {
        return publishNewsFeed;
    }

    public void setPublishNewsFeed(String publishNewsFeed) {
        this.publishNewsFeed = publishNewsFeed;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getUnfollowerId() {
        return unfollowerId;
    }

    public void setUnfollowerId(String unfollowerId) {
        this.unfollowerId = unfollowerId;
    }

    public Boolean getGroupChatalive() {
        return isGroupChatalive;
    }

    public void setGroupChatalive(Boolean groupChatalive) {
        isGroupChatalive = groupChatalive;
    }

    public WISUser(Boolean isGroupChatAlive, String unfollowerId, String objectId, String publishNewsFeed, int id, String fullName, String pic, int nFriend) {
        this.id = id;

        this.fullName = fullName;
        this.publishNewsFeed = publishNewsFeed;
        this.objectId = objectId;

        this.pic = pic;

        this.nFriend = nFriend;
        this.unfollowerId = unfollowerId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public int getnFriend() {
        return nFriend;
    }

    public void setnFriend(int nFriend) {
        this.nFriend = nFriend;
    }

    public String getpType() {
        return pType;
    }

    public void setpType(String pType) {
        this.pType = pType;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public int getnCFriend() {
        return nCFriend;
    }

    public void setnCFriend(int nCFriend) {
        this.nCFriend = nCFriend;
    }

    public int getlInvit() {
        return lInvit;
    }

    public void setlInvit(int lInvit) {
        this.lInvit = lInvit;
    }
}
