package com.oec.wis.entities;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * Created by asareri08 on 02/06/16.
 */
public class UserNotification {

    static int badge_count;
    static Boolean CHAT_SELECTED;
    static String act_id;
    static Boolean DISCUSSIONSELECTED;
    static Boolean LocationSelected;
    static String viewType;
    static  Boolean CREATEGROUPCHAT;
    static Boolean CHAT_WITH_CUSTOMTEXT_SELECTED;
    static String CustomText;
    static String CustomImagePath;

    public static int getTotalBadgeValues() {
        return TotalBadgeValues;
    }

    public static void setTotalBadgeValues(int totalBadgeValues) {
        TotalBadgeValues = totalBadgeValues;
    }

    static int TotalBadgeValues;

    static Boolean shareVideo=false;

    static  String videoId;

    public static String getPublisherid() {
        return publisherid;
    }

    public static void setPublisherid(String publisherid) {
        UserNotification.publisherid = publisherid;
    }

    static String publisherid;

    public static String getVideoId() {
        return videoId;
    }

    public static void setVideoId(String videoId) {
        UserNotification.videoId = videoId;
    }

    public static Boolean getShareVideo() {
        return shareVideo;
    }

    public static void setShareVideo(Boolean shareVideo) {
        UserNotification.shareVideo = shareVideo;
    }

    public static Boolean getFromweather() {
        return fromweather;
    }

    public static void setFromweather(Boolean fromweather) {
        UserNotification.fromweather = fromweather;
    }

    public static Boolean getFromdirect() {
        return fromdirect;
    }

    public static void setFromdirect(Boolean fromdirect) {
        UserNotification.fromdirect = fromdirect;
    }

    static Boolean fromweather;
    static Boolean fromdirect;

    public static Boolean getFromnotif() {
        return fromnotif;
    }

    public static void setFromnotif(Boolean fromnotif) {
        UserNotification.fromnotif = fromnotif;
    }

    static Boolean fromnotif;

    static String wistitle;

    static int totalbatchcount;
    //static int wisactualitycount;
    static int wisdirectcount;
    static int wischatcount;
    static int wisgroupcount;
    static int wisphonecount;

    public static int getBatchcount() {
        return totalbatchcount;
    }

    public static void setBatchcount(int totalbatchcount) {
        UserNotification.totalbatchcount = totalbatchcount;
    }

//    public static int getWisactualitycount() {
//        return wisactualitycount;
//    }
//
//    public static void setWisactualitycount(int wisactualitycount) {
//        UserNotification.wisactualitycount = wisactualitycount;
//    }

    public static int getDirectcount() {
        return wisdirectcount;
    }

    public static void setWisdirectcount(int wisdirectcount) {
        UserNotification.wisdirectcount = wisdirectcount;
    }

    public static int getChatcount() {
        return wischatcount;
    }

    public static void setWischatcount(int wischatcount) {
        UserNotification.wischatcount = wischatcount;
    }

    public static int getGroupcount() {
        return wisgroupcount;
    }

    public static void setWisgroupcount(int wisgroupcount) {
        UserNotification.wisgroupcount = wisgroupcount;
    }

    public static int getPhonecount() {
        return wisphonecount;
    }

    public static void setWisphonecount(int wisphonecount) {
        UserNotification.wisphonecount = wisphonecount;
    }



    public static String getWistitle() {
        return wistitle;
    }

    public static void setWistitle(String wistitle) {
        UserNotification.wistitle = wistitle;
    }

    public static String getImagepath() {
        return imagepath;
    }

    public static void setImagepath(String imagepath) {
        UserNotification.imagepath = imagepath;
    }

    public static double getLat() {
        return lat;
    }

    public static void setLat(double lat) {
        UserNotification.lat = lat;
    }

    public static double getLan() {
        return lan;
    }

    public static void setLan(double lan) {
        UserNotification.lan = lan;
    }

    static String imagepath;
    static double lat;
    static  double lan;




    static Boolean pubmusic;

    static String songThumb;




    static Boolean topub;
    static Boolean uploadSong;


    static Boolean playlist;


    static Boolean updateplaylist;

    static String location;

    static double langtitude;



    static  double lattitude;



    static ArrayList<Integer> amisids = new ArrayList<Integer>();

    static int selected;



    static Boolean clicked;


    static Boolean checkboxs;

    static Boolean isFromPubView;



    static ArrayList<Integer> songids = new ArrayList<Integer>();

    static ArrayList<Integer> albumids = new ArrayList<Integer>();

    static List<String> ids = new ArrayList<String>();



    static List<String> names = new ArrayList<String>();

    static Boolean FROMMUSIC;
    static int object;

    static String type,audio;

    static List<WISMusic> Songlist;
    static List<WISSongs> Songs;

    static List<WISPlaylist> PlaySongs;

    public static WISAds getAds() {
        return ads;
    }

    public static void setAds(WISAds ads) {
        UserNotification.ads = ads;
    }

    static WISAds ads;


    public static List<String> selectImages;
    public static List<String> songNames;
    public static List<String> artistnames;
    public static List<String> songdurations;
    public static List<String> songIcons;

    public static Boolean isAlbumUploaded;

    public static Boolean getIsAlbumUploaded() {
        return isAlbumUploaded;
    }

    public static void setIsAlbumUploaded(Boolean isAlbumUploaded) {
        UserNotification.isAlbumUploaded = isAlbumUploaded;
    }
    public static Boolean getPubmusic() {
        return pubmusic;
    }

    public static void setPubmusic(Boolean pubmusic) {
        UserNotification.pubmusic = pubmusic;
    }
    public static List<String> getSelectImages() {
        return selectImages;
    }

    public static void setSelectImages(List<String> selectImages) {
        UserNotification.selectImages = selectImages;
    }

    public static List<String> getSongNames() {
        return songNames;
    }

    public static void setSongNames(List<String> songNames) {
        UserNotification.songNames = songNames;
    }
    public static Boolean getTopub() {
        return topub;
    }

    public static void setTopub(Boolean topub) {
        UserNotification.topub = topub;
    }

    public static List<String> getArtistnames() {
        return artistnames;
    }

    public static void setArtistnames(List<String> artistnames) {
        UserNotification.artistnames = artistnames;
    }

    public static List<String> getSongdurations() {
        return songdurations;
    }

    public static void setSongdurations(List<String> songdurations) {
        UserNotification.songdurations = songdurations;
    }

    public static double getLattitude() {
        return lattitude;
    }

    public static void setLattitude(double lattitude) {
        UserNotification.lattitude = lattitude;
    }

    public static double getLangtitude() {
        return langtitude;
    }

    public static void setLangtitude(double langtitude) {
        UserNotification.langtitude = langtitude;
    }

    public static List<String> getSongIcons() {
        return songIcons;
    }

    public static void setSongIcons(List<String> songIcons) {
        UserNotification.songIcons = songIcons;
    }

    public static void setSonglist(String songlist) {
        UserNotification.songlist = songlist;
    }

    public static String getArtistlist() {
        return artistlist;
    }

    public static void setArtistlist(String artistlist) {
        UserNotification.artistlist = artistlist;
    }

    public static String getDurationlist() {
        return durationlist;
    }

    public static void setDurationlist(String durationlist) {
        UserNotification.durationlist = durationlist;
    }

    public static String songlist;
    public static String artistlist;
    public static String durationlist;

    static String MusicChatMessage="";

    public static String getMusicChatMessage() {
        return MusicChatMessage;
    }

    public static void setMusicChatMessage(String musicChatMessage) {
        MusicChatMessage = musicChatMessage;
    }
    public static String getSongThumb() {
        return songThumb;
    }

    public static void setSongThumb(String songThumb) {
        UserNotification.songThumb = songThumb;
    }
    public static Boolean getPlaylist() {
        return playlist;
    }

    public static void setPlaylist(Boolean playlist) {
        UserNotification.playlist = playlist;
    }
    public static String getLocation() {
        return location;
    }

    public static void setLocation(String location) {
        UserNotification.location = location;
    }

    public static Boolean getClicked() {
        return clicked;
    }

    public static void setClicked(Boolean clicked) {
        UserNotification.clicked = clicked;
    }
    public static Boolean getIsFromPubView() {
        return isFromPubView;
    }

    public static void setIsFromPubView(Boolean isFromPubView) {
        UserNotification.isFromPubView = isFromPubView;
    }
    public static Boolean getUploadSong() {
        return uploadSong;
    }

    public static void setUploadSong(Boolean uploadSong) {
        UserNotification.uploadSong = uploadSong;
    }

    public static Boolean getUpdateplaylist() {
        return updateplaylist;
    }

    public static void setUpdateplaylist(Boolean updateplaylist) {
        UserNotification.updateplaylist = updateplaylist;
    }

    public static ArrayList<Integer> getAmisids() {
        return amisids;
    }

    public static void setAmisids(ArrayList<Integer> amisids) {
        UserNotification.amisids = amisids;
    }
    public static List<String> getNames() {
        return names;
    }

    public static void setNames(List<String> names) {
        UserNotification.names = names;
    }

    public static List<String> getIds() {
        return ids;
    }

    public static void setIds(List<String> ids) {
        UserNotification.ids = ids;
    }

    static List<WISPlaylist1> PlayAlbum;
    public static List<WISPlaylist1> getPlayAlbum() {
        return PlayAlbum;
    }

    public static void setPlayAlbum(List<WISPlaylist1> playAlbum) {
        PlayAlbum = playAlbum;
    }
    public static ArrayList<Integer> getAlbumids() {
        return albumids;
    }

    public static void setAlbumids(ArrayList<Integer> albumids) {
        UserNotification.albumids = albumids;
    }

    public static ArrayList<Integer> getSongids() {
        return songids;
    }

    public static void setSongids(ArrayList<Integer> songids) {
        UserNotification.songids = songids;
    }

    public static List<WISPlaylist> getPlaySongs() {
        return PlaySongs;
    }

    public static void setPlaySongs(List<WISPlaylist> playSongs) {
        PlaySongs = playSongs;
    }

    public static int getSelected() {
        return selected;
    }
    public static void setSelected(int selected) {
        UserNotification.selected = selected;
    }

    public static String getType() {
        return type;
    }

    public static void setType(String type) {
        UserNotification.type = type;
    }
    public static String getAudio() {
        return audio;
    }

    public static void setAudio(String audio) {
        UserNotification.audio = audio;
    }

    public static Boolean getCheckboxs() {
        return checkboxs;
    }

    public static void setCheckboxs(Boolean checkboxs) {
        UserNotification.checkboxs = checkboxs;
    }
    public static int getObject() {
        return object;
    }

    public static void setObject(int object) {
        UserNotification.object = object;
    }

    public static Boolean getFROMMUSIC() {
        return FROMMUSIC;
    }

    public static void setFROMMUSIC(Boolean FROMMUSIC) {
        UserNotification.FROMMUSIC = FROMMUSIC;
    }

    public static List<WISMusic> getSonglist() {
        return Songlist;
    }

    public static void setSonglist(List<WISMusic> songlist) {
        Songlist = songlist;
    }

    public static List<WISSongs> getSongs() {
        return Songs;
    }

    public static void setSongs(List<WISSongs> songs) {
        Songs = songs;
    }

    public static Boolean getCREATEGROUPCHAT() {
        return CREATEGROUPCHAT;
    }

    public static Boolean getChatWithCustomtextSelected() {
        return CHAT_WITH_CUSTOMTEXT_SELECTED;
    }

    public static void setChatWithCustomtextSelected(Boolean chatWithCustomtextSelected) {
        CHAT_WITH_CUSTOMTEXT_SELECTED = chatWithCustomtextSelected;
    }

    public static String getCustomText() {
        return CustomText;
    }

    public static void setCustomText(String customText) {
        CustomText = customText;
    }

    public static String getCustomImagePath() {
        return CustomImagePath;
    }

    public static void setCustomImagePath(String customImagePath) {
        CustomImagePath = customImagePath;
    }

    public static void setCREATEGROUPCHAT(Boolean CREATEGROUPCHAT) {
        UserNotification.CREATEGROUPCHAT = CREATEGROUPCHAT;
    }

    public static String getViewType() {
        return viewType;
    }

    public static void setViewType(String viewType) {
        UserNotification.viewType = viewType;
    }

    public static String getAddress() {
        return address;
    }

    public static void setAddress(String address) {
        UserNotification.address = address;
    }

    static String address;

    public static Boolean getLocationSelected() {
        return LocationSelected;
    }

    public static void setLocationSelected(Boolean locationSelected) {
        LocationSelected = locationSelected;
    }

    public static Boolean getDISCUSSIONSELECTED() {
        return DISCUSSIONSELECTED;
    }

    public static void setDISCUSSIONSELECTED(Boolean DISCUSSIONSELECTED) {
        UserNotification.DISCUSSIONSELECTED = DISCUSSIONSELECTED;
    }


    public static String getAct_id() {
        return act_id;
    }

    public static void setAct_id(String act_id) {
        UserNotification.act_id = act_id;
    }

    public static int getBadge_count() {
        return badge_count;
    }

    public static void setBadge_count(int badge_count) {
        UserNotification.badge_count = badge_count;
    }

    public static Boolean getChatSelected() {
        return CHAT_SELECTED;
    }

    public static void setChatSelected(Boolean chatSelected) {
        CHAT_SELECTED = chatSelected;
    }

}
