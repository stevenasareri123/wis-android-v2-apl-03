package com.oec.wis.entities;

/**
 * Created by asareri12 on 07/11/16.
 */

public class PubMusic {

    private long id;
    private String title;

    private String disc;

    public PubMusic(long id, String title, String disc){
        this.id=id;
        this.title=title;
        this.disc=disc;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    public String getDisc() {
        return disc;
    }

    public void setDisc(String disc) {
        this.disc = disc;
    }

}
