package com.oec.wis.entities;

import java.util.List;

public class WISAdsPrice {
    private List<AdsVisib> visib;

    public WISAdsPrice(List<AdsVisib> visib) {
        this.visib = visib;
    }

    public class AdsVisib {
        private List<Double> price;
    }
}
